use Config;
use Cwd 'abs_path';
my $squish_lib_perl;
if ($^O eq "MSWin32") {
    $squish_lib_perl = abs_path($Config{"scriptdir"}."/../../lib/perl");
} else {
    $squish_lib_perl = abs_path($Config{"prefixexp"}."/../lib/perl");
}
if (-d $squish_lib_perl) {
    unshift(@INC, $squish_lib_perl);
}
