/**********************************************************************
** Copyright (C) 2005 froglogic GmbH.
**
** This file is part of Squish.
**
** The following licensing terms and conditions constitute an
** exception from the standard Squish License Agreement and are
** incorporated therein by this reference. The purpose of this
** exception is to permit third parties the inclusion and compilation
** of this header file into such parties' proprietary programs.
**
** This software is provided 'as-is', without any express or implied
** warranty. In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/

#ifndef QTINTRUSIVE_H
#define QTINTRUSIVE_H

#include "qtbuiltinhook.h"

// for backward compatibility

namespace Squish {

    bool installIntrusiveHook()
    {
	return Squish::installBuiltinHook();
    }

}

#endif // QTINTRUSIVE_H
