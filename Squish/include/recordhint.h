/**********************************************************************
** Copyright (C) 2005 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/

#ifndef SQUISH_RECORDHINT_H
#define SQUISH_RECORDHINT_H

#if QT_VERSION < 0x040000
# include <qevent.h>
# include <qapplication.h>
#else
#ifndef QT3_SUPPORT
# define UNDEF_QT3_SUPPORT
# define QT3_SUPPORT
#endif
# include <QtCore/QEvent>
#ifdef UNDEF_QT3_SUPPORT
#  undef QT3_SUPPORT
#endif
# include <QtCore/QCoreApplication>
#endif

#ifndef FLIST_H
#define FLIST_H

#if !defined(SQUISH_FK_NODLL)
#  if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#    if defined(SQUISH_FK_MAKEDLL) // create dll
#      define SQUISH_FK_EXPORT __declspec(dllexport)
#    else // use dll
#      define SQUISH_FK_EXPORT  __declspec(dllimport)
#    endif
#    if !defined(__GNUC__)
#      define SQUISH_FK_TEMPLATEDLL
#      define SQUISH_FK_TEMPLATE_EXTERN
#    endif
#  else
#   define SQUISH_FK_EXPORT
#  endif
#else
#  define SQUISH_FK_EXPORT
#endif

#ifdef FLIST_TYPE
#  undef FLIST_TYPE
#endif
#ifdef FLIST_ITERATOR_TYPE
#  undef FLIST_ITERATOR_TYPE
#endif
#ifdef FLIST_CONSTITERATOR_TYPE
#  undef FLIST_CONSTITERATOR_TYPE
#endif

#if QT_VERSION < 0x040000
#  define FLIST_TYPE QValueList<T>
#  define FLIST_ITERATOR_TYPE QValueListIterator<T>
#  define FLIST_CONSTITERATOR_TYPE QValueListConstIterator<T>
#  define FLIST_TYPE_(T) QValueList<T>
#  define FLIST_ITERATOR_TYPE_(T) QValueListIterator<T>
#  define FLIST_CONSTITERATOR_TYPE_(T) QValueListConstIterator<T>
#  include <qvaluelist.h>
#else
#  define FLIST_TYPE QLinkedList<T>
#  define FLIST_ITERATOR_TYPE typename QLinkedList<T>::iterator
#  define FLIST_CONSTITERATOR_TYPE typename QLinkedList<T>::const_iterator
#  define FLIST_TYPE_(T) QLinkedList<T>
#  define FLIST_ITERATOR_TYPE_(T) typename QLinkedList<T>::iterator
#  define FLIST_CONSTITERATOR_TYPE_(T) typename QLinkedList<T>::const_iterator
#  include <qlinkedlist.h>
#endif

template <typename T> class FListIterator;
template <typename T> class FListConstIterator;
template <typename T> class FList;

template <typename T>
class FListIterator
{
    friend class FListConstIterator<T>;
    friend class FList<T>;

public:
    FListIterator() { }
    FListIterator( const FLIST_ITERATOR_TYPE &it ) : impl( it ) { }

    bool operator==( const FListIterator &rhs ) const {
	return impl == rhs.impl;
    }
    bool operator!=( const FListIterator &rhs ) const {
	return !operator==( rhs );
    }

    const T &operator*() const { return *impl; }
    T &operator*() { return *impl; }

    FListIterator<T> &operator++() { ++impl; return *this; }
    FListIterator<T> &operator++( int ) { impl++; return *this; }
    FListIterator<T> &operator--() { --impl; return *this; }
    FListIterator<T> &operator--( int ) { impl--; return *this; }

private:
    FLIST_ITERATOR_TYPE impl;
};

template <typename T>
class FListConstIterator
{
    friend class FList<T>;

public:
    FListConstIterator() { }
    FListConstIterator( const FListIterator<T> &it ) : impl( it.impl ) { }
    FListConstIterator( const FLIST_ITERATOR_TYPE &it ) : impl( it ) { }
    FListConstIterator( const FLIST_CONSTITERATOR_TYPE &it ) : impl( it ) { }

    bool operator==( const FListConstIterator &rhs ) const {
	return impl == rhs.impl;
    }
    bool operator!=( const FListConstIterator &rhs ) const {
	return !operator==( rhs );
    }

    const T &operator*() const { return *impl; }

    FListConstIterator<T> &operator++() { ++impl; return *this; }
    FListConstIterator<T> &operator++( int ) { impl++; return *this; }
    FListConstIterator<T> &operator--() { --impl; return *this; }
    FListConstIterator<T> &operator--( int ) { impl--; return *this; }


private:
    FLIST_CONSTITERATOR_TYPE impl;
};


template <typename T>
class FList
{
public:
    typedef FListIterator<T> Iterator;
    typedef FListIterator<T> iterator;
    typedef FListConstIterator<T> ConstIterator;
    typedef FListConstIterator<T> const_iterator;

    FList() { }
    FList( const FLIST_TYPE &l ) : impl( l ) { }

    bool operator==( const FList &rhs ) const { return impl == rhs.impl; }
    bool operator!=( const FList &rhs ) const { return !operator==( rhs ); }
    FList<T>& operator<< ( const T& x ) { append(x); return *this; }

    ConstIterator begin() const { return ConstIterator( impl.begin() ); }
    Iterator begin() { return Iterator( impl.begin() ); }
    ConstIterator end() const { return ConstIterator( impl.end() ); }
    Iterator end() { return Iterator( impl.end() ); }

    bool isEmpty() const { return impl.isEmpty(); }
    bool empty() const { return impl.empty(); }

    size_t count() const { return impl.count(); }
    size_t size() const { return impl.size(); }

    const T &operator[]( size_t pos ) const {
#if QT_VERSION < 0x040000
	return impl[pos];
#else
	((FList<T>*)this)->impl.detach();
	return *(impl.begin() + pos);
#endif
    }
    T &operator[]( size_t pos ) {
#if QT_VERSION < 0x040000
	return impl[pos];
#else
	impl.detach();
	return *(impl.begin() + pos);
#endif
    }

    const T &at( size_t pos ) const { return operator[]( pos ); }
    T &at( size_t pos ) { return operator[]( pos ); }

    void operator+=( const T &v ) { impl += v; }
    void operator+=( const FList<T> &l ) { impl += l.impl; }

    const T &first() const { return impl.first(); }
    T &first() { return impl.first(); }
    const T &last() const { return impl.last(); }
    T &last() { return impl.last(); }

    Iterator append( const T &v ) { impl.append( v ); return --impl.end(); }
    Iterator prepend( const T &v ) { return impl.insert( impl.begin(), v ); }
    Iterator insert( const FListIterator<T> &pos, const T &value )
    { return impl.insert( pos.impl, value ); }
    void clear() { impl.clear(); }

    int remove( const T &v )
    {
#if QT_VERSION >= 0x040000
	return impl.removeAll( v );
#else
	return impl.remove( v );
#endif
    }

    Iterator remove( const Iterator &it )
    {
#if QT_VERSION >= 0x040000
	return impl.erase( it.impl );
#else
	return impl.remove( it.impl );
#endif
    }

    void push_front( const T &v ) { impl.push_front( v ); }
    void pop_front() { remove( begin() ); }

    Iterator find( const T &v )
    {
#if QT_VERSION >= 0x040000
	return qFind( impl.begin(), impl.end(), v );
#else
	return impl.find( v );
#endif
    }

    ConstIterator find( const T &v ) const
    {
#if QT_VERSION >= 0x040000
        ConstIterator it = impl.constBegin();
        while ( it != end() && !(*it == v) )
            ++it;
        return it;
#else
	return impl.find( v );
#endif
    }



    ConstIterator findRev( const T &v ) const
    {
        ConstIterator it = end();
        it--;
        for( ; it != begin(); it-- ) {
            if( *it == v ) {
                return it;
            }
        }
        it = begin();
        if( *it == v ) return begin();
        return end();
    }

    Iterator findRev( const T &v )
    {
        Iterator it = end();
        it--;
        for( ; it != begin(); it-- ) {
            if( *it == v ) {
                return it;
            }
        }
        it = begin();
        if( *it == v ) return begin();
        return end();
    }

    bool contains( const T &v ) const { return find( v ) != end(); }

    int findIndex( const T &v ) const
    {
#if QT_VERSION >= 0x040000
	int i = 0;
	for ( ConstIterator it = impl.begin(); it != impl.end(); ++it, ++i )
	    if ( *it == v )
		return i;
	return -1;
#else
	return impl.findIndex( v );
#endif
    }

    FLIST_TYPE toQtList() const { return impl; }

private:
    FLIST_TYPE impl;
};

#undef FLIST_TYPE
#undef FLIST_ITERATOR_TYPE
#undef FLIST_CONSTITERATOR_TYPE

#endif // FLIST_H

namespace Squish
{
    class RecordHintArgument
    {
    public:
	enum DataType {
	    IntArg,
	    BoolArg,
	    StringArg,
	    QObjectArg,
	    DoubleArg
	};

	RecordHintArgument()
	    : dt( IntArg ), v(), o( 0 ) {}
	RecordHintArgument( DataType t, const QString &value )
	    : dt( t ), v( value ), o( 0 ) {}
	RecordHintArgument( const QString &value )
	    : dt( StringArg ), v( value ), o( 0 ) {}
	RecordHintArgument( const char *value )
	    : dt( StringArg ), v( QString::fromLocal8Bit( value ) ), o( 0 ) {}
	RecordHintArgument( int value )
	    : dt( IntArg ), v( QString::number( value ) ), o( 0 ) {}
	RecordHintArgument( double value )
	    : dt( DoubleArg ), v( QString::number( value ) ), o( 0 ) {}
	RecordHintArgument( bool value )
	    : dt( BoolArg ), v( value ? "True" : "False" ), o( 0 ) {}
	RecordHintArgument( QObject *obj )
	    : dt( QObjectArg ), v(), o( obj ) {}

	DataType dataType() const { return dt; }
	QString value() const { return v; }
	QObject *object() const { return o; }

    private:
	DataType dt;
	QString v;
	QObject *o;
	void *d;

    };

    typedef FList<RecordHintArgument> RecordHintArgumentList;

    class RecordHint
    {
    public:
	enum Type {
	    Comment,
	    Function,
            SetEventCompression
	};

	enum InsertionMode {
	    BeforeCommand,
	    AfterCommand,
	    OverwriteCommand
	};

	RecordHint() {}

	RecordHint( Type t, const QString &commentOrFunc = QString::null )
	    : typ( t ), cof( commentOrFunc ), insMod( AfterCommand ) {}

	void addArgument( const RecordHintArgument &a ) {
	    args.append( a );
	}
	void addArgument( const QString &v ) {
	    args.append( RecordHintArgument( v ) );
	}
	void addArgument( const char *v ) {
	    args.append( RecordHintArgument( v ) );
	}
	void addArgument( int v ) {
	    args.append( RecordHintArgument( v ) );
	}
	void addArgument( bool v ) {
	    args.append( RecordHintArgument( v ) );
	}
	void addArgument( double v ) {
	    args.append( RecordHintArgument( v ) );
	}
	void addArgument( QObject *v ) {
	    args.append( RecordHintArgument( v ) );
	}

	Type type() const { return typ; }
	QString commentOrFunction() const { return cof; }
	RecordHintArgumentList recordHintArguments() const { return args; }

	void send( InsertionMode i = AfterCommand );

	InsertionMode insertionMode() const { return insMod; }

    private:
	Type typ;
	QString cof;
	RecordHintArgumentList args;
	InsertionMode insMod;

    };

    class RecordHintList
    {
    public:
	RecordHintList() {}

	void addRecordHint( const RecordHint &rh ) {
	    l.append( rh );
	}

	void send( RecordHint::InsertionMode i = RecordHint::AfterCommand ) {
	    for ( FList<RecordHint>::Iterator it = l.begin();
		  it != l.end(); ++it )
		(*it).send( i );
	}

    private:
	FList<RecordHint> l;

    };

    class RecordHintEvent : public QEvent
    {
    public:
	enum {
	    Id = QEvent::User + 8002
	};

	RecordHintEvent( const RecordHint &recordHint )
	    : QEvent( QEvent::Type( Id ) ),
	    hint( recordHint )
	{ }

	const RecordHint &recordHint() const { return hint; }

    private:
	RecordHint hint;
	void *d;

    };

    inline void RecordHint::send( InsertionMode i )
    {
	insMod = i;
#if QT_VERSION < 0x040000
	QApplication::postEvent( qApp, new RecordHintEvent( *this ) );
#else
	QCoreApplication::postEvent( qApp, new RecordHintEvent( *this ) );
#endif
    }
}

#endif
