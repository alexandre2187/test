#ifndef SQUISH_NULL_EXTENSION_H
#define SQUISH_NULL_EXTENSION_H

/* winextension.h is the only header which needs to be included
 * when developing Squish extensions.
 */
#include "sdk/extensions/winextension.h"

/* This class demonstrates the smallest extension possible. It only
 * implements the single pure virtual method in the Squish::Extension
 * class.
 */
class NullExtension : public Squish::Extension
{
public:
    virtual Squish::GUIObjectPtr createObject( HWND hnd, int relativeX, int relativeY );
};

#endif // !defined(SQUISH_NULL_EXTENSION_H)

