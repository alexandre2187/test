#include "null.h"

using namespace Squish;

GUIObjectPtr NullExtension::createObject( HWND hnd, int relativeX, int relativeY )
{
    // This extension cannot produce any objects, it just returns null
    return 0;
}

/* Using this macro is needed to make the plugin export the
 * NullExtension interface, so that Squish can access it.
 */
SQUISH_EXPORT_EXTENSION(NullExtension)

