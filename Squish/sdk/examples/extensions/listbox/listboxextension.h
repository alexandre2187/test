#ifndef SQUISH_LISTBOX_EXTENSION_H
#define SQUISH_LISTBOX_EXTENSION_H

#include "sdk/extensions/winextension.h"
#include "sdk/extensions/extensionhelper.h" // for Squish::ObjectWithIface*

#include <map>
#include <set>

#include <windows.h>

class ItemQuery : public Squish::ObjectQuery
{
public:
    ItemQuery( const Squish::IString &itemType, Squish::ObjectQuery *constraints );

    virtual bool hasProperty( const Squish::IString &name ) const;
    virtual Squish::IString getProperty( const Squish::IString &name ) const;
    virtual MatchingMode matchingMode( const Squish::IString &propertyName ) const;

    virtual bool matches( Squish::GUIObjectPtr object );

    virtual Squish::IString asString() const;

private:
    Squish::IString m_itemType;
    Squish::ObjectQuery *m_constraints;
};

class ListBoxItem : public Squish::ObjectWithIface6<Squish::GUIObject,
                                            Squish::GeometryInterface,
                                            Squish::TraversalInterface,
                                            Squish::PropertyInterface,
                                            Squish::NeighbourhoodInterface,
                                            Squish::InteractionInterface,
                                            Squish::IdentityInterface>
{
public:
    explicit ListBoxItem( HWND handle, int index );

    virtual const char *type() const { return "ListItem"; }

    virtual const Squish::Extension *extension() const;

    // IdentityInterface
    virtual bool sameObjectAs( Squish::GUIObjectPtr other ) const;

    // GeometryInterface
    virtual void rect( int *x, int *y, int *width, int *height ) const;

    // TraversalInterface
    virtual Squish::GUIObjectPtr parent() const;
    virtual Squish::GUIObjectList children() const;

    // PropertyInterface
    virtual std::list<Property> properties() const;
    virtual Squish::IString property( const std::string &name ) const;

    // NeighbourhoodInterface
    virtual Squish::GUIObjectPtr getContainerObject() const;
    virtual Squish::GUIObjectPtr getLeftObject() const;
    virtual Squish::GUIObjectPtr getAboveObject() const;

    // InteractionInterface
    virtual bool click( int x, int y, int, int btn ) const;

private:
    HWND m_handle;
    int m_index;
};

class ListBox : public Squish::ObjectWithIface3<Squish::GUIObject,
                                        Squish::NativeObjectInterface,
                                        Squish::TraversalInterface,
                                        Squish::ItemContainerInterface>
{
public:
    explicit ListBox( HWND handle ) : m_handle( handle ) { }

    virtual const char *type() const { return "List"; }

    virtual const Squish::Extension *extension() const;

    // TraversalInterface
    virtual Squish::GUIObjectPtr parent() const;
    virtual Squish::GUIObjectList children() const;

    // NativeObjectInterface
    virtual HWND handle() const { return m_handle; }

    // ItemContainerInterface
    virtual Squish::GUIObjectPtr findContainedItem( Squish::ObjectQuery *query ) const;
    virtual int itemCount() const;

private:
    HWND m_handle;
};

class ListBoxExtension : public Squish::Extension
{
public:
    virtual Squish::GUIObjectPtr createObject( HWND hnd, int relativeX, int relativeY );

private:
    bool isListBox( HWND hnd ) const;
};

#endif // !defined(SQUISH_LISTBOX_EXTENSION_H)
