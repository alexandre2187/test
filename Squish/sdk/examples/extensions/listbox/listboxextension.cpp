#include "listboxextension.h"

#include <windowsx.h> // for ListBox_GetItemRect macro

// for MSVC6:
#if !defined(WC_LISTBOXA)
#  define WC_LISTBOXA             "ListBox"
#endif

SQUISH_EXPORT_EXTENSION(ListBoxExtension)

using namespace std;
using namespace Squish;

static wstring utf8ToUTF16( const string &s )
{
    const int size = ::MultiByteToWideChar( CP_UTF8, 0, s.c_str(), -1, 0, 0 );

    vector<wchar_t> buf( size );
    ::MultiByteToWideChar( CP_UTF8, 0, s.c_str(), -1, &buf[0], size );

    return wstring( &buf[0] );
}

static string ansiClassName( HWND hnd )
{
    if ( !::IsWindow( hnd ) ) {
        return string();
    }

    char buf[ 1024 ];
    if ( ::GetClassNameA( hnd, buf, sizeof( buf ) ) == 0 ) {
        Squish::Log::systemError( "listboxextension.dll: ansiClassName(): GetClassNameA failed" );
        return string();
    }
    return buf;
}

ItemQuery::ItemQuery( const IString &itemType, ObjectQuery *constraints )
    : m_itemType( itemType ),
    m_constraints( constraints )
{
}


bool ItemQuery::hasProperty( const IString &name ) const
{
    return name == ILatin1String( "type" ) || m_constraints->hasProperty( name );
}

IString ItemQuery::getProperty( const IString &name ) const
{
    if ( name == ILatin1String( "type" ) ) {
        return m_itemType;
    }
    return m_constraints->getProperty( name );
}

ObjectQuery::MatchingMode ItemQuery::matchingMode( const IString &propertyName ) const
{
    if ( propertyName == ILatin1String( "type" ) ) {
        return StrictMatch;
    }
    return m_constraints->matchingMode( propertyName );
}

bool ItemQuery::matches( GUIObjectPtr object )
{
    if ( m_itemType != ILatin1String( object->type() ) ) {
        return false;
    }
    return m_constraints->matches( object );
}

IString ItemQuery::asString() const
{
    string s = "ItemQuery for type '";
    s += m_itemType.utf8();
    s += "': ";
    s += m_constraints->asString().utf8();
    return IString::fromUtf8( s.c_str() );
}
ListBoxItem::ListBoxItem( HWND handle, int index )
    : m_handle( handle ), m_index( index )
{
}

const Extension *ListBoxItem::extension() const
{
    return g_extensionObject;
}

bool ListBoxItem::sameObjectAs( GUIObjectPtr other ) const
{
    const ListBoxItem *otherItem = static_cast<const ListBoxItem *>( other.object() );
    return m_handle == otherItem->m_handle &&
        m_index == otherItem->m_index;
}

void ListBoxItem::rect( int *x, int *y, int *width, int *height ) const
{
    RECT r;
    if ( !ListBox_GetItemRect( m_handle, m_index, &r ) ) {
        *x = *y = *width = *height = -1;
    } else {
        currentEnv()->clientRectToAbsCoords( m_handle, r, x, y, width, height );
    }
}

GUIObjectPtr ListBoxItem::parent() const
{
    return new ListBox( m_handle );
}

GUIObjectList ListBoxItem::children() const
{
    return GUIObjectList();
}

list<PropertyInterface::Property> ListBoxItem::properties() const
{
    list<Property> props;
    props.push_back( Property( Property::String, "text" ) );
    return props;
}

IString ListBoxItem::property( const string &name ) const
{
    if ( name == "text" ) {
        vector<wchar_t> buffer( 1024 );
        if ( ::SendMessageW( m_handle, LB_GETTEXT, m_index, (LPARAM)&buffer[0] ) == LB_ERR ) {
            Log::warning( ILatin1String( "ListBoxItem::property: failed to read out text!" ) );
            return IString();
        }

        return makeIString( &buffer[0] );
    }

    return IString();
}

bool ListBoxItem::click( int x, int y, int, int btn ) const
{
    if ( ::SendMessage( m_handle, LB_SETTOPINDEX, m_index, 0 ) == LB_ERR ) {
        return false;
    }
    return currentEnv()->click( const_cast<ListBoxItem *>( this ), x, y, (MouseButton::Value)btn );
}

GUIObjectPtr ListBoxItem::getContainerObject() const
{
    return new ListBox( m_handle );
}

GUIObjectPtr ListBoxItem::getLeftObject() const
{
    return 0;
}

GUIObjectPtr ListBoxItem::getAboveObject() const
{
    return 0;
}

const Extension *ListBox::extension() const
{
    return g_extensionObject;
}

GUIObjectPtr ListBox::parent() const
{
    return currentEnv()->parent( m_handle );
}

GUIObjectList ListBox::children() const
{
    GUIObjectList l = currentEnv()->children( m_handle );

    int cnt = ::SendMessage( handle(), LB_GETCOUNT, 0, 0 );
    for ( int i = 0 ; i < cnt; ++i ) {
        l.push_back( new ListBoxItem( handle(), i ) );
    }
    return l;
}

GUIObjectPtr ListBox::findContainedItem( ObjectQuery *query ) const
{
    ItemQuery itemQuery( ILatin1String( "ListItem" ), query );
    return currentEnv()->findContainedObject( this, &itemQuery );
}

int ListBox::itemCount() const
{
    return (int)SendMessage( handle(), LB_GETCOUNT, 0, 0 );
}

bool ListBoxExtension::isListBox( HWND hnd ) const
{
    /* If the given window has the class name 'ListBox', then we consider
     * it a real list box object.
     */
    const string cn = ansiClassName( hnd );
    if ( cn == WC_LISTBOXA ) {
        return true;
    }

    /* Check whether any of the class names given in the configuration setting
     * 'ListBoxClasses' match the class name of the given window. If so, then
     * we consider this window a list box object, too.
     */
    const ExtensionConfiguration *cfg = configuration();
    if ( cfg->hasSetting( "ListBoxClasses" ) ) {
        const vector<string> extraListBoxClasses = cfg->getSetting( "ListBoxClasses" );
        vector<string>::const_iterator it, end = extraListBoxClasses.end();
        for ( it = extraListBoxClasses.begin(); it != end; ++it ) {
            if ( cn == *it ) {
                return true;
            }
        }
    }

    return false;
}

GUIObjectPtr ListBoxExtension::createObject( HWND hwnd, int relativeX, int relativeY )
{
    if ( isListBox( hwnd ) ) {
        /* If relativeX and relativeY are both -1 then we shouldn't perform a
         * hit test but just look at the given window handle itself.
         */
        if ( relativeX != -1 && relativeY != -1 ) {
            LRESULT result = ::SendMessage( hwnd, LB_ITEMFROMPOINT, 0, MAKELPARAM( relativeX, relativeY ) );
            if ( HIWORD( result ) == 0 ) {
                return new ListBoxItem( hwnd, LOWORD( result ) );
            }
        }
        return new ListBox( hwnd );
    }

    return 0;
}


