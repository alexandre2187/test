/**********************************************************************
** Copyright (C) 2009 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#ifndef EXTENSIONHELPER_H
#define EXTENSIONHELPER_H

namespace Squish
{

class GUIObjectInterface;
class GUIObjectPtr;
class ObjectQuery;
class TraversalInterface;

template<class Base, class If1>
class ObjectWithIface1
    : public Base,
      public If1
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

template<class Base, class If1, class If2>
class ObjectWithIface2
    : public Base,
      public If1, public If2
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        case If2::Id:
            return static_cast<const If2*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

template<class Base, class If1, class If2, class If3>
class ObjectWithIface3
    : public Base,
      public If1, public If2, public If3
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        case If2::Id:
            return static_cast<const If2*>( this );
        case If3::Id:
            return static_cast<const If3*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

template<class Base, class If1, class If2, class If3, class If4>
class ObjectWithIface4
    : public Base,
      public If1, public If2, public If3, public If4
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        case If2::Id:
            return static_cast<const If2*>( this );
        case If3::Id:
            return static_cast<const If3*>( this );
        case If4::Id:
            return static_cast<const If4*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

template<class Base, class If1, class If2, class If3, class If4, class If5>
class ObjectWithIface5
    : public Base,
      public If1, public If2, public If3, public If4, public If5
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        case If2::Id:
            return static_cast<const If2*>( this );
        case If3::Id:
            return static_cast<const If3*>( this );
        case If4::Id:
            return static_cast<const If4*>( this );
        case If5::Id:
            return static_cast<const If5*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

template<class Base, class If1, class If2, class If3, class If4, class If5, class If6>
class ObjectWithIface6
    : public Base,
      public If1, public If2, public If3, public If4, public If5, public If6
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        case If2::Id:
            return static_cast<const If2*>( this );
        case If3::Id:
            return static_cast<const If3*>( this );
        case If4::Id:
            return static_cast<const If4*>( this );
        case If5::Id:
            return static_cast<const If5*>( this );
        case If6::Id:
            return static_cast<const If6*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

template<class Base, class If1, class If2, class If3, class If4, class If5, class If6, class If7>
class ObjectWithIface7
    : public Base,
      public If1, public If2, public If3, public If4,
      public If5, public If6, public If7
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        case If2::Id:
            return static_cast<const If2*>( this );
        case If3::Id:
            return static_cast<const If3*>( this );
        case If4::Id:
            return static_cast<const If4*>( this );
        case If5::Id:
            return static_cast<const If5*>( this );
        case If6::Id:
            return static_cast<const If6*>( this );
        case If7::Id:
            return static_cast<const If7*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

template<class Base, class If1, class If2, class If3, class If4, class If5, class If6, class If7,
                     class If8>
class ObjectWithIface8
    : public Base,
      public If1, public If2, public If3, public If4,
      public If5, public If6, public If7, public If8
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        case If2::Id:
            return static_cast<const If2*>( this );
        case If3::Id:
            return static_cast<const If3*>( this );
        case If4::Id:
            return static_cast<const If4*>( this );
        case If5::Id:
            return static_cast<const If5*>( this );
        case If6::Id:
            return static_cast<const If6*>( this );
        case If7::Id:
            return static_cast<const If7*>( this );
        case If8::Id:
            return static_cast<const If8*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

template<class Base, class If1, class If2, class If3, class If4, class If5, class If6, class If7,
                     class If8, class If9>
class ObjectWithIface9
    : public Base,
      public If1, public If2, public If3, public If4,
      public If5, public If6, public If7, public If8,
      public If9
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        case If2::Id:
            return static_cast<const If2*>( this );
        case If3::Id:
            return static_cast<const If3*>( this );
        case If4::Id:
            return static_cast<const If4*>( this );
        case If5::Id:
            return static_cast<const If5*>( this );
        case If6::Id:
            return static_cast<const If6*>( this );
        case If7::Id:
            return static_cast<const If7*>( this );
        case If8::Id:
            return static_cast<const If8*>( this );
        case If9::Id:
            return static_cast<const If9*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

template<class Base, class If1, class If2, class If3, class If4,
                     class If5, class If6, class If7, class If8,
                     class If9, class If10>
class ObjectWithIface10
    : public Base,
      public If1, public If2, public If3, public If4,
      public If5, public If6, public If7, public If8,
      public If9, public If10
{
public:
    virtual const GUIObjectInterface *getInterface( int id ) const
    {
        switch ( id ) {
        case If1::Id:
            return static_cast<const If1*>( this );
        case If2::Id:
            return static_cast<const If2*>( this );
        case If3::Id:
            return static_cast<const If3*>( this );
        case If4::Id:
            return static_cast<const If4*>( this );
        case If5::Id:
            return static_cast<const If5*>( this );
        case If6::Id:
            return static_cast<const If6*>( this );
        case If7::Id:
            return static_cast<const If7*>( this );
        case If8::Id:
            return static_cast<const If8*>( this );
        case If9::Id:
            return static_cast<const If9*>( this );
        case If10::Id:
            return static_cast<const If10*>( this );
        default:
            return Base::getInterface( id );
        }
    }
};

}

#endif // !defined(EXTENSIONHELPER_H)

