/**********************************************************************
** Copyright (C) 2008 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#ifndef EXTENSION_GENERIC_H
#define EXTENSION_GENERIC_H

#include <algorithm>
#include <cassert>
#include <list>
#include <string>
#include <vector>

#include "sdk/common/guiconstants.h"
#include "sdk/common/exportmacro.h"
#include <sdk/common/macros.h>

#if defined(_WIN32)
#  include <windows.h>
#  define vsnprintf _vsnprintf
#endif

#include <ctype.h> // for tolower()
#if defined(_MSC_VER) && _MSC_VER < 1600
typedef __int64 int64_t;
typedef unsigned __int32 uint32_t;
#else
#  include <stdint.h>
#endif
#include <stdarg.h> // for va_list et al
#include <stdio.h> // for sprintf(), sscanf()
#include <stdlib.h> // for atoi()

namespace Squish
{

//! RGB color with alpha channel
class RgbColor {
    public:
        /** Create an invalid instance **/
        RgbColor(): mAlpha( 0 ), mRed( 0 ), mGreen( 0 ), mBlue( 0 ) { }

        /** Create instance from an 0xAARRGGBB encoded value, i.e. a
         *  QRgb value.
         *  @note For a win32 COLORREF use RgbColor( r, g, b ) instead
         *        (different byte order)
         **/
        explicit RgbColor( unsigned int value ) {
            mAlpha = value >> 24;
            mRed   = ( value >> 16 ) & 0xff;
            mGreen = ( value >> 8 ) & 0xff;
            mBlue  = value & 0xff;
        }

        /** Create instance from hex-encoded color string (as used in CSS)
         *  Supported formats:
         *  - #AARRGGBB
         *  - #RRGGBB
         **/
        explicit RgbColor( const std::string &hexColor ) {
            bool ok = false;
            const char *s = hexColor.c_str();
            size_t sl = hexColor.length();
            if ( ( sl > 1 ) && ( s[0] == '#' ) ) {
                unsigned int ia, ir, ig, ib;
                ++s; --sl; // skip '#' for parsing;
                switch ( sl ) {
                    case 8: {
                        ok = sscanf( s, "%02X%02X%02X%02X", &ia, &ir, &ig, &ib ) == 4;
                        break;
                    }
                    case 6: {
                        ia = 0xFF;
                        ok = sscanf( s, "%02X%02X%02X", &ir, &ig, &ib ) == 3;
                        break;
                    }
                }
                if ( ok ) {
                    mAlpha = (unsigned char)ia;
                    mRed = (unsigned char)ir;
                    mGreen = (unsigned char)ig;
                    mBlue = (unsigned char)ib;
                }
            }
            if ( !ok )
                clear();
        }

        /** Create instance from RGB values without alpha channel **/
        RgbColor( unsigned char r, unsigned char g, unsigned char b )
            : mAlpha( 0xFF ), mRed( r ), mGreen( g ), mBlue( b ) { }

        /** Create instance from ARGB values **/
        RgbColor( unsigned char a, unsigned char r, unsigned char g, unsigned char b )
            : mAlpha( a ), mRed( r ), mGreen( g ), mBlue( b ) { }

        void clear() {
            mAlpha = mRed = mGreen = mBlue = 0;
        }

        bool isValid() const {
            return mAlpha || mRed || mGreen || mBlue;
        }

        bool operator ==( const RgbColor &o ) const {
            return ( mAlpha == o.mAlpha )
                && ( mRed == o.mRed )
                && ( mGreen == o.mGreen )
                && ( mBlue == o.mBlue );
        }

        bool operator !=( const RgbColor &o ) const {
            return ! ( *this == o );
        }

        operator std::string() const {
            char ccolor[10];
            if ( sprintf( ccolor, "#%02X%02X%02X%02X", mAlpha, mRed, mGreen,
                        mBlue ) < 0 ) {
                return std::string();
            }
            return std::string( ccolor );
        }

        unsigned int alpha() const { return mAlpha; }
        unsigned int red() const { return mRed; }
        unsigned int green() const { return mGreen; }
        unsigned int blue() const { return mBlue; }

    private:
        unsigned char mAlpha, mRed, mGreen, mBlue;
};


class GUIObjectInterface
{
public:
    GUIObjectInterface() {}
    virtual ~GUIObjectInterface() {}

private:
    GUIObjectInterface( const GUIObjectInterface *other ); // disabled
    void operator=( const GUIObjectInterface &rhs ); // disabled
};

class ILatin1String;

class IString;

bool operator==( const IString &lhs, const IString &rhs );

class IString
{
    friend bool operator==( const IString &lhs, const IString &rhs );
public:
    IString() {}
    IString( const ILatin1String &s );

    static IString fromUtf8( const char *s )
    {
        return IString( s );
    }
    static IString fromBool( bool b )
    {
        return b ? "true" : "false";
    }
    static IString fromInt( int i )
    {
        char c[12];
        sprintf( c, "%d", i );
        return IString::fromUtf8( c );
    }

    const char *utf8() const { return m_str.c_str(); }
    int toInt() const
    {
        return atoi( m_str.c_str() );
    }
    IString toLower() const
    {
        std::string s = m_str;
        std::transform( s.begin(), s.end(), s.begin(), tolower );
        return fromUtf8( s.c_str() );
    }
    bool toBool() const {
        return !( m_str.empty() || m_str == "0" || toLower() == "false" );
    }
    bool isEmpty() const { return m_str.empty(); }

private:
    IString( const std::string &str ) : m_str( str ) {}
    IString( const char *str ) : m_str( str ) {}

private:
    std::string m_str;
};

inline bool operator==( const IString &lhs, const IString &rhs )
{
    return lhs.m_str == rhs.m_str;
}

inline bool operator!=( const IString &lhs, const IString &rhs )
{
    return !operator==( lhs, rhs );
}

class ILatin1String
{
public:
    explicit ILatin1String( const char *s ) : m_str( s ) { }

    const char *latin1() const { return m_str; }

private:
    const char *m_str;
};

inline IString::IString( const ILatin1String &s )
{
    // #### transcode to UTF-8
    m_str = s.latin1();
}

class GUIObjectPtr;

class Extension;
class GUIObject
{
public:
    static inline void *operator new( size_t size );
    static inline void operator delete( void *mem );

    GUIObject() : m_refcount( 0 ) {}
    virtual ~GUIObject() {}

    virtual const Extension *extension() const = 0;

    virtual const char *type() const = 0;
    virtual const GUIObjectInterface *getInterface( int id ) const = 0;

    void ref() { ++m_refcount; }
    void deref() { if ( !--m_refcount ) delete this; }

private:
    GUIObject( const GUIObject &rhs );
    void operator=( const GUIObject &rhs );

    int m_refcount;
};

class GUIObjectPtr
{
public:
    GUIObjectPtr() : o( 0 ) { }
    ~GUIObjectPtr() { if ( o ) o->deref(); }
    GUIObjectPtr( GUIObject *go ) : o( go ) { if ( go ) go->ref(); }
    GUIObjectPtr( const GUIObjectPtr &other ) { if ( other.o ) other.o->ref(); o = other.o; }
    GUIObjectPtr &operator=( const GUIObjectPtr &other ) {
        if ( other.o ) other.o->ref();
        if ( o ) o->deref();
        o = other.o;
        return *this;
    }

    operator bool() const { return o != 0; }
    GUIObject *object() { return o; }
    const GUIObject *object() const { return o; }
    GUIObject *operator->() { return o; }
    const GUIObject *operator->() const { return o; }

private:
    GUIObject *o;
};

// pure virtual. still supply a default implementation for those
// sub-class just passing on the request to their base.
inline const GUIObjectInterface *GUIObject::getInterface( int /*id*/ ) const
{
    return 0;
}

template <class Iface>
const Iface *iface_cast( const GUIObject *o ) {
    if( !o ) {
        return 0;
    }
    return static_cast<const Iface *>( o->getInterface( static_cast<const Iface *>( 0 )->Id ) );
}

template <class Iface>
const Iface *iface_cast( GUIObjectPtr o ) {
    return iface_cast<Iface>( o.object() );
}

typedef std::list<GUIObjectPtr> GUIObjectList;

class TraversalInterface : public GUIObjectInterface
{
public:
    enum { Id = 10001 };

    virtual GUIObjectPtr parent() const = 0;
    virtual GUIObjectList children() const = 0;
};

class PropertyInterface : public GUIObjectInterface
{
public:
    struct Property {
        enum Type { String, Number, Boolean, DateTime } type;
        std::string name;
        Property() : type( String ) {}
        Property( Type t, const std::string &s ) : type( t ), name( s ) {}
    };

    enum { Id = 10002 };

    static IString UndefinedPropertyValue() {
        return ILatin1String( "__squish__undefined__property__value__" );
    }

    virtual std::list<Property> properties() const = 0;
    virtual IString property( const std::string &name ) const = 0;
};

inline bool operator==( const PropertyInterface::Property &l,
        const PropertyInterface::Property &r )
{
    return ( l.type == r.type ) && ( l.name == r.name );
}

class GeometryInterface : public GUIObjectInterface
{
public:
    enum { Id = 10003 };

    int x() const
    {
        int x, y, w, h;
        rect( &x, &y, &w, &h );
        return x;
    }
    int y() const
    {
        int x, y, w, h;
        rect( &x, &y, &w, &h );
        return y;
    }
    int width() const
    {
        int x, y, w, h;
        rect( &x, &y, &w, &h );
        return w;
    }
    int height() const
    {
        int x, y, w, h;
        rect( &x, &y, &w, &h );
        return h;
    }
    virtual void rect( int *x, int *y, int *width, int *height ) const = 0;
};

class ObjectQuery
{
public:
    enum MatchingMode {
        StrictMatch, WildcardMatch, RegexpMatch
    };

    ObjectQuery() {}
    virtual ~ObjectQuery() {}

    virtual bool hasProperty( const IString &name ) const = 0;
    virtual IString getProperty( const IString &name ) const = 0;
    virtual MatchingMode matchingMode( const IString &propertyName ) const = 0;

    virtual bool matches( GUIObjectPtr object ) = 0;

    virtual IString asString() const = 0;

private:
    ObjectQuery( const ObjectQuery &other );
    void operator=( const ObjectQuery &rhs );
};

class ObjectQuery2 : public ObjectQuery
{
public:
    virtual std::list<IString> unmatchedProperties() const = 0;
};

class NeighbourhoodInterface : public GUIObjectInterface
{
public:
    enum { Id = 10004 };

    virtual GUIObjectPtr getContainerObject() const = 0;
    virtual GUIObjectPtr getLeftObject() const = 0;
    virtual GUIObjectPtr getAboveObject() const = 0;
};

class InteractionInterface : public GUIObjectInterface
{
public:
    enum { Id = 10005 };

    virtual bool focus() const { return true; }
    // TODO: MouseButton enum is not used since Qt wrapper packs keyflags into button
    virtual bool click( int x, int y, int state, int btn ) const = 0;
    // TODO: should become pure virtual
    virtual bool doubleclick( int /*x*/, int /*y*/, int /*state*/, int /*btn*/ ) const { return false; }
    virtual bool setValue( int /*value*/ ) const { return false; }
    virtual bool expand() const { return false; }
    virtual bool collapse() const { return false; }
    virtual bool drag( int /*rx*/, int /*ry*/, GUIObjectPtr /*target*/, int /*rx2*/, int /*ry2*/ ) const { return false; }
    virtual bool ensureVisible( int /*x*/, int /*y*/ ) const { return true; }
};

class MethodInterface : public GUIObjectInterface
{
public:
    // TODO: Consolidate with PropertyInterface::Property::Type
    enum Type { String, Number, Boolean, Object };
    enum { Id = 10006 };

    struct ArgumentDescriptor {
        enum Type { String, Number, Boolean } type;
        IString name;
    };

    class ReturnValue {
    public:
        static ReturnValue nothing() { return ReturnValue(); }
        static ReturnValue fromInt( int i ) { return ReturnValue( i ); }
        static ReturnValue fromBool( bool b ) { return ReturnValue( b ); }
        static ReturnValue fromUtf8( const char *s ) { return ReturnValue( s ); }
        static ReturnValue fromObject( GUIObjectPtr o ) { return ReturnValue( o ); }

        int toInt() const { assert( m_type == Number ); return m_intValue; }
        bool toBool() const { assert( m_type == Boolean ); return m_boolValue; }
        const char *utf8() const { assert( m_type == String ); return m_utf8Value.c_str(); }
        GUIObjectPtr toObject() const { assert( m_type == Object ); return m_objectValue; }

    private:
        enum Type {
            String, Number, Boolean, Void, Object
        };

        ReturnValue() : m_type( Void ), m_utf8Value( "" ), m_boolValue( false ), m_intValue( 0 ), m_objectValue( 0 ) { }
        explicit ReturnValue( int i ) : m_type( Number ), m_utf8Value( "" ), m_boolValue( false ), m_intValue( i ), m_objectValue( 0 ) { }
        explicit ReturnValue( bool b ) : m_type( Boolean ), m_utf8Value( "" ), m_boolValue( b ), m_intValue( 0 ), m_objectValue( 0 ) { }
        explicit ReturnValue( const char *s ) : m_type( String ), m_utf8Value( s ? s : "" ), m_boolValue( false ), m_intValue( 0 ), m_objectValue( 0 ) { }
        explicit ReturnValue( GUIObjectPtr o ) : m_type( Object ), m_utf8Value( "" ), m_boolValue( false ), m_intValue( 0 ), m_objectValue( o ) { }

        const Type m_type;
        const std::string m_utf8Value;
        const bool m_boolValue;
        const int m_intValue;
        GUIObjectPtr m_objectValue;
    };

    struct MethodDescriptor {
        bool returnsValue;
        Type returnType;
        IString name;
        std::list<ArgumentDescriptor> arguments;
    };

    virtual int numMethods() const = 0;
    virtual MethodDescriptor method( int id ) const = 0;
    virtual ReturnValue invoke( int id, const std::vector<IString> &args ) const = 0;
};

class IdentityInterface : public GUIObjectInterface
{
public:
    enum { Id = 10008 };

    virtual bool sameObjectAs( GUIObjectPtr other ) const = 0;
};

class ReadynessInterface : public GUIObjectInterface
{
public:
    enum { Id = 10009 };

    virtual bool isObjectReady() const = 0;
};

class NativeObjectWrapper;

class NativeObjectAccessInterface : public GUIObjectInterface
{
public:
    enum { Id = 10010 };

    // TODO: Consolidate with PropertyInterface::Property::Type
    enum Type { String, Number, Boolean, Object } type;

    struct Value {
        Type type;
        IString stringValue;
        int numberValue;
        bool boolValue;
        NativeObjectWrapper *objectValue;
    };

    virtual Value nativeProperty( const IString &name ) const = 0;
    virtual bool setNativeProperty( const IString &name,
                                    const Value &value ) const = 0;
};

class NativeObjectWrapper
{
public:
    NativeObjectWrapper() {}
    virtual ~NativeObjectWrapper() {}

    virtual NativeObjectAccessInterface::Value property( const IString &name ) const = 0;
    virtual bool setProperty( const IString &name, const NativeObjectAccessInterface::Value &value ) const = 0;
    virtual IString toString() const = 0;
    virtual void deref() = 0;

private:
    NativeObjectWrapper( const NativeObjectWrapper &rhs ); // disabled
    void operator=( const NativeObjectWrapper &other ); // disabled
};

class PrimitiveNativeObjectType;
class NativeObject;

class NativeObjectType
{
public:
    enum PrimitiveType {
        PShort = 0,
        PInt,
        PLong,
        PBool,
        PChar,
        PDouble,
        PUInt,
        PVoid,
        PUndefined,
        PEnd
    };

    struct PropertyDeclaration {
        IString name;
        const NativeObjectType *type;
        bool isWriteable;
        bool isStatic;
    };
    typedef std::vector<PropertyDeclaration> PropertyDeclarations;

    struct ArgumentDeclaration {
        IString name;
        const NativeObjectType *type;
    };
    typedef std::vector<ArgumentDeclaration> ArgumentDeclarations;

    struct MethodDeclaration {
        IString name;
        const NativeObjectType *returnType;
        ArgumentDeclarations arguments;
        bool isStatic;
    };
    typedef std::vector<MethodDeclaration> MethodDeclarations;

    struct Error {
        virtual ~Error() {}
        virtual bool isSet() const = 0;
        virtual void set( const IString &description ) = 0;
    };

    static const NativeObjectType *primitiveString();
    static const NativeObjectType *primitiveShort();
    static const NativeObjectType *primitiveInt();
    static const NativeObjectType *primitiveLong();
    static const NativeObjectType *primitiveUnsignedInt();
    static const NativeObjectType *primitiveDouble();
    static const NativeObjectType *primitiveBoolean();

    virtual ~NativeObjectType() { }

    virtual std::vector<std::string> nameSpace() const { return std::vector<std::string>(); }

    virtual IString name() const = 0;
    virtual PropertyDeclarations properties() const = 0;
    virtual MethodDeclarations methods() const = 0;
    virtual PrimitiveType primitiveType() const { return PEnd; }

    virtual const NativeObjectType *super() const { return NULL; }

    virtual NativeObject *getProperty( const IString &name, const NativeObject *object, Error* err ) const = 0;
    virtual void setProperty( const IString &name, NativeObject *object, const NativeObject *value, Error* err ) const = 0;
    virtual NativeObject *invoke( int id, NativeObject *object,
                            const std::vector<NativeObject *> &args, Error* err ) const = 0;
protected:
    NativeObjectType() { }

private:
    NativeObjectType( const NativeObjectType &rhs ); // disabled
    void operator=( const NativeObjectType &other ); // disabled
};

class PrimitiveNativeObjectType : public NativeObjectType
{
public:
    PrimitiveNativeObjectType( const IString &name, PrimitiveType type ) :
        m_name( name ),
        m_type( type )
    {
    }

    virtual IString name() const { return m_name; }
    virtual PropertyDeclarations properties() const { return PropertyDeclarations(); }
    virtual MethodDeclarations methods() const { return MethodDeclarations(); }
    virtual PrimitiveType primitiveType() const { return m_type; }

    virtual NativeObject *getProperty( const IString&, const NativeObject*, Error* ) const { return NULL; }
    virtual void setProperty( const IString&, NativeObject*, const NativeObject*, Error* ) const { }
    virtual NativeObject *invoke( int, NativeObject*,
            const std::vector<NativeObject *>&, Error* ) const { return NULL; }

private:
    IString m_name;
    PrimitiveType m_type;
};

inline const NativeObjectType *NativeObjectType::primitiveString()
{
    static PrimitiveNativeObjectType t( ILatin1String( "PrimitiveString" ), PChar );
    return &t;
}

inline const NativeObjectType *NativeObjectType::primitiveShort()
{
    static PrimitiveNativeObjectType t( ILatin1String( "PrimitiveShort" ), PShort );
    return &t;
}

inline const NativeObjectType *NativeObjectType::primitiveInt()
{
    static PrimitiveNativeObjectType t( ILatin1String( "PrimitiveInt" ), PInt );
    return &t;
}

inline const NativeObjectType *NativeObjectType::primitiveLong()
{
    static PrimitiveNativeObjectType t( ILatin1String( "PrimitiveLong" ), PLong );
    return &t;
}

inline const NativeObjectType *NativeObjectType::primitiveUnsignedInt()
{
    static PrimitiveNativeObjectType t( ILatin1String( "PrimitiveUnsignedInt" ), PUInt );
    return &t;
}

inline const NativeObjectType *NativeObjectType::primitiveDouble()
{
    static PrimitiveNativeObjectType t( ILatin1String( "PrimitiveDouble" ), PDouble );
    return &t;
}

inline const NativeObjectType *NativeObjectType::primitiveBoolean()
{
    static PrimitiveNativeObjectType t( ILatin1String( "PrimitiveBoolean" ), PBool );
    return &t;
}

class NativeObject
{
public:
    virtual ~NativeObject() { }

    const NativeObjectType *type() const { return m_type; }

    virtual IString asString() const { return IString(); }
    virtual int asInteger() const { return 0; }
    virtual bool asBoolean() const { return false; }
    virtual double asDouble() const { return 0.0; }
    virtual int64_t asLong() const { return 0L; }
    virtual uint32_t asUnsignedInt() const { return 0; }

    virtual void deref() = 0;

protected:
    NativeObject( const NativeObjectType *type ) : m_type( type ) { }

private:
    NativeObject( const NativeObject &rhs ); // disabled
    void operator=( const NativeObject &other ); // disabled

    const NativeObjectType *m_type;
};

class NativeObjectAccessInterface2 : public GUIObjectInterface
{
public:
    enum { Id = 10013 };

    virtual NativeObject *nativeObject() const = 0;
};

class TreeInterface : public GUIObjectInterface
{
public:
    enum { Id = 10011 };

    virtual int indent() const = 0;
};

class ItemContainerInterface : public GUIObjectInterface
{
public:
    enum { Id = 10012 };

    virtual GUIObjectPtr findContainedItem( ObjectQuery *query ) const = 0;
    virtual int itemCount() const = 0;
};

class ObjectLookupInterface : public GUIObjectInterface
{
public:
    enum { Id = 10014 };

    virtual bool canContainObjectType( const IString &typeName ) const = 0;
    virtual bool hasOptimizedLookupFor( ObjectQuery *query ) const = 0;
    virtual GUIObjectPtr performOptimizedLookup( ObjectQuery *query ) const = 0;
};

class NamespaceInterface : public GUIObjectInterface
{
public:
    enum { Id = 10015 };

    virtual IString name() const = 0;
};

class TableInterface : public GUIObjectInterface
{
public:
    enum { Id = 10016 };

    virtual int rowCount() const = 0;
    virtual int columnCount() const = 0;
    virtual std::string getCellText( int row, int column ) const = 0;
    virtual RgbColor getCellBackgroundColor( int row, int column ) const = 0;
};

class TableCellInterface : public GUIObjectInterface
{
public:
    enum { Id = 10017 };

    virtual GUIObjectPtr parentTable() const = 0;
};

class SplitterInterface : public GUIObjectInterface
{
public:
    enum { Id = 10018 };

    enum Orientation { Vertical, Horizontal };

    virtual int splitterPosition() const = 0;
    virtual Orientation splitterOrientation() const = 0;
};

class ShortNameInterface : public GUIObjectInterface
{
public:
    enum { Id = 10019 };

    virtual IString shortName() const = 0;
};


// XXX This implementation will truncate after 1023 characters
#define GET_VARARGS_INTO_BUF \
    va_list argList; \
    va_start( argList, format ); \
    \
    std::vector<char> buf( 1024, '\0' ); \
    vsnprintf( &buf[0], buf.size() - 1, format, argList ); \
    va_end( argList );

class Logger
{
public:
    virtual ~Logger() {}
    virtual void info( const IString &msg ) = 0;
    virtual void warning( const IString &msg ) = 0;
    virtual void error( const IString &msg ) = 0;
    virtual void systemError( const IString &msg ) = 0;

    SQUISH_ATTRIBUTE_FORMAT_PRINTF(2, 3)
    inline void info( const char *format, ... ) {
        GET_VARARGS_INTO_BUF
        info( IString::fromUtf8( &buf[0] ) );
    }

    SQUISH_ATTRIBUTE_FORMAT_PRINTF(2, 3)
    inline void warning( const char *format, ... ) {
        GET_VARARGS_INTO_BUF
        warning( IString::fromUtf8( &buf[0] ) );
    }

    SQUISH_ATTRIBUTE_FORMAT_PRINTF(2, 3)
    inline void error( const char *format, ... ) {
        GET_VARARGS_INTO_BUF
        error( IString::fromUtf8( &buf[0] ) );
    }

    SQUISH_ATTRIBUTE_FORMAT_PRINTF(2, 3)
    inline void systemError( const char *format, ... ) {
        GET_VARARGS_INTO_BUF
        systemError( IString::fromUtf8( &buf[0] ) );
    }
};

class Env;
Env *currentEnv();

#if defined(SQUISH_EXPORT_EXTENSION_NAME) //  build against a static Qt

#if !defined(xstr)
#  define xstr(s) xstr_helper(s)
#  define xstr_helper(s) #s
#endif

#define SQUISH_CONCAT(a, b) SQUISH_CONCAT_HELPER(a, b) // use helper for macro expansion to happen
#define SQUISH_CONCAT_HELPER(a, b) a ## b

#define SQUISH_EXPORT_EXTENSION(ExtensionName) \
static ExtensionName *g_extensionObject; \
static void *init_extension() \
{ \
    static ExtensionName extensionObject; \
    g_extensionObject = &extensionObject; \
    return &extensionObject; \
} \
extern "C" { \
Squish::ExtensionInitFunctionRegistry \
SQUISH_CONCAT(registerInitFunction_, SQUISH_EXPORT_EXTENSION_NAME)( \
        xstr(SQUISH_EXPORT_EXTENSION_NAME), \
        &init_extension \
    ); \
}

#else

#define SQUISH_EXPORT_EXTENSION(ExtensionName) \
ExtensionName *g_extensionObject; \
Squish::Env *g_currentEnv; \
Squish::Env *Squish::currentEnv() { return g_currentEnv; } \
extern "C" SQUISH_EXPORT void *init_extension( Squish::Env *env ) \
{ \
    g_currentEnv = env; \
    static ExtensionName extensionObject; \
    g_extensionObject = &extensionObject; \
    return &extensionObject; \
}

#endif

class ExtensionConfiguration
{
public:
    ExtensionConfiguration() {}
    virtual ~ExtensionConfiguration() {}

    virtual bool hasSetting( const std::string &name ) const = 0;
    virtual const std::vector<std::string> &getSetting( const std::string &name ) const = 0;

private:
    ExtensionConfiguration( const ExtensionConfiguration &other );
    void operator=( const ExtensionConfiguration &rhs );
};

}

#endif // !defined(EXTENSION_GENERIC_H)

