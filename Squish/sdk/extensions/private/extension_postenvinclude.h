/**********************************************************************
** Copyright (C) 2008 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
class Log
{
public:
    static void info( const IString &msg )
    {
        currentEnv()->log()->info( msg );
    }
    static void warning( const IString &msg )
    {
        currentEnv()->log()->warning( msg );
    }
    static void error( const IString &msg )
    {
        currentEnv()->log()->error( msg );
    }
    static void systemError( const IString &msg )
    {
        currentEnv()->log()->systemError( msg );
    }

    SQUISH_ATTRIBUTE_FORMAT_PRINTF(1, 2)
    static void info( const char *format, ... )
    {
        GET_VARARGS_INTO_BUF
        currentEnv()->log()->info( ILatin1String( &buf[0] ) );
    }

    SQUISH_ATTRIBUTE_FORMAT_PRINTF(1, 2)
    static void warning( const char *format, ... )
    {
        GET_VARARGS_INTO_BUF
        currentEnv()->log()->warning( ILatin1String( &buf[0] ) );
    }

    SQUISH_ATTRIBUTE_FORMAT_PRINTF(1, 2)
    static void error( const char *format, ... )
    {
        GET_VARARGS_INTO_BUF
        currentEnv()->log()->error( ILatin1String( &buf[0] ) );
    }

    SQUISH_ATTRIBUTE_FORMAT_PRINTF(1, 2)
    static void systemError( const char *format, ... )
    {
        GET_VARARGS_INTO_BUF
        currentEnv()->log()->systemError( ILatin1String( &buf[0] ) );
    }
};

void *GUIObject::operator new( size_t size )
{
    return currentEnv()->allocateMemory( size );
}

void GUIObject::operator delete( void *mem )
{
    currentEnv()->freeMemory( mem );
}

