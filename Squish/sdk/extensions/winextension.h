/**********************************************************************
** Copyright (C) 2008 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#ifndef EXTENSION_WIN_H
#define EXTENSION_WIN_H

#include "private/extension_generic.h"

namespace Squish
{

inline IString makeIString( const wchar_t *s )
{
    const int len = ::WideCharToMultiByte( CP_UTF8, 0, s, -1, NULL, 0, 0, 0 );
    if ( len > 1 ) {
        char *buf = new char[ len ];
        ::WideCharToMultiByte( CP_UTF8, 0, s, -1, buf, len, 0, 0 );
        IString utf8String = IString::fromUtf8( buf );
        delete [] buf;
        return utf8String;
    }
    return IString();
}

class NativeObjectInterface : public GUIObjectInterface
{
public:
    enum { Id = 10007 };

    virtual HWND handle() const = 0;
};

class SetStringValueInterface : public GUIObjectInterface
{
public:
    enum { Id = 12001 };

    virtual bool setStringValue( const IString& value ) const = 0;

    virtual bool hasValue() const = 0;
    virtual IString getValue() const = 0;
};

//! Marker interface for messagebox-like windows or dialogs.
class MessageBoxInterface : public GUIObjectInterface
{
public:
    enum { Id = 12002 };
};

class Env
{
public:
    virtual ~Env() {}
    virtual void setError( const IString &errMessage ) const = 0;
    virtual Logger *log() const = 0;

    // Memory allocation takes place in the Wrapper
    virtual void *allocateMemory( size_t size ) const = 0;
    virtual void freeMemory( void *mem ) const = 0;

    virtual void *queryInterface( const IString &name ) const = 0;

    virtual DWORD autPID() const = 0;

    // Utilities
    virtual std::wstring windowClassName( HWND handle ) const = 0;
    virtual void stripSingleAmpersands( wchar_t *s ) const = 0;
    virtual void clientRectToAbsCoords( HWND hnd, RECT r, int *x, int *y, int *w, int *h ) const = 0;

    // TraversalInterface helpers
    virtual GUIObjectPtr parent( HWND handle ) const = 0;
    virtual GUIObjectList children( HWND handle ) const = 0;
    virtual GUIObjectList scrollBarsForWindow( HWND handle ) const = 0;
    virtual GUIObjectPtr menuForWindow( HWND handle ) const = 0;

    // GeometryInterface helpers
    virtual void rect( HWND handle, int *x, int *y, int *width, int *height ) const = 0;

    // NeighbourhoodInterface
    virtual GUIObjectPtr getContainerObject( HWND handle ) const = 0;
    virtual GUIObjectPtr getLeftObject( GUIObjectPtr reference ) const = 0;
    virtual GUIObjectPtr getAboveObject( GUIObjectPtr reference ) const = 0;
    virtual GUIObjectPtr findContainedObject( const TraversalInterface *ti, ObjectQuery *query ) const = 0;
    virtual GUIObjectPtr objectFromHandle( HWND handle ) const = 0;

    // PropertyInterface helpers
    virtual std::list<PropertyInterface::Property> properties( HWND handle ) const = 0;
    virtual IString property( HWND handle, const std::string &name ) const = 0;
    virtual void addPropertyToList( std::list<PropertyInterface::Property> *list,
                                    const PropertyInterface::Property &p ) const = 0;

    typedef bool (*CanOverlapFn)( GUIObjectPtr other, GUIObjectPtr object );

    // InteractionInterface helpers
    virtual bool focus( HWND handle ) const = 0;
    virtual bool click( GUIObjectPtr object, int rx, int ry, MouseButton::Value button, bool animateCursorMovement = true, bool setForegroundWindow = true, CanOverlapFn canOverlap = 0 ) const = 0;
    virtual bool doubleClick( GUIObjectPtr object, int rx, int ry, MouseButton::Value butto, bool animateCursorMovement = true, CanOverlapFn canOverlap = 0 ) const = 0;
    virtual bool drag( GUIObjectPtr source, int rx, int ry, GUIObjectPtr dest, int rx2, int ry2, bool animateCursorMovement = true ) const = 0;
    virtual bool uncheckedClick( const GeometryInterface *gi, int rx, int ry, MouseButton::Value button, bool animateCursorMovement = true ) const = 0;
    virtual bool uncheckedDoubleClick( const GeometryInterface *gi, int rx, int ry, MouseButton::Value button, bool animateCursorMovement = true ) const = 0;
    virtual bool uncheckedDrag( GUIObjectPtr source, int rx, int ry, GUIObjectPtr dest, int rx2, int ry2, bool animateCursorMovement = true ) const = 0;

    // MethodInterface helpers
    virtual int numMethods( HWND handle ) const = 0;
    virtual MethodInterface::MethodDescriptor method( HWND handle, int id ) const = 0;
    virtual MethodInterface::ReturnValue invoke( HWND handle, int id, const std::vector<IString> &args ) const = 0;

    virtual GUIObjectPtr nativeMenuHitTest( HWND handle, int relativeX, int relativeY ) const = 0;
    virtual GUIObjectPtr nativeScrollBarHitTest( HWND handle, int relativeX, int relativeY ) const = 0;
};

#include "private/extension_postenvinclude.h"

class Extension
{
public:
    Extension() : m_configuration( 0 ) {}
    virtual ~Extension() { }

    const std::string &name() const { return m_name; }
    void setName( const std::string &name ) { m_name = name; }

    const std::string &nameSpace() const { return m_namespace.empty() ? m_name : m_namespace; }
    void setNamespace( const std::string &nameSpace ) { m_namespace = nameSpace; }

    const ExtensionConfiguration *configuration() const { return m_configuration; }
    void setConfiguration( ExtensionConfiguration *configuration ) {
        m_configuration = configuration;
    }

    virtual GUIObjectPtr createObject( HWND hnd, int relativeX, int relativeY ) = 0;
    virtual GUIObjectPtr focusedObject() = 0;

private:
    Extension( const Extension &other );
    void operator=( const Extension &rhs );

    std::string m_name;
    std::string m_namespace;
    ExtensionConfiguration *m_configuration;
};

}

#endif // !defined(EXTENSION_WIN_H)

