/**********************************************************************
** Copyright (C) 2015 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/

#ifndef SQOSDETECTION_H
#define SQOSDETECTION_H

#if defined(_AIX)
#  define SQUISH_OS_UNIX
#  define SQUISH_OS_AIX
#elif defined(__ANDROID__) || defined(ANDROID)
#  define SQUISH_OS_ANDROID
#  define SQUISH_OS_UNIX
#  define SQUISH_OS_LINUX
#elif defined(__linux__) || defined(linux)
#  define SQUISH_OS_UNIX
#  define SQUISH_OS_LINUX
#elif defined(__QNX__)
#  define SQUISH_OS_UNIX
#  define SQUISH_OS_QNX
#elif defined(__vxworks__) || defined(__VXWORKS__)
#  define SQUISH_OS_UNIX
#  define SQUISH_OS_VXWORKS
#elif defined(__SunOS_5_10) || defined(__sun)
#  define SQUISH_OS_UNIX
#  define SQUISH_OS_SOLARIS
#elif defined(__APPLE__)
#  define SQUISH_OS_UNIX
#  define SQUISH_OS_MAC
#  include <TargetConditionals.h>
#  if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
#    define SQUISH_OS_IOS
#  else
#    define SQUISH_OS_OSX
#  endif
#elif defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#  define SQUISH_OS_WIN32
#  if defined(_WIN32_WCE) || defined(WINCE)
#    define SQUISH_OS_WINCE
#  elif defined(WIN64) || defined(_WIN64)
#    define SQUISH_OS_WIN64
#  endif
#elif defined(__unix) || defined(__unix__) || defined(unix) || defined(__UNIX) || defined(__Lynx__)
#  define SQUISH_OS_UNIX
#endif

#endif
