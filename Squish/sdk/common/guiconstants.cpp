/**********************************************************************
** Copyright (C) 2009 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#include "guiconstants.h"

using namespace Squish;

const char *MouseButton::valueAsString( Value v )
{
#define SQUISH_CONSTANT(name, value) static const char str_##name[] = #name;
#include "mouseconstants.def"
#undef SQUISH_CONSTANT
    switch ( v ) {
        case None: return "None";
#define SQUISH_CONSTANT(name, value) case name: return str_##name;
#include "mouseconstants.def"
#undef SQUISH_CONSTANT
    }
    return 0;
}

const int *MouseButton::values()
{
    static const int a[] = {
        None,
#define SQUISH_CONSTANT(name, value) name,
#include "mouseconstants.def"
#undef SQUISH_CONSTANT
        -1
    };
    return a;
}

const char *Modifier::valueAsString( Value v )
{
#define SQUISH_CONSTANT(name, value) static const char str_##name[] = #name;
#include "modifierconstants.def"
#undef SQUISH_CONSTANT
    switch ( v ) {
        case None: return "None";
#define SQUISH_CONSTANT(name, value) case name: return str_##name;
#include "modifierconstants.def"
#undef SQUISH_CONSTANT
    }
    return 0;
}

const int *Modifier::values()
{
    static const int a[] = {
        None,
#define SQUISH_CONSTANT(name, value) name,
#include "modifierconstants.def"
#undef SQUISH_CONSTANT
        -1
    };
    return a;
}

