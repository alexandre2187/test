/**********************************************************************
** Copyright (C) 2015 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/

#ifndef SQARCHDETECTION_H
#define SQARCHDETECTION_H

#if defined(__i386__) || defined(__i386) || defined(_M_IX86) || defined(_X86_)
#  define SQUISH_ARCH_X86
#elif defined(__amd64__) || defined(__x86_64__) || defined(_M_X64) || defined(_M_AMD64)
#  define SQUISH_ARCH_X86_64
#elif defined(__aarch64__)
#  define SQUISH_ARCH_ARM64
#elif defined(__arm__) || defined(__thumb__) || defined(_M_ARM) || defined(_M_ARMT)
#  define SQUISH_ARCH_ARM
#elif defined(__mips__) || defined(mips) || defined(__mips)
#  define SQUISH_ARCH_MIPS
#elif defined(__powerpc__) || defined(__powerpc) || defined(__ppc__) || defined(_M_PPC)
#  define SQUISH_ARCH_PPC
#elif defined(__sparc__) || defined(__sparc_v8__) || defined(__sparc_v9__) || defined(__sparc) || defined(__sparcv8) || defined(__sparcv9)
#  define SQUISH_ARCH_SPARC
#endif

#endif
