/**********************************************************************
** Copyright (C) 2013 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/

#ifndef MACROS_H
#define MACROS_H

#include <assert.h>

#define SQUISH_ASSERT_MSG(cond, msg) assert( (cond) && (msg) )

#define SQUISH_UNREACHABLE SQUISH_ASSERT_MSG(0, "Reached a line considered to be unreachable")

#define SQUISH_UNUSED(x) (void)x;

#if defined(__GNUC__) && ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 1)))
#  define SQUISH_DEPRECATED __attribute__ ((deprecated))
#elif defined(_MSC_VER) && _MSC_VER > 1200
#  define SQUISH_DEPRECATED __declspec(deprecated)
#else
#  define SQUISH_DEPRECATED
#endif

#if defined(__clang__) || (defined(__GNUC__) && ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 2))))
#  define SQUISH_ATTRIBUTE_FORMAT_PRINTF(formatArgPos, varArgPos) \
        __attribute__((format(printf, (formatArgPos), (varArgPos))))
#else
#  define SQUISH_ATTRIBUTE_FORMAT_PRINTF(formatArgPos, varArgPos)
#endif

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#  define SQ_DECL_EXPORT __declspec(dllexport)
#  define SQ_DECL_IMPORT __declspec(dllimport)
#  if !defined(__GNUC__)
#    define SQ_DECL_TEMPLATEDLL
#  endif
#elif defined(__GNUC__) && __GNUC__ - 0 > 3
#  define SQ_DECL_EXPORT __attribute__ ((visibility("default")))
#  define SQ_DECL_IMPORT
#else
#  define SQ_DECL_EXPORT
#  define SQ_DECL_IMPORT
#endif

// clang-analyzer does not handle inline-asm in expanded FD_ZERO
#ifdef __clang_analyzer__
#  define SQ_FD_ZERO(fds) memset((fds), 0, sizeof((fds)))
#else
#  define SQ_FD_ZERO(fds) FD_ZERO((fds))
#endif

#define SQ_DISABLE_COPY(TypeName) \
    TypeName( const TypeName & ); \
    void operator=( const TypeName & )

#define SQ_XSTRINGIFY(s) SQ_STRINGIFY(s)
#define SQ_STRINGIFY(s) #s

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L) // C99 compiler
#  define SQ_FUNCTION __func__
#elif defined(_MSC_VER) && (_MSC_VER <= 1200) // MSVC <= 6.0
#  define SQ_FUNCTION __FILE__ ":" SQ_STRINGIFY( __LINE__ )
#else
#  define SQ_FUNCTION __FUNCTION__
#endif

#endif
