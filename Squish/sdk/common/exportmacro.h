/**********************************************************************
** Copyright (C) 2009 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/

#ifndef EXPORTMACRO_H
#define EXPORTMACRO_H

#ifndef SQUISH_EXPORT
#  if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#      define SQUISH_EXPORT __declspec(dllexport)
#      define SQUISH_IMPORT __declspec(dllimport)
#  elif defined(__GNUC__) && __GNUC__ - 0 > 3
#      define SQUISH_EXPORT __attribute__ ((visibility("default")))
#      define SQUISH_IMPORT
#  else
#      define SQUISH_EXPORT
#      define SQUISH_IMPORT
#  endif
#endif

#endif
