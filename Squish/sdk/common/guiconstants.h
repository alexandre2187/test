/**********************************************************************
** Copyright (C) 2009 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#ifndef GUICONSTANTS_H
#define GUICONSTANTS_H

namespace Squish
{

struct MouseButton {
    enum Value {
        None = 0
#define SQUISH_CONSTANT(name, value) ,name = value
#include "mouseconstants.def"
#undef SQUISH_CONSTANT
    };

    static const int *values();
    static const char *valueAsString( Value v );
};

struct Modifier {
    enum Value {
        None    = 0
#define SQUISH_CONSTANT(name, value) ,name = value
#include "modifierconstants.def"
#undef SQUISH_CONSTANT
    };

    static const int *values();
    static const char *valueAsString( Value v );
};

}

#endif
