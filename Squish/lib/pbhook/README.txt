Important notes regarding the PowerBuilder support in froglogic Squish
======================================================================

In order to make a PowerBuilder-based GUI application testable, an
'intrusive' hook has to be installed into the application. This means
that a tiny modification has to be applied to the application source
code.

I - Installing the intrusive hook into PowerBuilder applications
----------------------------------------------------------------

Please follow these steps (in exactly this order):

1.) Import the 'squishpbextension.dll' PowerBuilder extension into the .pbl of
your application. To do so, start the PowerBuilder IDE. Then right-click the
.pbl of your application. In the menu which shows up, select
'Import PB Extension...'. A file browser dialog shows up. Use the dialog to
select the 'squishpbextension.dll', then press the Open. button If the
extension was imported successfully, a 'squishhook' class should show up in
the .pbl of your application.

2.) Declare a global variable called 'g_squishhook' of type 'squishhook'
by adding the following line to the 'Global Variables' section of your
application:

    squishhook g_squishhook

3.) In the 'open' event handler of the application object, add the
following lines at the top of the event handler:

    if AddToLibraryList("squishscripthook.dll") = 1 then
        g_squishhook = CREATE USING "squishscripthook"
    end if

4.) In the 'open' event handler of each toplevel object (i.e. login dialogs or
the main window), add the following line at the top of the event handler:

    g_squishhook.f_add_toplevel_object(this)

After taking these steps, deploy your application as usual.

II - Impact of the intrusive hook on the PowerBuilder application
-----------------------------------------------------------------

There are three notable effects of the intrusive hook on your PowerBuilder
application:

1.) A new global variable 'g_squishhook' is introduced

2.) Your application offers a new method on the g_squishhook object
called 'f_invoke'. This method can be called via the PowerBuilder
Native Interface (PBNI).

3.) In order to be able to use the application with Squish, two files
have to be stored in the same directory as the .exe file of your
application:

  - squishpbextension.dll
  - squishscripthook.dll

There are no other external dependencies introduced.

