Installing the intrusive hook for Visual Basic 6 applications
-------------------------------------------------------------

In order to make the vb_name property (which is available on native Windows
controls in case the WinRunner compatibility functionality is enabled) yield
meaningful values for Visual Basic 6 applications, a small script module has
to be inserted into the application under test.

To do so, add the module 'SquishScriptHook.bas' to the project and then call
the SquishScriptHook.install() function as early as possible (e.g. in the
Form_Load() event handler of the main window). This makes it possible for
Squish to extract the names of Visual Basic 6 controls.

