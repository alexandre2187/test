Attribute VB_Name = "SquishScriptHook"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' Copyright (C) 2011 froglogic GmbH.
'' All rights reserved.
''
'' This file is part of Squish.
''
'' Licensees holding a valid Squish License Agreement may use this
'' file in accordance with the Squish License Agreement provided with
'' the Software.
''
'' This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
'' WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
''
'' See the LICENSE file in the toplevel directory of this package.
''
'' Contact contact@froglogic.com if any conditions of this licensing are
'' not clear to you.
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

Private Declare Function CreateWindowEx _
    Lib "user32" Alias "CreateWindowExA" (ByVal dwExStyle As Long, _
                                          ByVal lpClassName As String, _
                                          ByVal lpWindowName As String, _
                                          ByVal dwStyle As Long, _
                                          ByVal x As Long, _
                                          ByVal y As Long, _
                                          ByVal nWidth As Long, _
                                          ByVal nHeight As Long, _
                                          ByVal hWndParent As Long, _
                                          ByVal hMenu As Long, _
                                          ByVal hInstance As Long, _
                                          lpParam As Any) As Long
    
Private Declare Function SetWindowLong _
    Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, _
                                         ByVal dwNewLong As Long) As Long

Private Declare Function CallWindowProc _
    Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hwnd As Long, _
                                          ByVal Msg As Long, ByVal wParam As Long, ByVal lParam _
                                          As Long) As Long
                                          
Private Declare Function RegisterWindowMessage _
    Lib "user32" Alias "RegisterWindowMessageA" (ByVal lpString As String) As Long

Private Declare Function GetCurrentProcessId Lib "kernel32" () As Long

Private Declare Sub CopyMemory _
    Lib "kernel32" Alias "RtlMoveMemory" (dest As Any, source As Any, ByVal bytes As Long)
    
Private Const GWL_WNDPROC = (-4)

Dim origWndProc As Long

Private Function controlNameForHWnd(ByVal hwnd As Long) As String
    Dim f As Form
    For Each f In Forms
        If f.hwnd = hwnd Then
            controlNameForHWnd = f.name
            Exit Function
        End If
        
        Dim c As Control
        For Each c In f.Controls
            If c.hwnd = hwnd Then
                controlNameForHWnd = c.name
                Exit Function
            End If
        Next
    Next
End Function

Private Function wndProc(ByVal hwnd As Long, ByVal iMsg As Long, _
                         ByVal wParam As Long, ByVal lParam As Long) As Long
    Static WM_GETCONTROLNAME As Long
    If WM_GETCONTROLNAME = 0 Then
        WM_GETCONTROLNAME = RegisterWindowMessage("WM_GETCONTROLNAME")
    End If
    
    If iMsg = WM_GETCONTROLNAME Then
        Dim name As String
        name = controlNameForHWnd(wParam)
        
        ' Copy unicode string data into the memory segment pointed to by
        ' lParam; make sure to copy two more bytes (the trailing zero wide-char).
        CopyMemory ByVal lParam, ByVal StrPtr(name), LenB(name) + 2
        
        wndProc = 0
        Exit Function
    End If
    
    wndProc = CallWindowProc(origWndProc, hwnd, iMsg, wParam, lParam)
End Function

Public Sub install()
    If origWndProc <> 0 Then
        Exit Sub
    End If
    
    Dim caption As String
    caption = "SquishVB6ScriptHook_" + CStr(GetCurrentProcessId())
    
    Dim commWnd As Long
    commWnd = CreateWindowEx(0, "STATIC", caption, 0, 0, 0, 0, 0, 0, 0, App.hInstance, ByVal 0&)
    
    origWndProc = SetWindowLong(commWnd, GWL_WNDPROC, AddressOf wndProc)
End Sub

