<extension>
    <name>UIAutomation</name>
    <description>Provides recognition of GUI controls via Microsoft UI Automation API.</description>
    <library>uiautomation</library>
    <!--
      ******************************************************
      For test suite version 3 and higher (see "VERSION=..."
      in "suite.conf"):

      - If the priority specified below is lower than 1, it
        will be ignored, and the priority be set to 1.
       (So this extension will always be enabled.)

      - If the priority specified below is equal to or higher
        than 1, it is being used for this extension.
      ******************************************************

      ******************************************************
      For test suite version 2 and lower (see "VERSION=..."
      in "suite.conf"):

      - The priority specified below is being used for this
        extension.
      ******************************************************
    -->
    <priority>-10</priority>
    <configuration>
        <!--
          The BlacklistedClassNames and WhitelistedClassNames settings allow
          configuring, which controls Squish will automate via UI Automation. Each of
          the two settings takes a list of wildcard patterns (i.e. the strings
          may contain '*' or '?').

          By default, all windows are whitelisted, so they will be accessed via
          UI Automation. The blacklist can be used to ignore some windows. The
          whitelist can be used to overrule the blacklist.

          Hence, enabling just a single class name 'XTPToolbar' can be achieved
          by blacklisting "*" and whitelisting "XTPToolbar".
        -->
        <setting>
            <name>BlacklistedClassNames</name>
            <!--
            <value>ClassName1</value>
            <value>ClassName2</value>
            <value>ClassName3</value>
            -->
        </setting>
        <setting>
            <name>WhitelistedClassNames</name>
            <!--
            <value>ClassName1</value>
            <value>ClassName2</value>
            <value>ClassName3</value>
            -->
        </setting>

        <!--
          The HandleIndividualObjects setting defines whether the UI Automation
          extension takes care of modelling entire object trees, or just
          individual objects. By default, this setting is set to 'false' i.e.
          the extension handles entire object trees, i.e. if a single control
          is handled by the extension then all descendants of the control will
          be handled as well.

          This can be set to 'true' to make the UI Automation extension only
          take care of individual WindowsControl controls for which no other,
          more specialized, recognition is available. Other extensions will be
          considered for descendants.
        -->
        <setting>
            <name>HandleIndividualObjects</name>
            <value>false</value>
        </setting>
    </configuration>
</extension>
