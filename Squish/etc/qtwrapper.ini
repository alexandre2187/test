# Sample configuration file for customizing the automation of Qt AUTs.
# You may modify this file, but it is recommended that you copy it instead and
# then register it as an AUT-specific configuration file using the command
#
#   squishserver --config setConfig <aut> <path_to_file>
#
# That way, you can keep this file as a reference of the default values.
#
# See the chapter 'Squish Initialization Files' in the Squish Manual for the
# semantic of these configuration options.
[General]

# Defines whether Squish always traverses the list of toplevel widgets in
# Qt >= 4.0 when searching for an object. As that is a costly operation we
# don't do it by default, but for some cases it's necessary as we otherwise
# miss objects being re-created and hence finding such objects wouldn't work
# anymore.
AlwaysAddToplevels = 0

ExitQtApplicationOnScriptFinished=0
FilterViewportWidget=1
UseNativeMouseEvents=0
UseNativeDialogs=0
ForceWindows8DesktopView=0

# If this is set to 1, Squish will ignore properties in multi-property names
# during QObject lookup if the object type does not contain such a property.
# This is a backward-compatibility setting for old object names relying on
# this behavior.
QObjectLookupSkipMissingProperties = 0

# Determine if Squish/Qt toolkit support should watch for calls to the
# C Runtime exit() function in order to detect AUT shutdowns.
# This usually has no effect because Squish gets informed about AUT shutdown
# by Qt earlier if your AUT calls QCoreApplication::quit().
# It is just a safety net for AUTs bypassing proper Qt shutdown by calling
# exit(). In some circumstances this can cause the AUT to crash on exit
# (observed on Linux/X11 with Qt 4.x) so it can be disabled.
InstallAtExitHandler = 1

# If set to 1 enable logging of many Qt event types handled by Squish during
# recording/replay (needs Qt >= 4.0)
LogQtEvents = 0



# If set to 1 enable logging of Qt signals whose signal name matches a
# registered signal handler but which did not match otherwise (different signal
# function signature or different object). This allows debugging of signal
# handler registration.
LogUnmatchedQtSignals = 0

# Set the amount of event logging during recording (only applies to QtQuick 2.x
# recording)
#
# The level roughly translates to:
#  0 - no logging
#  1 - log all events used as input for recording
#  2 - also log event compression decisions and dropped events
EventRecorderLogLevel = 0

# QtQuick 2.x only!
# If set to 1, failure to change toplevel QWindow focus will not result in a
# script failure. This is mainly useful on embedded platforms with incomplete
# or non-functional focus handling (i.e. Wayland).
IgnoreWindowFocusFailure = 0

# QtQuick 2.x only!
# Objects that inherit from one of the types in this list will be recorded
# without object-relative coordinates if possible.
RecordWithoutCoordinates = "BasicButton", "QQuickText"

Blacklisted Type Names = "QDesktopScreenWidget", "QGuardedPtrPrivate", "QSignal", "QImageDrag", "QTimer", "QRubberBand", "QFontCache", "QStyleSheet", "QToolTipGroup"
Blacklisted Types =
Whitelisted Types =

# Objects whose exact type is in this list should be included as
# containers/parents in newly generated object names.
Container Type Names =

# Objects whose type inherits/extends from one of the types in this
# list should be included as containers/parents in newly generated
# object names.
Container Types =

KeyEventCompression/Enabled = "QTextEdit", "QLineEdit", "QGraphicsView", "QPlainTextEdit"
KeyEventCompression/Disabled = "QTextBrowser"

# Enable recording of native mouse commands for the mentioned widget classes.
RecordNativeMouseCommands =

# Unique properties for generating spy and hierarchical object names.
# - Unique properties used on Qt 3 only
UniqueProperty/3/QButton = "text"
UniqueProperty/3/QGroupBox = "title"
UniqueProperty/3/QLabel = "text"
UniqueProperty/3/QWidget = "caption"
# - Unique properties used on Qt 4 and Qt 5
UniqueProperty/4/QAbstractButton = "text"
UniqueProperty/4/QAction = "text"
UniqueProperty/4/QGroupBox = "title"
UniqueProperty/4/QLabel = "text"
UniqueProperty/4/QMenu = "title"
UniqueProperty/4/QWidget = "windowTitle"
# - Unique properties used on Qt 5 only
UniqueProperty/5/QWindow = "title"
