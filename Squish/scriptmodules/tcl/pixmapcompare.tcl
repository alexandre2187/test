# Saves a screenshot of the widget {widget} in the local 
# test data directory of the current test case under
# the name {filename}

proc saveScreenshot {widget filename} {
    set pix [invoke grabWidget $widget]
    invoke $pix save $filename "BMP"
    testData create local $filename
    testData get $filename
    # workaround for testData get bug saving the file in the test case's dir and not in testdata
    file rename -force $filename "testdata/$filename"
}

# Compares a screenshot of the widget {widget} with the
# pixmap {filename} saved in the local test data directory 
# of the current test case. The result of the comparison
# is added to the current test result set

proc compareScreenshot {widget filename} {
    set pix [invoke grabWidget $widget]
    invoke $pix save "_squish_tmp.bmp" "BMP"
    testData put $filename
    set img [construct QImage $filename]
    set img2 [construct QImage "_squish_tmp.bmp"]
    set ret [test compare $img2 $img]
    testData delete "_squish_tmp.bmp"
    return $ret
}
