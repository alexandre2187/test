proc squishIntern_dumpObjectTree {withProperty obj level} {
    if {[isNull $obj]} {
        return
    }
    if {![invoke $obj isWidgetType]} {
	return
    }
    set buf [string repeat " " [expr $level * 4]]
    set name [objectName $obj]
    if {[string length $name] == 0} {
	return
    }
    set className [invoke $obj className]
    set buf "$buf * OBJECT:'$name': (Type: $className"
    set caption [toString [invoke [cast $obj "QWidget"] caption]]
    set buf "$buf, Caption: '$caption')"
    puts $buf
    
    if {$withProperty} {
	set mo [invoke $obj metaObject]
	set names [invoke $mo propertyNames true]
	if {[invoke $names count] > 0} {
	    set buf [string repeat " " [expr ($level+1) * 4]]
	    set sl [invoke QStringList fromStrList $names]
	    set names [toString [invoke $sl join "\n             $buf"]]
	    set buf "$buf Properties: $names"
	    puts $buf
	}
    }

    set children [invoke $obj children]
    if {![isNull $children]} {
        set num [invoke $children count]
        incr num -1
        while {$num >= 0} {
            set o [invoke $children at $num]
            squishIntern_dumpObjectTree $withProperty $o [expr $level + 1]
            incr num -1
        }
    }
}

# Prints the QObject hierarchy to STDOUT. If withProperty is 
# true, the properties of the objects  are printed in a list
# as well. If object is a reference to a QObject, the hierarchy
# of this object is printed. If it is 0, the hierarchy of all
# toplevel widgets is printed.
#
# Example:
#
# proc main {} {
#     # include the file
#     source [findFile scripts "tcl/qobjectinspect.tcl"]
# 
#     ... (some code)
#
#     dumpObjectTree [findObject "main window"] true
# }


proc dumpObjectTree {object withProperty} {
    if {$object == 0} {
	set ol [invoke QObject objectTrees]
	set num [invoke $ol count]
        while {$num >= 0} {
	    squishIntern_dumpObjectTree $withProperty [invoke $ol at $num] 0
            incr num -1
	}
    } else {
	squishIntern_dumpObjectTree $withProperty $object 0
    }
}
