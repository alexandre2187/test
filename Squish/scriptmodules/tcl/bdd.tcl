# Copyright (C) 2016 froglogic GmbH.
# All rights reserved.
#
# This file is part of Squish.
#
# Licensees holding a valid Squish License Agreement may use this
# file in accordance with the Squish License Agreement provided with
# the Software.
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#
# See the LICENSE file in the toplevel directory of this package.
#
# Contact contact@froglogic.com if any conditions of this licensing are
# not clear to you.

namespace eval Squish {
namespace eval BDD {
set step_functions {{}}
set context_user_data ""

proc get_matching_step_defs {step_text keyword} {
    set result {}

    variable step_functions
    foreach step [join $step_functions] {
        foreach {pattern _ _ _ callsite _ kw} $step break
        if {$kw != "Step" && $kw != $keyword} {
            continue
        }

        if {[regexp $pattern $step_text]} {
            lappend result $callsite
        }
    }

    return $result
}

proc get_registered_step_patterns {keyword} {
    set result {}

    foreach step [join $Squish::BDD::step_functions] {
        foreach {_ _ _ pattern _ mode kw} $step break
        if {$kw != "Step" && $kw != $keyword} {
            continue
        }

        if {$mode == "-simple"} {
            lappend result $pattern
        }
    }

    return $result
}

proc get_execute_step_cmd {step_text multi_text table keyword} {
    variable step_functions
    variable matched_invokeCommand
    variable matched_args
    variable matched_callsite
    variable num_matched_steps 0
    variable visiblePatterns_matched_steps {}
    foreach step [join $step_functions] {
        foreach {pattern invokeCommand argCount visiblePattern callsite _ kw} $step break
        if {$kw != $keyword && $kw != "Step"} {
            continue
        }

        set args [regexp -inline $pattern $step_text]
        set numMatches [llength $args]
        if {$numMatches == 0 } {
            continue
        }

        if {$argCount != [llength $args]} {
            return -code error -errorcode 1 "step with pattern '$visiblePattern' matched, but it expected $argCount arguments ([llength $args] given)"
        }

        if {[lsearch -exact "$visiblePatterns_matched_steps" "$visiblePattern"] != -1} {
            continue
        }
        lappend visiblePatterns_matched_steps "$visiblePattern"

        if {$num_matched_steps == 0} {
            set matched_invokeCommand "$invokeCommand"
            set matched_args "$args"
            set matched_callsite "$callsite"
        }
        incr num_matched_steps
    }

    if {$num_matched_steps == 0} {
        return -code error -errorcode 3
    }
    if {$num_matched_steps > 1} {
        return -code error -errorcode 4 "$num_matched_steps steps match the step text"
    }

    # Replace initial 'complete' match (which we don't need, we only
    # need the grouped sub-matches comprising the individual step
    # arguments) with a 'context' value granting access to optional
    # table and multi-line step arguments.
    proc context {cmd args} "
        switch \$cmd {
            table {
                if {\[llength \$args\] > 0} {
                    error \"table field of context value cannot be written to\"
                }
                return {$table}
            }
            multiLineText {
                if {\[llength \$args\] > 0} {
                    error \"table field of multiLineText value cannot be written to\"
                }
                return {$multi_text}
            }
            userData {
                if {\[llength \$args\] > 0} {
                    set Squish::BDD::context_user_data \[join \$args\]
                }
                return \$Squish::BDD::context_user_data
            }
            default {
                error \"unknown context field '\$cmd'\"
            }
        }
    "
    set matched_args [lreplace $matched_args 0 0 context]

    return [list $matched_callsite [linsert $matched_args 0 $matched_invokeCommand]]
}

proc recursiveglob {prefix pattern} {
    set result [glob -nocomplain -directory $prefix -type f $pattern]
    foreach subdir [glob -nocomplain -directory $prefix -type d *] {
      set result [concat $result [recursiveglob $subdir $pattern]]
    }
    return $result
}

proc setupHooks {args} {
    foreach what {Feature Scenario Step} {
        foreach when {Start End} {
            set Squish::BDD::On${what}${when}Hooks {}
        }
    }

    foreach f $args {
        if { [file exists $f] } {
            source $f
        }
    }
}

proc collectStepDefinitions {args} {
    set Squish::BDD::step_functions {}

    foreach dir $args {
        lappend Squish::BDD::step_functions {}
        foreach f [lsort [recursiveglob $dir *.tcl]] {
            source $f
        }
    }
}

proc re_escape {s} {
    return [regsub -all {([.*+?|{}()[\]^$\\])} $s {\\\1}]
}

set known_placeholders {
    {|word|}   {(\w+)}
    {|integer|} {([-+]?\d+)}
    {|any|}    {(.+)}
}

for {set i 0} {$i < [llength $known_placeholders]} {incr i 2} {
    lset known_placeholders $i [re_escape [lindex $known_placeholders $i]]
}

proc register_step {func_name args} {
    switch [llength $args] {
        3 {
            foreach {pattern params body} $args break
            set mode "-simple"
        }
        4 {
            foreach {mode pattern params body} $args break
        }
        default {
            error {wrong # args: should be "$func_name ?mode? pattern args body"}
        }
    }

    switch -- $mode {
        -rx {
            set effectivePattern $pattern
        }
        -simple {
            set pattern [Squish::BDD::re_escape $pattern]

            # Detect unknown placeholders; the strategy is to replace all
            # known placeholders with empty strings and then see whether
            # there is anything left which is enclosed in pipes.
            set m $Squish::BDD::known_placeholders
            for {set i 1} {$i < [llength $m]} {incr i 2} {
                lset m $i ""
            }
            if {[string match "*|*|*" [string map $m $pattern]]} {
                error {unknown placeholder in pattern}
            }

            set effectivePattern "^[string map $Squish::BDD::known_placeholders $pattern]$"
        }
        default {
            error {mode argument must be "-rx" or "-simple"}
        }
    }

    if {$effectivePattern == ""} {
        return -code error "step pattern must not be empty"
    }

    if {$params == {}} {
        return -code error "step parameters must at least include 'context' argument"
    }

    if {[catch {regexp $effectivePattern ""} msg]} {
        if {$mode == "-simple"} {
            return -code error "failed to convert $pattern to regular expression $effectivePattern: $msg"
        } else {
            return -code error "invalid pattern specified: $msg"
        }
    }

    variable step_functions
    set lastBucket [lindex $step_functions end]
    foreach step $lastBucket {
        foreach {p _ _ vp location _ keyword} $step break
        if {$p == $effectivePattern && $func_name == $keyword} {
            foreach {fileName lineNo} $location break
            return -code error "pattern '$pattern' duplicates previously registered pattern '$vp' (defined at $fileName:$lineNo)"
        }
    }

    set invokeCommand ::Squish::BDD::invoke_step_[llength [join $step_functions]]

    proc $invokeCommand $params $body

    foreach {callerFrame} [test stackTrace 2 2] break

    lappend lastBucket [list \
        $effectivePattern \
        $invokeCommand \
        [llength $params] \
        $pattern \
        $callerFrame \
        $mode \
        $func_name]
    lset Squish::BDD::step_functions end $lastBucket
}

}
}

# ----------------------------------------------------------------------------

proc Step args {
    if {[catch {eval Squish::BDD::register_step "Step" $args} errMsg]} {
        return -code error $errMsg
    }
}

proc Given args {
    if {[catch {eval Squish::BDD::register_step "Given" $args} errMsg]} {
        return -code error $errMsg
    }
}

proc When args {
    if {[catch {eval Squish::BDD::register_step "When" $args} errMsg]} {
        return -code error $errMsg
    }
}

proc Then args {
    if {[catch {eval Squish::BDD::register_step "Then" $args} errMsg]} {
        return -code error $errMsg
    }
}

foreach what {Feature Scenario Step} {
    foreach when {Start End} {
        set hookList Squish::BDD::On${what}${when}Hooks
        set $hookList {}
        proc On${what}${when} {args body} "
            set procName Squish::BDD::On${what}${when}Hook_\[llength \$$hookList\]
            proc \$procName \$args \$body
            lappend $hookList \$procName
        "
    }
}
unset when what hookList

