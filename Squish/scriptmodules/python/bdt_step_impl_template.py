# -*- coding: utf-8 -*-

# A quick introduction to implementing scripts for BDD tests:
#
# This file contains snippets of script code to be executed as the .feature
# file is processed. See the section 'Behavior Driven Testing' in the 'API
# Reference Manual' chapter of the Squish manual for a comprehensive reference:
#
#   https://doc.froglogic.com/squish/latest/api.bdt.functions.html
#
# The decorators Given/When/Then/Step can be used to associate a script snippet
# with a pattern which is matched against the steps being executed. Optional
# table/multi-line string arguments of the step are passed via a mandatory
# 'context' parameter:
#
#   @When("I enter the text")
#   def whenTextEntered(context):
#       <code here>
#
# The pattern is a plain string without the leading keyword ("Given",
# "When", "Then", etc.).
#
# Inside of the pattern the following placeholders are supported:
#
#   |any|
#   |word|
#   |integer|
#
# These placeholders can be used to extract arbitrary, alphanumeric and
# integer values resp. from the pattern; the extracted values are passed
# as additional arguments to the step implementation function:
#
#   @Then("I get |integer| different names")
#   def namesReceived(context, numNames):
#       <code here>
#
# Instead of using a string with placeholders, a regular expression can be
# specified. In that case, make sure to set the (optional) 'regexp' argument
# to True:
#
#   @Then("I get (\d+) different names", regexp=True)
#   def namesReceived(context, numNames):
#       <code here>
