import os

# Saves a screenshot of the widget {widget} in the local 
# test data directory of the current test case under
# the name {filename}

def saveScreenshot(widget, filename):
    pixmap = grabWidget(widget)
    pixmap.save(filename, "BMP")
    testData.create("local", filename)
    testData.get(filename)
    os.rename(filename, "testdata/" + filename)

# Compares a screenshot of the widget {widget} with the
# pixmap {filename} saved in the local test data directory 
# of the current test case. The result of the comparison
# is added to the current test result set

def compareScreenshot(widget, filename):
    tempImageFile = "_squish_tmp.bmp"
    pixmap = grabWidget(widget)
    pixmap.save(tempImageFile, "BMP")
    testData.put(filename)
    image1 = QImage(filename)
    image2 = QImage(tempImageFile)
    same = test.compare(image1, image2)
    testData.delete(tempImageFile)
    return same
