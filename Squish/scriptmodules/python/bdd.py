# -*- coding: utf-8 -*-
# Copyright (C) 2016 froglogic GmbH.
# All rights reserved.
#
# This file is part of Squish.
#
# Licensees holding a valid Squish License Agreement may use this
# file in accordance with the Squish License Agreement provided with
# the Software.
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#
# See the LICENSE file in the toplevel directory of this package.
#
# Contact contact@froglogic.com if any conditions of this licensing are
# not clear to you.

import re
import inspect
import sys
import traceback
import linecache
import os
import os.path
import squish

try:
    import __builtin__ # Python 2

    def unicodeString(s, encoding):
        if isinstance(s, unicode):
            return s
        else:
            return unicode(s, encoding)

except ImportError:
    import builtins as __builtin__ # Python 3
    basestring = str

    def cmp(a, b):
        return (a > b) - (a < b)

    def unicodeString(s, encoding):
        return s # assumption: string already unicode in Python 3

AbortScenario = '__SquishAbortScenario'

_dryRun = '--dry-run' in sys.argv
_setupStepDefinitionCache = '--setup-step-definition-cache' in sys.argv
_enableStepDefinitionCache = '--reuse-step-definition-cache' in sys.argv or _setupStepDefinitionCache

# The key to this dictionary is the placeholder name (with regular expression
# escaping applied). The value is a tuple with the regular expression for
# matching and a function for transforming the string to the desired type.
__squish_placeholders = {
    r'\|word\|': (
        r'(\w+)',
        lambda x: x,
    ),
    r'\|integer\|': (
        r'([-+]?\d+)',
        lambda x: __builtin__.int(x),
    ),
    r'\|any\|': (
        r'(.+)',
        lambda x: x,
    ),
}

class SquishBDDContext(__builtin__.object):
    _userData = None

    def __init__(self, **data):
        super.__setattr__(self, '_data', data)

    def __getattr__(self, name):
        if name == 'userData':
            return SquishBDDContext._userData
        elif name in self._data:
            return self._data[name]
        else:
            raise AttributeError("context object has no attribute '%s'" % name)

    def __setattr__(self, name, value):
        if name == 'userData':
            SquishBDDContext._userData = value
        else:
            raise AttributeError("attribute '%s' is read-only; use the userData attribute for writable data on the context object" % name)

class SquishBDDStepRegisterException(Exception):
    def __init__(self, message, filename, line):
        Exception.__init__(self, message)
        self.filename = filename
        self.line = line

# Setup a persistent cache for step registries; it's stored in the 'squish'
# module since that module is loaded once (and only once) for the entire
# interpreter whereas the 'bdd' module is loaded once for each test case - via
# the 'source' command.
if 'step_registry_cache' not in dir(squish):
    squish.step_registry_cache = {}

class SquishStepRegistry:
    def __init__(self):
        self.clear()

    def clear(self):
        self.registry = {
            'Step'  : [],
            'Given' : [],
            'When'  : [],
            'Then'  : [],
        }
        self.registry_by_pattern = {
            'Step'  : {},
            'Given' : {},
            'When'  : {},
            'Then'  : {},
        }
        self.clear_current_dirctory()

    def clear_current_dirctory(self):
        self.registry_for_current_directory = {
            'Step'  : {},
            'Given' : {},
            'When'  : {},
            'Then'  : {},
        }

    def append(self, keyword, step_entry):
        self.registry[keyword].append(step_entry)
        if not step_entry['regexp']:
            unicodePattern = unicodeString(step_entry['pattern'], encoding='utf-8')
            if unicodePattern not in self.registry_by_pattern[keyword]:
                self.registry_by_pattern[keyword][unicodePattern] = step_entry
        self.registry_for_current_directory[keyword][step_entry['pattern']] = step_entry

    def find_in_current_directory_with_pattern(self, keyword, pattern):
        return self.registry_for_current_directory[keyword].get(pattern, None)

    def registered_patterns(self):
        ret = {}
        for keyword in ('Step', 'Given', 'When', 'Then',):
            ret[keyword] = [step_entry['pattern'] for step_entry in self.registry[keyword]]
        return ret

    def __relevant_keywords(self, keyword):
        if keyword == 'All':
            return [
                'Given',
                'When',
                'Then',
                'Step',
            ]
        elif keyword == 'Step':
            raise RuntimeError('Invalid keyword "Step" used for lookup of step function')
        else:
            return [
                keyword,
                'Step',
            ]

    def __registries_to_check(self, keyword):
        return [self.registry[kw] for kw in self.__relevant_keywords(keyword)]

    def iter_matches(self, keyword, step_text):
        unicode_pattern = unicodeString(step_text, encoding='utf-8')

        for kw in self.__relevant_keywords(keyword):
            step_entry = self.registry_by_pattern[kw].get(unicode_pattern)
            if step_entry:
                yield step_entry, []

        for registry in self.__registries_to_check(keyword):
            for step_entry in registry:
                rx = step_entry['regexp']
                if rx:
                    match = rx.search(unicode_pattern)
                    if match:
                        yield step_entry, match.groups()

    def registered_simple_step_patterns(self, keyword):
        ret = []
        for registry in self.__registries_to_check(keyword):
            ret.extend([step_entry['pattern'] for step_entry in registry if 'placeholderFormat' in step_entry])
        return ret

__squish_step_registry = SquishStepRegistry()

def __squish_location_of_function(f):
    code = f.__code__
    return code.co_filename, code.co_firstlineno,

def __squish_register_step(keyword, pattern, regexp, f):
    # determine file/line info of the step definition
    f_args, f_varargs, _, _ = inspect.getargspec(f)

    if not pattern:
        filename, line = __squish_location_of_function(f)
        raise SquishBDDStepRegisterException("None or empty pattern", filename, line)

    if not _dryRun:
        step_entry = __squish_step_registry.find_in_current_directory_with_pattern(keyword, pattern)
        if step_entry:
            filename, line = __squish_location_of_function(f)
            step_filename, step_line = __squish_location_of_function(step_entry['f'])
            raise SquishBDDStepRegisterException(
                "Pattern '%s' duplicates previously registered pattern at %s:%s" % (
                    pattern,
                    step_filename,
                    step_line,
                ), filename, line
            )

    if len(f_args) < 1 and not f_varargs:
        filename, line = __squish_location_of_function(f)
        raise SquishBDDStepRegisterException("Step function lacks required context argument", filename, line)

    if ( not isinstance(pattern, basestring) ) and ( 'pattern' in dir(pattern) ):
        # Assumption: the pattern argument is a regular expression object
        # already.
        #
        # It would be nice if we could test it directly, but there does not
        # seem to be a documented way to do it (Python documentation claims
        # it should be an object of class re.RegexObject, but this class
        # does not exist in Python 2.7 on OS X at least).
        __squish_step_registry.append(keyword, {
            'f'        : f,
            'args'     : f_args,
            'varargs'  : f_varargs,
            'pattern'  : pattern.pattern,
            'regexp'   : pattern,
        })
    elif regexp:
        try:
            unicodePattern = unicodeString(pattern, encoding='utf-8')
            __squish_step_registry.append(keyword, {
                'f'        : f,
                'args'     : f_args,
                'varargs'  : f_varargs,
                'pattern'  : pattern,
                'regexp'   : re.compile(unicodePattern),
            })
        except re.error:
            e = sys.exc_info()[1]
            filename, line = __squish_location_of_function(f)
            raise SquishBDDStepRegisterException("Invalid regular expression: %s" % str(e), filename, line)
    else:
        def regexpForPlaceholder(matchobj):
            regexp, formatter = __squish_placeholders[matchobj.group(0)]
            placeholderFormat.append(formatter)
            return regexp

        placeholders = re.findall(r'\|[^|]*\|', pattern)

        # shortcut in case the pattern does not use any placeholders
        if not placeholders:
            __squish_step_registry.append(keyword, {
                'f'                 : f,
                'args'              : f_args,
                'varargs'           : f_varargs,
                'pattern'           : pattern,
                'regexp'            : None,
                'placeholderFormat' : [],
            })
            return f

        # validate pattern
        for placeholder in placeholders:
            if re.escape(placeholder) not in __squish_placeholders.keys():
                filename, line = __squish_location_of_function(f)
                raise SquishBDDStepRegisterException('Unknown placeholder %s' % placeholder, filename, line)

        placeholderFormat = []

        regexpPattern = re.escape(unicodeString(pattern, encoding='utf-8'))
        regexpPattern = re.sub(
            '(%s)' % '|'.join(map(re.escape, __squish_placeholders.keys())),
            regexpForPlaceholder,
            regexpPattern
        )
        regexpPattern = '^' + regexpPattern + '$'

        __squish_step_registry.append(keyword, {
            'f'                 : f,
            'args'              : f_args,
            'varargs'           : f_varargs,
            'pattern'           : pattern,
            'regexp'            : re.compile(regexpPattern, re.UNICODE),
            'placeholderFormat' : placeholderFormat,
        })
    return f

def __squish_matching_steps(keyword, step_text):
    matching_steps = []
    for step_entry, _ in __squish_step_registry.iter_matches(keyword, step_text):
        matching_steps.append(__squish_location_of_function(step_entry['f']))
    return matching_steps

def __squish_get_registered_step_patterns(keyword):
    return __squish_step_registry.registered_simple_step_patterns(keyword)

def __squish_execute_step(keyword, step_text, multi_text, table):

    def step_with_pattern_already_in_list(pattern, matches):
        for se, _ in matches:
            if se['pattern'] == pattern:
                return True
        return False

    matches = []
    for se, m in __squish_step_registry.iter_matches(keyword, step_text):
        if not step_with_pattern_already_in_list(se['pattern'], matches):
            matches.append( (se, m,) )

    lmatches = len(matches)
    if lmatches == 0:
        return (
            1, "Could not find matching step",
            None, None,
        )
    else:
        if lmatches > 1:
            return (
                2, '%s steps match the step text' % lmatches,
                None, None,
            )
        step_entry, extracted_args = matches[0]

        f = step_entry['f']
        f_args = step_entry['args']
        f_varargs = step_entry['varargs']

        # determine arguments for step function
        if table:
            context = SquishBDDContext(table=table)
        elif multi_text:
            context = SquishBDDContext(multiLineText=multi_text)
        else:
            context = SquishBDDContext()

        if 'placeholderFormat' in step_entry:
            extracted_args = [ pf(a) for a, pf in zip(extracted_args, step_entry['placeholderFormat']) ]

        # check number of arguments of step function
        num_args_cmp = cmp(len(f_args), len(extracted_args) + 1)
        if num_args_cmp == -1 and not f_varargs:
            wrong_number_of_arguments = True
        elif num_args_cmp == 1:
            wrong_number_of_arguments = True
        else:
            wrong_number_of_arguments = False
        if wrong_number_of_arguments:
            if f_varargs:
                argsQualifier = 'at least '
            else:
                argsQualifier = ''
            filename, line = __squish_location_of_function(step_entry['f'])
            return (
                4, "Step with pattern '%s' matched, but it expected %s%d arguments (%d given)" % (
                    step_entry['pattern'],
                    argsQualifier,
                    len(f_args),
                    len(extracted_args) + 1,
                ),
                filename, line,
            )

        # call step function and handle errors
        try:
            f_ret = f(context, *extracted_args)
        except SquishVerificationFailedException:
            filename, line = __squish_location_of_function(step_entry['f'])
            return (
                5, 'Success (Failed Verification, Abort Scenario)',
                filename, line,
            )
        except SquishTestcaseSkippedException:
            filename, line = __squish_location_of_function(step_entry['f'])
            return (
                5, 'Success (Testcase Skipped, Abort Scenario)',
                filename, line,
            )
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            message = ''.join(traceback.format_exception_only(exc_type, exc_value)).strip()
            filename, line, _, _ = traceback.extract_tb(exc_traceback)[-1]
            return (
                3, message,
                filename, line,
            )
        if f_ret == AbortScenario:
            filename, line = __squish_location_of_function(step_entry['f'])
            return (
                5, 'Success (Abort Scenario)',
                filename, line,
            )
        else:
            filename, line = __squish_location_of_function(step_entry['f'])
            return (
                0, 'Success',
                filename, line,
            )

def Step(pattern, regexp=False):
    def _step(f):
        return __squish_register_step('Step', pattern, regexp, f)
    return _step

def Given(pattern, regexp=False):
    def _step(f):
        return __squish_register_step('Given', pattern, regexp, f)
    return _step

def When(pattern, regexp=False):
    def _step(f):
        return __squish_register_step('When', pattern, regexp, f)
    return _step

def Then(pattern, regexp=False):
    def _step(f):
        return __squish_register_step('Then', pattern, regexp, f)
    return _step

def setupHooks(*hook_files):
    for hook_type in __squish_hook_registry:
        __squish_hook_registry[hook_type] = []
    for f in hook_files:
        if os.path.isfile(f):
            abspath = os.path.abspath(unicodeString(f, "utf-8"))
            code = compile(open(abspath).read(), abspath, 'exec')
            exec(code, globals())

def collectStepDefinitions(*step_dirs):
    global __squish_step_registry

    if _enableStepDefinitionCache:
        step_registry_cache_key = tuple([os.path.abspath(d) for d in step_dirs if os.path.exists(d)])

    if _setupStepDefinitionCache:
        squish.step_registry_cache.clear()
    elif _enableStepDefinitionCache:
        cached_step_registry = squish.step_registry_cache.get(step_registry_cache_key)
        if cached_step_registry:
            __squish_step_registry = cached_step_registry
            return

    # The inspect module uses linecache. So ensure that we are not working on a
    # stale cache (the files might have changed due to the user editing them).
    linecache.checkcache()

    __squish_step_registry.clear()
    for directory in step_dirs:
        __squish_step_registry.clear_current_dirctory()
        scriptFiles = []
        for root, dirs, files in os.walk(directory):
            for f in files:
                if f.endswith('.py'):
                    abspath = os.path.abspath(os.path.join(root, unicodeString(f, "utf-8")))
                    scriptFiles.append(abspath)
        for abspath in sorted(scriptFiles):
            code = compile(open(abspath).read(), abspath, 'exec')
            exec(code, globals())

    if _enableStepDefinitionCache:
        squish.step_registry_cache[step_registry_cache_key] = __squish_step_registry

__squish_hook_registry = {
    'FeatureStart'  : [],
    'FeatureEnd'    : [],
    'ScenarioStart' : [],
    'ScenarioEnd'   : [],
    'StepStart'     : [],
    'StepEnd'       : [],
}

def __squish_execute_hook(hookType, hookOrder, contextTitle):
    for f in  __squish_hook_registry[hookType + hookOrder]:
        if hookType in ('Feature', 'Scenario',):
            context = SquishBDDContext(title=contextTitle)
        elif hookType == 'Step':
            context = SquishBDDContext(text=contextTitle)
        else:
            context = SquishBDDContext()
        f(context)

def OnFeatureStart(f):
    __squish_hook_registry['FeatureStart'].append(f)
    return f

def OnFeatureEnd(f):
    __squish_hook_registry['FeatureEnd'].append(f)
    return f

def OnScenarioStart(f):
    __squish_hook_registry['ScenarioStart'].append(f)
    return f

def OnScenarioEnd(f):
    __squish_hook_registry['ScenarioEnd'].append(f)
    return f

def OnStepStart(f):
    __squish_hook_registry['StepStart'].append(f)
    return f

def OnStepEnd(f):
    __squish_hook_registry['StepEnd'].append(f)
    return f
