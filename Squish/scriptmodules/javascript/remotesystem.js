/**
 * Copyright (C) 2016 froglogic GmbH.
 * All rights reserved.
 *
 * This file is part of Squish.
 *
 * Licensees holding a valid Squish License Agreement may use this
 * file in accordance with the Squish License Agreement provided with
 * the Software.
 *
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file in the toplevel directory of this package.
 *
 * Contact contact@froglogic.com if any conditions of this licensing are
 * not clear to you.
 **/

function flatten(arr) {
  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
  }, []);
}

function RemoteSystem(host, port) {
    this.host = host;
    this.port = port;
    this.connectionHandle = -1;

    if ((host === undefined) && (port !== undefined)) {
        throw new TypeError("Host not specified");
    }

    if ((host !== undefined) && (port === undefined)) {
        this.port = 4322;
    }

    this.connectionHandle = -1;
    if ((host === undefined) && (port === undefined)) {
        this.connectionHandle = _squish_connect_to_remote_system();
    } else {
        this.connectionHandle = _squish_connect_to_remote_system(this.host, +this.port);
    }
};

RemoteSystem.prototype.execute = function (cmd, cwd, env, options) {
    if (cmd === undefined) {
        throw new TypeError("Command not specified");
    }

    if (cwd === undefined) {
        cwd = "";
    }

    if (env === undefined) {
        env = {};
    }

    if (options === undefined) {
        options = {};
    }

    cmd = [].map.call(flatten(cmd), String);

    cwd = String(cwd);

    for (var prop in env) {
        if (!env.hasOwnProperty(prop)) {
            continue;
        }
        env[prop] = String(env[prop]);
    }

    var result = _squish_remote_execute_command(this.connectionHandle, cmd, cwd, env, options);

    if ((result["errornr"] + 0) > 0) {
        throw new Error(result["errorstr"]);
    }

    delete result["errornr"]
    delete result["errorstr"]

    return result;
};

RemoteSystem.prototype.getEnvironmentVariable = function( varname ) {
    try {
        return _squish_remote_get_environment_variable( this.connectionHandle, varname )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.listFiles = function( path ) {
    try {
        return _squish_remote_get_directory_content( this.connectionHandle, path )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.stat = function( path, propertyname ) {
    try {
        return _squish_remote_get_path_property( this.connectionHandle, path, propertyname )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.exists = function( path ) {
    try {
        return _squish_remote_path_exists( this.connectionHandle, path )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.deleteFile = function( path ) {
    try {
        return _squish_remote_delete_file( this.connectionHandle, path )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.deleteDirectory = function( path, recursive ) {
    try {
        recursive = (recursive) ? true : false;
        return _squish_remote_delete_path( this.connectionHandle, path, recursive )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.createDirectory = function( path ) {
    try {
        return _squish_remote_create_path( this.connectionHandle, path, false )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.createPath = function( path ) {
    try {
        return _squish_remote_create_path( this.connectionHandle, path, true )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.rename = function( oldpath, newpath ) {
    try {
        return _squish_remote_rename_path( this.connectionHandle, oldpath, newpath )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.createTextFile = function( path, content, encoding ) {
    try {
        encoding = (typeof encoding !== 'undefined') ?  encoding : "UTF-8";

        return _squish_remote_create_file( this.connectionHandle, path, content, encoding )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.upload = function( localpath, remotepath ) {
    try {
        return _squish_remote_upload_file( this.connectionHandle, localpath, remotepath )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.download = function( remotepath, localpath ) {
    try {
        return _squish_remote_download_file( this.connectionHandle, remotepath, localpath )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};

RemoteSystem.prototype.copy = function( sourcepath, destpath ) {
    try {
        return _squish_remote_copy_path( this.connectionHandle, sourcepath, destpath )
    } catch (e) {
        throw "Remote system call failed: " + e;
    }
};
