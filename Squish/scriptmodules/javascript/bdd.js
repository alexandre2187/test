/**
 * Copyright (C) 2016 froglogic GmbH.
 * All rights reserved.
 *
 * This file is part of Squish.
 *
 * Licensees holding a valid Squish License Agreement may use this
 * file in accordance with the Squish License Agreement provided with
 * the Software.
 *
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file in the toplevel directory of this package.
 *
 * Contact contact@froglogic.com if any conditions of this licensing are
 * not clear to you.
 **/

/**
* @param {...*} arrays_of_hookfiles
*/
function setupHooks()
{
    this._hook_functions = {};
    for (var i = 0; i < arguments.length; i++) {
        var files = [].concat(arguments[i]);
        for (var j = 0; j < files.length; j++) {
            var file = files[j];

            if (!File.exists(file)) {
                continue;
            }
            source(file);
        }
    }
}

/**
* @param {...*} arrays_of_step_dirs
*/
function collectStepDefinitions()
{
    var setup_cache = OS.argv.indexOf( "--setup-step-definition-cache" ) >= 0;
    var use_cache = OS.argv.indexOf( "--reuse-step-definition-cache" ) >= 0 || setup_cache;
    if (setup_cache) {
        this.__step_cache = {}
    }

    var cache_name = "";
    var all_dirs = [];
    for (var i = 0; i < arguments.length; i++) {
        all_dirs = [].concat(arguments[i]);
    }
    for (var i = 0; i < all_dirs.length; i++) {
        var directory = all_dirs[i];
        if (!File.exists(directory)) {
            continue;
        }
        cache_name += OS.absolutePath(directory) + "\n";
    }

    if (use_cache && this.__step_cache[cache_name]) {
        this._step_functions = this.__step_cache[cache_name];
        return;
    }

    this._step_functions = [];

    for (var i = 0; i < arguments.length; i++) {
        var dirs = [].concat(arguments[i]);
        for (var j = 0; j < dirs.length; j++) {
            var directory = dirs[j];
            var files = [];

            if (!File.exists(directory)) {
                continue;
            }
            this._step_functions.push( {} );

            File.glob(directory + "/**/*.js").forEach(function(fileName) {
                files.push(fileName);
            });
            files.sort().forEach(function(fileName) {
                source(fileName);
            });
        }
    }

    if (use_cache) {
        this.__step_cache[cache_name] = this._step_functions;
    }
}

function squish_step(keyword, pattern, func)
{
    if ( pattern == undefined ) {
        throw "No pattern";
    }
    var is_string = "string" == typeof pattern;
    var is_regex = !is_string && "function" == typeof pattern.exec;
    if ( !is_string && !is_regex ) {
        throw "Registering step requires either a string or regex as first argument";
    }
    if ( "function" != typeof func ) {
        throw "Registering step requires a function as second argument";
    }
    if (undefined == this._step_functions || undefined == this._step_functions.length) {
        this._step_functions = [];
    }
    if ( 0 == this._step_functions.length ) {
        this._step_functions.push( {} );
    }
    if ( !pattern ) {
        throw("No pattern");
    }
    var str_pattern = "" + pattern;

    var step_functions = this._step_functions[this._step_functions.length-1];
    var step_functions_by_keyword = step_functions[keyword];
    if ( step_functions_by_keyword ) {
        if ( OS.argv.indexOf( "--dry-run" ) < 0 ) {
            var step = step_functions_by_keyword[str_pattern];
            if ( step ) {
                throw ("pattern '" + pattern + "' duplicates previously registered pattern at " + step.filepos.fileName + ":" + step.filepos.line);
            }
        }
    } else {
        step_functions_by_keyword = step_functions[keyword] = {};
    }

    var placeholders = [];
    var regex;
    if ("function" == typeof pattern.exec) {
        if ( !pattern.source ) {
            throw("Empty pattern");
        }
        regex = pattern;
    } else {
        var rx = "";
        var special = "-[]/{}()*+?.\\^$|";
        for ( var i = 0; i < str_pattern.length; ++i ) {
            var c = str_pattern.charAt( i );
            if ( c == '|' ) {
                if ( str_pattern.substr( i + 1, 4 ) == "any|" ) {
                    placeholders.push( String );
                    rx += "(.+)";
                    i += 4;
                } else if ( str_pattern.substr( i + 1, 5 ) == "word|" ) {
                    placeholders.push( String );
                    rx += "([\\p{L}\\p{N}_]+)";
                    i += 5;
                } else if ( str_pattern.substr( i + 1, 8 ) == "integer|" ) {
                    placeholders.push( Number );
                    rx += "(-?\\d+)";
                    i += 8;
                } else {
                    var alt = /^\S+|/.exec( str_pattern.substr( i + 1 ) );
                    if ( alt )
                        throw "Unknown placeholder |" + alt[0];
                    rx += "\\|";
                }
            } else if ( special.indexOf( c ) > -1 ) {
                rx += "\\" + c;
            } else {
                rx += c;
            }
        }
        if (rx != str_pattern) {
            regex = new RegExp("^" + rx + "$");
        }
    }
    var loc = test.stackTrace(2,1)[0];
    step_functions_by_keyword[str_pattern] = {
        regex: regex,
        subroutine: func,
        filepos: loc,
        text: str_pattern,
        placeholders: placeholders,
        steptype: keyword
    };
}

function Step( pattern, func )
{
    squish_step( "", pattern, func );
}

function Given( pattern, func )
{
    squish_step( "Given", pattern, func );
}

function When( pattern, func )
{
    squish_step( "When", pattern, func );
}

function Then( pattern, func )
{
    squish_step( "Then", pattern, func );
}

["Feature", "Scenario", "Step"].forEach(function(what) {
    ["End", "Start"].forEach(function(when) {
        if (undefined == this._hook_functions) {
            this._hook_functions = {};
        }
        this["On"+what+when] = function(func) {
            if ( !this._hook_functions["On"+what+when] ) {
                this._hook_functions["On"+what+when] = [];
            }
            this._hook_functions["On"+what+when].push( func );
        };
    })
});

AbortScenario = "__SquishAbortScenario";
