/**
 * Copyright (C) 2016 froglogic GmbH.
 * All rights reserved.
 *
 * This file is part of Squish.
 *
 * Licensees holding a valid Squish License Agreement may use this
 * file in accordance with the Squish License Agreement provided with
 * the Software.
 *
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file in the toplevel directory of this package.
 *
 * Contact contact@froglogic.com if any conditions of this licensing are
 * not clear to you.
 **/


/**
 * @constructor
 * @param {RType} window
 */
var ToplevelWindow = function(window) {
    // TODO error handling, check validity of window
    this.window = window;

    Object.defineProperty(this, 'nativeObject', {
        get: function() {
            return this.window;
        }
    });

    Object.defineProperty(this, 'geometry', {
        get: function() {
            return _squish_window_geometry_get(this.window);
        }
    });

};

/**
 * @param {String} objectName
 * @param {Number} [timeout]
 * @returns {ToplevelWindow}
 */
ToplevelWindow.byName = function(objectName, timeout) {
    if (!timeout) {
        return new ToplevelWindow(waitForObjectExists(objectName));
    }
    return new ToplevelWindow(waitForObjectExists(objectName, timeout));
};

/**
 * @param {RType} objectReference
 * @returns {ToplevelWindow}
 */
ToplevelWindow.byObject = function(objectReference) {
    return new ToplevelWindow(objectReference);
};

/**
 * @return {ToplevelWindow}
 */
ToplevelWindow.focused = function() {
    return new ToplevelWindow(_squish_window_focused());
};

ToplevelWindow.prototype.close = function() {
    _squish_window_close(this.window);
};

ToplevelWindow.prototype.maximize = function() {
    _squish_window_maximize(this.window);
};

ToplevelWindow.prototype.minimize = function() {
    _squish_window_minimize(this.window);
};

/**
 * @param {Number} x
 * @param {Number} y
 */
ToplevelWindow.prototype.moveTo = function(x, y) {
    _squish_window_moveto(this.window, x, y);
};

/**
 * @param {Number} w
 * @param {Number} h
 */
ToplevelWindow.prototype.resizeTo = function(w, h) {
    _squish_window_resizeto(this.window, w, h);
};

ToplevelWindow.prototype.restore = function() {
    _squish_window_restore(this.window);
};

ToplevelWindow.prototype.setFocus = function() {
    _squish_window_focus_set(this.window);
};

ToplevelWindow.prototype.setForeground = function() {
    _squish_window_foreground_set(this.window);
};

ToplevelWindow.prototype.setFullscreen = function() {
    _squish_window_fullscreen_set(this.window);
};
