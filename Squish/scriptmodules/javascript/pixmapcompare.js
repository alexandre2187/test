function compareScreenshot(widget, filename)
{
    var pix = grabWidget(widget);
    pix.save("_squish_tmp.bmp", "BMP");
    testData.put(filename);
    var img = new QImage(filename);
    var img2 = new QImage("_squish_tmp.bmp");
    var ret = test.compare(img2, img);
    testData.remove("_squish_tmp.bmp");
    return ret;
}

function saveScreenshot(widget, filename)
{
  var pix = grabWidget(widget);
  pix.save(filename, "BMP");
  testData.create("local",filename);
  testData.get(filename);
}

