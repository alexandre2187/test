/**
 * Copyright (C) 2016 froglogic GmbH.
 * All rights reserved.
 *
 * This file is part of Squish.
 *
 * Licensees holding a valid Squish License Agreement may use this
 * file in accordance with the Squish License Agreement provided with
 * the Software.
 *
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file in the toplevel directory of this package.
 *
 * Contact contact@froglogic.com if any conditions of this licensing are
 * not clear to you.
 **/

/**
 * @constructor
 * @param {Number} index
 */
var Screen = function(index) {
    // TODO validate index range
    this._index = index;
    var undefinedFunctionError = "Trying to call undefined function. Make sure the AUT is running.";

    Object.defineProperty(this, 'index', {
        get: function() { return this._index; }
    });

    Object.defineProperty(this, 'geometry', {
        get: function() {
            if (typeof _squish_screen_geometry_get === 'undefined')
                throw undefinedFunctionError
            return _squish_screen_geometry_get(this._index);
        }
    });

    Object.defineProperty(this, 'orientation', {
        get: function() {
            if (typeof _squish_screen_orientation_get === 'undefined')
                throw undefinedFunctionError
            return _squish_screen_orientation_get(this._index);
        },
        set: function(orientation) {
            if (typeof _squish_screen_orientation_set === 'undefined')
                throw undefinedFunctionError
            return _squish_screen_orientation_set(this._index, orientation);
        }
    });
}

Object.defineProperty(Screen, 'LandscapeOrientation', { value: 0x01 } );
Object.defineProperty(Screen, 'PortraitOrientation', { value: 0x02 } );
Object.defineProperty(Screen, 'ReverseLandscapeOrientation', { value: 0x04 } );
Object.defineProperty(Screen, 'ReversePortraitOrientation', { value: 0x08 } );

/**
 * @return {Number}
 */
Screen.count = function() {
    if (typeof _squish_screen_count_get === 'undefined')
        throw undefinedFunctionError
    return _squish_screen_count_get();
}

/**
 * @param {Number} index
 * @returns {Screen}
 */
Screen.byIndex = function(index) {
    return new Screen(index);
}

