/**********************************************************************
** Copyright  (C) 2016 froglogic GmbH.
** All rights reserved.
**
** This file is part of Squish.
**
** Licensees holding a valid Squish License Agreement may use this
** file in accordance with the Squish License Agreement provided with
** the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the LICENSE file in the toplevel directory of this package.
**
** Contact contact@froglogic.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/

SquishCmd = {};
SquishCmd.refs = {};
SquishCmd.refCtr = 0;

// Creates a ref string of the form "SquishCmd.ref[0]" for the specified object.
SquishCmd.createRef = function (obj) {
    if (typeof (obj) != "object" && typeof (obj) != "function") {
       throw "createRef: obj must be an object or a function";
    }

    newRef = SquishCmd.refCtr++;
    this.refs[newRef] = obj;
    return "SquishCmd.refs[" + newRef.toString () + "]";
}

// Accepts a string of the form "SquishCmd.refs[0]" or just the ref index.
SquishCmd.unref = function (ref) {
    refIndex = NaN;

    if (typeof (ref) == "number") {
        refIndex = ref;
    } else {
        refIndex = parseInt(ref.substr(15, ref.length - 2));
    }

    if (refIndex == NaN) {
        throw "Cannot unref invalid reference: " + ref;
    }

    delete this.refs[refIndex];
    return refIndex;
}

SquishCmd.eval = function (code) {
    result = eval (code);

    if (typeof (result) == "object" || typeof (result) == "function") {
        return "ref " + this.createRef (result);
    } else if (typeof (result) == "undefined") {
        return "void";
    } else if (typeof (result) == "number") {
        if (result.toString().indexOf (".") > -1) {
            return "float " + result;
        } else {
            return "int " + result;
        }
    } else {
        return typeof (result) + " " + result;
    }
}
