﻿/* 
    Copyright (c) 2009-10 froglogic GmbH. All rights reserved.

    This file is part of an example program for Squish. it may be used,
    distributed, and modified, without limitation.

    Note that the icons used are from KDE (www.kde.org) and subject to
    the KDE's license. 
*/
namespace AddressbookWPF
{
    // data for one row on the table-grid-panel:
    public class Record 
    {
        private string[] fields;

        // properties
        public string Forename { 
            get { return fields[0]; }
            set { fields[0] = value; }
        }
        public string Surname { 
            get { return fields[1]; }
            set { fields[1] = value; }
        }
        public string Email {
            get { return fields[2]; }
            set { fields[2] = value; }
        }
        public string Phone { 
            get { return fields[3]; }
            set { fields[3] = value; }
        }

        // Construction:
        public Record(string datas)
        { fields = datas.Split("|".ToCharArray()); }

        // operator[]
        public string this[int index]
        { get { return fields[index]; }
          set { fields[index] = value; } }

        // get as String
        public override string ToString()
        { return  fields[0] 
          + "|" + fields[1] 
          + "|" + fields[2] 
          + "|" + fields[3]; }
    }
}
