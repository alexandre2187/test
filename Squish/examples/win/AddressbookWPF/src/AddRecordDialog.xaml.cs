﻿/* 
    Copyright (c) 2009-10 froglogic GmbH. All rights reserved.

    This file is part of an example program for Squish. it may be used,
    distributed, and modified, without limitation.

    Note that the icons used are from KDE (www.kde.org) and subject to
    the KDE's license. 
*/
using System;
using System.Windows;

namespace AddressbookWPF
{
    // Dialog for adding an entry to the addressbooks addresslist
    public partial class AddRecordDialog : Window, IAddressbookDialog
    {
        public delegate void retreiverFunction(Record data);
        private retreiverFunction RetreiveData;

        // property: allFieldsFilled - get returns [true] if all textboxes got filled.
        //                             set will clear all textboxes when asigned [false].
        private bool allFieldsFilled
        {
            get
            {
                return(tbx_Forename.Text != "" 
                    && tbx_Surname.Text != ""
                    && tbx_Email.Text != "" 
                    && tbx_Phone.Text != "" );
            }
            set
            {
                if (!value)
                {
                    tbx_Forename.Text = "";
                    tbx_Surname.Text = "";
                    tbx_Email.Text = "";
                    tbx_Phone.Text = "";
                }
            }
        }

        public AddRecordDialog()
        {// constructor
            InitializeComponent();

            btn_Ok.Click     += btn_Ok_click;
            btn_Cancel.Click += btn_Cancel_Click;

            tbx_Forename.TextChanged += onTextInput;
            tbx_Surname.TextChanged  += onTextInput;
            tbx_Email.TextChanged    += onTextInput;
            tbx_Phone.TextChanged    += onTextInput;
        }
        
        void IAddressbookDialog.SetRetreiverFunction(Delegate funct)
        {// set function to be called back... 
            RetreiveData = (retreiverFunction)funct;
        }
 
        private void btn_Ok_click(object sender,EventArgs e)
        {// handle OK button clicks:
            if (allFieldsFilled && RetreiveData != null)
            {
                RetreiveData( new Record( tbx_Forename.Text
                                  + "|" + tbx_Surname.Text
                                  + "|" + tbx_Email.Text
                                  + "|" + tbx_Phone.Text )  );

                btn_Cancel_Click(sender, e);
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {// handle CANCEL button clicks:
            allFieldsFilled = false;
            this.Hide();
        }

        private void onTextInput(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {// enable the ok button if all fields are filled:
            btn_Ok.IsEnabled = allFieldsFilled;
        }
    }
}
