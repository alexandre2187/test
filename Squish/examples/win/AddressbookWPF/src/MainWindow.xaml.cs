﻿/* 
    Copyright (c) 2009-10 froglogic GmbH. All rights reserved.

    This file is part of an example program for Squish. it may be used,
    distributed, and modified, without limitation.

    Note that the icons used are from KDE (www.kde.org) and subject to
    the KDE's license. 
*/
using System;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Collections.Generic;

namespace AddressbookWPF
{
    // Addressbook's main window:
    public partial class MainWindow : Window
    {
        [Flags]
        public enum INPUT_TYPE { MOUSE = 0x01, TOUCH = 0x02 };
        private readonly INPUT_TYPE InputTypeFlags;

        public App App;

        private volatile bool shouldClose = false;
        private volatile bool shouldClear = false;
        private volatile bool isSafeToClose = true;
        private volatile bool isSafeToUpdate = false;

        private ImageSource[] buttonImages;
        private bool imagesGreyedOut = true;

        private List<Record> records;
        private ICollectionView tableView;

        private string _WindowTitle;
        public string WindowTitle
        {// string property which will generate a window title
         // containing currently opened filename and adds a '*' 
         // to it if the table would contain unsaved changes:
            get { return _WindowTitle 
                         + (App.fileDialog != null ?
                   " - " + (isSafeToClose ? "" : "*") 
                         + App.fileDialog.FileName.Substring(
                           App.fileDialog.FileName.LastIndexOf('\\') + 1 ) 
                         : " - Unnamed"); }

            set { _WindowTitle = value; }
        }


        public MainWindow()
            : this(INPUT_TYPE.MOUSE)
        {// Constructor:

        }
        public MainWindow(INPUT_TYPE inputType)
            : base()
        {
            InputTypeFlags = inputType;
            InitializeComponent();
            App = (App.Current as App);
            connectHandlerFunctions();

            // create record-list and bind it to the table-view:
            App.ShutdownMode = ShutdownMode.OnMainWindowClose;
            tableView = CollectionViewSource.GetDefaultView(new List<Record>());
            dataGridPanel.ItemsSource = tableView;
            records = tableView.SourceCollection as List<Record>;

            // load alternative BitmapImages to be used when buttons grayed out/in:
            buttonImages = new ImageSource[3];
            buttonImages[0] = new BitmapImage(new Uri("pack://application:,,,/images/editadd.png"));
            buttonImages[1] = new BitmapImage(new Uri("pack://application:,,,/images/editdelete.png"));
            buttonImages[2] = new BitmapImage(new Uri("pack://application:,,,/images/filesave.png"));

            tableView.CurrentChanging += onBeginTableViewInterAction;
            isSafeToUpdate = true;
        }


        private void connectHandlerFunctions()
        {// connect handler functions for the toolbar buttons here:
            if(InputTypeFlags.HasFlag(INPUT_TYPE.MOUSE))
            {
                this.tbn_New.Click    += mnuFile_tbnNew_Click;
                this.tbn_Open.Click   += mnuFile_tbnOpen_Click;
                this.tbn_Add.Click    += mnuEdit_tbnAdd_Click;
                this.tbn_Save.Click   += mnuFile_tbnSave_Click;
                this.tbn_Remove.Click += mnuEdit_tbnRemove_Click;

                this.img_tbn_New.MouseEnter += hoverButton;
                this.img_tbn_Open.MouseEnter += hoverButton;
                this.img_tbn_Add.MouseEnter += hoverButton;
                this.img_tbn_Save.MouseEnter += hoverButton;
                this.img_tbn_Remove.MouseEnter += hoverButton;
                this.img_tbn_New.MouseLeave += hoverButton;
                this.img_tbn_Open.MouseLeave += hoverButton;
                this.img_tbn_Add.MouseLeave += hoverButton;
                this.img_tbn_Save.MouseLeave += hoverButton;
                this.img_tbn_Remove.MouseLeave += hoverButton;
            }
            if (InputTypeFlags.HasFlag(INPUT_TYPE.TOUCH))
            {
                this.tbn_New.TouchUp    += mnuFile_tbnNew_Click;
                this.tbn_Open.TouchUp   += mnuFile_tbnOpen_Click;
                this.tbn_Add.TouchUp    += mnuEdit_tbnAdd_Click;
                this.tbn_Save.TouchUp   += mnuFile_tbnSave_Click;
                this.tbn_Remove.TouchUp += mnuEdit_tbnRemove_Click;

                this.img_tbn_New.TouchEnter += hoverButton;
                this.img_tbn_Open.TouchEnter += hoverButton;
                this.img_tbn_Add.TouchEnter += hoverButton;
                this.img_tbn_Save.TouchEnter += hoverButton;
                this.img_tbn_Remove.TouchEnter += hoverButton;
                this.img_tbn_New.TouchLeave += hoverButton;
                this.img_tbn_Open.TouchLeave += hoverButton;
                this.img_tbn_Add.TouchLeave += hoverButton;
                this.img_tbn_Save.TouchLeave += hoverButton;
                this.img_tbn_Remove.TouchLeave += hoverButton;
            }
        }


        public DialogType RegisterDialog<DialogType>() where DialogType : IAddressbookDialog, new()
        {// Instantiate the other dialog windows and set callbacks 
         // on them for receiving submitted data on "OK" press.
            Delegate func = null;
            DialogType dialog = new DialogType();

            if(typeof(DialogType) == typeof(AskForSaveOnExit))
                func = new AskForSaveOnExit.retreiverFunction(retrieveUserChoice);
            else
            if(typeof(DialogType) == typeof(AddRecordDialog))
                func = new AddRecordDialog.retreiverFunction(retrieveRecord);
            else
            if(typeof(DialogType) == typeof(AskForDelete))
                func = new AskForDelete.retreiverFunction(userWantDelete);

            dialog.SetRetreiverFunction(func);
            return dialog;
        }

        void onBeginTableViewInterAction(object sender, EventArgs e)
        {// called when user begins editing a table cell by dobbleclicking it.
         // It sets the "isSafeToUpdate" flag to false, so no other threads 
         // will access or modify the table data also (would lead to crash).
            isSafeToUpdate = false;
        }

        private void updateChanges()
        {// check if table data currently is dirty.
         // if so, try to commit any changes made.
            if(!isSafeToUpdate)
                isSafeToUpdate = dataGridPanel.CommitEdit();

         // again, check if table data still might be dirty.
         // if clean, update the table view.
            if(isSafeToUpdate)
                tableView.Refresh();

         // renews the window's title: 
            this.Title = WindowTitle;
        }

        private void SetEditMenuEnabled(bool enabled)
        {// Enable the initially disabled Edit-Menu:

            mnu_Edit.IsEnabled       =
            tbn_Add.IsEnabled        =
            img_tbn_Add.IsEnabled    =
            tbn_Save.IsEnabled       =
            img_tbn_Save.IsEnabled   =
            tbn_Remove.IsEnabled     =
            img_tbn_Remove.IsEnabled = enabled;
            
            if(enabled == imagesGreyedOut)
            {
                ImageSource tmp = img_tbn_Add.Source;
                img_tbn_Add.Source = buttonImages[0];
                buttonImages[0] = tmp;
                tmp = img_tbn_Remove.Source;
                img_tbn_Remove.Source = buttonImages[1];
                buttonImages[1] = tmp;
                tmp = img_tbn_Save.Source;
                img_tbn_Save.Source = buttonImages[2];
                buttonImages[2] = tmp;
                imagesGreyedOut = !imagesGreyedOut;
            }

            img_tbn_Remove.Opacity = 
              img_tbn_Save.Opacity = 
               img_tbn_Add.Opacity = 0.9;
            
            isSafeToClose = !enabled;
        }

        private void ClearTable()
        {// removes all entries from the address list
         // (prompts for saving the table first)  
            if(isSafeToClose)
            {
                records.Clear();
                shouldClear = false;
                SetEditMenuEnabled(true);
                App.fileDialog = null;
                isSafeToClose = true;
                updateChanges();
            }
            else
            {
                shouldClear = true;
                App.askForSaveDialog.ShowDialog();
            }
        }


        private void SaveAs()
        {// Save address list to file by calling SaveFileDialog
            string fileName = "";
            if (App.fileDialog != null) {
                if ( App.fileDialog.FileName != "" 
                && new FileInfo(App.fileDialog.FileName).Exists )
                    fileName = App.fileDialog.FileName;
            }
            if (!(App.fileDialog is SaveFileDialog))
                App.fileDialog = new SaveFileDialog();

         // prepare the Save Dialog for showing up
            App.fileDialog.InitialDirectory = fileName;
            App.fileDialog.Title = "Save Addresslist";
            App.fileDialog.Tag = App.fileDialog.Title;
            App.fileDialog.FileOk += fileDialog_FileSave;
            App.fileDialog.ShowDialog();
        }
        private void fileDialog_FileSave(object sender, CancelEventArgs e)
        {// saves file by that name the SaveFileDialog returns.
            (sender as FileDialog).FileOk -= fileDialog_FileSave;
            SaveTable((sender as FileDialog).FileName);
        }
        private void SaveTable(string filename)
        {// Save address list to file by giving filename
            if(filename != "")
            {
                TextWriter text = File.CreateText(filename);
                foreach (var rec in dataGridPanel.Items)
                    text.WriteLine(rec);
                text.Flush(); text.Close();
                isSafeToClose = true;
                if(shouldClose)
                    App.Shutdown();
                if(shouldClear)
                    ClearTable();
            }
            else// if given filename is empty, show a SaveFileDialog.
                SaveAs();
        }


        private void OpenTable()
        {// Calls an OpenFileDialog for loading an address list file
            App.fileDialog = new OpenFileDialog();
            App.fileDialog.FileOk += fileDialog_FileOpen;
            App.fileDialog.Title = "Open Addresslist";
            App.fileDialog.Tag = App.fileDialog.Title;
            App.fileDialog.ShowDialog();
        }
        private void fileDialog_FileOpen(object sender, CancelEventArgs a)
        {// Load the file that's name was returned by the OpenFileDialog..
            (sender as FileDialog).FileOk -= fileDialog_FileOpen;
            LoadTable((sender as FileDialog).FileName);
        }
        private void LoadTable(string filename)
        {// Loads an address list from an .adr file
            if (new FileInfo(filename).Exists)
            {
                string line;
                records.Clear();
                TextReader text = File.OpenText(filename);
                while((line=text.ReadLine())!=null)
                    records.Add(new Record(line));
                SetEditMenuEnabled(true);
                isSafeToClose = true;
                updateChanges();
                text.Close();
            }
        }


        private void RemoveSelectedRecords()
        {// first it checks if some tablecells might be edited by
         // the user currently. If so, it will cancel the editing
         // operation before it will invoke a dialog which will 
         // request the user for permission to delete the curently
         // selected table entries.
            dataGridPanel.BeginEdit();
            dataGridPanel.CellEditEnding += onFinishedEditingTable;
            dataGridPanel.CancelEdit();
        }
        void onFinishedEditingTable(object sender, DataGridCellEditEndingEventArgs e)
        {// called when Editing is disabled and deleting the selection would be safe.
            dataGridPanel.CellEditEnding -= onFinishedEditingTable;
            object currentSelectedItem = dataGridPanel.SelectedItem;
            if (currentSelectedItem == null)
                currentSelectedItem = dataGridPanel.CurrentCell.Item;
            if (currentSelectedItem != null) {
                dataGridPanel.SelectedItem = currentSelectedItem;
                App.askForDeleteDialog.requestAnswer( dataGridPanel.SelectedItem as Record );
            }
        }
        void userWantDelete(bool shouldDelete)
        {// reteives answer from user if sellection should be deleted.
         // if answer was "Yes", the selected entries will be deleted.
            if(shouldDelete)
                deleteSelectedEntries();
        }
        private void deleteSelectedEntries()
        {// removes the currently selected record-items from the list.
            if (dataGridPanel.HasItems)
            {
                int remove = -1;
                int[] removals = new int[dataGridPanel.SelectedItems.Count];
                foreach (Record r in dataGridPanel.SelectedItems)
                    removals[++remove] = dataGridPanel.Items.IndexOf(r);
                for(; remove >= 0; remove--)
                    records.RemoveAt(removals[remove]);
                isSafeToClose = false;
                updateChanges();
            }
        }


        void retrieveRecord(Record newData)
        {// stores the data record returned by the AddRecordDialog.
            if (dataGridPanel.HasItems && dataGridPanel.SelectedItem != null)
                records.Insert(dataGridPanel.Items.IndexOf(dataGridPanel.SelectedItem),
                                    newData ); 
            else
                records.Add(newData);
         // set a flag that means that tabledata needs to be saved
         // when not wanted to loose any changes made when closing 
            isSafeToClose = false;
        }

        void retrieveUserChoice(bool? saveAddressTable)
        {// get answer from user if table should be saved.
            if(saveAddressTable == null)
                shouldClose = shouldClear = false;
            else if(saveAddressTable.Value)
                SaveAs();
            else if(shouldClose)
                App.Shutdown();
            else if(shouldClear)
            { isSafeToClose = true; ClearTable(); }
        }


    /////////////////////////////////////////////////////////////////////////
    /// handler functions connected to the GUI:
    ///

        private void mnuFile_tbnNew_Click(object sender, RoutedEventArgs e)
        {
            ClearTable();
        }

        private void mnuEdit_tbnRemove_Click(object sender, RoutedEventArgs e)
        {
            RemoveSelectedRecords();
        }

        private void mnuEdit_tbnAdd_Click(object sender, RoutedEventArgs e)
        {
            App.addRecordDialog.ShowDialog();
            updateChanges();
        }

        private void mnuFile_tbnOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenTable();
        }

        private void mnuFile_tbnSave_Click(object sender, RoutedEventArgs e)
        {
            if (App.fileDialog != null)
                SaveTable(App.fileDialog.FileName);
            else
                SaveAs();
        }
        
        private void mnuFile_SaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveAs();
        }

        private void mnu_Quit_Click(object sender, RoutedEventArgs e)
        {
            if(isSafeToClose)
                App.Shutdown();
            else
            {
                shouldClose = true;
                App.askForSaveDialog.ShowDialog();
            }
        }

        private void hoverButton(object sender, EventArgs e)
        {// makes the toolbar buttons wobbeling when toolbar is hovered...

            if ((!InputTypeFlags.HasFlag(INPUT_TYPE.TOUCH))
                && e is System.Windows.Input.TouchEventArgs)
                return;
            if ((!InputTypeFlags.HasFlag(INPUT_TYPE.MOUSE))
                && e is System.Windows.Input.MouseEventArgs)
                return;

            Image buttonImage = sender as Image;
            if(buttonImage.IsEnabled) {
                int v = 35-(int)buttonImage.Width;
                buttonImage.Opacity = v<0? 0.9:1.0;
                buttonImage.Height = buttonImage.Width = 35+v;
                buttonImage.Margin = new Thickness( buttonImage.Margin.Left-v,
                                                    buttonImage.Margin.Top,
                                                    buttonImage.Margin.Right-v,
                                                    buttonImage.Margin.Bottom );
            } else buttonImage.Opacity = 0.2;
        }   
    }
}
