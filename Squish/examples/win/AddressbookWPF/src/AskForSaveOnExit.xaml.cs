﻿/* 
    Copyright (c) 2009-10 froglogic GmbH. All rights reserved.

    This file is part of an example program for Squish. it may be used,
    distributed, and modified, without limitation.

    Note that the icons used are from KDE (www.kde.org) and subject to
    the KDE's license. 
*/
using System;
using System.Windows;

namespace AddressbookWPF
{
    // Dialog that will ask for saving the addresslist when addressbook
    // closes or when the addresslist is about to be cleared. 
    public partial class AskForSaveOnExit : Window, IAddressbookDialog
    {
        public delegate void retreiverFunction(bool? usersAnswer);
        private retreiverFunction UserHasChoosen;

        public AskForSaveOnExit()
        {// constructor:
            InitializeComponent();
        }

        void IAddressbookDialog.SetRetreiverFunction(Delegate funct)
        {
            UserHasChoosen = (retreiverFunction)funct;
        }

        private void ReturnChoice(object sender, RoutedEventArgs e)
        {// handle button clicks:
            if(UserHasChoosen != null)
            {
            UIElement btn = sender as UIElement;

                 if(btn == btn_Discard)
                     UserHasChoosen(false);
            else if(btn == btn_Save)
                     UserHasChoosen(true);
            else if(btn == btn_Cancel)
                     UserHasChoosen(null);
            }
            this.Hide();
        }
    }
}
