﻿/* 
    Copyright (c) 2009-10 froglogic GmbH. All rights reserved.

    This file is part of an example program for Squish. it may be used,
    distributed, and modified, without limitation.

    Note that the icons used are from KDE (www.kde.org) and subject to
    the KDE's license. 
*/
using System;

namespace AddressbookWPF
{
    // Interface for dialog windows which will collect 
    // inputs from the user, submitting it to the addressbook.
    public interface IAddressbookDialog 
    {
        void SetRetreiverFunction(Delegate funct);
    }
}
