﻿/*
    Copyright (c) 2009-10 froglogic GmbH. All rights reserved.

    This file is part of an example program for Squish. it may be used,
    distributed, and modified, without limitation.

    Note that the icons used are from KDE (www.kde.org) and subject to
    the KDE's license.
*/
using System;
using System.Windows;
using Microsoft.Win32;
using System.Collections.Generic;
using INPUT_TYPE = AddressbookWPF.MainWindow.INPUT_TYPE;


namespace AddressbookWPF
{
    public partial class App : Application
    {
        internal FileDialog fileDialog;
        internal AskForSaveOnExit askForSaveDialog;
        internal AddRecordDialog addRecordDialog;
        internal AskForDelete askForDeleteDialog;

        public void Application_Startup(object sender,StartupEventArgs e)
        {
            List<string> argv = new List<string>(e.Args);

            INPUT_TYPE input = INPUT_TYPE.MOUSE;
            string msg = "Starting AddressbookWPF";

            if (argv.Contains("--touch-input")){
                input |= INPUT_TYPE.TOUCH;
                msg += " ...with TouchInput enabled!";
            }
            if (argv.Contains("--no-mouse-input")){
                input ^= INPUT_TYPE.MOUSE;
                msg += " ...MouseInput disabled!";
            }
            if (argv.Contains("-h")||
                argv.Contains("/?")||
                argv.Contains("--help")) {
                    msg += "\nSupported commandline parameter are:"
                        +  "\n  [--touch-input]"
                        +  "\n  [--no-mouse-input]";
            }

            if (argv.Count > 0)
            {
                Console.Out.Write(Console.Out.NewLine);
                Console.Out.Write(msg);
                Console.Out.Write(Console.Out.NewLine);
            }

            MainWindow         = new MainWindow(input);
            MainWindow.Title   = (MainWindow as MainWindow).WindowTitle = "Address Book";
            askForSaveDialog   = (MainWindow as MainWindow).RegisterDialog<AskForSaveOnExit>();
            addRecordDialog    = (MainWindow as MainWindow).RegisterDialog<AddRecordDialog>();
            askForDeleteDialog = (MainWindow as MainWindow).RegisterDialog<AskForDelete>();

            MainWindow.Show();
        }
    }
}
