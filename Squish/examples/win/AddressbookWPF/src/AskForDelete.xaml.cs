﻿/* 
    Copyright (c) 2009-10 froglogic GmbH. All rights reserved.

    This file is part of an example program for Squish. it may be used,
    distributed, and modified, without limitation.

    Note that the icons used are from KDE (www.kde.org) and subject to
    the KDE's license. 
*/
using System;
using System.Windows;

namespace AddressbookWPF
{
    partial class AskForDelete : Window, IAddressbookDialog
    {
        public delegate void retreiverFunction(bool usersAnswer);
        private retreiverFunction UserHasChoosen;

        public AskForDelete()
        {// constructor:
            InitializeComponent();
            Yes.Click += ReturnChoice;
            No.Click += ReturnChoice;
        }

        void IAddressbookDialog.SetRetreiverFunction(Delegate funct)
        {
            UserHasChoosen = (retreiverFunction)funct;
        }

        private void ReturnChoice(object sender, RoutedEventArgs e)
        {// handle button clicks:
            if( UserHasChoosen!=null )
                UserHasChoosen(sender as UIElement == Yes);
            this.Hide();
        }
        
        public void requestAnswer(Record willBeDeleted)
        {// sets text to display and then shows the dialog:
            this.dlg_msg.Content = "Really want to delete:"
                                 + "             \n       "
                                 + willBeDeleted.Forename + 
                                 " " +willBeDeleted.Surname
                                 + "\n     from the list ?";
            this.Show();
        }
    }
}
