# -*- coding: utf-8 -*-

import names

# A quick introduction to implementing scripts for BDD tests:
#
# This file contains snippets of script code to be executed as the .feature
# file is processed. See the section 'Behaviour Driven Testing' in the 'API
# Reference Manual' chapter of the Squish manual for a comprehensive reference.
#
# The decorators Given/When/Then/Step can be used to associate a script snippet
# with a pattern which is matched against the steps being executed. Optional
# table/multi-line string arguments of the step are passed via a mandatory
# 'context' parameter:
#
#   @When("I enter the text")
#   def whenTextEntered(context):
#      <code here>
#
# The pattern is a plain string without the leading keyword, but a couple of
# placeholders including |any|, |word| and |integer| are supported which can be
# used to extract arbitrary, alphanumeric and integer values resp. from the
# pattern; the extracted values are passed as additional arguments:
#
#   @Then("I get |integer| different names")
#   def namesReceived(context, numNames):
#      <code here>
#
# Instead of using a string with placeholders, a regular expression can be
# specified. In that case, make sure to set the (optional) 'regexp' argument
# to True.

import __builtin__



@Given("addressbook application is running")
def step(context):
    startApplication("AddressbookWPF")

@When("I create a new addressbook")
def step(context):
    mouseClick(waitForObject(names.file_MenuItem))
    mouseClick(waitForObject(names.file_New_MenuItem))
    test.compare(waitForObjectExists(names.address_Book_Unnamed_Table).rowCount, 0)

@Then("addressbook should have zero entries")
def step(context):
    test.compare(waitForObjectExists(names.address_Book_Unnamed_Table).rowCount, 0)


@Then("'1' entries should be present")
def step(context):
    test.compare(waitForObjectExists(names.address_Book_Unnamed_Table).rowCount, 1)


@When("I add new persons to address book")
def step(context):
    mouseClick(waitForObject(names.edit_MenuItem))
    mouseClick(waitForObject(names.edit_Add_MenuItem))
    mouseClick(waitForObject(names.address_Book_Add_Forename_Edit), 32, 14, MouseButton.PrimaryButton)
    type(waitForObject(names.address_Book_Add_Forename_Edit), "John")
    type(waitForObject(names.address_Book_Add_Forename_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Surname_Edit), "Smith")
    type(waitForObject(names.address_Book_Add_Surname_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Email_Edit), "john")
    type(waitForObject(names.address_Book_Add_Email_Edit), "<Ctrl+Alt+Q>")
    type(waitForObject(names.address_Book_Add_Email_Edit), "m.com")
    type(waitForObject(names.address_Book_Add_Email_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Phone_Edit), "123123123")
    type(waitForObject(names.address_Book_Add_Phone_Edit), "<Return>")
    mouseClick(waitForObject(names.edit_MenuItem))
    mouseClick(waitForObject(names.edit_Add_MenuItem))
    mouseClick(waitForObject(names.address_Book_Add_Forename_Edit), 89, 17, MouseButton.PrimaryButton)
    type(waitForObject(names.address_Book_Add_Forename_Edit), "Alice")
    type(waitForObject(names.address_Book_Add_Forename_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Surname_Edit), "Thompson")
    type(waitForObject(names.address_Book_Add_Surname_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Email_Edit), "alice")
    type(waitForObject(names.address_Book_Add_Email_Edit), "<Ctrl+Alt+Q>")
    type(waitForObject(names.address_Book_Add_Email_Edit), "m.com")
    type(waitForObject(names.address_Book_Add_Email_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Phone_Edit), "321321321")
    type(waitForObject(names.address_Book_Add_Phone_Edit), "<Return>")

@Then("'2' entries should be present")
def step(context):
    test.compare(waitForObjectExists(names.address_Book_Unnamed_Table).rowCount, 2)


@When("I add a new person '|any|','|any|','|any|','|integer|' to address book")
def step(context,forename,lastname,email,phone):
    mouseClick(waitForObject(names.edit_MenuItem))
    mouseClick(waitForObject(names.edit_Add_MenuItem))
    mouseClick(waitForObject(names.address_Book_Add_Forename_Edit), 25, 2, MouseButton.PrimaryButton)
    type(waitForObject(names.address_Book_Add_Forename_Edit), forename)
    type(waitForObject(names.address_Book_Add_Forename_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Surname_Edit), lastname)
    type(waitForObject(names.address_Book_Add_Surname_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Email_Edit), email)
    type(waitForObject(names.address_Book_Add_Email_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Phone_Edit), phone)
    type(waitForObject(names.address_Book_Add_Phone_Edit), "<Return>")


