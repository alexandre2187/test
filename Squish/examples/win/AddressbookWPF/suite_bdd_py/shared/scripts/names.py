# encoding: UTF-8

from objectmaphelper import *

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

address_Book_Add_Window = {"text": "Address Book - Add", "type": "Window"}
address_Book_Add_Email_Edit = {"container": address_Book_Add_Window, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Email:' type='Label'}", "type": "Edit"}
add_Record_Window = {"text": "Add Record", "type": "Window"}
add_Record_Email_Label = {"container": add_Record_Window, "text": "Email:", "type": "Label"}
address_Book_Add_Forename_Edit = {"container": address_Book_Add_Window, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Forename:' type='Label'}", "type": "Edit"}
add_Record_Forename_Label = {"container": add_Record_Window, "text": "Forename:", "type": "Label"}
add_Record_OK_Button = {"container": add_Record_Window, "text": "OK", "type": "Button"}
address_Book_Add_Phone_Edit = {"container": address_Book_Add_Window, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Phone:' type='Label'}", "type": "Edit"}
add_Record_Phone_Label = {"container": add_Record_Window, "text": "Phone:", "type": "Label"}
address_Book_Add_Surname_Edit = {"container": address_Book_Add_Window, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Surname:' type='Label'}", "type": "Edit"}
add_Record_Surname_Label = {"container": add_Record_Window, "text": "Surname:", "type": "Label"}
address_Book_Unnamed_Window = {"text": "Address Book - Unnamed", "type": "Window"}
address_Book_Unnamed_MenuBar = {"container": address_Book_Unnamed_Window, "type": "MenuBar"}
address_Book_Unnamed_Table = {"container": address_Book_Unnamed_Window, "type": "Table"}
address_Book_Window = {"text": "Address Book", "type": "Window"}
address_Book_MenuBar = {"container": address_Book_Window, "type": "MenuBar"}
address_Book_Table = {"container": address_Book_Window, "type": "Table"}
edit_MenuItem = {"container": address_Book_Unnamed_MenuBar, "text": "Edit", "type": "MenuItem"}
edit_Add_MenuItem = {"container": edit_MenuItem, "text": "Add...", "type": "MenuItem"}
file_MenuItem = {"container": address_Book_MenuBar, "text": "File", "type": "MenuItem"}
file_New_MenuItem = {"container": file_MenuItem, "text": "New", "type": "MenuItem"}
