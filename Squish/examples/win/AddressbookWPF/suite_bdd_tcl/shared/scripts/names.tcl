package require squish::objectmaphelper
# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

namespace eval ::names {
set Address_Book_Add_Window [::Squish::ObjectName text {Address Book - Add} type Window]
set Address_Book_Add_Email_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject {\{container=\{text='Address Book - Add' type='Window'\} text='Email:' type='Label'\}} type Edit]
set Add_Record_Window [::Squish::ObjectName text {Add Record} type Window]
set Add_Record_Email_Label [::Squish::ObjectName container $Add_Record_Window text Email: type Label]
set Address_Book_Add_Forename_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject {\{container=\{text='Address Book - Add' type='Window'\} text='Forename:' type='Label'\}} type Edit]
set Add_Record_Forename_Label [::Squish::ObjectName container $Add_Record_Window text Forename: type Label]
set Add_Record_OK_Button [::Squish::ObjectName container $Add_Record_Window text OK type Button]
set Address_Book_Add_Phone_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject {\{container=\{text='Address Book - Add' type='Window'\} text='Phone:' type='Label'\}} type Edit]
set Add_Record_Phone_Label [::Squish::ObjectName container $Add_Record_Window text Phone: type Label]
set Address_Book_Add_Surname_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject {\{container=\{text='Address Book - Add' type='Window'\} text='Surname:' type='Label'\}} type Edit]
set Add_Record_Surname_Label [::Squish::ObjectName container $Add_Record_Window text Surname: type Label]
set Address_Book_Unnamed_Window [::Squish::ObjectName text {Address Book - Unnamed} type Window]
set Address_Book_Unnamed_MenuBar [::Squish::ObjectName container $Address_Book_Unnamed_Window type MenuBar]
set Address_Book_Unnamed_Table [::Squish::ObjectName container $Address_Book_Unnamed_Window type Table]
set Address_Book_Window [::Squish::ObjectName text {Address Book} type Window]
set Address_Book_MenuBar [::Squish::ObjectName container $Address_Book_Window type MenuBar]
set Address_Book_Table [::Squish::ObjectName container $Address_Book_Window type Table]
set Edit_MenuItem [::Squish::ObjectName container $Address_Book_Unnamed_MenuBar text Edit type MenuItem]
set Edit_Add_MenuItem [::Squish::ObjectName container $Edit_MenuItem text Add... type MenuItem]
set File_MenuItem [::Squish::ObjectName container $Address_Book_MenuBar text File type MenuItem]
set File_New_MenuItem [::Squish::ObjectName container $File_MenuItem text New type MenuItem]
}
