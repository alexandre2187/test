source [findFile "scripts" "names.tcl"]

# A quick introduction to implementing scripts for BDD tests:
#
# This file contains snippets of script code to be executed as the .feature
# file is processed. See the section 'Behaviour Driven Testing' in the 'API
# Reference Manual' chapter of the Squish manual for a comprehensive reference.
#
# The commands Given/When/Then/Step can be used to associate a script snippet
# with a pattern which is matched against the steps being executed. Optional
# table/multi-line string arguments of the step are passed via a mandatory
# 'context' parameter:
#
#   When "I enter the text" {context} {
#     <code here>
#   }
#
# The pattern is a plain string without the leading keyword, but a couple of
# placeholders including |any|, |word| and |integer| are supported which can be
# used to extract arbitrary, alphanumeric and integer values resp. from the
# pattern; the extracted values are passed as additional arguments:
#
#   Then "I get |integer| different names" {context numNames} {
#     <code here>
#   }
#
# Instead of using placeholders, a "-rx" switch can be passed as the first
# argument to Given/When/Then/Step to use regular expressions.
#

Given "addressbook application is running" {context} {
    startApplication "AddressbookWPF"
    invoke mouseClick [waitForObject $names::Address_Book_Window] 279 245 [enum MouseButton PrimaryButton]
    invoke mouseClick [waitForObject $names::Address_Book_Table] 256 47 [enum MouseButton PrimaryButton]
    test compare [property get [waitForObjectExists $names::Address_Book_Table] rowCount] 0
}

When "I create a new addressbook" {context} {
    invoke mouseClick [waitForObject $names::Address_Book_Window] 143 225 [enum MouseButton PrimaryButton]
    invoke mouseClick [waitForObject $names::File_MenuItem]
    invoke mouseClick [waitForObject $names::File_New_MenuItem]
}

Then "addressbook should have zero entries" {context} {
    invoke mouseClick [waitForObject $names::Address_Book_Unnamed_Window] 254 286 [enum MouseButton PrimaryButton]
    test compare [property get [waitForObjectExists $names::Address_Book_Unnamed_Table] rowCount] 0
}

When "I add new persons to address book" {context} {
    invoke mouseClick [waitForObject $names::Edit_MenuItem]
    invoke mouseClick [waitForObject $names::Edit_Add_MenuItem]
    invoke mouseClick [waitForObject $names::Address_Book_Add_Forename_Edit] 57 9 [enum MouseButton PrimaryButton]
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] "John"
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] "Smith"
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "john"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "<Ctrl+Alt+Q>"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "m.com"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] "123321231"
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] "<Return>"
    invoke mouseClick [waitForObject $names::Edit_MenuItem]
    invoke mouseClick [waitForObject $names::Edit_Add_MenuItem]
    invoke mouseClick [waitForObject $names::Address_Book_Add_Forename_Edit] 90 5 [enum MouseButton PrimaryButton]
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] "Alice"
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] "Thompson"
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "alice"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "<Ctrl+Alt+Q>"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "m.com"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] "234123567"
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] "<Return>"
}

Then "'2' entries should be present" {context} {
    test compare [property get [waitForObjectExists $names::Address_Book_Unnamed_Table] rowCount] 2
}

When "I add a new person '|any|','|any|','|any|','|integer|' to address book" {context forename lastname email phone} {
    invoke mouseClick [waitForObject $names::Edit_MenuItem]
    invoke mouseClick [waitForObject $names::Edit_Add_MenuItem]
    invoke mouseClick [waitForObject $names::Address_Book_Add_Forename_Edit] 37 6 [enum MouseButton PrimaryButton]
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] $forename
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] $lastname
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] $email
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] $phone
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] "<Return>"
}

Then "'1' entries should be present" {context} {
    test compare [property get [waitForObjectExists $names::Address_Book_Unnamed_Table] rowCount] 1
}
