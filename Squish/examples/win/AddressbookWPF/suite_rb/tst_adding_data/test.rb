require 'names';

# encoding: UTF-8
require 'squish'
include Squish


def invokeMenuItem(menu, item)
    mouseClick(waitForObject("{type='MenuItem' text='"+menu+"'}"))
    mouseClick(waitForObject("{type='MenuItem' text='"+item+"'}"))
end


def addNameAndAddress(oneNameAndAddress)
    invokeMenuItem("Edit", "Add...")
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), oneNameAndAddress[0])
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), oneNameAndAddress[1])
    type(waitForObject(Names::Address_Book_Add_Email_Edit), oneNameAndAddress[2])
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), oneNameAndAddress[3])
    clickButton(waitForObject("{type='Button' text='OK'}"))
end


def closeWithoutSaving
    invokeMenuItem("File", "Quit")
    clickButton(waitForObject("{type='Button' text='No'}"))
end

def checkNameAndAddress(table, record)
    waitForObject(table)
    table=table.nativeObject
    # New addresses are inserted at the start:
    table.SelectedIndex=0
    rows = table.SelectedItem.ToString().split("|")
    for column in 0...TestData.fieldNames(record).length
        Test.compare(rows[column],TestData.field(record, column))
    end
end



def main
    startApplication("AddressbookWPF")
    table = waitForObject(Names::Address_Book_Table)
    invokeMenuItem("File", "New")
    Test.verify(table.rowCount == 0,
              "On startup, the address list is empty.")
    # To avoid testing 100s of rows since that would be boring:
    limit = 10
    rows = 0
    TestData.dataset("MyAddresses.tsv").each_with_index do
        |record, row|
        forename = TestData.field(record, "Forename")
        surname = TestData.field(record, "Surname")
        email = TestData.field(record, "Email")
        phone = TestData.field(record, "Phone")
        # pass as a single Array:
        addNameAndAddress([forename, surname, email, phone])
        checkNameAndAddress(table, record)
        # always insert at the start:
        mouseClick(waitForObject("{type='TableCell' row='0' column='1'}"))
        rows += 1
        break if row+1 >= limit
    end
    Test.compare(table.rowCount, rows,
               "Table contains as many rows as addresses had been add.")
    closeWithoutSaving
end
