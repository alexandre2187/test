require 'names';

# encoding: UTF-8
require 'squish'

include Squish

def main
    startApplication("AddressbookWPF")
    mouseClick(waitForObject("{type='MenuItem' text='File'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Open...'}"))
    type(waitForObject(Names::Open_Addresslist_Edit),
        Squishinfo.testCase + "\\..\\..\\MyAddresses.adr")
    type(waitForObject(Names::Open_Addresslist_Edit),"<Return>")
    snooze(1)
    #clickButton(waitForObject(Names::Open_Addresslist_Öffnen_Button))
    mouseClick(waitForObject("{type='TableCell' row='0' column='0'}"))
    table = waitForObject("{container=Names::Address_Book_MyAddresses_adr_Window_2 type='Table'}")
    Test.compare(table.rowCount, 125)

    mouseClick(waitForObject("{type='MenuItem' text='Edit'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Add...'}"))
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), "Jane")
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), "Doe")
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "jane.doe@nowhere.com")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), "555 123 4567")
    clickButton(waitForObject("{type='Button' text='OK'}"))

    doubleClick(waitForObject("{type='TableCell' row='2' column='1'}"))
    nativeType("Doe")
    nativeType("<Return>")
    waitForObject(Names::Address_Book_MyAddresses_adr_Table)
    Test.compare(table.rowCount, 126)

    mouseClick(waitForObject("{type='TableCell' row='2' column='0'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Edit'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Remove...'}"))
    clickButton(waitForObject("{type='Button' text='Yes'}"))
    Test.compare(table.rowCount, 125)

    Test.compare(waitForObjectExists(Names::O0_0_TableCell).text, "Jane")
    Test.compare(waitForObjectExists(Names::O0_1_TableCell).text, "Doe")
    Test.compare(waitForObjectExists(Names::O0_2_TableCell).text, "jane.doe@nowhere.com")
    Test.compare(waitForObjectExists(Names::O0_3_TableCell).text, "555 123 4567")

    mouseClick(waitForObject("{type='MenuItem' text='File'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Quit'}"))
    clickButton(waitForObject("{type='Button' text='No'}"))
end
