require 'names';

# encoding: UTF-8
require 'squish'
include Squish

def invokeMenuItem(menu, item)
    mouseClick(waitForObject("{type='MenuItem' text='"+menu+"'}"))
    mouseClick(waitForObject("{type='MenuItem' text='"+item+"'}"))
end


def addNameAndAddress(oneNameAndAddress)
    invokeMenuItem("Edit", "Add...")
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), oneNameAndAddress[0])
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), oneNameAndAddress[1])
    type(waitForObject(Names::Address_Book_Add_Email_Edit), oneNameAndAddress[2])
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), oneNameAndAddress[3])
    clickButton(waitForObject("{type='Button' text='OK'}"))
end


def closeWithoutSaving
    invokeMenuItem("File", "Quit")
    clickButton(waitForObject("{type='Button' text='No'}"))
end


def main
    startApplication("AddressbookWPF")
    table = waitForObject(Names::Address_Book_Table)
    invokeMenuItem("File", "New")
    Test.verify(table.rowCount == 0,
              "On startup, the address list is empty.")
    data = [["Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"],
          ["Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"],
          ["Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654"]]
    data.each do |oneNameAndAddress|
        addNameAndAddress(oneNameAndAddress)
    end
    waitForObject(table)
    Test.compare(table.rowCount, data.length,
               "Table contains as many rows as addresses had been add.")
    closeWithoutSaving
end
