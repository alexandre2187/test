# encoding: UTF-8

require 'squish/objectmaphelper'
# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

module Names

include Squish::ObjectMapHelper

Address_Book_MyAddresses_adr_Window = {:text => "Address Book - *MyAddresses.adr", :type => "Window"}
Address_Book_MyAddresses_adr_Table = {:container => Address_Book_MyAddresses_adr_Window, :type => "Table"}
O0_0_TableCell = {:column => 0, :container => Address_Book_MyAddresses_adr_Table, :row => 0, :type => "TableCell"}
O0_1_TableCell = {:column => 1, :container => Address_Book_MyAddresses_adr_Table, :row => 0, :type => "TableCell"}
O0_2_TableCell = {:column => 2, :container => Address_Book_MyAddresses_adr_Table, :row => 0, :type => "TableCell"}
O0_3_TableCell = {:column => 3, :container => Address_Book_MyAddresses_adr_Table, :row => 0, :type => "TableCell"}
Address_Book_Add_Window = {:text => "Address Book - Add", :type => "Window"}
Address_Book_Add_Email_Edit = {:container => Address_Book_Add_Window, :leftObject => "{container={text='Address Book - Add' type='Window'} text='Email:' type='Label'}", :type => "Edit"}
Address_Book_Add_Forename_Edit = {:container => Address_Book_Add_Window, :leftObject => "{container={text='Address Book - Add' type='Window'} text='Forename:' type='Label'}", :type => "Edit"}
Address_Book_Add_Phone_Edit = {:container => Address_Book_Add_Window, :leftObject => "{container={text='Address Book - Add' type='Window'} text='Phone:' type='Label'}", :type => "Edit"}
Address_Book_Add_Surname_Edit = {:container => Address_Book_Add_Window, :leftObject => "{container={text='Address Book - Add' type='Window'} text='Surname:' type='Label'}", :type => "Edit"}
Add_Record_Window = {:text => "Add Record", :type => "Window"}
Open_Addresslist_Dialog = {:text => "Open Addresslist", :type => "Dialog"}
Open_Addresslist_Edit = {:container => Open_Addresslist_Dialog, :type => "Edit"}
Open_Addresslist_Öffnen_Button = {:container => Open_Addresslist_Dialog, :text => "Öffnen", :type => "Button"}
Address_Book_Window = {:text => "Address Book", :type => "Window"}
Address_Book_Table = {:container => Address_Book_Window, :type => "Table"}
Address_Book_MyAddresses_adr_Window_2 = {:text => "Address Book - MyAddresses.adr", :type => "Window"}
Öffnen_Dialog = {:text => "Öffnen", :type => "Dialog"}
end
