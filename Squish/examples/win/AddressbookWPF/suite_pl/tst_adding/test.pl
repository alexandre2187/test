require 'names.pl';

sub invokeMenuItem
{
    my ($menu, $item) = @_;
    mouseClick(waitForObject("{type='MenuItem' text='${menu}'}"));
    mouseClick(waitForObject("{type='MenuItem' text='${item}'}"));
}

sub addNameAndAddress
{
    my(@oneNameAndAddress) = @_;
    invokeMenuItem("Edit", "Add...");
    type(waitForObject($Names::address_book_add_forename_edit), $oneNameAndAddress[0]);
    type(waitForObject($Names::address_book_add_surname_edit), $oneNameAndAddress[1]);
    type(waitForObject($Names::address_book_add_email_edit), $oneNameAndAddress[2]);
    type(waitForObject($Names::address_book_add_phone_edit), $oneNameAndAddress[3]);
    clickButton(waitForObject("{type='Button' text='OK'}"));
}

sub closeWithoutSaving
{
    invokeMenuItem("File", "Quit");
    clickButton(waitForObject("{type='Button' text='No'}"));
}


sub main
{
    startApplication("AddressbookWPF");
    my $table = waitForObject($Names::address_book_table);
    test::verify($table->rowCount == 0,
        "On startup, address list is empty.");
    invokeMenuItem("File", "New");
    my @data = (["Andy", "Beach", "andy.beach\@nowhere.com", "555 123 6786"],
                ["Candy", "Deane", "candy.deane\@nowhere.com", "555 234 8765"],
                ["Ed", "Fernleaf", "ed.fernleaf\@nowhere.com", "555 876 4654"]);
    foreach $oneNameAndAddress (@data) {
        addNameAndAddress(@{$oneNameAndAddress});
    }
    waitForObject($table);
    test::compare($table->rowCount, scalar(@data),
        "Table contains as many rows as addresses where add.");
    closeWithoutSaving();
}
