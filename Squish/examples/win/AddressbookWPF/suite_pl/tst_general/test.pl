require 'names.pl';

sub main
{
    startApplication("AddressbookWPF");
    mouseClick(waitForObject("{type='MenuItem' text='File'}")) ;
    mouseClick(waitForObject("{type='MenuItem' text='Open...'}"));
    mouseClick(waitForObject($Names::open_addresslist_edit));
    type(waitForObject($Names::open_addresslist_edit), "MyAddresses.adr");
    type(waitForObject($Names::open_addresslist_edit), "<Return>");
    mouseClick(waitForObject("{type='TableCell' row='1' column='0'}"));
    my $table = waitForObject($Names::address_book_myaddresses_adr_table_2);
    test::compare($table->rowCount, 125);
    mouseClick(waitForObject("{type='MenuItem' text='Edit'}"));
    mouseClick(waitForObject("{type='MenuItem' text='Add...'}"));
    type(waitForObject($Names::address_book_add_forename_edit), "Jane");
    type(waitForObject($Names::address_book_add_forename_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_surname_edit), "Doe");
    type(waitForObject($Names::address_book_add_surname_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_email_edit), "jane.doe\@nowhere.com");
    type(waitForObject($Names::address_book_add_email_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_phone_edit), "555 123 4567");
    clickButton(waitForObject("{type='Button' text='OK'}"));
    doubleClick(waitForObject("{type='TableCell' row='3' column='1'}"));
    nativeType("Doe");
    nativeType("<Return>");
    waitForObject("{type='TableCell' row='0' column='1'}");
    test::compare($table->rowCount, 126);
    mouseClick(waitForObject("{type='TableCell' row='0' column='1'}"));
    mouseClick(waitForObject("{type='MenuItem' text='Edit'}"));
    mouseClick(waitForObject("{type='MenuItem' text='Remove...'}"));
    clickButton(waitForObject("{type='Button' text='Yes'}"));
    test::compare($table->rowCount, 125);
    test::compare(waitForObjectExists($Names::o0_0_tablecell)->text, "Jane");
    test::compare(waitForObjectExists($Names::o0_1_tablecell)->text, "Doe");
    test::compare(waitForObjectExists($Names::o0_2_tablecell)->text, "jane.doe\@nowhere.com");
    test::compare(waitForObjectExists($Names::o0_3_tablecell)->text, "555 123 4567");
    mouseClick(waitForObject("{type='MenuItem' text='File'}"));
    mouseClick(waitForObject("{type='MenuItem' text='Quit'}"));
    clickButton(waitForObject("{type='Button' text='No'}"));
}
