require 'names.pl';

sub invokeMenuItem
{
    my ($menu, $item) = @_;
    mouseClick(waitForObject("{type='MenuItem' text='${menu}'}"));
    mouseClick(waitForObject("{type='MenuItem' text='${item}'}"));
}

sub addNameAndAddress
{
    my(@oneNameAndAddress) = @_;
    invokeMenuItem("Edit", "Add...");
    type(waitForObject($Names::address_book_add_forename_edit), $oneNameAndAddress[0]);
    type(waitForObject($Names::address_book_add_surname_edit), $oneNameAndAddress[1]);
    type(waitForObject($Names::address_book_add_email_edit), $oneNameAndAddress[2]);
    type(waitForObject($Names::address_book_add_phone_edit), $oneNameAndAddress[3]);
    clickButton(waitForObject("{type='Button' text='OK'}"));
}

sub closeWithoutSaving
{
    invokeMenuItem("File", "Quit");
    clickButton(waitForObject("{type='Button' text='No'}"));
}

sub checkNameAndAddress
{
    my($table, $record) = @_;
    my @columnNames = testData::fieldNames($record);
    my @sellectedRow = split /\|/, $table->nativeProperty("SelectedItem");
  # New addresses are inserted at the start:
    for(my $column = 0; $column < scalar(@columnNames); $column++) {
         test::compare( $sellectedRow[$column],
                        testData::field($record, $column) );
    }
}


sub main
{
    startApplication("AddressbookWPF");
    my $table = waitForObject($Names::address_book_table);
    test::verify($table->rowCount == 0,
        "On startup, address list is empty.");
    invokeMenuItem("File", "New");
    # To avoid testing 100s of rows: (since that would be boring)
    my $limit = 10;
    my @records = testData::dataset("MyAddresses.tsv");
    my $row = 0;
    for (; $row < scalar(@records); ++$row) {
        my $record = $records[$row];
        my $forename = testData::field($record, "Forename");
        my $surname = testData::field($record, "Surname");
        my $email = testData::field($record, "Email");
        my $phone = testData::field($record, "Phone");
        addNameAndAddress($forename, $surname, $email, $phone);
        # always insert at the start:
        mouseClick(waitForObject("{type='TableCell' row='0' column='0'}"));
        checkNameAndAddress($table, $record);
        if ($row+1 >= $limit) {
            last;
        }
    }
    test::compare( $table->rowCount, $row + 1,
        "Addresslist contains as many rows as rows have been added!" );
    closeWithoutSaving();
}
