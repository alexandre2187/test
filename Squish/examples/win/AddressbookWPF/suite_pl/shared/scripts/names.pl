package Names;

use utf8;
use strict;
use warnings;
use Squish::ObjectMapHelper::ObjectName;

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'ObjectMapHelper' module.
#
our $address_book_myaddresses_adr_window = {"text" => "Address Book - *MyAddresses.adr", "type" => "Window"};
our $address_book_myaddresses_adr_table = {"container" => $address_book_myaddresses_adr_window, "type" => "Table"};
our $o0_0_tablecell = {"column" => 0, "container" => $address_book_myaddresses_adr_table, "row" => 0, "type" => "TableCell"};
our $o0_1_tablecell = {"column" => 1, "container" => $address_book_myaddresses_adr_table, "row" => 0, "type" => "TableCell"};
our $o0_2_tablecell = {"column" => 2, "container" => $address_book_myaddresses_adr_table, "row" => 0, "type" => "TableCell"};
our $o0_3_tablecell = {"column" => 3, "container" => $address_book_myaddresses_adr_table, "row" => 0, "type" => "TableCell"};
our $address_book_add_window = {"text" => "Address Book - Add", "type" => "Window"};
our $address_book_add_email_edit = {"container" => $address_book_add_window, "leftObject" => "{container={text='Address Book - Add' type='Window'} text='Email:' type='Label'}", "type" => "Edit"};
our $address_book_add_forename_edit = {"container" => $address_book_add_window, "leftObject" => "{container={text='Address Book - Add' type='Window'} text='Forename:' type='Label'}", "type" => "Edit"};
our $address_book_add_phone_edit = {"container" => $address_book_add_window, "leftObject" => "{container={text='Address Book - Add' type='Window'} text='Phone:' type='Label'}", "type" => "Edit"};
our $address_book_add_surname_edit = {"container" => $address_book_add_window, "leftObject" => "{container={text='Address Book - Add' type='Window'} text='Surname:' type='Label'}", "type" => "Edit"};
our $add_record_window = {"text" => "Add Record", "type" => "Window"};
our $address_book_myaddresses_adr_window_2 = {"text" => "Address Book - MyAddresses.adr", "type" => "Window"};
our $address_book_myaddresses_adr_table_2 = {"container" => $address_book_myaddresses_adr_window_2, "type" => "Table"};
our $address_book_window = {"text" => "Address Book", "type" => "Window"};
our $address_book_table = {"container" => $address_book_window, "type" => "Table"};
our $open_addresslist_dialog = {"text" => "Open Addresslist", "type" => "Dialog"};
our $öffnen_dialog = {"text" => "Öffnen", "type" => "Dialog"};
our $open_addresslist_edit = {"container" => $open_addresslist_dialog, "type" => "Edit"};
1;
