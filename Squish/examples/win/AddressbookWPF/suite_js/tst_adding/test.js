import * as names from 'names.js';

function invokeMenuItem(menu, item) {
    mouseClick(waitForObject("{type='MenuItem' text='"+menu+"'}"));
    mouseClick(waitForObject("{type='MenuItem' text='"+item+"'}"));
}

function addNameAndAddress(oneNameAndAddress) {
    invokeMenuItem("Edit", "Add...");
    type(waitForObject(names.addressBookAddForenameEdit), oneNameAndAddress[0]);
    type(waitForObject(names.addressBookAddSurnameEdit), oneNameAndAddress[1]);
    type(waitForObject(names.addressBookAddEmailEdit), oneNameAndAddress[2]);
    type(waitForObject(names.addressBookAddPhoneEdit), oneNameAndAddress[3]);
    clickButton(waitForObject("{type='Button' text='OK'}"));
}

function closeWithoutSaving() {
    invokeMenuItem("File", "Quit");
    clickButton(waitForObject("{type='Button' text='No'}"));
}

function main() {
    startApplication("AddressbookWPF");
    var table = waitForObject(names.addressBookTable);
    invokeMenuItem("File", "New");
    test.verify(table.rowCount == 0, "On startup, address list is empty.");
    var data = new Array(
        new Array("Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"),
        new Array("Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"),
        new Array("Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654"));
    for (var row = 0; row < data.length; ++row)
        addNameAndAddress(data[row]);
    waitForObject(table);
    test.compare(table.rowCount, data.length,
        "Table contains as many rows as addresses where add.");
    closeWithoutSaving();
}

