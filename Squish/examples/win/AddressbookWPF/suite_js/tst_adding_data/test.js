import * as names from 'names.js';

function invokeMenuItem(menu, item) {
    mouseClick(waitForObject("{type='MenuItem' text='"+menu+"'}"));
    mouseClick(waitForObject("{type='MenuItem' text='"+item+"'}"));
}

function addNameAndAddress(oneNameAndAddress) {
    invokeMenuItem("Edit", "Add...");
    type(waitForObject(names.addressBookAddForenameEdit), oneNameAndAddress[0]);
    type(waitForObject(names.addressBookAddSurnameEdit), oneNameAndAddress[1]);
    type(waitForObject(names.addressBookAddEmailEdit), oneNameAndAddress[2]);
    type(waitForObject(names.addressBookAddPhoneEdit), oneNameAndAddress[3]);
    clickButton(waitForObject("{type='Button' text='OK'}"));
}

function closeWithoutSaving() {
    invokeMenuItem("File", "Quit");
    clickButton(waitForObject("{type='Button' text='No'}"));
}

function checkNameAndAddress(table, record)
{
    waitForObject(table);
    // New addresses are inserted at the start
    for(var column = 0; column < testData.fieldNames(record).length; ++column)
        test.compare( findObject("{type='TableCell' row='0' column='"+column+"'}").text,
                      testData.field(record, column) );
}



function main()
{
    startApplication("AddressbookWPF");
    var table = waitForObject(names.addressBookTable);
    invokeMenuItem("File", "New");
    test.verify(table.rowCount == 0, "On startup, address list is empty.");
    var limit = 10; // To avoid testing 100s of rows since that would be boring
    var records = testData.dataset("MyAddresses.tsv");
    for (var row = 0; row < records.length; ++row) {
        var record = records[row];
        var forename = testData.field(record, "Forename");
        var surname = testData.field(record, "Surname");
        var email = testData.field(record, "Email");
        var phone = testData.field(record, "Phone");
        addNameAndAddress(new Array(forename, surname, email, phone));
        checkNameAndAddress(table, record);
        if (row+1 >= limit)
            break;
        // always insert at the start:
        mouseClick("{type='TableCell' row='0' column='0'}");
    }
    test.compare(table.rowCount, row + 1,
        "Table contains as many rows as addresses where add.");
    closeWithoutSaving();
}