import * as names from 'names.js';

function main() {
    startApplication("AddressbookWPF");
    mouseClick(waitForObject(names.fileMenuItem));
    mouseClick(waitForObject(names.fileNewMenuItem));
    mouseClick(waitForObject("{type='MenuItem' text='File'}"));
    mouseClick(waitForObject("{type='MenuItem' text='Open...'}"));
    mouseClick(waitForObject(names.openAddresslistEdit), 67, 10, MouseButton.PrimaryButton);
    type(waitForObject(names.openAddresslistEdit), squishinfo.testCase + "\\..\\..\\MyAddresses.adr");
    type(waitForObject(names.openAddresslistEdit), "<Return>");

    mouseClick(waitForObject("{type='TableCell' row='7' column='0'}"));
    mouseClick(waitForObject("{type='MenuItem' text='Edit'}"));
    mouseClick(waitForObject("{type='MenuItem' text='Add...'}"));
    mouseClick(waitForObject(names.addressBookAddForenameEdit));
    type(waitForObject(names.addressBookAddForenameEdit), "Jane");
    type(waitForObject(names.addressBookAddForenameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddSurnameEdit), "Doe");
    type(waitForObject(names.addressBookAddSurnameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddEmailEdit), "jane.doe@nowhere.com");
    type(waitForObject(names.addressBookAddEmailEdit), "<Tab>");
    type(waitForObject(names.addressBookAddPhoneEdit), "555 123 4567");
    type(waitForObject(names.addressBookAddPhoneEdit), "<Return>");

    doubleClick(waitForObject("{type='TableCell' row='2' column='1'}"));
    nativeType("Doe");
    mouseClick(waitForObject("{type='MenuItem' text='Edit'}"));
    mouseClick(waitForObject("{type='MenuItem' text='Remove...'}"));
    clickButton(waitForObject("{type='Button' text='Yes'}"));

    test.compare(waitForObjectExists(names.o60TableCell).text, "Jane");
    test.compare(waitForObjectExists(names.o61TableCell).text, "Doe");
    test.compare(waitForObjectExists(names.o62TableCell).text, "jane.doe@nowhere.com");
    test.compare(waitForObjectExists(names.o63TableCell).text, "555 123 4567");

    mouseClick(waitForObject("{type='MenuItem' text='File'}"));
    mouseClick(waitForObject("{type='MenuItem' text='Quit'}"));
    mouseClick(waitForObject("{type='Button' text='No'}"));
}

