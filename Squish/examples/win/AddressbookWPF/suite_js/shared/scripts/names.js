// See the chapter 'Script-Based Object Map API' in the Squish manual for
// documentation of the functionality offered by the 'objectmaphelper' module.
import { RegularExpression, Wildcard } from 'objectmaphelper.js';

export var addressBookMyAddressesAdrWindow = {"text": "Address Book - *MyAddresses.adr", "type": "Window"};
export var addressBookMyAddressesAdrTable = {"container": addressBookMyAddressesAdrWindow, "type": "Table"};
export var o60TableCell = {"column": 0, "container": addressBookMyAddressesAdrTable, "row": 6, "type": "TableCell"};
export var o61TableCell = {"column": 1, "container": addressBookMyAddressesAdrTable, "row": 6, "type": "TableCell"};
export var o62TableCell = {"column": 2, "container": addressBookMyAddressesAdrTable, "row": 6, "type": "TableCell"};
export var o63TableCell = {"column": 3, "container": addressBookMyAddressesAdrTable, "row": 6, "type": "TableCell"};
export var addressBookAddWindow = {"text": "Address Book - Add", "type": "Window"};
export var addressBookAddEmailEdit = {"container": addressBookAddWindow, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Email:' type='Label'}", "type": "Edit"};
export var addressBookAddForenameEdit = {"container": addressBookAddWindow, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Forename:' type='Label'}", "type": "Edit"};
export var addressBookAddPhoneEdit = {"container": addressBookAddWindow, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Phone:' type='Label'}", "type": "Edit"};
export var addressBookAddSurnameEdit = {"container": addressBookAddWindow, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Surname:' type='Label'}", "type": "Edit"};
export var addRecordWindow = {"text": "Add Record", "type": "Window"};
export var addressBookWindow = {"text": "Address Book", "type": "Window"};
export var addressBookMenuBar = {"container": addressBookWindow, "type": "MenuBar"};
export var addressBookTable = {"container": addressBookWindow, "type": "Table"};
export var fileMenuItem = {"container": addressBookMenuBar, "text": "File", "type": "MenuItem"};
export var fileNewMenuItem = {"container": fileMenuItem, "text": "New", "type": "MenuItem"};
export var openAddresslistDialog = {"text": "Open Addresslist", "type": "Dialog"};
export var openAddresslistEdit = {"container": openAddresslistDialog, "type": "Edit"};
export var ffnenDialog = {"text": "Öffnen", "type": "Dialog"};
