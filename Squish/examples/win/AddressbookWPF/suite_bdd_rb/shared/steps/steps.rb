require 'names';

# encoding: UTF-8

# A quick introduction to implementing scripts for BDD tests:
#
# This file contains snippets of script code to be executed as the .feature
# file is processed. See the section 'Behaviour Driven Testing' in the 'API
# Reference Manual' chapter of the Squish manual for a comprehensive reference.
#
# The functions Given/When/Then/Step can be used to associate a script snippet
# with a pattern which is matched against the steps being executed. Optional
# table/multi-line string arguments of the step are passed via a mandatory
# 'context' parameter:
#
#   When("I enter the text") do |context|
#     <code here>
#   end
#
# The pattern is a plain string without the leading keyword, but a couple of
# placeholders including |any|, |word| and |integer| are supported which can be
# used to extract arbitrary, alphanumeric and integer values resp. from the
# pattern; the extracted values are passed as additional arguments:
#
#   Then("I get |integer| different names") do |context, numNames|
#     <code here>
#   end
#
# Instead of using a string with placeholders, a regular expression object can
# be passed to Given/When/Then/Step to use regular expressions.
#

require 'squish'

include Squish

Given("addressbook application is running") do |context|
    startApplication("AddressbookWPF")
    mouseClick(waitForObject(Names::Address_Book_Table), 108, 44, MouseButton::PRIMARY_BUTTON)
end

When("I create a new addressbook") do |context|
    mouseClick(waitForObject(Names::File_MenuItem))
    mouseClick(waitForObject(Names::File_New_MenuItem))
end

Then("addressbook should have zero entries") do |context|
    Test.compare(waitForObjectExists(Names::Address_Book_Unnamed_Table).rowCount, 0)
end


When("I add new persons to address book") do |context|
    mouseClick(waitForObject(Names::Edit_MenuItem))
    mouseClick(waitForObject(Names::Edit_Add_MenuItem))
    mouseClick(waitForObject(Names::Address_Book_Add_Forename_Edit), 17, 11, MouseButton::PRIMARY_BUTTON)
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), "John")
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), "Smith")
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "john")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "<Ctrl+Alt+Q>")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "m.com")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), "123123123")
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), "<Return>")
    mouseClick(waitForObject(Names::Edit_MenuItem))
    mouseClick(waitForObject(Names::Edit_Add_MenuItem))
    mouseClick(waitForObject(Names::Address_Book_Add_Window), 116, 38, MouseButton::PRIMARY_BUTTON)
    mouseClick(waitForObject(Names::Address_Book_Add_Forename_Edit), 32, 7, MouseButton::PRIMARY_BUTTON)
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), "Alice")
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), "Thompson")
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "alice")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "<Ctrl+Alt+Q>")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "m.com")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), "234234234")
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), "<Return>")
end

Then("'2' entries should be present") do |context|
    Test.compare(waitForObjectExists(Names::Address_Book_Unnamed_Table).rowCount, 2)
end


When("I add a new person '|any|','|any|','|any|','|integer|' to address book") do |context,forename,lastname,email,phone|
    mouseClick(waitForObject(Names::Edit_MenuItem))
    mouseClick(waitForObject(Names::Edit_Add_MenuItem))
    mouseClick(waitForObject(Names::Address_Book_Add_Forename_Edit), 68, 9, MouseButton::PRIMARY_BUTTON)
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), forename)
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), lastname)
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), email)
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), phone)
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), "<Return>")
end

Then("'1' entries should be present") do |context|
    Test.compare(waitForObjectExists(Names::Address_Book_Unnamed_Table).rowCount, 1)
end

