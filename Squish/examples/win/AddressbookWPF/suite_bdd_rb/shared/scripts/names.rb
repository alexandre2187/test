# encoding: UTF-8

require 'squish/objectmaphelper'
# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

module Names

include Squish::ObjectMapHelper

Address_Book_Add_Window = {:text => "Address Book - Add", :type => "Window"}
Address_Book_Add_Email_Edit = {:container => Address_Book_Add_Window, :leftObject => "{container={text='Address Book - Add' type='Window'} text='Email:' type='Label'}", :type => "Edit"}
Address_Book_Add_Email_Label = {:container => Address_Book_Add_Window, :text => "Email:", :type => "Label"}
Address_Book_Add_Forename_Edit = {:container => Address_Book_Add_Window, :leftObject => "{container={text='Address Book - Add' type='Window'} text='Forename:' type='Label'}", :type => "Edit"}
Address_Book_Add_Forename_Label = {:container => Address_Book_Add_Window, :text => "Forename:", :type => "Label"}
Address_Book_Add_Phone_Edit = {:container => Address_Book_Add_Window, :leftObject => "{container={text='Address Book - Add' type='Window'} text='Phone:' type='Label'}", :type => "Edit"}
Address_Book_Add_Phone_Label = {:container => Address_Book_Add_Window, :text => "Phone:", :type => "Label"}
Address_Book_Add_Surname_Edit = {:container => Address_Book_Add_Window, :leftObject => "{container={text='Address Book - Add' type='Window'} text='Surname:' type='Label'}", :type => "Edit"}
Address_Book_Add_Surname_Label = {:container => Address_Book_Add_Window, :text => "Surname:", :type => "Label"}
Address_Book_Unnamed_Window = {:text => "Address Book - Unnamed", :type => "Window"}
Address_Book_Unnamed_MenuBar = {:container => Address_Book_Unnamed_Window, :type => "MenuBar"}
Address_Book_Unnamed_Table = {:container => Address_Book_Unnamed_Window, :type => "Table"}
Address_Book_Window = {:text => "Address Book", :type => "Window"}
Address_Book_MenuBar = {:container => Address_Book_Window, :type => "MenuBar"}
Address_Book_Table = {:container => Address_Book_Window, :type => "Table"}
Edit_MenuItem = {:container => Address_Book_Unnamed_MenuBar, :text => "Edit", :type => "MenuItem"}
Edit_Add_MenuItem = {:container => Edit_MenuItem, :text => "Add...", :type => "MenuItem"}
File_MenuItem = {:container => Address_Book_MenuBar, :text => "File", :type => "MenuItem"}
File_New_MenuItem = {:container => File_MenuItem, :text => "New", :type => "MenuItem"}
end
