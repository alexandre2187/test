require 'names.pl';

=begin comment A quick introduction to implementing scripts for BDD tests:

This file contains snippets of script code to be executed as the .feature
file is processed. See the section 'Behaviour Driven Testing' in the 'API
Reference Manual' chapter of the Squish manual for a comprehensive reference.

The functions Given/When/Then/Step can be used to associate a script snippet
with a pattern which is matched against the steps being executed. Optional
table/multi-line string arguments of the step are passed via a mandatory
'context' parameter:

  When("I enter the text", sub {
    my %context = %{shift()};
    <code here>
  });

The pattern is a plain string without the leading keyword, but a couple of
placeholders including |any|, |word| and |integer| are supported which can be
used to extract arbitrary, alphanumeric and integer values resp. from the
pattern; the extracted values are passed as additional arguments:

  Then("I get |integer| different names", sub {
    my %context = %{shift()};
    my $numNames = shift();
    <code here>
  });

Instead of using a string with placeholders, a regular expression object can
be passed to Given/When/Then/Step to use regular expressions.
=end comment
=cut

use warnings;
use utf8;
use Squish::BDD;

package main;

1;

Given("addressbook application is running", sub {
    my $context = shift;
    startApplication("AddressbookWPF");
});

When("I create a new addressbook", sub {
    my $context = shift;
    mouseClick(waitForObject($Names::address_book_window), 168, 248, MouseButton::PrimaryButton);
    mouseClick(waitForObject($Names::file_menuitem));
    mouseClick(waitForObject($Names::file_new_menuitem));
});

Then("addressbook should have zero entries", sub {
    my $context = shift;
    test::compare(waitForObjectExists($Names::address_book_unnamed_table)->rowCount, 0);
});

When("I add a new person 'John','Doe','john\@m.com','500600700' to address book", sub {
    my $context = shift;
    mouseClick(waitForObject($Names::edit_menuitem));
    mouseClick(waitForObject($Names::edit_add_menuitem));
    mouseClick(waitForObject($Names::address_book_add_forename_edit), 53, 11, MouseButton::PrimaryButton);
    type(waitForObject($Names::address_book_add_forename_edit), "John");
    type(waitForObject($Names::address_book_add_forename_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_surname_edit), "Doe");
    type(waitForObject($Names::address_book_add_surname_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_email_edit), "john");
    type(waitForObject($Names::address_book_add_email_edit), "<Ctrl+Alt+Q>");
    type(waitForObject($Names::address_book_add_email_edit), "m.com");
    type(waitForObject($Names::address_book_add_email_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_phone_edit), "500600700");
    type(waitForObject($Names::address_book_add_phone_edit), "<Return>");
});

When("I add a new person 'Bob','Koo','bob\@m.com','500600800' to address book", sub {
    my $context = shift;
    mouseClick(waitForObject($Names::edit_menuitem));
    mouseClick(waitForObject($Names::edit_add_menuitem));
    mouseClick(waitForObject($Names::address_book_add_forename_edit), 53, 11, MouseButton::PrimaryButton);
    type(waitForObject($Names::address_book_add_forename_edit), "John");
    type(waitForObject($Names::address_book_add_forename_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_surname_edit), "Doe");
    type(waitForObject($Names::address_book_add_surname_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_email_edit), "john");
    type(waitForObject($Names::address_book_add_email_edit), "<Ctrl+Alt+Q>");
    type(waitForObject($Names::address_book_add_email_edit), "m.com");
    type(waitForObject($Names::address_book_add_email_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_phone_edit), "500600800");
    type(waitForObject($Names::address_book_add_phone_edit), "<Return>");
});

Then("'1' entries should be present", sub {
    my $context = shift;
    test::compare(waitForObjectExists($Names::address_book_unnamed_table)->rowCount, 1);
});


When("I add new persons to address book", sub {
    my $context = shift;
    mouseClick(waitForObject($Names::edit_menuitem));
    mouseClick(waitForObject($Names::edit_add_menuitem));
    mouseClick(waitForObject($Names::address_book_add_forename_edit), 35, 8, MouseButton::PrimaryButton);
    type(waitForObject($Names::address_book_add_forename_edit), "John");
    type(waitForObject($Names::address_book_add_forename_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_surname_edit), "Smith");
    type(waitForObject($Names::address_book_add_surname_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_email_edit), "john");
    type(waitForObject($Names::address_book_add_email_edit), "<Ctrl+Alt+Q>");
    type(waitForObject($Names::address_book_add_email_edit), "m.com");
    type(waitForObject($Names::address_book_add_email_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_phone_edit), "123123");
    type(waitForObject($Names::address_book_add_phone_edit), "<Return>");
    mouseClick(waitForObject($Names::edit_menuitem));
    mouseClick(waitForObject($Names::edit_add_menuitem));
    mouseClick(waitForObject($Names::address_book_add_forename_edit), 29, 8, MouseButton::PrimaryButton);
    type(waitForObject($Names::address_book_add_forename_edit), "Alice");
    type(waitForObject($Names::address_book_add_forename_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_surname_edit), "Thomson");
    type(waitForObject($Names::address_book_add_surname_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_email_edit), "alice");
    type(waitForObject($Names::address_book_add_email_edit), "<Ctrl+Alt+Q>");
    type(waitForObject($Names::address_book_add_email_edit), "m.com");
    type(waitForObject($Names::address_book_add_email_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_phone_edit), "234234");
    type(waitForObject($Names::address_book_add_phone_edit), "<Return>");
});

Then("'2' entries should be present", sub {
    my $context = shift;
    test::compare(waitForObjectExists($Names::address_book_unnamed_table)->rowCount, 2);
});


When("I add a new person 'Bob','Doe','Bob\@m.com','123321231' to address book", sub {
    my $context = shift;
    mouseClick(waitForObject($Names::edit_menuitem));
    mouseClick(waitForObject($Names::edit_add_menuitem));
    mouseClick(waitForObject($Names::address_book_add_forename_edit), 52, 12, MouseButton::PrimaryButton);
    type(waitForObject($Names::address_book_add_forename_edit), "Bob");
    type(waitForObject($Names::address_book_add_forename_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_surname_edit), "Doe");
    type(waitForObject($Names::address_book_add_surname_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_email_edit), "Bob");
    type(waitForObject($Names::address_book_add_email_edit), "<Ctrl+Alt+Q>");
    type(waitForObject($Names::address_book_add_email_edit), "m.com");
    type(waitForObject($Names::address_book_add_email_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_phone_edit), "123321231");
    type(waitForObject($Names::address_book_add_phone_edit), "<Return>");
});

Then("previously entered forename and surname shall be at the top", sub {
    my $context = shift;
    test::compare(waitForObjectExists($Names::o_tablerow)->row, 0);
});
