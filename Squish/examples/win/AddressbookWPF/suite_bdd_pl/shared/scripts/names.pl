package Names;

use utf8;
use strict;
use warnings;
use Squish::ObjectMapHelper::ObjectName;

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'ObjectMapHelper' module.
#
our $address_book_add_window = {"text" => "Address Book - Add", "type" => "Window"};
our $address_book_add_email_edit = {"container" => $address_book_add_window, "leftObject" => "{container={text='Address Book - Add' type='Window'} text='Email:' type='Label'}", "type" => "Edit"};
our $add_record_window = {"text" => "Add Record", "type" => "Window"};
our $add_record_email_label = {"container" => $add_record_window, "text" => "Email:", "type" => "Label"};
our $address_book_add_forename_edit = {"container" => $address_book_add_window, "leftObject" => "{container={text='Address Book - Add' type='Window'} text='Forename:' type='Label'}", "type" => "Edit"};
our $add_record_forename_label = {"container" => $add_record_window, "text" => "Forename:", "type" => "Label"};
our $address_book_add_phone_edit = {"container" => $address_book_add_window, "leftObject" => "{container={text='Address Book - Add' type='Window'} text='Phone:' type='Label'}", "type" => "Edit"};
our $add_record_phone_label = {"container" => $add_record_window, "text" => "Phone:", "type" => "Label"};
our $address_book_add_surname_edit = {"container" => $address_book_add_window, "leftObject" => "{container={text='Address Book - Add' type='Window'} text='Surname:' type='Label'}", "type" => "Edit"};
our $add_record_surname_label = {"container" => $add_record_window, "text" => "Surname:", "type" => "Label"};
our $address_book_unnamed_window = {"text" => "Address Book - Unnamed", "type" => "Window"};
our $address_book_unnamed_menubar = {"container" => $address_book_unnamed_window, "type" => "MenuBar"};
our $address_book_unnamed_table = {"container" => $address_book_unnamed_window, "type" => "Table"};
our $address_book_window = {"text" => "Address Book", "type" => "Window"};
our $address_book_menubar = {"container" => $address_book_window, "type" => "MenuBar"};
our $edit_menuitem = {"container" => $address_book_unnamed_menubar, "text" => "Edit", "type" => "MenuItem"};
our $edit_add_menuitem = {"container" => $edit_menuitem, "text" => "Add...", "type" => "MenuItem"};
our $file_menuitem = {"container" => $address_book_menubar, "text" => "File", "type" => "MenuItem"};
our $file_new_menuitem = {"container" => $file_menuitem, "text" => "New", "type" => "MenuItem"};
our $o_tablerow = {"container" => $address_book_unnamed_table, "type" => "TableRow"};
1;
