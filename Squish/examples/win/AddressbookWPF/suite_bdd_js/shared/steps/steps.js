import * as names from 'names.js';

// A quick introduction to implementing scripts for BDD tests:
//
// This file contains snippets of script code to be executed as the .feature
// file is processed. See the section 'Behaviour Driven Testing' in the 'API
// Reference Manual' chapter of the Squish manual for a comprehensive reference.
//
// The functions Given/When/Then/Step can be used to associate a script snippet
// with a pattern which is matched against the steps being executed. Optional
// table/multi-line string arguments of the step are passed via a mandatory
// 'context' parameter:
//
//   When("I enter the text", function(context) {
//     <code here>
//   });
//
// The pattern is a plain string without the leading keyword, but a couple of
// placeholders including |any|, |word| and |integer| are supported which can
// be used to extract arbitrary, alphanumeric and integer values resp. from the
// pattern; the extracted values are passed as additional arguments:
//
//   Then("I get |integer| different names", function(context, numNames) {
//     <code here>
//   });
//
// Instead of using a string with placeholders, a regular expression object can
// be passed to Given/When/Then/Step to use regular expressions.
//

Given("addressbook application is running", function(context) {
    startApplication("AddressbookWPF");
});

When("I create a new addressbook", function(context) {
    mouseClick(waitForObject(names.fileMenuItem));
    mouseClick(waitForObject(names.fileNewMenuItem));
});

Then("addressbook should have zero entries", function(context) {
    test.compare(waitForObjectExists(names.addressBookUnnamedTable).rowCount, 0);
});

When("I add a new person 'John','Doe','john@m.com','500600700' to address book", function(context) {
    mouseClick(waitForObject(names.editMenuItem));
    mouseClick(waitForObject(names.editAddMenuItem));
    mouseClick(waitForObject(names.addressBookAddForenameEdit), 46, 9, MouseButton.PrimaryButton);
    type(waitForObject(names.addressBookAddForenameEdit), "John");
    type(waitForObject(names.addressBookAddForenameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddSurnameEdit), "Doe");
    type(waitForObject(names.addressBookAddSurnameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddEmailEdit), "john");
    type(waitForObject(names.addressBookAddEmailEdit), "<Ctrl+Alt+Q>");
    type(waitForObject(names.addressBookAddEmailEdit), "m.com");
    type(waitForObject(names.addressBookAddEmailEdit), "<Tab>");
    type(waitForObject(names.addressBookAddPhoneEdit), "500600700");
    type(waitForObject(names.addressBookAddPhoneEdit), "<Return>");
});

When("I add a new person 'Bob','Koo','bob@m.com','500600800' to address book", function(context) {
    mouseClick(waitForObject(names.editMenuItem));
    mouseClick(waitForObject(names.editAddMenuItem));
    mouseClick(waitForObject(names.addressBookAddForenameEdit), 46, 9, MouseButton.PrimaryButton);
    type(waitForObject(names.addressBookAddForenameEdit), "Bob");
    type(waitForObject(names.addressBookAddForenameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddSurnameEdit), "Koo");
    type(waitForObject(names.addressBookAddSurnameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddEmailEdit), "bob");
    type(waitForObject(names.addressBookAddEmailEdit), "<Ctrl+Alt+Q>");
    type(waitForObject(names.addressBookAddEmailEdit), "m.com");
    type(waitForObject(names.addressBookAddEmailEdit), "<Tab>");
    type(waitForObject(names.addressBookAddPhoneEdit), "500600800");
    type(waitForObject(names.addressBookAddPhoneEdit), "<Return>");
});

Then("'1' entries should be present", function(context) {
    test.compare(waitForObjectExists(names.addressBookUnnamedTable).rowCount, 1);
});

When("I add new persons to address book", function(context) {
    mouseClick(waitForObject(names.editMenuItem));
    mouseClick(waitForObject(names.editAddMenuItem));
    mouseClick(waitForObject(names.addressBookAddForenameEdit), 44, 11, MouseButton.PrimaryButton);
    type(waitForObject(names.addressBookAddForenameEdit), "John");
    type(waitForObject(names.addressBookAddForenameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddSurnameEdit), "Smith");
    type(waitForObject(names.addressBookAddSurnameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddEmailEdit), "john");
    type(waitForObject(names.addressBookAddEmailEdit), "<Ctrl+Alt+Q>");
    type(waitForObject(names.addressBookAddEmailEdit), "m.com");
    type(waitForObject(names.addressBookAddEmailEdit), "<Tab>");
    type(waitForObject(names.addressBookAddPhoneEdit), "123123");
    type(waitForObject(names.addressBookAddPhoneEdit), "<Return>");
    mouseClick(waitForObject(names.editMenuItem));
    mouseClick(waitForObject(names.editAddMenuItem));
    mouseClick(waitForObject(names.addressBookAddForenameEdit), 22, 13, MouseButton.PrimaryButton);
    type(waitForObject(names.addressBookAddForenameEdit), "Alice");
    type(waitForObject(names.addressBookAddForenameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddSurnameEdit), "Thomson");
    type(waitForObject(names.addressBookAddSurnameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddEmailEdit), "alice");
    type(waitForObject(names.addressBookAddEmailEdit), "<Ctrl+Alt+Q>");
    type(waitForObject(names.addressBookAddEmailEdit), "m.com");
    type(waitForObject(names.addressBookAddEmailEdit), "<Tab>");
    type(waitForObject(names.addressBookAddPhoneEdit), "234234");
    type(waitForObject(names.addressBookAddPhoneEdit), "<Return>");
});

Then("'2' entries should be present", function(context) {
    test.compare(waitForObjectExists(names.addressBookUnnamedTable).rowCount, 2);
});

When("I add a new person 'Bob','Doe','Bob@m.com','123321231' to address book", function(context) {
    mouseClick(waitForObject(names.editMenuItem));
    mouseClick(waitForObject(names.editAddMenuItem));
    mouseClick(waitForObject(names.addressBookAddForenameEdit), 46, 6, MouseButton.PrimaryButton);
    type(waitForObject(names.addressBookAddForenameEdit), "Bob");
    type(waitForObject(names.addressBookAddForenameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddSurnameEdit), "Doe");
    type(waitForObject(names.addressBookAddSurnameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddEmailEdit), "bob");
    type(waitForObject(names.addressBookAddEmailEdit), "<Ctrl+Alt+Q>");
    type(waitForObject(names.addressBookAddEmailEdit), "m.com");
    type(waitForObject(names.addressBookAddEmailEdit), "<Tab>");
    type(waitForObject(names.addressBookAddPhoneEdit), "123321231");
    type(waitForObject(names.addressBookAddPhoneEdit), "<Return>");
});

Then("previously entered forename and surname shall be at the top", function(context) {
    test.vp("VP1");
});
