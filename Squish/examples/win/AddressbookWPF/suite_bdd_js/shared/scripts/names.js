// See the chapter 'Script-Based Object Map API' in the Squish manual for
// documentation of the functionality offered by the 'objectmaphelper' module.
import { RegularExpression, Wildcard } from 'objectmaphelper.js';

export var addressBookAddWindow = {"text": "Address Book - Add", "type": "Window"};
export var addressBookAddEmailEdit = {"container": addressBookAddWindow, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Email:' type='Label'}", "type": "Edit"};
export var addRecordWindow = {"text": "Add Record", "type": "Window"};
export var addRecordEmailLabel = {"container": addRecordWindow, "text": "Email:", "type": "Label"};
export var addressBookAddForenameEdit = {"container": addressBookAddWindow, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Forename:' type='Label'}", "type": "Edit"};
export var addRecordForenameLabel = {"container": addRecordWindow, "text": "Forename:", "type": "Label"};
export var addressBookAddPhoneEdit = {"container": addressBookAddWindow, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Phone:' type='Label'}", "type": "Edit"};
export var addRecordPhoneLabel = {"container": addRecordWindow, "text": "Phone:", "type": "Label"};
export var addressBookAddSurnameEdit = {"container": addressBookAddWindow, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Surname:' type='Label'}", "type": "Edit"};
export var addRecordSurnameLabel = {"container": addRecordWindow, "text": "Surname:", "type": "Label"};
export var addressBookUnnamedWindow = {"text": "Address Book - Unnamed", "type": "Window"};
export var addressBookUnnamedMenuBar = {"container": addressBookUnnamedWindow, "type": "MenuBar"};
export var addressBookUnnamedTable = {"container": addressBookUnnamedWindow, "type": "Table"};
export var addressBookWindow = {"text": "Address Book", "type": "Window"};
export var addressBookMenuBar = {"container": addressBookWindow, "type": "MenuBar"};
export var editMenuItem = {"container": addressBookUnnamedMenuBar, "text": "Edit", "type": "MenuItem"};
export var editAddMenuItem = {"container": editMenuItem, "text": "Add...", "type": "MenuItem"};
export var fileMenuItem = {"container": addressBookMenuBar, "text": "File", "type": "MenuItem"};
export var fileNewMenuItem = {"container": fileMenuItem, "text": "New", "type": "MenuItem"};
