source [findFile "scripts" "names.tcl"]

proc invokeMenuItem {menu item} {
    invoke mouseClick [waitForObject "{type='MenuItem' text='$menu'}"]
    invoke mouseClick [waitForObject "{type='MenuItem' text='$item'}"]
}

proc addNameAndAddress {oneNameAndAddress} {
    invokeMenuItem "Edit" "Add..."
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] [lindex $oneNameAndAddress 0]
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] [lindex $oneNameAndAddress 1]
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] [lindex $oneNameAndAddress 2]
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] [lindex $oneNameAndAddress 3]
    invoke clickButton [waitForObject "{type='Button' text='OK'}"]
}

proc closeWithoutSaving {} {
    invokeMenuItem "File" "Quit"
    invoke clickButton [waitForObject "{type='Button' text='No'}"]
}

proc checkNameAndAddress {insertionRow record} {
    set columns [llength [testData fieldNames $record]]
    for {set column 0} {$column < 4} {incr column} {
        set cell [waitForObject "{type='TableCell' row='$insertionRow' column='$column'}"]
        test compare [property get $cell text] [testData field $record [expr {$column}]]
    }
}

proc main {} {
    startApplication "AddressbookWPF"
    set table [waitForObject $names::Address_Book_Table 1000]
    test compare [property get $table rowCount] 0 \
        "on startup, address list is empty."
    invokeMenuItem "File" "New"
    set limit 10
    set data [testData dataset "MyAddresses.tsv"]
    set columns [llength [testData fieldNames [lindex $data 0]]]
    set row 0
    for {} {$row < [llength $data]} {incr row} {
        set record [lindex $data $row]
        set forename [testData field $record "Forename"]
        set surname [testData field $record "Surname"]
        set email [testData field $record "Email"]
        set phone [testData field $record "Phone"]
        set details [list $forename $surname $email $phone]
        addNameAndAddress $details
        set tableRow [ waitForObject "{type='TableRow' row='0'}" ]
        checkNameAndAddress $row $record
        if {$row+1 >= $limit} {
            break
        }
    }
    test compare [property get $table rowCount] [expr $row + 1] \
        "address list contains as many entries as addresses had been add."
    closeWithoutSaving
}
