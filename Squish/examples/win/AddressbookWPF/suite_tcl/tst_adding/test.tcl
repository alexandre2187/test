source [findFile "scripts" "names.tcl"]

proc invokeMenuItem {menu item} {
    invoke mouseClick [waitForObject "{type='MenuItem' text='$menu'}"]
    invoke mouseClick [waitForObject "{type='MenuItem' text='$item'}"]
}

proc addNameAndAddress {oneNameAndAddress} {
    invokeMenuItem "Edit" "Add..."
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] [lindex $oneNameAndAddress 0]
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] [lindex $oneNameAndAddress 1]
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] [lindex $oneNameAndAddress 2]
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] [lindex $oneNameAndAddress 3]
    invoke clickButton [waitForObject "{type='Button' text='OK'}"]
}

proc closeWithoutSaving {} {
    invokeMenuItem "File" "Quit"
    invoke clickButton [waitForObject "{type='Button' text='No'}"]
}


proc main {} {
    startApplication "AddressbookWPF"
    set table [waitForObject $names::Address_Book_Table 1000]
    test compare [property get $table rowCount] 0 \
        "on startup, address list is empty."
    invokeMenuItem "File" "New"
    set data [list \
        [list "Andy" "Beach" "andy.beach@nowhere.com" "555 123 6786"] \
        [list "Candy" "Deane" "candy.deane@nowhere.com" "555 234 8765"] \
        [list "Ed" "Fernleaf" "ed.fernleaf@nowhere.com" "555 876 4654"] ]
    for {set i 0} {$i < [llength $data]} {incr i} {
        addNameAndAddress [lindex $data $i]
    }
    waitForObject $table
    test compare [property get $table rowCount] [llength $data] \
        "address list contains as many entries as addresses had been add."
    closeWithoutSaving
}
