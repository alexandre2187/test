source [findFile "scripts" "names.tcl"]

proc main {} {
    startApplication "AddressbookWPF"
    invoke mouseClick [waitForObject "{type='MenuItem' text='File'}"]
    invoke mouseClick [waitForObject "{type='MenuItem' text='Open...'}"]
    invoke mouseClick [waitForObject $names::Open_Addresslist_Edit 1000]
    set testcase [squishinfo testCase]
    invoke type [waitForObject $names::Open_Addresslist_Edit 1000] "$testcase\\..\\..\\MyAddresses.adr"
    invoke type [findObject $names::Open_Addresslist_Edit] "<Return>"


    invoke mouseClick [waitForObject "{type='TableCell' row='0' column='0'}"]
    set table [waitForObject "{container=$names::Address_Book_MyAddresses_adr_Window_2 type='Table'}" 1000]
    test compare [property get $table rowCount] 125
    invoke mouseClick [waitForObject "{type='MenuItem' text='Edit'}"]
    invoke mouseClick [waitForObject "{type='MenuItem' text='Add...'}"]
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] "Jane"
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] "Doe"
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "jane.doe@nowhere.com"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] "555 123 4567"
    invoke clickButton [waitForObject "{type='Button' text='OK'}"]

    invoke mouseClick [waitForObject "{type='TableCell' row='2' column='0'}"]
    invoke type [waitForObject "{type='TableCell' row='2' column='0'}"] "<Shift>"
    invoke type [waitForObject "{type='TableCell' row='2' column='0'}"] "D"
    invoke type [waitForObject "{type='TableCell' row='2' column='0'}"] "oe"
    invoke type [waitForObject "{type='TableCell' row='2' column='0'}"] "<Return>"
    waitForObject $table
    test compare [property get $table rowCount] 126
    invoke mouseClick [waitForObject "{type='TableCell' row='2' column='0'}"]
    invoke mouseClick [waitForObject "{type='MenuItem' text='Edit'}"]
    invoke mouseClick [waitForObject "{type='MenuItem' text='Remove...'}"]
    invoke clickButton [waitForObject "{type='Button' text='Yes'}"]
    test compare [property get $table rowCount] 125

    test compare [property get [waitForObjectExists $names::0_0_TableCell_2] text] "Jane"
    test compare [property get [waitForObjectExists $names::0_1_TableCell_2] text] "Doe"
    test compare [property get [waitForObjectExists $names::0_2_TableCell_2] text] "jane.doe@nowhere.com"
    test compare [property get [waitForObjectExists $names::0_3_TableCell_2] text] "555 123 4567"

    invoke mouseClick [waitForObject "{type='MenuItem' text='File'}"]
    invoke mouseClick [waitForObject "{type='MenuItem' text='Quit'}"]
    invoke clickButton [waitForObject "{type='Button' text='No'}"]
}
