import names

def invokeMenuItem(menu, item):
    mouseClick(waitForObject("{type='MenuItem' text='%s'}" % menu))
    mouseClick(waitForObject("{type='MenuItem' text='%s'}" % item))

def addNameAndAddress(oneNameAndAddress):
    invokeMenuItem("Edit", "Add...")
    type(waitForObject(names.address_Book_Add_Forename_Edit), oneNameAndAddress[0])
    type(waitForObject(names.address_Book_Add_Surname_Edit), oneNameAndAddress[1])
    type(waitForObject(names.address_Book_Add_Email_Edit), oneNameAndAddress[2])
    type(waitForObject(names.address_Book_Add_Phone_Edit), oneNameAndAddress[3])
    type(waitForObject(names.address_Book_Add_Phone_Edit), "<Return>")

def checkNameAndAddress(insertionRow, record):
    for column in range(0, len(testData.fieldNames(record))):
        cell = waitForObject("{row='%d' column='%d' type='TableCell'}" % (
            insertionRow, column))
        test.compare(cell.text, testData.field(record, column))

def closeWithoutSaving():
    invokeMenuItem("File", "Quit")
    clickButton(waitForObject(names.save_Table_No_Button))

def main():
    testSettings.setWrappersForApplication("AddressbookWPF",["Windows"])
    startApplication("AddressbookWPF")
    table = waitForObject(names.address_Book_Table)
    invokeMenuItem("File", "New")
    test.verify(table.rowCount == 0,
                "On startup, the address list is empty.")
    limit = 10 # To avoid testing 100s of rows since that would be boring
    verificationTable = testData.dataset("MyAddresses.tsv");
    for row, record in enumerate(verificationTable):
        forename = testData.field(record, "Forename")
        surname = testData.field(record, "Surname")
        email = testData.field(record, "Email")
        phone = testData.field(record, "Phone")
        addNameAndAddress((forename, surname, email, phone))
        checkNameAndAddress(row, record)
        if row+1 >= limit:
            break
    test.compare(table.rowCount, row+1,
                "After adding " + str(table.rowCount)
                +" addresses, the table contains "
                +str(row+1) + " rows!")
    closeWithoutSaving()

