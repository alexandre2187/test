# encoding: UTF-8

from objectmaphelper import *

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

address_Book_MyAddresses_adr_Window = {"text": "Address Book - *MyAddresses.adr", "type": "Window"}
address_Book_MyAddresses_adr_Table = {"container": address_Book_MyAddresses_adr_Window, "type": "Table"}
o1_0_TableCell = {"column": 0, "container": address_Book_MyAddresses_adr_Table, "row": 1, "type": "TableCell"}
o1_1_TableCell = {"column": 1, "container": address_Book_MyAddresses_adr_Table, "row": 1, "type": "TableCell"}
o1_2_TableCell = {"column": 2, "container": address_Book_MyAddresses_adr_Table, "row": 1, "type": "TableCell"}
o1_3_TableCell = {"column": 3, "container": address_Book_MyAddresses_adr_Table, "row": 1, "type": "TableCell"}
address_Book_Add_Window = {"text": "Address Book - Add", "type": "Window"}
address_Book_Add_Email_Edit = {"container": address_Book_Add_Window, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Email:' type='Label'}", "type": "Edit"}
address_Book_Add_Forename_Edit = {"container": address_Book_Add_Window, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Forename:' type='Label'}", "type": "Edit"}
address_Book_Add_Phone_Edit = {"container": address_Book_Add_Window, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Phone:' type='Label'}", "type": "Edit"}
address_Book_Add_Surname_Edit = {"container": address_Book_Add_Window, "leftObject": "{container={text='Address Book - Add' type='Window'} text='Surname:' type='Label'}", "type": "Edit"}
open_Addresslist_Dialog = {"text": "Open Addresslist", "type": "Dialog"}
open_Addresslist_Edit = {"container": open_Addresslist_Dialog, "type": "Edit"}
address_Book_Window = {"text": "Address Book", "type": "Window"}
address_Book_Table = {"container": address_Book_Window, "type": "Table"}
save_Table_Window = {"text": "Save Table ?", "type": "Window"}
save_Table_No_Button = {"container": save_Table_Window, "text": "No", "type": "Button"}
