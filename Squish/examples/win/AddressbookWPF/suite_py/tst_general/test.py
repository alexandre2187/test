import names

# -*- coding: utf-8 -*-

def main():
    startApplication("AddressbookWPF")
    mouseClick(waitForObject("{type='MenuItem' text='File'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Open...'}"))
    type(waitForObject(names.open_Addresslist_Edit),
        squishinfo.testCase + "\\..\\..\\MyAddresses.adr")
    type(waitForObject(names.open_Addresslist_Edit), "<Return>")
    mouseClick(waitForObject("{type='TableCell' row='2' column='2'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Edit'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Add...'}"))
    type(waitForObject(names.address_Book_Add_Forename_Edit), "Jane")
    type(waitForObject(names.address_Book_Add_Forename_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Surname_Edit), "Doe")
    type(waitForObject(names.address_Book_Add_Surname_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Email_Edit), "jane.doe@nowhere.com")
    type(waitForObject(names.address_Book_Add_Email_Edit), "<Tab>")
    type(waitForObject(names.address_Book_Add_Phone_Edit), "555 123 4567")
    type(waitForObject(names.address_Book_Add_Phone_Edit), "<Return>")
    snooze(1)
    doubleClick(waitForObject("{type='TableCell' row='3' column='3'}"))
    type(waitForObject("{type='TableCell' row='3' column='3'}"), "Doe")
    type(waitForObject("{type='TableCell' row='3' column='3'}"), "<Return>")
    mouseClick(waitForObject("{type='TableCell' row='1' column='0'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Edit'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Remove...'}"))
    clickButton(waitForObject("{type='Button' text='Yes'}"))
    test.compare(waitForObjectExists(names.o1_0_TableCell).text, "Jane")
    test.compare(waitForObjectExists(names.o1_1_TableCell).text, "Doe")
    test.compare(waitForObjectExists(names.o1_2_TableCell).text, "jane.doe@nowhere.com")
    test.compare(waitForObjectExists(names.o1_3_TableCell).text, "555 123 4567")
    table = waitForObject(names.address_Book_MyAddresses_adr_Table)
    test.compare(table.rowCount, 125, "Table contains 125 rows!")
    mouseClick(waitForObject(names.address_Book_MyAddresses_adr_Window))
    mouseClick(waitForObject("{type='MenuItem' text='File'}"))
    mouseClick(waitForObject("{type='MenuItem' text='Quit'}"))
    clickButton(waitForObject("{type='Button' text='No'}"))


