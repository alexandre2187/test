import names

def invokeMenuItem(menu, item):
    mouseClick(waitForObject("{type='MenuItem' text='%s'}" % menu))
    mouseClick(waitForObject("{type='MenuItem' text='%s'}" % item))

def addNameAndAddress(oneNameAndAddress):
    invokeMenuItem("Edit", "Add...")
    type(waitForObject(names.address_Book_Add_Forename_Edit), oneNameAndAddress[0])
    type(waitForObject(names.address_Book_Add_Surname_Edit), oneNameAndAddress[1])
    type(waitForObject(names.address_Book_Add_Email_Edit), oneNameAndAddress[2])
    type(waitForObject(names.address_Book_Add_Phone_Edit), oneNameAndAddress[3])
    type(waitForObject(names.address_Book_Add_Phone_Edit), "<Return>")

def closeWithoutSaving():
    invokeMenuItem("File", "Quit")
    clickButton(waitForObject("{type='Button' text='No'}"))

def main():
    testSettings.setWrappersForApplication("AddressbookWPF",["Windows"])
    startApplication("AddressbookWPF")
    table = waitForObject(names.address_Book_Table)
    invokeMenuItem("File", "New")
    test.verify(table.rowCount == 0, "On startup, the address list is empty.")
    data = [("Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"),
            ("Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"),
            ("Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654")]
    for oneNameAndAddress in data:
        addNameAndAddress(oneNameAndAddress)
    test.compare(table.rowCount, len(data),
                "After adding "+ str(len(data))
                +" addresses, the table contains "
                + str(table.rowCount) + " rows!")
    closeWithoutSaving()
