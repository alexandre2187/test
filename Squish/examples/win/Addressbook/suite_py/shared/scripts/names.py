# encoding: UTF-8

from objectmaphelper import *

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

address_Book_MyAddresses_adr_Window = {"text": "Address Book - MyAddresses.adr", "type": "Window"}
address_Book_MyAddresses_adr_Table = {"container": address_Book_MyAddresses_adr_Window, "type": "Table"}
table = {"type": "Table"}
o1_1_TableCell = {"column": 1, "container": address_Book_MyAddresses_adr_Table, "row": 1, "type": "TableCell"}
address_Book_Unnamed_Window = {"text": "Address Book - Unnamed", "type": "Window"}
address_Book_Unnamed_Table = {"container": address_Book_Unnamed_Window, "type": "Table"}
o1_1_TableCell_2 = {"column": 1, "container": address_Book_Unnamed_Table, "row": 1, "type": "TableCell"}
o1_2_TableCell = {"column": 2, "container": address_Book_MyAddresses_adr_Table, "row": 1, "type": "TableCell"}
o1_2_TableCell_2 = {"column": 2, "container": address_Book_Unnamed_Table, "row": 1, "type": "TableCell"}
o1_3_TableCell = {"column": 3, "container": address_Book_MyAddresses_adr_Table, "row": 1, "type": "TableCell"}
o1_4_TableCell = {"column": 4, "container": address_Book_MyAddresses_adr_Table, "row": 1, "type": "TableCell"}
o2_2_TableCell = {"column": 2, "container": address_Book_MyAddresses_adr_Table, "row": 2, "type": "TableCell"}
o4_2_TableCell = {"column": 2, "container": address_Book_MyAddresses_adr_Table, "row": 4, "type": "TableCell"}
address_Book_Add_Window = {"text": "Address Book - Add", "type": "Window"}
address_Book_Add_Email_Label = {"container": address_Book_Add_Window, "text": "Email:", "type": "Label"}
address_Book_Add_Email_Edit = {"container": address_Book_Add_Window, "leftObject": address_Book_Add_Email_Label, "type": "Edit"}
address_Book_Add_Forename_Label = {"container": address_Book_Add_Window, "text": "Forename:", "type": "Label"}
address_Book_Add_Forename_Edit = {"container": address_Book_Add_Window, "leftObject": address_Book_Add_Forename_Label, "type": "Edit"}
address_Book_Add_OK_Button = {"container": address_Book_Add_Window, "text": "OK", "type": "Button"}
address_Book_Add_Phone_Label = {"container": address_Book_Add_Window, "text": "Phone:", "type": "Label"}
address_Book_Add_Phone_Edit = {"container": address_Book_Add_Window, "leftObject": address_Book_Add_Phone_Label, "type": "Edit"}
address_Book_Add_Surname_Label = {"container": address_Book_Add_Window, "text": "Surname:", "type": "Label"}
address_Book_Add_Surname_Edit = {"container": address_Book_Add_Window, "leftObject": address_Book_Add_Surname_Label, "type": "Edit"}
address_Book_Choose_File_Dialog = {"text": "Address Book - Choose File", "type": "Dialog"}
address_Book_Choose_File_Open_CheckBox = {"container": address_Book_Choose_File_Dialog, "text": "Open", "type": "CheckBox"}
address_Book_Choose_File_Edit = {"container": address_Book_Choose_File_Dialog, "type": "Edit"}
address_Book_Choose_File_WindowsControl = {"class": "DirectUIHWND", "container": address_Book_Choose_File_Dialog, "occurrence": 2, "type": "WindowsControl"}
address_Book_Delete_Window = {"text": "Address Book - Delete", "type": "Window"}
address_Book_Delete_Yes_Button = {"container": address_Book_Delete_Window, "text": "Yes", "type": "Button"}
address_Book_MyAddresses_adr_Menubar = {"container": address_Book_MyAddresses_adr_Window, "type": "Menubar"}
address_Book_Unnamed_Menubar = {"container": address_Book_Unnamed_Window, "type": "Menubar"}
address_Book_Unnamed_Toolbar = {"container": address_Book_Unnamed_Window, "type": "Toolbar"}
address_Book_Window = {"text": "Address Book", "type": "Window"}
address_Book_No_Button = {"container": address_Book_Window, "text": "No", "type": "Button"}
edit_MenuItem = {"container": address_Book_Unnamed_Menubar, "text": "Edit", "type": "MenuItem"}
edit_Add_MenuItem = {"container": edit_MenuItem, "text": "Add...", "type": "MenuItem"}
edit_MenuItem_2 = {"container": address_Book_MyAddresses_adr_Menubar, "text": "Edit", "type": "MenuItem"}
file_MenuItem = {"container": address_Book_Unnamed_Menubar, "text": "File", "type": "MenuItem"}
file_New_MenuItem = {"container": file_MenuItem, "text": "New", "type": "MenuItem"}
file_MenuItem_2 = {"container": address_Book_MyAddresses_adr_Menubar, "text": "File", "type": "MenuItem"}
new_ToolbarItem = {"container": address_Book_Unnamed_Toolbar, "text": "New", "type": "ToolbarItem"}
o_Edit = {"container": address_Book_MyAddresses_adr_Table, "type": "Edit"}
