import names

def main():
    startApplication("Addressbook")
    table = waitForObject(names.table)
    invokeMenuItem("File", "New")
    test.verify(table.rowCount == 0)
    limit = 10 # To avoid testing 100s of rows since that would be boring
    for row, record in enumerate(testData.dataset("MyAddresses.tsv")):
        forename = testData.field(record, "Forename")
        surname = testData.field(record, "Surname")
        email = testData.field(record, "Email")
        phone = testData.field(record, "Phone")
        addNameAndAddress((forename, surname, email, phone))
        insertionRow = row # Modern Python: insertionRow = row if row >= 1 else 1
        if row < 1:
            insertionRow = 1
        checkNameAndAddress(insertionRow, record)
        if row > limit:
            break
    test.compare(table.rowCount, row + 1)
    closeWithoutSaving()


def invokeMenuItem(menu, item):
    mouseClick(waitForObjectItem({"type": "Menubar"}, menu))
    mouseClick(waitForObject({'type': 'MenuItem', 'text' : item}))

def addNameAndAddress(oneNameAndAddress):
    invokeMenuItem("Edit", "Add...")
    type(waitForObject(names.address_Book_Add_Forename_Edit), oneNameAndAddress[0])
    type(waitForObject(names.address_Book_Add_Surname_Edit), oneNameAndAddress[1])
    type(waitForObject(names.address_Book_Add_Email_Edit), oneNameAndAddress[2])
    type(waitForObject(names.address_Book_Add_Phone_Edit), oneNameAndAddress[3])
    clickButton(waitForObject(names.address_Book_Add_OK_Button))

def checkNameAndAddress(insertionRow, record):
    for column in range(1, 1 + len(testData.fieldNames(record))):
        cell = waitForObject({'container' : names.table, 'row': insertionRow, 'column' : column, 'type' :'TableCell'})
        test.compare(cell.text, testData.field(record, column - 1))

def closeWithoutSaving():
    invokeMenuItem("File", "Quit")
    clickButton(waitForObject(names.address_Book_No_Button))

