import names

def main():
    startApplication("Addressbook")
    table = waitForObject(names.address_Book_Unnamed_Table)
    invokeMenuItem("File", "New")
    test.verify(table.rowCount == 0)
    data = [("Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"),
            ("Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"),
            ("Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654")]
    for oneNameAndAddress in data:
        addNameAndAddress(oneNameAndAddress)
    test.compare(table.rowCount, len(data))
    closeWithoutSaving()

def invokeMenuItem(menu, item):
    mouseClick(waitForObjectItem({"type": "Menubar"}, menu))
    mouseClick(waitForObject({'type': 'MenuItem', 'text' : item}))

def addNameAndAddress(oneNameAndAddress):
    invokeMenuItem("Edit", "Add...")
    type(waitForObject(names.address_Book_Add_Forename_Edit), oneNameAndAddress[0])
    type(waitForObject(names.address_Book_Add_Surname_Edit), oneNameAndAddress[1])
    type(waitForObject(names.address_Book_Add_Email_Edit), oneNameAndAddress[2])
    type(waitForObject(names.address_Book_Add_Phone_Edit), oneNameAndAddress[3])
    clickButton(waitForObject(names.address_Book_Add_OK_Button))

def closeWithoutSaving():
    invokeMenuItem("File", "Quit")
    clickButton(waitForObject(names.address_Book_No_Button))
