# encoding: UTF-8
require 'names';
include Squish

def main
    startApplication("Addressbook")
    mouseClick(waitForObjectItem(Names::Address_Book_Unnamed_Menubar, "File"))
    mouseClick(waitForObjectItem(Names::File_MenuItem, "Open..."))
    type(waitForObject(Names::Address_Book_Choose_File_Edit),
    Squishinfo.testCase + "\\..\\..\\MyAddresses.adr")
    type(waitForObject(Names::Address_Book_Choose_File_Edit), "<Return>")
    mouseClick(waitForObject(Names::O2_2_TableCell))
    mouseClick(waitForObjectItem(Names::Address_Book_MyAddresses_adr_Menubar, "Edit"))
    mouseClick(waitForObjectItem(Names::Edit_MenuItem_2, "Add..."))
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), "Jane")
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), "Doe")
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Email_Edit),"jane.doe@nowhere.com")
    type(waitForObject(Names::Address_Book_Add_Email_Edit), "<Tab>")
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), "555 123 4567")
    clickButton(waitForObject(Names::Address_Book_Add_OK_Button))
    mouseClick(waitForObject(Names::O4_2_TableCell))
    type(waitForObject(Names::Address_Book_MyAddresses_adr_Table), "Doe")
    type(waitForObject(Names::Address_Book_MyAddresses_adr_Table), "<Return>")
    mouseClick(waitForObject(Names::O1_2_TableCell))
    mouseClick(waitForObjectItem(Names::Address_Book_MyAddresses_adr_Menubar, "Edit"))
    mouseClick(waitForObjectItem(Names::Edit_MenuItem_2, "Remove..."))
    clickButton(waitForObject(Names::Address_Book_Delete_Yes_Button))
    Test.compare(waitForObjectExists(Names::O1_1_TableCell).text, "Jane")
    Test.compare(waitForObjectExists(Names::O1_2_TableCell).text, "Doe")
    Test.compare(waitForObjectExists(Names::O1_3_TableCell).text, "jane.doe@nowhere.com")
    Test.compare(waitForObjectExists(Names::O1_4_TableCell).text, "555 123 4567")
    table = waitForObject(Names::Address_Book_MyAddresses_adr_Table)
    Test.compare(table.rowCount, 125)
    mouseClick(waitForObject(Names::Address_Book_MyAddresses_adr_Window),
    287, 11, MouseButton::LEFT_BUTTON)
    mouseClick(waitForObjectItem(Names::Address_Book_MyAddresses_adr_Menubar, "File"))
    mouseClick(waitForObjectItem(Names::File_MenuItem_2, "Quit"))
    clickButton(waitForObject(Names::Address_Book_No_Button))
end
