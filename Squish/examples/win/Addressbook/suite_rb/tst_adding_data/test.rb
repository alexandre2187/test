# encoding: UTF-8
require 'squish'
require 'names';

include Squish

def main
    startApplication("Addressbook")
    table = waitForObject(Names::Address_Book_Unnamed_Table)
    invokeMenuItem("File", "New")
    Test.verify(table.rowCount == 0)
    limit = 10 # To avoid testing 100s of rows since that would be boring
    rows = 0
    TestData.dataset("MyAddresses.tsv").each_with_index do
        |record, row|
        forename = TestData.field(record, "Forename")
        surname = TestData.field(record, "Surname")
        email = TestData.field(record, "Email")
        phone = TestData.field(record, "Phone")
        addNameAndAddress([forename, surname, email, phone])
        insertionRow = row >= 1 ? row : 1
        checkNameAndAddress(insertionRow, record)
        break if row > limit
        rows += 1
    end
    Test.compare(table.rowCount, rows + 1)
    closeWithoutSaving
end

def invokeMenuItem(menu, item)
    mouseClick(waitForObjectItem({:type => "Menubar"}, menu))
    mouseClick(waitForObject({:type => 'MenuItem', :text => item}))
end

def addNameAndAddress(oneNameAndAddress)
    invokeMenuItem("Edit", "Add...")
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), oneNameAndAddress[0])
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), oneNameAndAddress[1])
    type(waitForObject(Names::Address_Book_Add_Email_Edit), oneNameAndAddress[2])
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), oneNameAndAddress[3])
    clickButton(waitForObject(Names::Address_Book_Add_OK_Button))
end

def checkNameAndAddress(row, record)
    for column in 1..TestData.fieldNames(record).length
        cell = waitForObject({:container => Names::Table, :row => row, :column => column, :type => 'TableCell'})
        Test.compare(cell.text, TestData.field(record, column - 1))
    end
end

def closeWithoutSaving
    invokeMenuItem("File", "Quit")
    clickButton(waitForObject(Names::Address_Book_No_Button))
end

