# encoding: UTF-8

require 'squish/objectmaphelper'
# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

module Names

include Squish::ObjectMapHelper

Address_Book_MyAddresses_adr_Window = {:text => "Address Book - MyAddresses.adr", :type => "Window"}
Address_Book_MyAddresses_adr_Table = {:container => Address_Book_MyAddresses_adr_Window, :type => "Table"}
Table = {:type => "Table"}
O1_1_TableCell = {:column => 1, :container => Address_Book_MyAddresses_adr_Table, :row => 1, :type => "TableCell"}
O1_2_TableCell = {:column => 2, :container => Address_Book_MyAddresses_adr_Table, :row => 1, :type => "TableCell"}
Address_Book_Unnamed_Window = {:text => "Address Book - Unnamed", :type => "Window"}
Address_Book_Unnamed_Table = {:container => Address_Book_Unnamed_Window, :type => "Table"}
O1_2_TableCell_2 = {:column => 2, :container => Address_Book_Unnamed_Table, :row => 1, :type => "TableCell"}
O1_3_TableCell = {:column => 3, :container => Address_Book_MyAddresses_adr_Table, :row => 1, :type => "TableCell"}
O1_4_TableCell = {:column => 4, :container => Address_Book_MyAddresses_adr_Table, :row => 1, :type => "TableCell"}
O2_2_TableCell = {:column => 2, :container => Address_Book_MyAddresses_adr_Table, :row => 2, :type => "TableCell"}
O4_2_TableCell = {:column => 2, :container => Address_Book_MyAddresses_adr_Table, :row => 4, :type => "TableCell"}
Address_Book_Add_Window = {:text => "Address Book - Add", :type => "Window"}
Address_Book_Add_Email_Label = {:container => Address_Book_Add_Window, :text => "Email:", :type => "Label"}
Address_Book_Add_Email_Edit = {:container => Address_Book_Add_Window, :leftObject => Address_Book_Add_Email_Label, :type => "Edit"}
Address_Book_Add_Forename_Label = {:container => Address_Book_Add_Window, :text => "Forename:", :type => "Label"}
Address_Book_Add_Forename_Edit = {:container => Address_Book_Add_Window, :leftObject => Address_Book_Add_Forename_Label, :type => "Edit"}
Address_Book_Add_OK_Button = {:container => Address_Book_Add_Window, :text => "OK", :type => "Button"}
Address_Book_Add_Phone_Label = {:container => Address_Book_Add_Window, :text => "Phone:", :type => "Label"}
Address_Book_Add_Phone_Edit = {:container => Address_Book_Add_Window, :leftObject => Address_Book_Add_Phone_Label, :type => "Edit"}
Address_Book_Add_Surname_Label = {:container => Address_Book_Add_Window, :text => "Surname:", :type => "Label"}
Address_Book_Add_Surname_Edit = {:container => Address_Book_Add_Window, :leftObject => Address_Book_Add_Surname_Label, :type => "Edit"}
Address_Book_Choose_File_Dialog = {:text => "Address Book - Choose File", :type => "Dialog"}
Address_Book_Choose_File_Open_CheckBox = {:container => Address_Book_Choose_File_Dialog, :text => "Open", :type => "CheckBox"}
Address_Book_Choose_File_Edit = {:container => Address_Book_Choose_File_Dialog, :type => "Edit"}
Address_Book_Choose_File_WindowsControl = {:class => "DirectUIHWND", :container => Address_Book_Choose_File_Dialog, :occurrence => 2, :type => "WindowsControl"}
Address_Book_Delete_Window = {:text => "Address Book - Delete", :type => "Window"}
Address_Book_Delete_Yes_Button = {:container => Address_Book_Delete_Window, :text => "Yes", :type => "Button"}
Address_Book_MyAddresses_adr_Menubar = {:container => Address_Book_MyAddresses_adr_Window, :type => "Menubar"}
Address_Book_Unnamed_Menubar = {:container => Address_Book_Unnamed_Window, :type => "Menubar"}
Address_Book_Window = {:text => "Address Book", :type => "Window"}
Address_Book_No_Button = {:container => Address_Book_Window, :text => "No", :type => "Button"}
Edit_MenuItem = {:container => Address_Book_Unnamed_Menubar, :text => "Edit", :type => "MenuItem"}
Edit_Add_MenuItem = {:container => Edit_MenuItem, :text => "Add...", :type => "MenuItem"}
Edit_MenuItem_2 = {:container => Address_Book_MyAddresses_adr_Menubar, :text => "Edit", :type => "MenuItem"}
File_MenuItem = {:container => Address_Book_Unnamed_Menubar, :text => "File", :type => "MenuItem"}
File_New_MenuItem = {:container => File_MenuItem, :text => "New", :type => "MenuItem"}
File_MenuItem_2 = {:container => Address_Book_MyAddresses_adr_Menubar, :text => "File", :type => "MenuItem"}
O_Edit = {:container => Address_Book_MyAddresses_adr_Table, :type => "Edit"}
end
