# encoding: UTF-8
require 'squish'
require 'names';
include Squish

def main
    startApplication("Addressbook")
    table = waitForObject(Names::Address_Book_Unnamed_Table)
    invokeMenuItem("File", "New")
    Test.verify(table.rowCount == 0)
    data = [["Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"],
        ["Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"],
        ["Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654"]]
    data.each do |oneNameAndAddress|
        addNameAndAddress(oneNameAndAddress)
    end
    Test.compare(table.rowCount, data.length)
    closeWithoutSaving
end

def invokeMenuItem(menu, item)
    mouseClick(waitForObjectItem({:type => "Menubar"}, menu))
    mouseClick(waitForObject({:type => 'MenuItem', :text => item}))
end

def addNameAndAddress(oneNameAndAddress)
    invokeMenuItem("Edit", "Add...")
    type(waitForObject(Names::Address_Book_Add_Forename_Edit), oneNameAndAddress[0])
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), oneNameAndAddress[1])
    type(waitForObject(Names::Address_Book_Add_Email_Edit), oneNameAndAddress[2])
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), oneNameAndAddress[3])
    clickButton(waitForObject(Names::Address_Book_Add_OK_Button))
end

def closeWithoutSaving
    invokeMenuItem("File", "Quit")
    clickButton(waitForObject(Names::Address_Book_No_Button))
end
