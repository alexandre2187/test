package Names;

use utf8;
use strict;
use warnings;
use Squish::ObjectMapHelper::ObjectName;

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'ObjectMapHelper' module.
#
our $address_book_myaddresses_adr_window = {"text" => "Address Book - MyAddresses.adr", "type" => "Window"};
our $address_book_myaddresses_adr_table = {"container" => $address_book_myaddresses_adr_window, "type" => "Table"};
our $table = {"type" => "Table"};
our $o1_1_tablecell = {"column" => 1, "container" => $address_book_myaddresses_adr_table, "row" => 1, "type" => "TableCell"};
our $o1_2_tablecell = {"column" => 2, "container" => $address_book_myaddresses_adr_table, "row" => 1, "type" => "TableCell"};
our $address_book_unnamed_window = {"text" => "Address Book - Unnamed", "type" => "Window"};
our $address_book_unnamed_table = {"container" => $address_book_unnamed_window, "type" => "Table"};
our $o1_2_tablecell_2 = {"column" => 2, "container" => $address_book_unnamed_table, "row" => 1, "type" => "TableCell"};
our $o1_3_tablecell = {"column" => 3, "container" => $address_book_myaddresses_adr_table, "row" => 1, "type" => "TableCell"};
our $o1_4_tablecell = {"column" => 4, "container" => $address_book_myaddresses_adr_table, "row" => 1, "type" => "TableCell"};
our $o2_2_tablecell = {"column" => 2, "container" => $address_book_myaddresses_adr_table, "row" => 2, "type" => "TableCell"};
our $o4_2_tablecell = {"column" => 2, "container" => $address_book_myaddresses_adr_table, "row" => 4, "type" => "TableCell"};
our $address_book_add_window = {"text" => "Address Book - Add", "type" => "Window"};
our $address_book_add_email_label = {"container" => $address_book_add_window, "text" => "Email:", "type" => "Label"};
our $address_book_add_email_edit = {"container" => $address_book_add_window, "leftObject" => $address_book_add_email_label, "type" => "Edit"};
our $address_book_add_forename_label = {"container" => $address_book_add_window, "text" => "Forename:", "type" => "Label"};
our $address_book_add_forename_edit = {"container" => $address_book_add_window, "leftObject" => $address_book_add_forename_label, "type" => "Edit"};
our $address_book_add_ok_button = {"container" => $address_book_add_window, "text" => "OK", "type" => "Button"};
our $address_book_add_phone_label = {"container" => $address_book_add_window, "text" => "Phone:", "type" => "Label"};
our $address_book_add_phone_edit = {"container" => $address_book_add_window, "leftObject" => $address_book_add_phone_label, "type" => "Edit"};
our $address_book_add_surname_label = {"container" => $address_book_add_window, "text" => "Surname:", "type" => "Label"};
our $address_book_add_surname_edit = {"container" => $address_book_add_window, "leftObject" => $address_book_add_surname_label, "type" => "Edit"};
our $address_book_choose_file_dialog = {"text" => "Address Book - Choose File", "type" => "Dialog"};
our $address_book_choose_file_open_checkbox = {"container" => $address_book_choose_file_dialog, "text" => "Open", "type" => "CheckBox"};
our $address_book_choose_file_edit = {"container" => $address_book_choose_file_dialog, "type" => "Edit"};
our $address_book_choose_file_windowscontrol = {"class" => "DirectUIHWND", "container" => $address_book_choose_file_dialog, "occurrence" => 2, "type" => "WindowsControl"};
our $address_book_delete_window = {"text" => "Address Book - Delete", "type" => "Window"};
our $address_book_delete_yes_button = {"container" => $address_book_delete_window, "text" => "Yes", "type" => "Button"};
our $address_book_myaddresses_adr_menubar = {"container" => $address_book_myaddresses_adr_window, "type" => "Menubar"};
our $address_book_unnamed_menubar = {"container" => $address_book_unnamed_window, "type" => "Menubar"};
our $address_book_window = {"text" => "Address Book", "type" => "Window"};
our $address_book_no_button = {"container" => $address_book_window, "text" => "No", "type" => "Button"};
our $edit_menuitem = {"container" => $address_book_unnamed_menubar, "text" => "Edit", "type" => "MenuItem"};
our $edit_add_menuitem = {"container" => $edit_menuitem, "text" => "Add...", "type" => "MenuItem"};
our $edit_menuitem_2 = {"container" => $address_book_myaddresses_adr_menubar, "text" => "Edit", "type" => "MenuItem"};
our $file_menuitem = {"container" => $address_book_unnamed_menubar, "text" => "File", "type" => "MenuItem"};
our $file_new_menuitem = {"container" => $file_menuitem, "text" => "New", "type" => "MenuItem"};
our $file_menuitem_2 = {"container" => $address_book_myaddresses_adr_menubar, "text" => "File", "type" => "MenuItem"};
our $o_edit = {"container" => $address_book_myaddresses_adr_table, "type" => "Edit"};
1;
