require 'names.pl';

sub main
{
    startApplication("Addressbook");
    my $table = waitForObject($Names::address_book_unnamed_table);
    invokeMenuItem("File", "New");
    test::verify($table->rowCount == 0);
    my $limit = 10; # To avoid testing 100s of rows since that would be boring
    my @records = testData::dataset("MyAddresses.tsv");
    my $row = 0;
    for (; $row < scalar(@records); ++$row) {
        my $record = $records[$row];
        my $forename = testData::field($record, "Forename");
        my $surname = testData::field($record, "Surname");
        my $email = testData::field($record, "Email");
        my $phone = testData::field($record, "Phone");
        addNameAndAddress($forename, $surname, $email, $phone);
        my $insertionRow = $row < 1 ? 1 : $row;
        checkNameAndAddress($insertionRow, $record);
        if ($row > $limit) {
            last;
        }
    }
    test::compare($table->rowCount, $row + 1);
    closeWithoutSaving();
}

sub invokeMenuItem
{
    my ($menu, $item) = @_;
    mouseClick(waitForObjectItem({"type" => "Menubar"}, $menu));
    mouseClick(waitForObject({'type' => 'MenuItem', 'text' => $item}));
}

sub addNameAndAddress
{
    my (@oneNameAndAddress) = @_;
    invokeMenuItem("Edit", "Add...");
    type(waitForObject($Names::address_book_add_forename_edit), $oneNameAndAddress[0]);
    type(waitForObject($Names::address_book_add_surname_edit), $oneNameAndAddress[1]);
    type(waitForObject($Names::address_book_add_email_edit), $oneNameAndAddress[2]);
    type(waitForObject($Names::address_book_add_phone_edit), $oneNameAndAddress[3]);
    clickButton(waitForObject($Names::address_book_add_ok_button));
}

sub checkNameAndAddress
{
    my($insertionRow, $record) = @_;
    my @columnNames = testData::fieldNames($record);
    for (my $column = 1; $column <= scalar(@columnNames); $column++) {
        my $cell = waitForObject({'container' => $Names::table, 'row' => $insertionRow, 'column'=> $column, 'type' => 'TableCell'});
        test::compare($cell->text, testData::field($record, $column - 1));
    }
}

sub closeWithoutSaving
{
    invokeMenuItem("File", "Quit");
    clickButton(waitForObject($Names::address_book_no_button));
}
