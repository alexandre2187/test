require 'names.pl';

sub main
{
    startApplication("Addressbook");
    mouseClick(waitForObjectItem($Names::address_book_unnamed_menubar, "File"));
    mouseClick(waitForObjectItem($Names::file_menuitem, "Open..."));
    type(waitForObject($Names::address_book_choose_file_edit),
        squishinfo->testCase . "\\..\\..\\MyAddresses.adr");
    type(waitForObject($Names::address_book_choose_file_edit), "<Return>");
    mouseClick(waitForObject($Names::o2_2_tablecell));
    mouseClick(waitForObjectItem($Names::address_book_myaddresses_adr_menubar, "Edit"));
    mouseClick(waitForObjectItem($Names::edit_menuitem_2, "Add..."));
    type(waitForObject($Names::address_book_add_forename_edit), "Jane");
    type(waitForObject($Names::address_book_add_forename_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_surname_edit), "Doe");
    type(waitForObject($Names::address_book_add_surname_edit), "<Tab>");
    type(waitForObject($Names::address_book_add_email_edit),"jane.doe\@nowhere.com");
    type(waitForObject($Names::address_book_add_email_edit),"<Tab>");
    type(waitForObject($Names::address_book_add_phone_edit), "555 123 4567");
    clickButton(waitForObject($Names::address_book_add_ok_button));
    mouseClick(waitForObject($Names::o4_2_tablecell));
    type(waitForObject($Names::address_book_myaddresses_adr_table), "Doe");
    type(waitForObject($Names::address_book_myaddresses_adr_table), "<Return>");
    mouseClick(waitForObject($Names::o1_2_tablecell));
    mouseClick(waitForObjectItem($Names::address_book_myaddresses_adr_menubar,"Edit"));
    mouseClick(waitForObjectItem($Names::edit_menuitem_2, "Remove..."));
    clickButton(waitForObject($Names::address_book_delete_yes_button));
    test::compare(waitForObjectExists($Names::o1_1_tablecell)->text, "Jane");
    test::compare(waitForObjectExists($Names::o1_2_tablecell)->text, "Doe");
    test::compare(waitForObjectExists($Names::o1_3_tablecell)->text, "jane.doe\@nowhere.com");
    test::compare(waitForObjectExists($Names::o1_4_tablecell)->text, "555 123 4567");
    my $table = waitForObject($Names::address_book_myaddresses_adr_table);
    test::compare($table->rowCount, 125);
    mouseClick(waitForObject($Names::address_book_myaddresses_adr_window),
        287, 11, MouseButton::LeftButton);
    mouseClick(waitForObjectItem($Names::address_book_myaddresses_adr_menubar,
        "File"));
    mouseClick(waitForObjectItem($Names::file_menuitem_2, "Quit"));
    clickButton(waitForObject($Names::address_book_no_button));
}
