require 'names.pl';

=begin comment A quick introduction to implementing scripts for BDD tests:

This file contains snippets of script code to be executed as the .feature
file is processed. See the section 'Behaviour Driven Testing' in the 'API
Reference Manual' chapter of the Squish manual for a comprehensive reference.

The functions Given/When/Then/Step can be used to associate a script snippet
with a pattern which is matched against the steps being executed. Optional
table/multi-line string arguments of the step are passed via a mandatory
'context' parameter:

  When("I enter the text", sub {
    my %context = %{shift()};
    <code here>
  });

The pattern is a plain string without the leading keyword, but a couple of
placeholders including |any|, |word| and |integer| are supported which can be
used to extract arbitrary, alphanumeric and integer values resp. from the
pattern; the extracted values are passed as additional arguments:

  Then("I get |integer| different names", sub {
    my %context = %{shift()};
    my $numNames = shift();
    <code here>
  });

Instead of using a string with placeholders, a regular expression object can
be passed to Given/When/Then/Step to use regular expressions.
=end comment
=cut

use warnings;
use strict;
package main;

1;



Given("addressbook application is running", sub {
    my $context = shift;
    startApplication("Addressbook");
    test::compare(waitForObjectExists($Names::address_book_unnamed_window)->enabled, 1);
});

When("I create a new addressbook", sub {
    my $context = shift;
    clickButton(waitForObject($Names::new_toolbaritem));
});

Then("addressbook should have zero entries", sub {
    my $context = shift;
    test::compare(waitForObjectExists($Names::address_book_unnamed_table)->rowCount, 0);
});

When("I add a new person '|word|','|word|','|any|','|integer|' to address book", sub {
    my $context = shift;
    my ($forename, $surname, $email, $phone) = @_;
    clickButton(waitForObject($Names::add_toolbaritem));
    type(waitForObject($Names::address_book_add_forename_edit), $forename);
    type(waitForObject($Names::address_book_add_surname_edit), $surname);
    type(waitForObject($Names::address_book_add_email_edit), $email);
    type(waitForObject($Names::address_book_add_phone_edit), $phone);
    clickButton(waitForObject($Names::address_book_add_ok_button));
    $context->{'userData'} = {};
    $context->{'userData'}{'forename'} = $forename;
    $context->{'userData'}{'surname'} = $surname;
});

When("I add new persons to address book", sub {
    my %context = %{shift()};
    my @table = @{$context{'table'}};

    # Drop initial row with column headers
    shift(@table);

    for my $row (@table) {
        my ($forename, $surname, $email, $phone) = @{$row};
        clickButton(waitForObject($Names::add_toolbaritem));
        type(waitForObject($Names::address_book_add_forename_edit), $forename);
        type(waitForObject($Names::address_book_add_surname_edit), $surname);
        type(waitForObject($Names::address_book_add_email_edit), $email);
        type(waitForObject($Names::address_book_add_phone_edit), $phone);
        clickButton(waitForObject($Names::address_book_add_ok_button));
    }
});

Then("'|integer|' entries should be present", sub {
    my $context = shift;
    my ($entryCount) = @_;
    test::compare(waitForObjectExists($Names::address_book_unnamed_table)->rowCount, $entryCount);
});

Then("previously entered forename and surname shall be at the top", sub {
    my $context = shift;
    test::compare( waitForObject($Names::o1_1_tablecell)->text,  $context->{'userData'}{'forename'});
    test::compare( waitForObject($Names::o1_2_tablecell)->text,  $context->{'userData'}{'surname'});
});
