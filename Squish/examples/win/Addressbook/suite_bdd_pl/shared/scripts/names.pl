package Names;

use utf8;
use strict;
use warnings;
use Squish::ObjectMapHelper::ObjectName;

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'ObjectMapHelper' module.
#
our $address_book_unnamed_window = {"text" => "Address Book - Unnamed", "type" => "Window"};
our $address_book_unnamed_table = {"container" => $address_book_unnamed_window, "type" => "Table"};
our $o1_1_tablecell = {"column" => 1, "container" => $address_book_unnamed_table, "row" => 1, "type" => "TableCell"};
our $o1_2_tablecell = {"column" => 2, "container" => $address_book_unnamed_table, "row" => 1, "type" => "TableCell"};
our $address_book_unnamed_toolbar = {"container" => $address_book_unnamed_window, "type" => "Toolbar"};
our $add_toolbaritem = {"container" => $address_book_unnamed_toolbar, "text" => "Add", "type" => "ToolbarItem"};
our $address_book_add_window = {"text" => "Address Book - Add", "type" => "Window"};
our $address_book_add_email_label = {"container" => $address_book_add_window, "text" => "Email:", "type" => "Label"};
our $address_book_add_email_edit = {"container" => $address_book_add_window, "leftObject" => $address_book_add_email_label, "type" => "Edit"};
our $address_book_add_forename_label = {"container" => $address_book_add_window, "text" => "Forename:", "type" => "Label"};
our $address_book_add_forename_edit = {"container" => $address_book_add_window, "leftObject" => $address_book_add_forename_label, "type" => "Edit"};
our $address_book_add_ok_button = {"container" => $address_book_add_window, "text" => "OK", "type" => "Button"};
our $address_book_add_phone_label = {"container" => $address_book_add_window, "text" => "Phone:", "type" => "Label"};
our $address_book_add_phone_edit = {"container" => $address_book_add_window, "leftObject" => $address_book_add_phone_label, "type" => "Edit"};
our $address_book_add_surname_label = {"container" => $address_book_add_window, "text" => "Surname:", "type" => "Label"};
our $address_book_add_surname_edit = {"container" => $address_book_add_window, "leftObject" => $address_book_add_surname_label, "type" => "Edit"};
our $new_toolbaritem = {"container" => $address_book_unnamed_toolbar, "text" => "New", "type" => "ToolbarItem"};
1;
