import * as names from 'names.js';

function main()
{
    startApplication("Addressbook");
    var table = waitForObject(names.addressBookUnnamedTable);
    invokeMenuItem("File", "New");
    test.verify(table.rowCount == 0);
    var limit = 10; // To avoid testing 100s of rows since that would be boring
    var records = testData.dataset("MyAddresses.tsv");
    for (var row = 0; row < records.length; ++row) {
        var record = records[row];
        var forename = testData.field(record, "Forename");
        var surname = testData.field(record, "Surname");
        var email = testData.field(record, "Email");
        var phone = testData.field(record, "Phone");
        addNameAndAddress(new Array(forename, surname, email, phone));
        var insertionRow = row < 1 ? 1 : row;
        checkNameAndAddress(insertionRow, record);
        if (row > limit)
            break;
    }
    test.compare(table.rowCount, row + 1);
    closeWithoutSaving()
}

function invokeMenuItem(menu, item)
{
    mouseClick(waitForObjectItem({'type':'Menubar'}, menu));
    mouseClick(waitForObject({'type':'MenuItem', 'text': item}));
}

function addNameAndAddress(oneNameAndAddress)
{
    invokeMenuItem("Edit", "Add...");
    type(waitForObject(names.addressBookAddForenameEdit), oneNameAndAddress[0]);
    type(waitForObject(names.addressBookAddSurnameEdit), oneNameAndAddress[1]);
    type(waitForObject(names.addressBookAddEmailEdit), oneNameAndAddress[2]);
    type(waitForObject(names.addressBookAddPhoneEdit), oneNameAndAddress[3]);
    clickButton(waitForObject(names.addressBookAddOKButton));
}

function checkNameAndAddress(insertionRow, record)
{
    for (var column = 1; column <= testData.fieldNames(record).length;
            ++column) {
        var cell = waitForObject({'container' : names.table, 'row': insertionRow, 'column' : column, 'type' : 'TableCell'});
        test.compare(cell.text, testData.field(record, column - 1));
    }
}

function closeWithoutSaving()
{
    invokeMenuItem("File", "Quit");
    clickButton(waitForObject(names.addressBookNoButton));
}

