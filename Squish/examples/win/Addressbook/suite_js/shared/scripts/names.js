// See the chapter 'Script-Based Object Map API' in the Squish manual for
// documentation of the functionality offered by the 'objectmaphelper' module.
import { RegularExpression, Wildcard } from 'objectmaphelper.js';

export var addressBookMyAddressesAdrWindow = {"text": "Address Book - MyAddresses.adr", "type": "Window"};
export var addressBookMyAddressesAdrTable = {"container": addressBookMyAddressesAdrWindow, "type": "Table"};
export var table = {"type": "Table"};
export var o11TableCell = {"column": 1, "container": addressBookMyAddressesAdrTable, "row": 1, "type": "TableCell"};
export var o12TableCell = {"column": 2, "container": addressBookMyAddressesAdrTable, "row": 1, "type": "TableCell"};
export var addressBookUnnamedWindow = {"text": "Address Book - Unnamed", "type": "Window"};
export var addressBookUnnamedTable = {"container": addressBookUnnamedWindow, "type": "Table"};
export var o12TableCell_2 = {"column": 2, "container": addressBookUnnamedTable, "row": 1, "type": "TableCell"};
export var o13TableCell = {"column": 3, "container": addressBookMyAddressesAdrTable, "row": 1, "type": "TableCell"};
export var o14TableCell = {"column": 4, "container": addressBookMyAddressesAdrTable, "row": 1, "type": "TableCell"};
export var o22TableCell = {"column": 2, "container": addressBookMyAddressesAdrTable, "row": 2, "type": "TableCell"};
export var o42TableCell = {"column": 2, "container": addressBookMyAddressesAdrTable, "row": 4, "type": "TableCell"};
export var addressBookAddWindow = {"text": "Address Book - Add", "type": "Window"};
export var addressBookAddEmailLabel = {"container": addressBookAddWindow, "text": "Email:", "type": "Label"};
export var addressBookAddEmailEdit = {"container": addressBookAddWindow, "leftObject": addressBookAddEmailLabel, "type": "Edit"};
export var addressBookAddForenameLabel = {"container": addressBookAddWindow, "text": "Forename:", "type": "Label"};
export var addressBookAddForenameEdit = {"container": addressBookAddWindow, "leftObject": addressBookAddForenameLabel, "type": "Edit"};
export var addressBookAddOKButton = {"container": addressBookAddWindow, "text": "OK", "type": "Button"};
export var addressBookAddPhoneLabel = {"container": addressBookAddWindow, "text": "Phone:", "type": "Label"};
export var addressBookAddPhoneEdit = {"container": addressBookAddWindow, "leftObject": addressBookAddPhoneLabel, "type": "Edit"};
export var addressBookAddSurnameLabel = {"container": addressBookAddWindow, "text": "Surname:", "type": "Label"};
export var addressBookAddSurnameEdit = {"container": addressBookAddWindow, "leftObject": addressBookAddSurnameLabel, "type": "Edit"};
export var addressBookChooseFileDialog = {"text": "Address Book - Choose File", "type": "Dialog"};
export var addressBookChooseFileOpenCheckBox = {"container": addressBookChooseFileDialog, "text": "Open", "type": "CheckBox"};
export var addressBookChooseFileEdit = {"container": addressBookChooseFileDialog, "type": "Edit"};
export var addressBookChooseFileWindowsControl = {"class": "DirectUIHWND", "container": addressBookChooseFileDialog, "occurrence": 2, "type": "WindowsControl"};
export var addressBookDeleteWindow = {"text": "Address Book - Delete", "type": "Window"};
export var addressBookDeleteYesButton = {"container": addressBookDeleteWindow, "text": "Yes", "type": "Button"};
export var addressBookMyAddressesAdrMenubar = {"container": addressBookMyAddressesAdrWindow, "type": "Menubar"};
export var addressBookUnnamedMenubar = {"container": addressBookUnnamedWindow, "type": "Menubar"};
export var addressBookWindow = {"text": "Address Book", "type": "Window"};
export var addressBookNoButton = {"container": addressBookWindow, "text": "No", "type": "Button"};
export var editMenuItem = {"container": addressBookUnnamedMenubar, "text": "Edit", "type": "MenuItem"};
export var editAddMenuItem = {"container": editMenuItem, "text": "Add...", "type": "MenuItem"};
export var editMenuItem_2 = {"container": addressBookMyAddressesAdrMenubar, "text": "Edit", "type": "MenuItem"};
export var fileMenuItem = {"container": addressBookUnnamedMenubar, "text": "File", "type": "MenuItem"};
export var fileNewMenuItem = {"container": fileMenuItem, "text": "New", "type": "MenuItem"};
export var fileMenuItem_2 = {"container": addressBookMyAddressesAdrMenubar, "text": "File", "type": "MenuItem"};
export var edit = {"container": addressBookMyAddressesAdrTable, "type": "Edit"};
