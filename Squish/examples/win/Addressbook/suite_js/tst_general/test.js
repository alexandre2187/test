import * as names from 'names.js';

function main() {
    startApplication("Addressbook");
    mouseClick(waitForObjectItem(names.addressBookUnnamedMenubar, "File"));
    mouseClick(waitForObjectItem(names.fileMenuItem, "Open..."));
    type(waitForObject(names.addressBookChooseFileEdit),
        squishinfo.testCase + "\\..\\..\\MyAddresses.adr");
    type(waitForObject(names.addressBookChooseFileEdit), "<Return>");
    mouseClick(waitForObject(names.o22TableCell));
    mouseClick(waitForObjectItem(names.addressBookMyAddressesAdrMenubar, "Edit"));
    mouseClick(waitForObjectItem(names.editMenuItem_2, "Add..."));
    type(waitForObject(names.addressBookAddForenameEdit), "Jane");
    type(waitForObject(names.addressBookAddForenameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddSurnameEdit), "Doe");
    type(waitForObject(names.addressBookAddSurnameEdit), "<Tab>");
    type(waitForObject(names.addressBookAddEmailEdit),"jane.doe@nowhere.com");
    type(waitForObject(names.addressBookAddEmailEdit),"<Tab>");
    type(waitForObject(names.addressBookAddPhoneEdit), "555 123 4567");
    clickButton(waitForObject(names.addressBookAddOKButton));
    mouseClick(waitForObject(names.o42TableCell));
    type(waitForObject(names.addressBookMyAddressesAdrTable), "Doe");
    type(waitForObject(names.addressBookMyAddressesAdrTable), "<Return>");
    mouseClick(waitForObject(names.o12TableCell));
    mouseClick(waitForObjectItem(names.addressBookMyAddressesAdrMenubar,"Edit"));
    mouseClick(waitForObjectItem(names.editMenuItem_2, "Remove..."));
    clickButton(waitForObject(names.addressBookDeleteYesButton));
    test.compare(waitForObjectExists(names.o11TableCell).text, "Jane");
    test.compare(waitForObjectExists(names.o12TableCell).text, "Doe");
    test.compare(waitForObjectExists(names.o13TableCell).text, "jane.doe@nowhere.com");
    test.compare(waitForObjectExists(names.o14TableCell).text, "555 123 4567");
    var table = waitForObject(names.addressBookMyAddressesAdrTable);
    test.compare(table.rowCount, 125);
    mouseClick(waitForObject(names.addressBookMyAddressesAdrWindow), 317, 19, MouseButton.LeftButton);
    mouseClick(waitForObjectItem(names.addressBookMyAddressesAdrMenubar, "File"));
    mouseClick(waitForObjectItem(names.fileMenuItem_2, "Quit"));
    clickButton(waitForObject(names.addressBookNoButton));
}

