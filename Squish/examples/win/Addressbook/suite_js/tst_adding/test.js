import * as names from 'names.js';

function main()
{
    startApplication("Addressbook");
    var table = waitForObject(names.addressBookUnnamedTable);
    invokeMenuItem("File", "New");
    test.verify(table.rowCount == 0);
    var data = new Array(new Array("Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"),
        new Array("Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"),
        new Array("Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654"));
    for (var row = 0; row < data.length; ++row)
        addNameAndAddress(data[row]);
    test.compare(table.rowCount,  data.length);
    closeWithoutSaving()
}

function invokeMenuItem(menu, item)
{
    mouseClick(waitForObjectItem({'type':'Menubar'}, menu));
    mouseClick(waitForObject({'type':'MenuItem', 'text': item}));
}

function addNameAndAddress(oneNameAndAddress)
{
    invokeMenuItem("Edit", "Add...");
    type(waitForObject(names.addressBookAddForenameEdit), oneNameAndAddress[0]);
    type(waitForObject(names.addressBookAddSurnameEdit), oneNameAndAddress[1]);
    type(waitForObject(names.addressBookAddEmailEdit), oneNameAndAddress[2]);
    type(waitForObject(names.addressBookAddPhoneEdit), oneNameAndAddress[3]);
    clickButton(waitForObject(names.addressBookAddOKButton));
}

function closeWithoutSaving()
{
    invokeMenuItem("File", "Quit");
    clickButton(waitForObject(names.addressBookNoButton));
}
