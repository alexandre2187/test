require 'names';

# encoding: UTF-8

# A quick introduction to implementing scripts for BDD tests:
#
# This file contains snippets of script code to be executed as the .feature
# file is processed. See the section 'Behaviour Driven Testing' in the 'API
# Reference Manual' chapter of the Squish manual for a comprehensive reference.
#
# The functions Given/When/Then/Step can be used to associate a script snippet
# with a pattern which is matched against the steps being executed. Optional
# table/multi-line string arguments of the step are passed via a mandatory
# 'context' parameter:
#
#   When("I enter the text") do |context|
#     <code here>
#   end
#
# The pattern is a plain string without the leading keyword, but a couple of
# placeholders including |any|, |word| and |integer| are supported which can be
# used to extract arbitrary, alphanumeric and integer values resp. from the
# pattern; the extracted values are passed as additional arguments:
#
#   Then("I get |integer| different names") do |context, numNames|
#     <code here>
#   end
#
# Instead of using a string with placeholders, a regular expression object can
# be passed to Given/When/Then/Step to use regular expressions.
#

require 'squish'

include Squish

Given("addressbook application is running") do |context|
    startApplication("Addressbook")
    Test.compare(waitForObjectExists(Names::Address_Book_Unnamed_Window).enabled, true)
end

When("I create a new addressbook") do |context|
    clickButton(waitForObject(Names::New_ToolbarItem))
end

Then("addressbook should have zero entries") do |context|
    Test.compare(waitForObjectExists(Names::Address_Book_Unnamed_Table).rowCount, 0)
end

When("I add a new person '|word|','|word|','|any|','|integer|' to address book") do |context, forename, surname, email, phone|
    mouseClick(waitForObject(Names::Add_ToolbarItem))
    type(waitForObject(Names::Address_Book_Add_Forename_Edit),  forename)
    type(waitForObject(Names::Address_Book_Add_Surname_Edit), surname)
    type(waitForObject(Names::Address_Book_Add_Email_Edit), email)
    type(waitForObject(Names::Address_Book_Add_Phone_Edit), phone)
    clickButton(waitForObject(Names::Address_Book_Add_OK_Button))
    context.userData = Hash.new
    context.userData[:forename] = forename
    context.userData[:surname] = surname
end

When("I add new persons to address book") do |context|
    table = context.table
    # Drop initial row with column headers
    table.shift
    for forename, surname, email, phone in table do
        mouseClick(waitForObject(Names::Add_ToolbarItem))
        type(waitForObject(Names::Address_Book_Add_Forename_Edit),  forename)
        type(waitForObject(Names::Address_Book_Add_Surname_Edit), surname)
        type(waitForObject(Names::Address_Book_Add_Email_Edit), email)
        type(waitForObject(Names::Address_Book_Add_Phone_Edit), phone)
        clickButton(waitForObject(Names::Address_Book_Add_OK_Button))
    end
end

Then("'|integer|' entries should be present") do |context, entryCount|
    Test.compare(waitForObjectExists(Names::Address_Book_Unnamed_Table).rowCount, entryCount)
end

Then("previously entered forename and surname shall be at the top") do |context|
    Test.compare(waitForObjectExists(Names::O1_1_TableCell).text, context.userData[:forename])
    Test.compare(waitForObjectExists(Names::O1_2_TableCell).text, context.userData[:surname])
end
