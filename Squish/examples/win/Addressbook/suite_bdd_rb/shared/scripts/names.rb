# encoding: UTF-8

require 'squish/objectmaphelper'
# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

module Names

include Squish::ObjectMapHelper

Address_Book_Unnamed_Window = {:text => "Address Book - Unnamed", :type => "Window"}
Address_Book_Unnamed_Table = {:container => Address_Book_Unnamed_Window, :type => "Table"}
O1_1_TableCell = {:column => 1, :container => Address_Book_Unnamed_Table, :row => 1, :type => "TableCell"}
O1_2_TableCell = {:column => 2, :container => Address_Book_Unnamed_Table, :row => 1, :type => "TableCell"}
Address_Book_Unnamed_Toolbar = {:container => Address_Book_Unnamed_Window, :type => "Toolbar"}
Add_ToolbarItem = {:container => Address_Book_Unnamed_Toolbar, :text => "Add", :type => "ToolbarItem"}
Address_Book_Add_Window = {:text => "Address Book - Add", :type => "Window"}
Address_Book_Add_Email_Label = {:container => Address_Book_Add_Window, :text => "Email:", :type => "Label"}
Address_Book_Add_Email_Edit = {:container => Address_Book_Add_Window, :leftObject => Address_Book_Add_Email_Label, :type => "Edit"}
Address_Book_Add_Forename_Label = {:container => Address_Book_Add_Window, :text => "Forename:", :type => "Label"}
Address_Book_Add_Forename_Edit = {:container => Address_Book_Add_Window, :leftObject => Address_Book_Add_Forename_Label, :type => "Edit"}
Address_Book_Add_OK_Button = {:container => Address_Book_Add_Window, :text => "OK", :type => "Button"}
Address_Book_Add_Phone_Label = {:container => Address_Book_Add_Window, :text => "Phone:", :type => "Label"}
Address_Book_Add_Phone_Edit = {:container => Address_Book_Add_Window, :leftObject => Address_Book_Add_Phone_Label, :type => "Edit"}
Address_Book_Add_Surname_Label = {:container => Address_Book_Add_Window, :text => "Surname:", :type => "Label"}
Address_Book_Add_Surname_Edit = {:container => Address_Book_Add_Window, :leftObject => Address_Book_Add_Surname_Label, :type => "Edit"}
New_ToolbarItem = {:container => Address_Book_Unnamed_Toolbar, :text => "New", :type => "ToolbarItem"}
end
