import * as names from 'names.js';

// A quick introduction to implementing scripts for BDD tests:
//
// This file contains snippets of script code to be executed as the .feature
// file is processed. See the section 'Behaviour Driven Testing' in the 'API
// Reference Manual' chapter of the Squish manual for a comprehensive reference.
//
// The functions Given/When/Then/Step can be used to associate a script snippet
// with a pattern which is matched against the steps being executed. Optional
// table/multi-line string arguments of the step are passed via a mandatory
// 'context' parameter:
//
//   When("I enter the text", function(context) {
//     <code here>
//   });
//
// The pattern is a plain string without the leading keyword, but a couple of
// placeholders including |any|, |word| and |integer| are supported which can
// be used to extract arbitrary, alphanumeric and integer values resp. from the
// pattern; the extracted values are passed as additional arguments:
//
//   Then("I get |integer| different names", function(context, numNames) {
//     <code here>
//   });
//
// Instead of using a string with placeholders, a regular expression object can
// be passed to Given/When/Then/Step to use regular expressions.
//

Given("addressbook application is running", function(context) {
    startApplication("Addressbook");
    test.compare(waitForObjectExists(names.addressBookUnnamedWindow).enabled, true);
});

When("I create a new addressbook", function(context) {
    mouseClick(waitForObject(names.newToolbarItem));
});

Then("addressbook should have zero entries", function(context) {
    test.compare(waitForObjectExists(names.addressBookUnnamedTable).rowCount, 0);
});

When("I add a new person '|word|','|word|','|any|','|integer|' to address book", function(context, forename, surname, email, phone) {
    mouseClick(waitForObject(names.addToolbarItem));
    type(waitForObject(names.addressBookAddForenameEdit), forename);
    type(waitForObject(names.addressBookAddSurnameEdit), surname);
    type(waitForObject(names.addressBookAddEmailEdit), email);
    type(waitForObject(names.addressBookAddPhoneEdit), phone);
    clickButton(waitForObject(names.addressBookAddOKButton));
    context.userData["forename"] = forename;
    context.userData["surname"] = surname;
});

When("I add new persons to address book", function(context) {
    var table = context.table;
    // Drop initial row with column headers
    for (var i = 1; i < table.length; ++i) {
        var forename = table[i][0];
        var surname = table[i][1];
        var email = table[i][2];
        var phone = table[i][3];
        mouseClick(waitForObject(names.addToolbarItem));
        type(waitForObject(names.addressBookAddForenameEdit), forename);
        type(waitForObject(names.addressBookAddSurnameEdit), surname);
        type(waitForObject(names.addressBookAddEmailEdit), email);
        type(waitForObject(names.addressBookAddPhoneEdit), phone);
        clickButton(waitForObject(names.addressBookAddOKButton));
    }
});

Then("'|integer|' entries should be present", function(context, entryCount) {
    test.compare(waitForObjectExists(names.addressBookUnnamedTable).rowCount, entryCount);
});

Then("previously entered forename and surname shall be at the top", function(context) {
    test.compare(waitForObjectExists(names.o11TableCell).text, context.userData["forename"]);
    test.compare(waitForObjectExists(names.o12TableCell).text, context.userData["surname"]);
});
