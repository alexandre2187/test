// See the chapter 'Script-Based Object Map API' in the Squish manual for
// documentation of the functionality offered by the 'objectmaphelper' module.
import { RegularExpression, Wildcard } from 'objectmaphelper.js';

export var addressBookUnnamedWindow = {"text": "Address Book - Unnamed", "type": "Window"};
export var addressBookUnnamedTable = {"container": addressBookUnnamedWindow, "type": "Table"};
export var o11TableCell = {"column": 1, "container": addressBookUnnamedTable, "row": 1, "type": "TableCell"};
export var o12TableCell = {"column": 2, "container": addressBookUnnamedTable, "row": 1, "type": "TableCell"};
export var addressBookUnnamedToolbar = {"container": addressBookUnnamedWindow, "type": "Toolbar"};
export var addToolbarItem = {"container": addressBookUnnamedToolbar, "text": "Add", "type": "ToolbarItem"};
export var addressBookAddWindow = {"text": "Address Book - Add", "type": "Window"};
export var addressBookAddEmailLabel = {"container": addressBookAddWindow, "text": "Email:", "type": "Label"};
export var addressBookAddEmailEdit = {"container": addressBookAddWindow, "leftObject": addressBookAddEmailLabel, "type": "Edit"};
export var addressBookAddForenameLabel = {"container": addressBookAddWindow, "text": "Forename:", "type": "Label"};
export var addressBookAddForenameEdit = {"container": addressBookAddWindow, "leftObject": addressBookAddForenameLabel, "type": "Edit"};
export var addressBookAddOKButton = {"container": addressBookAddWindow, "text": "OK", "type": "Button"};
export var addressBookAddPhoneLabel = {"container": addressBookAddWindow, "text": "Phone:", "type": "Label"};
export var addressBookAddPhoneEdit = {"container": addressBookAddWindow, "leftObject": addressBookAddPhoneLabel, "type": "Edit"};
export var addressBookAddSurnameLabel = {"container": addressBookAddWindow, "text": "Surname:", "type": "Label"};
export var addressBookAddSurnameEdit = {"container": addressBookAddWindow, "leftObject": addressBookAddSurnameLabel, "type": "Edit"};
export var newToolbarItem = {"container": addressBookUnnamedToolbar, "text": "New", "type": "ToolbarItem"};
