source [findFile "scripts" "names.tcl"]

proc main {} {
    startApplication "Addressbook"
    invoke mouseClick [waitForObjectItem $names::Address_Book_Unnamed_Menubar "File"]
    invoke mouseClick [waitForObjectItem $names::File_MenuItem "Open..."]
    set testcase [squishinfo testCase]
    invoke type [waitForObject $names::Address_Book_Choose_File_Edit] "$testcase\\..\\..\\MyAddresses.adr"
    invoke type [waitForObject $names::Address_Book_Choose_File_Edit] "<Return>"
    invoke mouseClick [waitForObject $names::2_2_TableCell]
    invoke mouseClick [waitForObjectItem $names::Address_Book_MyAddresses_adr_Menubar "Edit"]
    invoke mouseClick [waitForObjectItem $names::Edit_MenuItem_2 "Add..."]
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] "Jane"
	invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] "Doe"
        invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "jane.doe@nowhere.com"
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] "<Tab>"
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] "555 123 4567"
    invoke clickButton [waitForObject $names::Address_Book_Add_OK_Button]
    invoke mouseClick [waitForObject $names::4_2_TableCell]
    invoke type [waitForObject $names::Address_Book_MyAddresses_adr_Table_2] "Doe"
    invoke type [waitForObject $names::Address_Book_MyAddresses_adr_Table_2] "<Return>"
    invoke mouseClick [waitForObject $names::1_2_TableCell]
    invoke mouseClick [waitForObjectItem $names::Address_Book_MyAddresses_adr_Menubar "Edit"]
    invoke mouseClick [waitForObjectItem $names::Edit_MenuItem_2 "Remove..."]
    invoke clickButton [waitForObject $names::Address_Book_Delete_Yes_Button]

    test compare [property get [waitForObjectExists $names::1_1_TableCell] text] "Jane"
    test compare [property get [waitForObjectExists $names::1_2_TableCell] text] "Doe"
    test compare [property get [waitForObjectExists $names::1_3_TableCell] text] "jane.doe@nowhere.com"
    test compare [property get [waitForObjectExists $names::1_4_TableCell] text] "555 123 4567"

    set table [waitForObject $names::Address_Book_MyAddresses_adr_Table_2]
    test compare [property get $table rowCount] 125
    invoke mouseClick [waitForObject $names::Address_Book_MyAddresses_adr_Window_2] \
        287 11 [enum MouseButton LeftButton]
    invoke mouseClick [waitForObjectItem \
        $names::Address_Book_MyAddresses_adr_Menubar "File"]
    invoke mouseClick [waitForObjectItem $names::File_MenuItem_2 "Quit"]
    invoke clickButton [waitForObject $names::Address_Book_No_Button]
}
