package require squish::objectmaphelper
# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

namespace eval ::names {
set Address_Book_MyAddresses_adr_Window [::Squish::ObjectName text {Address Book - *MyAddresses.adr} type Window]
set Address_Book_MyAddresses_adr_Table [::Squish::ObjectName container $Address_Book_MyAddresses_adr_Window type Table]
set Table [::Squish::ObjectName type Table]
set 0_0_TableCell [::Squish::ObjectName column 0 container $Address_Book_MyAddresses_adr_Table row 0 type TableCell]
set 0_1_TableCell [::Squish::ObjectName column 1 container $Address_Book_MyAddresses_adr_Table row 0 type TableCell]
set 0_2_TableCell [::Squish::ObjectName column 2 container $Address_Book_MyAddresses_adr_Table row 0 type TableCell]
set 0_3_TableCell [::Squish::ObjectName column 3 container $Address_Book_MyAddresses_adr_Table row 0 type TableCell]
set Address_Book_MyAddresses_adr_Window_2 [::Squish::ObjectName text {Address Book - MyAddresses.adr} type Window]
set Address_Book_MyAddresses_adr_Table_2 [::Squish::ObjectName container $Address_Book_MyAddresses_adr_Window_2 type Table]
set 1_1_TableCell [::Squish::ObjectName column 1 container $Address_Book_MyAddresses_adr_Table_2 row 1 type TableCell]
set 1_2_TableCell [::Squish::ObjectName column 2 container $Address_Book_MyAddresses_adr_Table_2 row 1 type TableCell]
set Address_Book_Unnamed_Window [::Squish::ObjectName text {Address Book - Unnamed} type Window]
set Address_Book_Unnamed_Table [::Squish::ObjectName container $Address_Book_Unnamed_Window type Table]
set 1_2_TableCell_2 [::Squish::ObjectName column 2 container $Address_Book_Unnamed_Table row 1 type TableCell]
set 1_3_TableCell [::Squish::ObjectName column 3 container $Address_Book_MyAddresses_adr_Table_2 row 1 type TableCell]
set 1_4_TableCell [::Squish::ObjectName column 4 container $Address_Book_MyAddresses_adr_Table_2 row 1 type TableCell]
set 2_2_TableCell [::Squish::ObjectName column 2 container $Address_Book_MyAddresses_adr_Table_2 row 2 type TableCell]
set 4_2_TableCell [::Squish::ObjectName column 2 container $Address_Book_MyAddresses_adr_Table_2 row 4 type TableCell]
set Address_Book_Add_Window [::Squish::ObjectName text {Address Book - Add} type Window]
set Address_Book_Add_Forename_Label [::Squish::ObjectName container $Address_Book_Add_Window text Forename: type Label]
set Address_Book_Add_Forename_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject $Address_Book_Add_Forename_Label type Edit]
set Open_Addresslist_Dialog [::Squish::ObjectName text {Open Addresslist} type Dialog]
set Open_Addresslist_Edit [::Squish::ObjectName container $Open_Addresslist_Dialog type Edit]
set Address_Book_Add_Email_Label [::Squish::ObjectName container $Address_Book_Add_Window text Email: type Label]
set Address_Book_Add_Email_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject $Address_Book_Add_Email_Label type Edit]
set Address_Book_Add_OK_Button [::Squish::ObjectName container $Address_Book_Add_Window text OK type Button]
set Address_Book_Add_Phone_Label [::Squish::ObjectName container $Address_Book_Add_Window text Phone: type Label]
set Address_Book_Add_Phone_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject $Address_Book_Add_Phone_Label type Edit]
set Address_Book_Add_Surname_Label [::Squish::ObjectName container $Address_Book_Add_Window text Surname: type Label]
set Address_Book_Add_Surname_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject $Address_Book_Add_Surname_Label type Edit]
set Address_Book_Choose_File_Dialog [::Squish::ObjectName text {Address Book - Choose File} type Dialog]
set Address_Book_Choose_File_Open_CheckBox [::Squish::ObjectName container $Address_Book_Choose_File_Dialog text Open type CheckBox]
set Address_Book_Choose_File_Edit [::Squish::ObjectName container $Address_Book_Choose_File_Dialog type Edit]
set Address_Book_Choose_File_WindowsControl [::Squish::ObjectName class DirectUIHWND container $Address_Book_Choose_File_Dialog occurrence 2 type WindowsControl]
set Address_Book_Delete_Window [::Squish::ObjectName text {Address Book - Delete} type Window]
set Address_Book_Delete_Yes_Button [::Squish::ObjectName container $Address_Book_Delete_Window text Yes type Button]
set Address_Book_MyAddresses_adr_Menubar [::Squish::ObjectName container $Address_Book_MyAddresses_adr_Window_2 type Menubar]
set Address_Book_Unnamed_Menubar [::Squish::ObjectName container $Address_Book_Unnamed_Window type Menubar]
set Address_Book_Window [::Squish::ObjectName text {Address Book} type Window]
set Address_Book_No_Button [::Squish::ObjectName container $Address_Book_Window text No type Button]
set Edit_MenuItem [::Squish::ObjectName container $Address_Book_Unnamed_Menubar text Edit type MenuItem]
set Edit_Add_MenuItem [::Squish::ObjectName container $Edit_MenuItem text Add... type MenuItem]
set Edit_MenuItem_2 [::Squish::ObjectName container $Address_Book_MyAddresses_adr_Menubar text Edit type MenuItem]
set File_MenuItem [::Squish::ObjectName container $Address_Book_Unnamed_Menubar text File type MenuItem]
set File_New_MenuItem [::Squish::ObjectName container $File_MenuItem text New type MenuItem]
set File_MenuItem_2 [::Squish::ObjectName container $Address_Book_MyAddresses_adr_Menubar text File type MenuItem]
set Edit [::Squish::ObjectName container $Address_Book_MyAddresses_adr_Table_2 type Edit]
}
