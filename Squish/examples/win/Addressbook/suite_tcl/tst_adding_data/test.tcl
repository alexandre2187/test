source [findFile "scripts" "names.tcl"]

proc main {} {
    startApplication "Addressbook"
    set table [waitForObject $names::Address_Book_Unnamed_Table]
    test compare [property get $table rowCount] 0
    invokeMenuItem "File" "New"
    set limit 10
    set data [testData dataset "MyAddresses.tsv"]
    set columns [llength [testData fieldNames [lindex $data 0]]]
    set row 0
    for {} {$row < [llength $data]} {incr row} {
        set record [lindex $data $row]
        set forename [testData field $record "Forename"]
        set surname [testData field $record "Surname"]
        set email [testData field $record "Email"]
        set phone [testData field $record "Phone"]
        set details [list $forename $surname $email $phone]
        addNameAndAddress $details
        set insertionRow $row
        if {$row < 1} {
            set insertionRow 1
        }
        checkNameAndAddress $insertionRow $record
        if {$row > $limit} {
            break
        }
    }
    test compare [property get $table rowCount] [expr $row + 1]
    closeWithoutSaving
}

proc invokeMenuItem {menu item} {
    invoke mouseClick [waitForObjectItem [::Squish::ObjectName type Menubar] $menu]
    invoke mouseClick [waitForObject [::Squish::ObjectName type MenuItem text $item]]
}

proc addNameAndAddress {oneNameAndAddress} {
    invokeMenuItem "Edit" "Add..."
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] [lindex $oneNameAndAddress 0]
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] [lindex $oneNameAndAddress 1]
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] [lindex $oneNameAndAddress 2]
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] [lindex $oneNameAndAddress 3]
    invoke clickButton [waitForObject $names::Address_Book_Add_OK_Button]
}

proc checkNameAndAddress {insertionRow record} {
    set columns [llength [testData fieldNames $record]]
    for {set column 1} {$column <= $columns} {incr column} {
        set cell [waitForObject [::Squish::ObjectName container $names::Table type TableCell row $insertionRow column $column]]
        test compare [property get $cell text] \
            [testData field $record [expr {$column - 1}]]
    }
}

proc closeWithoutSaving {} {
    invokeMenuItem "File" "Quit"
    invoke clickButton [waitForObject $names::Address_Book_No_Button]
}

