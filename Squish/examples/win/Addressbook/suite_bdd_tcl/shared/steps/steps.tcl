source [findFile "scripts" "names.tcl"]

# A quick introduction to implementing scripts for BDD tests:
#
# This file contains snippets of script code to be executed as the .feature
# file is processed. See the section 'Behaviour Driven Testing' in the 'API
# Reference Manual' chapter of the Squish manual for a comprehensive reference.
#
# The commands Given/When/Then/Step can be used to associate a script snippet
# with a pattern which is matched against the steps being executed. Optional
# table/multi-line string arguments of the step are passed via a mandatory
# 'context' parameter:
#
#   When "I enter the text" {context} {
#     <code here>
#   }
#
# The pattern is a plain string without the leading keyword, but a couple of
# placeholders including |any|, |word| and |integer| are supported which can be
# used to extract arbitrary, alphanumeric and integer values resp. from the
# pattern; the extracted values are passed as additional arguments:
#
#   Then "I get |integer| different names" {context numNames} {
#     <code here>
#   }
#
# Instead of using placeholders, a "-rx" switch can be passed as the first
# argument to Given/When/Then/Step to use regular expressions.
#

Given "addressbook application is running" {context} {
    startApplication "Addressbook"
    test compare [property get [waitForObjectExists  $names::Address_Book_Unnamed_Window] enabled] true
}

When "I create a new addressbook" {context} {
    invoke mouseClick [waitForObject $names::New_ToolbarItem]
    invoke mouseClick [waitForObject $names::New_ToolbarItem]
}

Then "addressbook should have zero entries" {context} {
    test compare [property get [waitForObjectExists $names::Address_Book_Unnamed_Table] rowCount] 0
}

When "I add a new person '|word|','|word|','|any|','|integer|' to address book" {context forename surname email phone} {
    invoke mouseClick [waitForObject $names::Add_ToolbarItem]
    invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] $forename
    invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] $surname
    invoke type [waitForObject $names::Address_Book_Add_Email_Edit] $email
    invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] $phone
    invoke clickButton [waitForObject $names::Address_Book_Add_OK_Button]
    $context userData [dict create forename $forename surname $surname]
}

When "I add new persons to address book" {context} {
    set table [$context table]
    # Drop initial row with column headers
    foreach row [lreplace $table 0 0] {
        foreach {forename surname email phone} $row break
        invoke clickButton [waitForObject $names::Add_ToolbarItem]
        invoke type [waitForObject $names::Address_Book_Add_Forename_Edit] $forename
        invoke type [waitForObject $names::Address_Book_Add_Surname_Edit] $surname
        invoke type [waitForObject $names::Address_Book_Add_Email_Edit] $email
        invoke type [waitForObject $names::Address_Book_Add_Phone_Edit] $phone
        invoke clickButton [waitForObject $names::Address_Book_Add_OK_Button]
    }
}

Then "'|integer|' entries should be present" {context entryCount} {
    test compare [property get [waitForObjectExists $names::Address_Book_Unnamed_Table] rowCount] $entryCount
}

Then "previously entered forename and surname shall be at the top" {context} {
    test compare [property get [waitForObjectExists $names::1_1_TableCell] text] [dict get [$context userData] forename]
    test compare [property get [waitForObjectExists $names::1_2_TableCell] text] [dict get [$context userData] surname]
}
