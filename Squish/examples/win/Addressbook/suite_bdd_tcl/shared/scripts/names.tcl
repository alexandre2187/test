package require squish::objectmaphelper
# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

namespace eval ::names {
set Address_Book_Unnamed_Window [::Squish::ObjectName text {Address Book - Unnamed} type Window]
set Address_Book_Unnamed_Table [::Squish::ObjectName container $Address_Book_Unnamed_Window type Table]
set 1_1_TableCell [::Squish::ObjectName column 1 container $Address_Book_Unnamed_Table row 1 type TableCell]
set 1_2_TableCell [::Squish::ObjectName column 2 container $Address_Book_Unnamed_Table row 1 type TableCell]
set Address_Book_Unnamed_Toolbar [::Squish::ObjectName container $Address_Book_Unnamed_Window type Toolbar]
set Add_ToolbarItem [::Squish::ObjectName container $Address_Book_Unnamed_Toolbar text Add type ToolbarItem]
set Address_Book_Add_Window [::Squish::ObjectName text {Address Book - Add} type Window]
set Address_Book_Add_Email_Label [::Squish::ObjectName container $Address_Book_Add_Window text Email: type Label]
set Address_Book_Add_Email_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject $Address_Book_Add_Email_Label type Edit]
set Address_Book_Add_Forename_Label [::Squish::ObjectName container $Address_Book_Add_Window text Forename: type Label]
set Address_Book_Add_Forename_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject $Address_Book_Add_Forename_Label type Edit]
set Address_Book_Add_OK_Button [::Squish::ObjectName container $Address_Book_Add_Window text OK type Button]
set Address_Book_Add_Phone_Label [::Squish::ObjectName container $Address_Book_Add_Window text Phone: type Label]
set Address_Book_Add_Phone_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject $Address_Book_Add_Phone_Label type Edit]
set Address_Book_Add_Surname_Label [::Squish::ObjectName container $Address_Book_Add_Window text Surname: type Label]
set Address_Book_Add_Surname_Edit [::Squish::ObjectName container $Address_Book_Add_Window leftObject $Address_Book_Add_Surname_Label type Edit]
set New_ToolbarItem [::Squish::ObjectName container $Address_Book_Unnamed_Toolbar text New type ToolbarItem]
}
