# encoding: UTF-8

from objectmaphelper import *

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

address_Book_Unnamed_Window = {"text": "Address Book - Unnamed", "type": "Window"}
address_Book_Unnamed_Table = {"container": address_Book_Unnamed_Window, "type": "Table"}
o1_1_TableCell = {"column": 1, "container": address_Book_Unnamed_Table, "row": 1, "type": "TableCell"}
o1_2_TableCell = {"column": 2, "container": address_Book_Unnamed_Table, "row": 1, "type": "TableCell"}
address_Book_Unnamed_Toolbar = {"container": address_Book_Unnamed_Window, "type": "Toolbar"}
add_ToolbarItem = {"container": address_Book_Unnamed_Toolbar, "text": "Add", "type": "ToolbarItem"}
address_Book_Add_Window = {"text": "Address Book - Add", "type": "Window"}
address_Book_Add_Email_Label = {"container": address_Book_Add_Window, "text": "Email:", "type": "Label"}
address_Book_Add_Email_Edit = {"container": address_Book_Add_Window, "leftObject": address_Book_Add_Email_Label, "type": "Edit"}
address_Book_Add_Forename_Label = {"container": address_Book_Add_Window, "text": "Forename:", "type": "Label"}
address_Book_Add_Forename_Edit = {"container": address_Book_Add_Window, "leftObject": address_Book_Add_Forename_Label, "type": "Edit"}
address_Book_Add_OK_Button = {"container": address_Book_Add_Window, "text": "OK", "type": "Button"}
address_Book_Add_Phone_Label = {"container": address_Book_Add_Window, "text": "Phone:", "type": "Label"}
address_Book_Add_Phone_Edit = {"container": address_Book_Add_Window, "leftObject": address_Book_Add_Phone_Label, "type": "Edit"}
address_Book_Add_Surname_Label = {"container": address_Book_Add_Window, "text": "Surname:", "type": "Label"}
address_Book_Add_Surname_Edit = {"container": address_Book_Add_Window, "leftObject": address_Book_Add_Surname_Label, "type": "Edit"}
new_ToolbarItem = {"container": address_Book_Unnamed_Toolbar, "text": "New", "type": "ToolbarItem"}
