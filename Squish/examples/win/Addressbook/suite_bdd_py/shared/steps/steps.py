# -*- coding: utf-8 -*-

import names

# A quick introduction to implementing scripts for BDD tests:
#
# This file contains snippets of script code to be executed as the .feature
# file is processed. See the section 'Behaviour Driven Testing' in the 'API
# Reference Manual' chapter of the Squish manual for a comprehensive reference.
#
# The decorators Given/When/Then/Step can be used to associate a script snippet
# with a pattern which is matched against the steps being executed. Optional
# table/multi-line string arguments of the step are passed via a mandatory
# 'context' parameter:
#
#   @When("I enter the text")
#   def whenTextEntered(context):
#      <code here>
#
# The pattern is a plain string without the leading keyword, but a couple of
# placeholders including |any|, |word| and |integer| are supported which can be
# used to extract arbitrary, alphanumeric and integer values resp. from the
# pattern; the extracted values are passed as additional arguments:
#
#   @Then("I get |integer| different names")
#   def namesReceived(context, numNames):
#      <code here>
#
# Instead of using a string with placeholders, a regular expression can be
# specified. In that case, make sure to set the (optional) 'regexp' argument
# to True.

@Given("addressbook application is running")
def step(context):
    startApplication("Addressbook")
    test.compare(waitForObjectExists(names.address_Book_Unnamed_Window).enabled, True)

@When("I create a new addressbook")
def step(context):
    mouseClick(waitForObject(names.new_ToolbarItem))

@Then("addressbook should have zero entries")
def step(context):
    test.compare(waitForObjectExists(names.address_Book_Unnamed_Table).rowCount, 0)

@When("I add a new person '|word|','|word|','|any|','|integer|' to address book")
def step(context, forename, surname, email, phone):
    clickButton(waitForObject(names.add_ToolbarItem))
    type(waitForObject(names.address_Book_Add_Forename_Edit), forename)
    type(waitForObject(names.address_Book_Add_Surname_Edit), surname)
    type(waitForObject(names.address_Book_Add_Email_Edit), email)
    type(waitForObject(names.address_Book_Add_Phone_Edit), phone)
    clickButton(waitForObject(names.address_Book_Add_OK_Button))
    context.userData = {}
    context.userData['forename'] = forename
    context.userData['surname'] = surname

@When("I add new persons to address book")
def step(context):
    table = context.table
    # Drop initial row with column headers
    table.pop(0)
    for (forename, surname, email, phone) in table:
        clickButton(waitForObject(names.add_ToolbarItem))
        type(waitForObject(names.address_Book_Add_Forename_Edit), forename)
        type(waitForObject(names.address_Book_Add_Surname_Edit), surname)
        type(waitForObject(names.address_Book_Add_Email_Edit), email)
        type(waitForObject(names.address_Book_Add_Phone_Edit), phone)
        clickButton(waitForObject(names.address_Book_Add_OK_Button))

@Then("previously entered forename and surname shall be at the top")
def step(context):
    test.compare(waitForObjectExists(names.o1_1_TableCell).text, context.userData['forename']);
    test.compare(waitForObjectExists(names.o1_2_TableCell).text, context.userData['surname']);

@Then("'|integer|' entries should be present")
def step(context, entryCount):
    test.compare(waitForObjectExists(names.address_Book_Unnamed_Table).rowCount, entryCount)

# eof
