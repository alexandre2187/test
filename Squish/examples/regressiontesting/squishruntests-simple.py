#!/usr/bin/env python
# For a script with more options and configurability, see 'squish6runtests.py'
# This script assumes squishrunner and squishserver are both in your PATH

import os, sys, subprocess, time

def is_windows():
    platform = sys.platform.lower()
    return platform.startswith("win") or platform.startswith("microsoft")

# Create a dated directory for the test results:
LOGDIRECTORY = os.path.normpath(os.path.expanduser("~/results-%s" % time.strftime("%Y-%m-%d")))
if not os.path.exists(LOGDIRECTORY):
    os.makedirs(LOGDIRECTORY)

# open file for capturing server log output:
serverlog = open(os.path.join(LOGDIRECTORY, "server.log"), "w+")

# start the squishserver with verbose output in background:
pipe = subprocess.Popen(["squishserver", "--verbose"],
            stdout=serverlog, stderr=subprocess.STDOUT, shell=True)
time.sleep(1)
# execute the test suite (and wait for it to finish):
exitcode = subprocess.call(["squishrunner", "--testsuite",
                            os.path.normpath(os.path.expanduser("~/suite_myapp")),
                            "--reportgen", "xml3.2,%s" % LOGDIRECTORY],  shell=is_windows())
if exitcode != 0:
    print ("ERROR: abnormal squishrunner exit. code: %d" % exitcode)

# terminate squishserver, close file:
pipe.terminate()
serverlog.close()
# eof
