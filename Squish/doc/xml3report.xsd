<?xml version="1.0"?>
<xs:schema
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        targetNamespace="http://www.froglogic.com/resources/schemas/xml3"
        xmlns="http://www.froglogic.com/resources/schemas/xml3"
        elementFormDefault="qualified">

    <xs:element name="SquishReport">
        <xs:complexType>
            <xs:sequence>
                <xs:choice maxOccurs="unbounded">
                    <xs:element name="test" type="testType" />
                    <xs:element name="skippedTest" type="skippedTestType" />
                </xs:choice>
            </xs:sequence>
            <xs:attribute name="version" use="required">
                <xs:simpleType>
                    <xs:restriction base="xs:decimal">
                        <xs:minInclusive value="3.0" />
                    </xs:restriction>
                </xs:simpleType>
            </xs:attribute>
        </xs:complexType>
    </xs:element>

    <xs:simpleType name="positiveIntegerOrMinusOne">
        <xs:restriction base="xs:integer">
            <xs:minInclusive value="-1"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="testType">
        <xs:sequence>
            <xs:element name="prolog" type="prologType"/>
            <xs:choice minOccurs="0" maxOccurs="unbounded">
                <xs:element name="verification" type="verificationType" />
                <xs:element name="message" type="messageType" />
                <xs:element name="test" type="testType" />
                <xs:element name="skippedTest" type="skippedTestType" />
                <xs:element name="attachment" type="attachmentType" />
            </xs:choice>
            <xs:element name="epilog" type="epilogType"/>
        </xs:sequence>
        <xs:attribute name="type" type="xs:string" />
    </xs:complexType>

    <xs:complexType name="skippedTestType">
        <xs:complexContent>
            <xs:extension base="prologType">
                <xs:attribute name="type" type="xs:string" />
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="prologType">
        <xs:sequence>
            <xs:element name="name">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:minLength value="1" />
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="summary" minOccurs="0">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:minLength value="1" />
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="description" minOccurs="0">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:minLength value="1" />
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="location" type="fileLocationType" />
        </xs:sequence>
        <xs:attribute name="time" type="xs:dateTime" use="required" />
    </xs:complexType>
    <xs:complexType name="verificationType">
        <xs:sequence>
            <xs:element name="location" type="fileWithLineLocationType" />
            <xs:choice>
                <xs:element name="screenshotVerificationResult" maxOccurs="unbounded" type="screenshotResultType" />
                <xs:element name="scriptedVerificationResult" maxOccurs="unbounded" type="scriptedResultType" />
                <xs:element name="tableVerificationResult" maxOccurs="unbounded" type="tableResultType" />
                <xs:element name="propertyVerificationResult" maxOccurs="unbounded" type="propertyResultType" />
            </xs:choice>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="messageType">
        <xs:sequence>
            <xs:element name="location" type="fileLocationType" />
            <xs:group ref="textDetailScreenshotGroup" />
        </xs:sequence>
        <xs:attribute name="time" type="xs:dateTime" use="required" />
        <xs:attribute name="type" use="required">
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:minLength value="1" />
                </xs:restriction>
            </xs:simpleType>
        </xs:attribute>
    </xs:complexType>
    <xs:complexType name="epilogType">
        <xs:attribute name="time" type="xs:dateTime" use="required" />
    </xs:complexType>


    <xs:group name="textDetailScreenshotGroup">
        <xs:sequence>
            <xs:element name="text">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:minLength value="1" />
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="detail" minOccurs="0">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:minLength value="1" />
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="screenshot" minOccurs="0" type="uriLocationType" />
        </xs:sequence>
    </xs:group>


    <xs:simpleType name="resultType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="PASS" />
            <xs:enumeration value="FAIL" />
            <xs:enumeration value="XPASS" />
            <xs:enumeration value="XFAIL" />
        </xs:restriction>
    </xs:simpleType>


    <xs:complexType name="propertyResultType">
        <xs:sequence>
            <xs:element name="objectPropertyLocation" type="objectPropertyLocationType" />
            <xs:group ref="textDetailScreenshotGroup" />
            <xs:element name="expectedValue" type="xs:string" />
            <xs:element name="actualValue" type="xs:string" />
        </xs:sequence>
        <xs:attribute name="time" type="xs:dateTime" use="required" />
        <xs:attribute name="type" type="resultType" use="required" />
    </xs:complexType>
    <xs:complexType name="screenshotResultType">
        <xs:sequence>
            <xs:element name="objectLocation" type="objectLocationType" />
            <xs:group ref="textDetailScreenshotGroup" />
            <xs:element name="failedImage" type="uriLocationType" minOccurs="0" />
        </xs:sequence>
        <xs:attribute name="time" type="xs:dateTime" use="required" />
        <xs:attribute name="type" type="resultType" use="required" />
    </xs:complexType>
    <xs:complexType name="scriptedResultType">
        <xs:sequence>
            <xs:element name="scriptedLocation" type="fileWithLineLocationType" />
            <xs:group ref="textDetailScreenshotGroup" />
        </xs:sequence>
        <xs:attribute name="time" type="xs:dateTime" use="required" />
        <xs:attribute name="type" type="resultType" use="required" />
    </xs:complexType>
    <xs:complexType name="tableResultType">
        <xs:sequence>
            <xs:element name="objectLocation" type="objectLocationType" />
            <xs:group ref="textDetailScreenshotGroup" />
            <xs:element name="tablediff" type="tableDiffType" minOccurs="0" />
        </xs:sequence>
        <xs:attribute name="time" type="xs:dateTime" use="required" />
        <xs:attribute name="type" type="resultType" use="required" />
    </xs:complexType>


    <xs:complexType name="objectLocationType">
        <xs:complexContent>
            <xs:extension base="uriLocationType">
                <xs:sequence>
                    <xs:element name="objectName">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:minLength value="1" />
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="objectPropertyLocationType">
        <xs:complexContent>
            <xs:extension base="objectLocationType">
                <xs:sequence>
                    <xs:element name="property">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:minLength value="1" />
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="fileLocationType">
        <xs:complexContent>
            <xs:extension base="uriLocationType">
                <xs:sequence>
                    <xs:element minOccurs="0" name="lineNo" type="positiveIntegerOrMinusOne" />
                    <xs:element minOccurs="0" name="charIdx" type="positiveIntegerOrMinusOne" />
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="fileWithLineLocationType">
        <xs:complexContent>
            <xs:restriction base="fileLocationType">
                <xs:sequence>
                    <xs:element name="uri" type="nonEmptyUriType" />
                    <xs:element name="lineNo" type="positiveIntegerOrMinusOne" />
                    <xs:element minOccurs="0" name="charIdx" type="positiveIntegerOrMinusOne" />
                </xs:sequence>
            </xs:restriction>
        </xs:complexContent>
    </xs:complexType>


    <xs:complexType name="uriLocationType">
        <xs:sequence>
            <xs:element name="uri" type="nonEmptyUriType" />
        </xs:sequence>
    </xs:complexType>
    <xs:simpleType name="nonEmptyUriType">
        <xs:restriction base="xs:anyURI">
            <xs:minLength value="1" />
        </xs:restriction>
    </xs:simpleType>


    <xs:complexType name="tableDiffType">
        <xs:choice maxOccurs="unbounded">
            <xs:element name="context" type="diffElementType" />
            <xs:element name="insertion" type="diffElementType" />
            <xs:element name="deletion" type="diffElementType" />
        </xs:choice>
    </xs:complexType>
    <xs:complexType name="diffElementType">
        <xs:choice maxOccurs="1" minOccurs="1">
            <xs:element name="row" type="rowDiffType" />
            <xs:element name="column" type="columnDiffType" />
            <xs:element name="cell" type="cellDiffType" />
        </xs:choice>
    </xs:complexType>
    <xs:complexType name="columnDiffType">
        <xs:sequence>
            <xs:element name="cell" type="cellDiffType" maxOccurs="unbounded" />
        </xs:sequence>
        <xs:attribute name="index" type="xs:nonNegativeInteger" use="required" />
    </xs:complexType>
    <xs:complexType name="rowDiffType">
        <xs:sequence>
            <xs:element name="cell" type="cellDiffType" maxOccurs="unbounded" />
        </xs:sequence>
        <xs:attribute name="line" type="xs:nonNegativeInteger" use="required" />
    </xs:complexType>
    <xs:complexType name="cellDiffType">
        <xs:sequence>
            <xs:element name="property" maxOccurs="unbounded">
                <xs:complexType>
                    <xs:simpleContent>
                        <xs:extension base="xs:string">
                            <xs:attribute name="name" type="xs:string"/>
                        </xs:extension>
                    </xs:simpleContent>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="row" type="xs:nonNegativeInteger" />
        <xs:attribute name="column" type="xs:nonNegativeInteger" />
    </xs:complexType>

    <xs:complexType name="attachmentType">
        <xs:sequence>
            <xs:element name="uri" type="nonEmptyUriType" />
        </xs:sequence>
        <xs:attribute name="time" type="xs:dateTime" use="required" />
        <xs:attribute name="type" type="xs:string" />
    </xs:complexType>
</xs:schema>
