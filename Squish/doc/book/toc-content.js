var tocList = [{href:'book-about.html',id:'book-about',text:'Welcome to Squish!',refs:[],children:[]}
,{href:'relnotes.html',id:'relnotes',text:'Release Notes—for Upgraders',refs:[],children:[{href:'rel-641.html',id:'rel-641',text:'Version 6.4.1',refs:[],children:[{href:'rel-641.html#rel-641-general',id:'rel-641-general',text:'General',refs:[],children:[]}
,{href:'rel-641.html#rel-641-ide',id:'rel-641-ide',text:'Squish IDE-specific',refs:[],children:[]}
,{href:'rel-641.html#rel-641-android',id:'rel-641-android',text:'Android-specific',refs:[],children:[]}
,{href:'rel-641.html#rel-641-web',id:'rel-641-web',text:'Web-specific',refs:[],children:[]}
,{href:'rel-641.html#rel-641-qt',id:'rel-641-qt',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-641.html#rel-641-java',id:'rel-641-java',text:'Java-specific',refs:[],children:[]}
,{href:'rel-641.html#rel-641-source',id:'rel-641-source',text:'Source Packages',refs:[],children:[]}
]}
,{href:'rel-640.html',id:'rel-640',text:'Version 6.4',refs:[],children:[{href:'rel-640.html#rel-640-script-based-objectmap',id:'rel-640-script-based-objectmap',text:'Script-Based Object Maps',refs:[],children:[]}
,{href:'rel-640.html#rel-640-image-search',id:'rel-640-image-search',text:'Image-based Object Lookup',refs:[],children:[]}
,{href:'rel-640.html#rel-640-general',id:'rel-640-general',text:'General',refs:[],children:[]}
,{href:'rel-640.html#rel-640-ide',id:'rel-640-ide',text:'Squish IDE-specific',refs:[],children:[]}
,{href:'rel-640.html#rel-640-scripting',id:'rel-640-scripting',text:'Scripting',refs:[],children:[]}
,{href:'rel-640.html#rel-640-qt',id:'rel-640-qt',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-640.html#rel-640-java',id:'rel-640-java',text:'Java-specific',refs:[],children:[]}
,{href:'rel-640.html#rel-640-web',id:'rel-640-web',text:'Web-specific',refs:[],children:[]}
,{href:'rel-640.html#rel-640-windows',id:'rel-640-windows',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-640.html#rel-640-mac',id:'rel-640-mac',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-640.html#rel-640-ios',id:'rel-640-ios',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-640.html#rel-640-android',id:'rel-640-android',text:'Android-specific',refs:[],children:[]}
,{href:'rel-640.html#rel-640-source',id:'rel-640-source',text:'Source Packages',refs:[],children:[]}
,{href:'rel-640.html#rel-640-docs',id:'rel-640-docs',text:'Documentation',refs:[],children:[]}
,{href:'rel-640.html#rel-640-known-issues',id:'rel-640-known-issues',text:'Known Issues',refs:[],children:[]}
,{href:'rel-640.html#rel-640-platform',id:'rel-640-platform',text:'Platform Support',refs:[],children:[]}
]}
,{href:'rel-632.html',id:'rel-632',text:'Version 6.3.2',refs:[],children:[{href:'rel-632.html#rel-632-qt',id:'rel-632-qt',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-632.html#rel-632-java',id:'rel-632-java',text:'Java-specific',refs:[],children:[]}
,{href:'rel-632.html#rel-632-windows',id:'rel-632-windows',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-632.html#rel-632-android',id:'rel-632-android',text:'Android-specific',refs:[],children:[]}
]}
,{href:'rel-631.html',id:'rel-631',text:'Version 6.3.1',refs:[],children:[{href:'rel-631.html#rel-631-ide',id:'rel-631-ide',text:'Squish IDE-specific',refs:[],children:[]}
,{href:'rel-631.html#rel-631-scripting',id:'rel-631-scripting',text:'Scripting',refs:[],children:[]}
,{href:'rel-631.html#rel-631-qt',id:'rel-631-qt',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-631.html#rel-631-java',id:'rel-631-java',text:'Java-specific',refs:[],children:[]}
,{href:'rel-631.html#rel-631-web',id:'rel-631-web',text:'Web-specific',refs:[],children:[]}
,{href:'rel-631.html#rel-631-windows',id:'rel-631-windows',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-631.html#rel-631-mac',id:'rel-631-mac',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-631.html#rel-631-ios',id:'rel-631-ios',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-631.html#rel-631-source',id:'rel-631-source',text:'Source Packages',refs:[],children:[]}
]}
,{href:'rel-630.html',id:'rel-630',text:'Version 6.3',refs:[],children:[{href:'rel-630.html#rel-630ff-image-search',id:'rel-630ff-image-search',text:'Image Search',refs:[],children:[]}
,{href:'rel-630.html#rel-630ff-comparisons',id:'rel-630ff-comparisons',text:'Comparison of Text and XML Files',refs:[],children:[]}
,{href:'rel-630.html#rel-630-general',id:'rel-630-general',text:'General',refs:[],children:[]}
,{href:'rel-630.html#rel-630-ide',id:'rel-630-ide',text:'Squish IDE-specific',refs:[],children:[]}
,{href:'rel-630.html#rel-630-scripting',id:'rel-630-scripting',text:'Scripting',refs:[],children:[]}
,{href:'rel-630.html#rel-630-bdd',id:'rel-630-bdd',text:'Behavior Driven Testing',refs:[],children:[]}
,{href:'rel-630.html#rel-630-qt',id:'rel-630-qt',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-630.html#rel-630-java',id:'rel-630-java',text:'Java-specific',refs:[],children:[]}
,{href:'rel-630.html#rel-630-web',id:'rel-630-web',text:'Web-specific',refs:[],children:[]}
,{href:'rel-630.html#rel-630-windows',id:'rel-630-windows',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-630.html#rel-630-mac',id:'rel-630-mac',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-630.html#rel-630-ios',id:'rel-630-ios',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-630.html#rel-630-android',id:'rel-630-android',text:'Android-specific',refs:[],children:[]}
,{href:'rel-630.html#rel-630-source',id:'rel-630-source',text:'Source Packages',refs:[],children:[]}
,{href:'rel-630.html',id:'',text:'Documentation',refs:[],children:[]}
,{href:'rel-630.html',id:'',text:'Known Issues',refs:[],children:[]}
]}
,{href:'rel-630beta.html',id:'rel-630beta',text:'Version 6.3 Beta',refs:[],children:[{href:'rel-630beta.html#rel-630-image-search',id:'rel-630-image-search',text:'Image Search',refs:[],children:[]}
,{href:'rel-630beta.html#rel-630-comparisons',id:'rel-630-comparisons',text:'Comparison of Text and XML Files',refs:[],children:[]}
]}
,{href:'rel-620.html',id:'rel-620',text:'Version 6.2',refs:[],children:[{href:'rel-620.html#rel-620-general',id:'rel-620-general',text:'General',refs:[],children:[]}
,{href:'rel-620.html#rel-620-ide',id:'rel-620-ide',text:'Squish IDE-specific',refs:[],children:[]}
,{href:'rel-620.html#rel-620-scripting',id:'rel-620-scripting',text:'Scripting',refs:[],children:[]}
,{href:'rel-620.html#rel-620-qt',id:'rel-620-qt',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-620.html#rel-620-java',id:'rel-620-java',text:'Java-specific',refs:[],children:[]}
,{href:'rel-620.html#rel-620-web',id:'rel-620-web',text:'Web-specific',refs:[],children:[]}
,{href:'rel-620.html#rel-620-windows',id:'rel-620-windows',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-620.html#rel-620-mac',id:'rel-620-mac',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-620.html#rel-620-ios',id:'rel-620-ios',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-620.html#rel-620-android',id:'rel-620-android',text:'Android-specific',refs:[],children:[]}
,{href:'rel-620.html#rel-620-tk',id:'rel-620-tk',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-620.html#rel-620-source',id:'rel-620-source',text:'Source Packages',refs:[],children:[]}
,{href:'rel-620.html#rel-620-platform',id:'rel-620-platform',text:'Platform Support',refs:[],children:[]}
]}
,{href:'rel-610.html',id:'rel-610',text:'Version 6.1',refs:[],children:[{href:'rel-610.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Squish IDE-specific',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Scripting',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Android-specific',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Source Packages',refs:[],children:[]}
,{href:'rel-610.html',id:'',text:'Documentation',refs:[],children:[]}
]}
,{href:'rel-610beta.html',id:'rel-610beta',text:'Version 6.1 Beta',refs:[],children:[{href:'rel-610beta.html',id:'',text:'General',refs:[],children:[]}
]}
,{href:'rel-6031.html',id:'rel-6031',text:'Version 6.0.3.1',refs:[],children:[{href:'rel-6031.html',id:'',text:'Android-specific',refs:[],children:[]}
]}
,{href:'rel-603.html',id:'rel-603',text:'Version 6.0.3',refs:[],children:[{href:'rel-603.html',id:'',text:'Squish IDE-specific',refs:[],children:[]}
,{href:'rel-603.html',id:'',text:'Scripting',refs:[],children:[]}
]}
,{href:'rel-602.html',id:'rel-602',text:'Version 6.0.2',refs:[],children:[{href:'rel-602.html',id:'',text:'Squish IDE-specific',refs:[],children:[]}
,{href:'rel-602.html',id:'',text:'Scripting',refs:[],children:[]}
,{href:'rel-602.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-602.html',id:'',text:'Flex-specific',refs:[],children:[]}
,{href:'rel-602.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-602.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-602.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-602.html',id:'',text:'Android-specific',refs:[],children:[]}
,{href:'rel-602.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-602.html',id:'',text:'Examples',refs:[],children:[]}
]}
,{href:'rel-601.html',id:'rel-601',text:'Version 6.0.1',refs:[],children:[{href:'rel-601.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Squish IDE-specific',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Scripting',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Mac-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Android-specific',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Documentation',refs:[],children:[]}
,{href:'rel-601.html',id:'',text:'Source Packages',refs:[],children:[]}
]}
,{href:'rel-600.html',id:'rel-600',text:'Version 6.0',refs:[],children:[{href:'rel-600.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Squish IDE',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Scripting',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Flex-specific',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Android-specific',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Add-Ons &amp; Integrations',refs:[],children:[]}
,{href:'rel-600.html',id:'',text:'Source Packages',refs:[],children:[]}
]}
,{href:'rel-600beta.html',id:'rel-600beta',text:'Version 6.0 Beta',refs:[],children:[{href:'rel-600beta.html',id:'',text:'Support for Behavior-Driven Development (BDD)',refs:[],children:[]}
,{href:'rel-600beta.html',id:'',text:'Advanced Reporting',refs:[],children:[]}
,{href:'rel-600beta.html',id:'',text:'Script Language Updates',refs:[],children:[]}
,{href:'rel-600beta.html',id:'',text:'GUI Coverage (Experimental)',refs:[],children:[]}
]}
,{href:'rel-515.html',id:'rel-515',text:'Version 5.1.5',refs:[],children:[]}
,{href:'rel-514.html',id:'rel-514',text:'Version 5.1.4',refs:[],children:[{href:'rel-514.html',id:'',text:'Windows-specific',refs:[],children:[]}
,{href:'rel-514.html',id:'',text:'iOS-specific',refs:[],children:[]}
,{href:'rel-514.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-514.html',id:'',text:'Flex-specific',refs:[],children:[]}
,{href:'rel-514.html',id:'',text:'Qt-specific',refs:[],children:[]}
]}
,{href:'rel-513.html',id:'rel-513',text:'Version 5.1.3',refs:[],children:[{href:'rel-513.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-513.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-513.html',id:'',text:'iOS-specific',refs:[],children:[]}
,{href:'rel-513.html',id:'',text:'Android-specific',refs:[],children:[]}
,{href:'rel-513.html',id:'',text:'IDE',refs:[],children:[]}
]}
,{href:'rel-512.html',id:'rel-512',text:'Version 5.1.2',refs:[],children:[{href:'rel-512.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-512.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-512.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-512.html',id:'',text:'iOS-specific',refs:[],children:[]}
,{href:'rel-512.html',id:'',text:'Android-specific',refs:[],children:[]}
,{href:'rel-512.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-512.html',id:'',text:'IDE',refs:[],children:[]}
,{href:'rel-512.html',id:'',text:'Scripting',refs:[],children:[]}
]}
,{href:'rel-511.html',id:'rel-511',text:'Version 5.1.1',refs:[],children:[{href:'rel-511.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'Squish IDE',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'Scripting',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'Android-specific',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'Add-Ons &amp; Integrations',refs:[],children:[]}
,{href:'rel-511.html',id:'',text:'Source Packages',refs:[],children:[]}
]}
,{href:'rel-510.html',id:'rel-510',text:'Version 5.1.0',refs:[],children:[{href:'rel-510.html',id:'',text:'General',refs:[],children:[{href:'rel-510.html',id:'',text:'Bug Fixes',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Features',refs:[],children:[]}
]}
,{href:'rel-510.html',id:'',text:'Squish IDE',refs:[],children:[{href:'rel-510.html',id:'',text:'Bug Fixes',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Features',refs:[],children:[]}
]}
,{href:'rel-510.html',id:'',text:'Scripting',refs:[],children:[{href:'rel-510.html',id:'',text:'Bug Fixes',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Features',refs:[],children:[]}
]}
,{href:'rel-510.html',id:'',text:'Qt-specific',refs:[],children:[{href:'rel-510.html',id:'',text:'Bug Fixes',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Features',refs:[],children:[]}
]}
,{href:'rel-510.html',id:'',text:'Java-specific',refs:[],children:[{href:'rel-510.html',id:'',text:'Bug Fixes',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Features',refs:[],children:[]}
]}
,{href:'rel-510.html',id:'',text:'Web-specific',refs:[],children:[{href:'rel-510.html',id:'',text:'Bug Fixes',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Features',refs:[],children:[]}
]}
,{href:'rel-510.html',id:'',text:'Windows (native)-specific',refs:[],children:[{href:'rel-510.html',id:'',text:'Bug Fixes',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Features',refs:[],children:[]}
]}
,{href:'rel-510.html',id:'',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Android-specific',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Add-Ons',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Documentation',refs:[],children:[]}
,{href:'rel-510.html',id:'',text:'Qt-specific Source Packages',refs:[],children:[]}
]}
,{href:'rel-504.html',id:'rel-504',text:'Version 5.0.4',refs:[],children:[{href:'rel-504.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-504.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-504.html',id:'',text:'Android-specific',refs:[],children:[]}
]}
,{href:'rel-503.html',id:'rel-503',text:'Version 5.0.3',refs:[],children:[{href:'rel-503.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-503.html',id:'',text:'iOS-specific',refs:[],children:[]}
]}
,{href:'rel-502.html',id:'rel-502',text:'Version 5.0.2',refs:[],children:[{href:'rel-502.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-502.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-502.html',id:'',text:'Android-specific',refs:[],children:[]}
]}
,{href:'rel-501.html',id:'rel-501',text:'Version 5.0.1',refs:[],children:[{href:'rel-501.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-501.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-501.html',id:'',text:'iOS-specific',refs:[],children:[]}
,{href:'rel-501.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-501.html',id:'',text:'Documentation',refs:[],children:[]}
]}
,{href:'rel-500.html',id:'rel-500',text:'Version 5.0.0',refs:[],children:[{href:'rel-500.html',id:'',text:'New Editions',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'IDE',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'Add-Ons',refs:[],children:[]}
,{href:'rel-500.html',id:'',text:'Documentation',refs:[],children:[]}
]}
,{href:'rel-423.html',id:'rel-423',text:'Version 4.2.3',refs:[],children:[{href:'rel-423.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'IDE',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'Add-Ons',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'Source Packages',refs:[],children:[]}
,{href:'rel-423.html',id:'',text:'Documentation',refs:[],children:[]}
]}
,{href:'rel-422.html',id:'rel-422',text:'Version 4.2.2',refs:[],children:[{href:'rel-422.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-422.html',id:'',text:'IDE',refs:[],children:[]}
,{href:'rel-422.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-422.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-422.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-422.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-422.html',id:'',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-422.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-422.html',id:'',text:'Tk-specific',refs:[],children:[]}
]}
,{href:'rel-421.html',id:'rel-421',text:'Version 4.2.1',refs:[],children:[{href:'rel-421.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'IDE',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'Source Packages',refs:[],children:[]}
,{href:'rel-421.html',id:'',text:'Documentation',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'rel-420',text:'Version 4.2.0',refs:[],children:[{href:'rel-420.html',id:'',text:'General',refs:[],children:[{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html#rel-422-version.4.2.0.ide.changes',id:'rel-422-version.4.2.0.ide.changes',text:'IDE',refs:[],children:[{href:'rel-420.html',id:'',text:'User Interface Rearrangement',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Other Changes',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'Qt-specific',refs:[],children:[{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'Java-specific',refs:[],children:[{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'Web-specific',refs:[],children:[{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'Windows (native)-specific',refs:[],children:[{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'macOS-specific (Cocoa/Carbon edition)',refs:[],children:[{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'iOS-specific (iPhone/iPad edition)',refs:[],children:[{href:'rel-420.html',id:'',text:'Other Changes',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'Tk-specific',refs:[],children:[{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'HP Quality Center-specific',refs:[],children:[{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'Source Packages',refs:[],children:[{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
,{href:'rel-420.html',id:'',text:'Documentation',refs:[],children:[{href:'rel-420.html',id:'',text:'New Features',refs:[],children:[]}
,{href:'rel-420.html',id:'',text:'Important Bug Fixes',refs:[],children:[]}
]}
]}
,{href:'rel-411.html',id:'rel-411',text:'Version 4.1.1',refs:[],children:[{href:'rel-411.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-411.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-411.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-411.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-411.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-411.html',id:'',text:'Mac-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-411.html',id:'',text:'iPhone/iPad-specific',refs:[],children:[]}
,{href:'rel-411.html',id:'',text:'Source Builds',refs:[],children:[]}
,{href:'rel-411.html',id:'',text:'Documentation',refs:[],children:[]}
]}
,{href:'rel-410final.html',id:'rel-410final',text:'Version 4.1.0 Final',refs:[],children:[{href:'rel-410final.html#highlights',id:'highlights',text:'Highlights',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'New IDE',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'Mac-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'iPhone/iPad-specific',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'HP Quality Center Plug-In',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'Documentation',refs:[],children:[]}
,{href:'rel-410final.html',id:'',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-410beta2.html',id:'rel-410beta2',text:'Version 4.1.0 Beta 2',refs:[],children:[{href:'rel-410beta2.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'New IDE',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'Mac-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'iPhone/iPad-specific',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'Documentation',refs:[],children:[]}
,{href:'rel-410beta2.html',id:'',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-410beta1.html',id:'rel-410beta1',text:'Version 4.1.0 Beta 1',refs:[],children:[{href:'rel-410beta1.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'New IDE',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'Windows (native)-specific',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'Mac-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'iPhone/iPad-specific',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'Documentation',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'Source Builds',refs:[],children:[]}
,{href:'rel-410beta1.html',id:'',text:'Classic IDE',refs:[],children:[]}
]}
,{href:'rel-402.html',id:'rel-402',text:'Version 4.0.2',refs:[],children:[{href:'rel-402.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'Classic IDE',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'New IDE',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'All toolkits',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'Native Windows (new edition)',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'Mac-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'iPhone/iPad-specific',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'Source Builds',refs:[],children:[]}
,{href:'rel-402.html',id:'',text:'Documentation',refs:[],children:[]}
]}
,{href:'rel-4011.html',id:'rel-4011',text:'Version 4.0.1.1',refs:[],children:[{href:'rel-4011.html',id:'',text:'iPhone/iPad-specific',refs:[],children:[]}
]}
,{href:'rel-401.html',id:'rel-401',text:'Version 4.0.1',refs:[],children:[{href:'rel-401.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-401.html',id:'',text:'New IDE',refs:[],children:[]}
,{href:'rel-401.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-401.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-401.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-401.html',id:'',text:'Native Windows (new edition)',refs:[],children:[]}
,{href:'rel-401.html',id:'',text:'Mac-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-401.html',id:'',text:'iPhone/iPad-specific',refs:[],children:[]}
,{href:'rel-401.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-401.html',id:'',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-400.html',id:'rel-400',text:'Version 4.0.0',refs:[],children:[{href:'rel-400.html',id:'',text:'The Squish 4.0 IDEs',refs:[],children:[]}
,{href:'rel-400.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-400.html',id:'',text:'Squish for Win32 Edition—New Edition',refs:[],children:[]}
,{href:'rel-400.html',id:'',text:'Squish for iPhone Edition—New Edition',refs:[],children:[]}
,{href:'rel-400.html',id:'',text:'Squish for Qt—Improved and Enhanced',refs:[],children:[]}
,{href:'rel-400.html',id:'',text:'Squish for Java—Improved and Enhanced',refs:[],children:[]}
,{href:'rel-400.html',id:'',text:'List of changes',refs:[],children:[]}
]}
,{href:'rel-4-beta3-changes.html',id:'rel-4-beta3-changes',text:'Changes in 4.0 Beta 3',refs:[],children:[{href:'rel-4-beta3-changes.html#rel-4-beta3-general',id:'rel-4-beta3-general',text:'General',refs:[],children:[]}
,{href:'rel-4-beta3-changes.html#rel-4-beta3-qt',id:'rel-4-beta3-qt',text:'Qt Edition',refs:[],children:[]}
,{href:'rel-4-beta3-changes.html#rel-4-beta3-mac',id:'rel-4-beta3-mac',text:'Mac Edition',refs:[],children:[]}
,{href:'rel-4-beta3-changes.html#rel-4-beta3-ide',id:'rel-4-beta3-ide',text:'New IDE',refs:[],children:[]}
,{href:'rel-4-beta3-changes.html#rel-4-beta3-classicide',id:'rel-4-beta3-classicide',text:'Classic IDE',refs:[],children:[]}
]}
,{href:'rel-4-beta2-changes.html',id:'rel-4-beta2-changes',text:'Changes in 4.0 Beta 2',refs:[],children:[{href:'rel-4-beta2-changes.html#rel-4-beta2-general',id:'rel-4-beta2-general',text:'General',refs:[],children:[]}
,{href:'rel-4-beta2-changes.html#rel-4-beta2-ide',id:'rel-4-beta2-ide',text:'New IDE',refs:[],children:[]}
]}
,{href:'rel-4-beta1-changes.html',id:'rel-4-beta1-changes',text:'Changes in 4.0 Beta 1',refs:[],children:[{href:'rel-4-beta1-changes.html#rel-4-changes-general',id:'rel-4-changes-general',text:'General',refs:[],children:[]}
,{href:'rel-4-beta1-changes.html#rel-4-changes-scripting',id:'rel-4-changes-scripting',text:'Scripting',refs:[],children:[]}
,{href:'rel-4-beta1-changes.html#rel-4-changes-web',id:'rel-4-changes-web',text:'Web-specific',refs:[],children:[]}
,{href:'rel-4-beta1-changes.html#rel-4-changes-qt',id:'rel-4-changes-qt',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-4-beta1-changes.html#rel-4-changes-java',id:'rel-4-changes-java',text:'Java-specific',refs:[],children:[]}
,{href:'rel-4-beta1-changes.html#rel-4-changes-mac',id:'rel-4-changes-mac',text:'Mac-specific',refs:[],children:[]}
,{href:'rel-4-beta1-changes.html#rel-4-changes-classic-ide',id:'rel-4-changes-classic-ide',text:'Classic IDE',refs:[],children:[]}
,{href:'rel-4-beta1-changes.html#rel-4-changes-build',id:'rel-4-changes-build',text:'Source builds',refs:[],children:[]}
]}
,{href:'rel-345.html',id:'rel-345',text:'Version 3.4.5',refs:[],children:[{href:'rel-345.html',id:'',text:'General',refs:[],children:[]}
,{href:'rel-345.html',id:'',text:'IDE',refs:[],children:[]}
,{href:'rel-345.html',id:'',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-345.html',id:'',text:'Java-specific',refs:[],children:[]}
,{href:'rel-345.html',id:'',text:'Web-specific',refs:[],children:[]}
,{href:'rel-345.html',id:'',text:'Mac-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-345.html',id:'',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-345.html',id:'',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-344.html',id:'rel-344',text:'Version 3.4.4',refs:[],children:[{href:'rel-344.html#general',id:'general',text:'General',refs:[],children:[]}
,{href:'rel-344.html#ide',id:'ide',text:'IDE',refs:[],children:[]}
,{href:'rel-344.html#qt-specific',id:'qt-specific',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-344.html#java-specific',id:'java-specific',text:'Java-specific',refs:[],children:[]}
,{href:'rel-344.html#web-specific',id:'web-specific',text:'Web-specific',refs:[],children:[]}
,{href:'rel-344.html#tk-specific',id:'tk-specific',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-344.html#source-builds',id:'source-builds',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-343.html',id:'rel-343',text:'Version 3.4.3',refs:[],children:[{href:'rel-343.html#general.1',id:'general.1',text:'General',refs:[],children:[]}
,{href:'rel-343.html#ide.1',id:'ide.1',text:'IDE',refs:[],children:[]}
,{href:'rel-343.html#qt-specific.1',id:'qt-specific.1',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-343.html#java-specific.1',id:'java-specific.1',text:'Java-specific',refs:[],children:[]}
,{href:'rel-343.html#web-specific.1',id:'web-specific.1',text:'Web-specific',refs:[],children:[]}
,{href:'rel-343.html#tk-specific.1',id:'tk-specific.1',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-343.html#mac-specific-cocoa-carbon-edition',id:'mac-specific-cocoa-carbon-edition',text:'Mac-specific (Cocoa/Carbon edition)',refs:[],children:[]}
,{href:'rel-343.html#source-builds.1',id:'source-builds.1',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-342.html',id:'rel-342',text:'Version 3.4.2',refs:[],children:[{href:'rel-342.html#general.2',id:'general.2',text:'General',refs:[],children:[]}
,{href:'rel-342.html#qt-specific.2',id:'qt-specific.2',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-342.html#java-specific.2',id:'java-specific.2',text:'Java-specific',refs:[],children:[]}
,{href:'rel-342.html#web-specific.2',id:'web-specific.2',text:'Web-specific',refs:[],children:[]}
,{href:'rel-342.html#tk-specific.2',id:'tk-specific.2',text:'Tk-specific',refs:[],children:[]}
]}
,{href:'rel-341.html',id:'rel-341',text:'Version 3.4.1',refs:[],children:[{href:'rel-341.html#general.3',id:'general.3',text:'General',refs:[],children:[]}
,{href:'rel-341.html#qt-specific.3',id:'qt-specific.3',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-341.html#java-specific.3',id:'java-specific.3',text:'Java-specific',refs:[],children:[]}
,{href:'rel-341.html#web-specific.3',id:'web-specific.3',text:'Web-specific',refs:[],children:[]}
,{href:'rel-341.html#tk-specific.3',id:'tk-specific.3',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-341.html#native-win32-support',id:'native-win32-support',text:'Native Win32 Support',refs:[],children:[]}
,{href:'rel-341.html#native-x11-support',id:'native-x11-support',text:'Native X11 Support',refs:[],children:[]}
]}
,{href:'rel-340.html',id:'rel-340',text:'Version 3.4.0',refs:[],children:[{href:'rel-340.html#general.4',id:'general.4',text:'General',refs:[],children:[]}
,{href:'rel-340.html#qt-specific.4',id:'qt-specific.4',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-340.html#java-specific.4',id:'java-specific.4',text:'Java-specific',refs:[],children:[]}
,{href:'rel-340.html#web-specific.4',id:'web-specific.4',text:'Web-specific',refs:[],children:[]}
,{href:'rel-340.html#tk-specific.4',id:'tk-specific.4',text:'Tk-specific',refs:[],children:[]}
,{href:'rel-340.html#source-builds.2',id:'source-builds.2',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-331.html',id:'rel-331',text:'Version 3.3.1',refs:[],children:[{href:'rel-331.html#general.5',id:'general.5',text:'General',refs:[],children:[]}
,{href:'rel-331.html#qt-specific.5',id:'qt-specific.5',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-331.html#java-specific.5',id:'java-specific.5',text:'Java-specific',refs:[],children:[]}
,{href:'rel-331.html#web-specific.5',id:'web-specific.5',text:'Web-specific',refs:[],children:[]}
,{href:'rel-331.html#source-builds.3',id:'source-builds.3',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-330.html',id:'rel-330',text:'Version 3.3.0',refs:[],children:[{href:'rel-330.html#general.6',id:'general.6',text:'General',refs:[],children:[]}
,{href:'rel-330.html#qt-specific.6',id:'qt-specific.6',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-330.html#java-specific.6',id:'java-specific.6',text:'Java-specific',refs:[],children:[]}
,{href:'rel-330.html#web-specific.6',id:'web-specific.6',text:'Web-specific',refs:[],children:[]}
,{href:'rel-330.html#mac-specific',id:'mac-specific',text:'Mac-specific',refs:[],children:[]}
,{href:'rel-330.html#source-builds.4',id:'source-builds.4',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-330b1.html',id:'rel-330b1',text:'Version 3.3.0 Beta 1',refs:[],children:[{href:'rel-330b1.html#general.7',id:'general.7',text:'General',refs:[],children:[]}
,{href:'rel-330b1.html#qt-specific.7',id:'qt-specific.7',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-330b1.html#java-specific.7',id:'java-specific.7',text:'Java-specific',refs:[],children:[]}
,{href:'rel-330b1.html#web-specific.7',id:'web-specific.7',text:'Web-specific',refs:[],children:[]}
,{href:'rel-330b1.html#mac-specific.1',id:'mac-specific.1',text:'Mac-specific',refs:[],children:[]}
,{href:'rel-330b1.html#source-builds.5',id:'source-builds.5',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-323.html',id:'rel-323',text:'Version 3.2.3',refs:[],children:[{href:'rel-323.html#general.8',id:'general.8',text:'General',refs:[],children:[]}
,{href:'rel-323.html#qt-specific.8',id:'qt-specific.8',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-323.html#web-specific.8',id:'web-specific.8',text:'Web-specific',refs:[],children:[]}
,{href:'rel-323.html#xview-specific',id:'xview-specific',text:'XView-specific',refs:[],children:[]}
,{href:'rel-323.html#source-builds.6',id:'source-builds.6',text:'Source Builds',refs:[],children:[]}
]}
,{href:'rel-322.html',id:'rel-322',text:'Version 3.2.2',refs:[],children:[{href:'rel-322.html#general.9',id:'general.9',text:'General',refs:[],children:[]}
,{href:'rel-322.html#qt-specific.9',id:'qt-specific.9',text:'Qt-specific',refs:[],children:[]}
,{href:'rel-322.html#web-specific.9',id:'web-specific.9',text:'Web-specific',refs:[],children:[]}
]}
,{href:'rel-320.html',id:'rel-320',text:'Version 3.2',refs:[],children:[{href:'rel-320.html#general.10',id:'general.10',text:'General',refs:[],children:[]}
,{href:'rel-320.html#qt',id:'qt',text:'Qt',refs:[],children:[]}
,{href:'rel-320.html#web',id:'web',text:'Web',refs:[],children:[]}
,{href:'rel-320.html#java',id:'java',text:'Java',refs:[],children:[]}
,{href:'rel-320.html#tk',id:'tk',text:'Tk',refs:[],children:[]}
]}
,{href:'rel-312.html',id:'rel-312',text:'Version 3.1.2',refs:[],children:[{href:'rel-312.html#general.11',id:'general.11',text:'General',refs:[],children:[]}
,{href:'rel-312.html#qt.1',id:'qt.1',text:'Qt',refs:[],children:[]}
,{href:'rel-312.html#java.1',id:'java.1',text:'Java',refs:[],children:[]}
,{href:'rel-312.html#web.1',id:'web.1',text:'Web',refs:[],children:[]}
,{href:'rel-312.html#tk.1',id:'tk.1',text:'Tk',refs:[],children:[]}
]}
,{href:'rel-311.html',id:'rel-311',text:'Version 3.1.1',refs:[],children:[{href:'rel-311.html#general.12',id:'general.12',text:'General',refs:[],children:[]}
,{href:'rel-311.html#qt.2',id:'qt.2',text:'Qt',refs:[],children:[]}
,{href:'rel-311.html#java.2',id:'java.2',text:'Java',refs:[],children:[]}
,{href:'rel-311.html#web.2',id:'web.2',text:'Web',refs:[],children:[]}
]}
,{href:'rel-31final.html',id:'rel-31final',text:'Version 3.1.0',refs:[],children:[{href:'rel-31final.html#general.13',id:'general.13',text:'General',refs:[],children:[]}
,{href:'rel-31final.html',id:'',text:'Web',refs:[],children:[]}
]}
]}
,{href:'install.html',id:'install',text:'How to Install Squish',refs:['rgc-installer','rg-squishconfig'],children:[{href:'ins-commandline-tools-server-ide.html',id:'ins-commandline-tools-server-ide',text:'Installing the Command Line Tools, Server, and IDEs',refs:[],children:[{href:'ins-commandline-tools-server-ide.html#ins-binary',id:'ins-binary',text:'Installing from Binary Packages',refs:[],children:[{href:'ins-commandline-tools-server-ide.html#ins-binary-get',id:'ins-binary-get',text:'Choosing the Correct Squish Package',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#ins-binary-config',id:'ins-binary-config',text:'Configuring the Package',refs:[],children:[{href:'ins-commandline-tools-server-ide.html#ins-binary-config-licensekey',id:'ins-binary-config-licensekey',text:'Entering the License Key',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#ins-binary-config-licensetext',id:'ins-binary-config-licensetext',text:'Acknowledging the Terms of Use',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#ins-binary-config-targetdir',id:'ins-binary-config-targetdir',text:'Installation Folder',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#ins-binary-config-userjavapath',id:'ins-binary-config-userjavapath',text:'Paths for Java Settings',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#ins-binary-config-review',id:'ins-binary-config-review',text:'Ready to Install',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#ins-binary-config-progress',id:'ins-binary-config-progress',text:'Executing the Installation',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#ins-binary-config-final',id:'ins-binary-config-final',text:'Concluding the Configuration',refs:[],children:[]}
]}
,{href:'ins-commandline-tools-server-ide.html#ins-binary-unattended',id:'ins-binary-unattended',text:'Performing Unattended Installations',refs:[],children:[]}
]}
,{href:'ins-commandline-tools-server-ide.html#ins-distributing',id:'ins-distributing',text:'Distributing and Sharing an Installation',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.on.mobile.devices',id:'installation.for.web.testing.on.mobile.devices',text:'Installation for Web Testing using browsers on mobile devices',refs:[],children:[{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.on.mobile.devices.configure.http.proxy',id:'installation.for.web.testing.on.mobile.devices.configure.http.proxy',text:'Start the HTTP proxy',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.on.mobile.devices.configure.device.proxy.connection',id:'installation.for.web.testing.on.mobile.devices.configure.device.proxy.connection',text:'Configure Device Proxy Connection',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.on.mobile.devices.configure.squish.ide',id:'installation.for.web.testing.on.mobile.devices.configure.squish.ide',text:'Configure Squish for Web IDE',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.on.mobile.devices.limitations',id:'installation.for.web.testing.on.mobile.devices.limitations',text:'Limitations',refs:[],children:[]}
]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.with.safari.on.osx.10.11.5',id:'installation.for.web.testing.with.safari.on.osx.10.11.5',text:'Installation for Web Testing using Safari on macOS 10.11.5 or later',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.connecting.to.running.browser',id:'installation.for.web.testing.connecting.to.running.browser',text:'Installation for Web Testing when connecting to an already running browser',refs:[],children:[{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.configure.ie.for.attaching',id:'installation.for.web.testing.configure.ie.for.attaching',text:'Setting up Internet Explorer so Squish/Web can attach to a running instance',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.configure.firefox.for.attaching',id:'installation.for.web.testing.configure.firefox.for.attaching',text:'Setting up Firefox so Squish/Web can attach to a running instance',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.configure.chrome.for.attaching',id:'installation.for.web.testing.configure.chrome.for.attaching',text:'Setting up Chrome so Squish/Web can attach to a running instance',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.configure.chromium.for.attaching',id:'installation.for.web.testing.configure.chromium.for.attaching',text:'Setting up a Chromium-based application so Squish/Web can attach to a running instance',refs:[],children:[]}
]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.with.opera',id:'installation.for.web.testing.with.opera',text:'Installation for Web Testing using Opera up to version 12',refs:[],children:[]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.with.microsoft.edge.on.windows.10',id:'installation.for.web.testing.with.microsoft.edge.on.windows.10',text:'Installation for Web Testing using Microsoft Edge on Windows 10',refs:[],children:[{href:'ins-commandline-tools-server-ide.html#web.testing.with.microsoft.edge.known.issues',id:'web.testing.with.microsoft.edge.known.issues',text:'Known issues when using Squish with Microsoft Edge',refs:[],children:[]}
]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.web.testing.with.chromium.based.applications',id:'installation.for.web.testing.with.chromium.based.applications',text:'Installation for Web Testing using Chromium-based applications',refs:[],children:[{href:'ins-commandline-tools-server-ide.html#web.testing.with.chromium.based.applications.known.issues',id:'web.testing.with.chromium.based.applications.known.issues',text:'Known issues when using Squish with Chromium-based Applications',refs:[],children:[]}
]}
,{href:'ins-commandline-tools-server-ide.html#installation.for.flex.testing',id:'installation.for.flex.testing',text:'Installation for Flex applets in web pages',refs:[],children:[]}
]}
,{href:'ins-new-ide.html',id:'ins-new-ide',text:'Standalone Installation of the IDE',refs:[],children:[{href:'ins-new-ide.html#ins-ide-win',id:'ins-ide-win',text:'Installing the IDE on Windows',refs:[],children:[]}
,{href:'ins-new-ide.html#ins-ide-linux',id:'ins-ide-linux',text:'Installing the IDE on Linux',refs:[],children:[]}
,{href:'ins-new-ide.html#ins-ide-mac',id:'ins-ide-mac',text:'Installing the IDE on macOS',refs:[],children:[]}
]}
,{href:'ins-binary-usage.html',id:'ins-binary-usage',text:'Using Squish',refs:[],children:[]}
]}
,{href:'tutorials.html',id:'tutorials',text:'Tutorials',refs:['ug-imagebasedtesting','rgs-squish'],children:[{href:'tutorials-web.html',id:'tutorials-web',text:'Squish for Web Tutorials',refs:['rgs-webconvenience','installation.for.web.testing.on.mobile.devices','installation.for.web.testing.with.opera','ugs-webapi'],children:[{href:'tutorials-web.html#tutorial-getting-started-web',id:'tutorial-getting-started-web',text:'Tutorial: Starting to Test Web Applications',refs:[],children:[{href:'tutorials-web.html#tgs-concepts-web',id:'tgs-concepts-web',text:'Squish Concepts',refs:[],children:[{href:'tutorials-web.html#tgsc_apptestable-web',id:'tgsc_apptestable-web',text:'Making an Application Testable',refs:[],children:[]}
]}
,{href:'tutorials-web.html#tut-web-creating-a-test-suite',id:'tut-web-creating-a-test-suite',text:'Creating a Test Suite',refs:[],children:[]}
,{href:'tutorials-web.html#tut-web-recording-tests',id:'tut-web-recording-tests',text:'Recording Tests and Verification Points',refs:[],children:[]}
,{href:'tutorials-web.html#tgs-vp-web',id:'tgs-vp-web',text:'Inserting Additional Verification Points',refs:[],children:[{href:'tutorials-web.html#test-results-web.1',id:'test-results-web.1',text:'Test Results',refs:[],children:[]}
]}
,{href:'tutorials-web.html#tut-web-creating-manual-tests',id:'tut-web-creating-manual-tests',text:'Creating Tests by Hand',refs:[],children:[{href:'tutorials-web.html#tut-web-modifying-and-refactoring-recorded-tests',id:'tut-web-modifying-and-refactoring-recorded-tests',text:'Modifying and Refactoring Recorded Tests',refs:[],children:[]}
,{href:'tutorials-web.html#tgs-datadriven-web',id:'tgs-datadriven-web',text:'Creating Data Driven Tests',refs:[],children:[]}
]}
,{href:'tutorials-web.html#web-tut-learning-more',id:'web-tut-learning-more',text:'Learning More',refs:[],children:[]}
]}
,{href:'tutorials-web.html#tutorial-bdd-web',id:'tutorial-bdd-web',text:'Tutorial: Designing Behavior Driven Development (BDD) Tests',refs:[],children:[{href:'tutorials-web.html#tut-bdd-web-intro',id:'tut-bdd-web-intro',text:'Introduction to Behavior Driven Development',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-gherkin',id:'tut-bdd-web-gherkin',text:'Gherkin syntax',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-test-impl',id:'tut-bdd-web-test-impl',text:'Test implementation',refs:[],children:[{href:'tutorials-web.html#tut-bdd-web-create-testsuite',id:'tut-bdd-web-create-testsuite',text:'Creating Test Suite',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-create-testcase',id:'tut-bdd-web-create-testcase',text:'Creating Test Case',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-record-step',id:'tut-bdd-web-record-step',text:'Recording Step implementation',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-step-parametrization',id:'tut-bdd-web-step-parametrization',text:'Step parametrization',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-provide-parameters',id:'tut-bdd-web-provide-parameters',text:'Provide parameters for Step in table',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-sharing',id:'tut-bdd-web-sharing',text:'Sharing data between Steps and Scenarios',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-scenario-outline',id:'tut-bdd-web-scenario-outline',text:'Scenario Outline',refs:[],children:[]}
]}
,{href:'tutorials-web.html#tut-bdd-web-test-exec',id:'tut-bdd-web-test-exec',text:'Test execution',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-test-debug',id:'tut-bdd-web-test-debug',text:'Test debugging',refs:[],children:[]}
,{href:'tutorials-web.html#tut-bdd-web-step-reuse',id:'tut-bdd-web-step-reuse',text:'Re-using Step definitions',refs:[],children:[]}
]}
,{href:'tutorials-web.html#tutorial-migration-bdd-web',id:'tutorial-migration-bdd-web',text:'Tutorial: Migration of existing tests to BDD',refs:[],children:[{href:'tutorials-web.html#bdd-migration-web-extend',id:'bdd-migration-web-extend',text:'Extend existing tests to BDD',refs:[],children:[]}
,{href:'tutorials-web.html#bdd-migration-web-convert',id:'bdd-migration-web-convert',text:'Convert existing tests to BDD',refs:[],children:[]}
]}
]}
,{href:'tutorials-win.html',id:'tutorials-win',text:'Squish for Windows Tutorials',refs:['rgs-windowsobjectapi','ug-how.to.use.the.windows.api'],children:[{href:'tutorials-win.html#tutorial-getting-started-win',id:'tutorial-getting-started-win',text:'Tutorial: Starting to Test Windows Applications',refs:[],children:[{href:'tutorials-win.html#tgs-concepts-win',id:'tgs-concepts-win',text:'Squish Concepts',refs:[],children:[{href:'tutorials-win.html#tgsc_apptestable-win',id:'tgsc_apptestable-win',text:'Making an Application Testable',refs:[],children:[]}
]}
,{href:'tutorials-win.html#win-tut-creating-a-test-suite',id:'win-tut-creating-a-test-suite',text:'Creating a Test Suite',refs:[],children:[]}
,{href:'tutorials-win.html#win-tut-recording-tests',id:'win-tut-recording-tests',text:'Recording Tests and Verification Points',refs:[],children:[]}
,{href:'tutorials-win.html#win-inserting.additional.verification.points',id:'win-inserting.additional.verification.points',text:'Inserting Additional Verification Points',refs:[],children:[{href:'tutorials-win.html#test-results-win.1',id:'test-results-win.1',text:'Test Results',refs:[],children:[]}
]}
,{href:'tutorials-win.html#tut-win-creating-manual-tests',id:'tut-win-creating-manual-tests',text:'Creating Tests by Hand',refs:[],children:[{href:'tutorials-win.html#tut-win-modifying-and-refactoring-recorded-tests',id:'tut-win-modifying-and-refactoring-recorded-tests',text:'Modifying and Refactoring Recorded Tests',refs:[],children:[]}
,{href:'tutorials-win.html#win-tgs-datadriven',id:'win-tgs-datadriven',text:'Creating Data Driven Tests',refs:[],children:[]}
]}
,{href:'tutorials-win.html#win-tut-learning-more',id:'win-tut-learning-more',text:'Learning More',refs:[],children:[]}
]}
,{href:'tutorials-win.html#tutorial-bdd-win',id:'tutorial-bdd-win',text:'Tutorial: Designing Behavior Driven Development (BDD) Tests',refs:[],children:[{href:'tutorials-win.html#tut-bdd-win-intro',id:'tut-bdd-win-intro',text:'Introduction to Behavior Driven Development',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-gherkin',id:'tut-bdd-win-gherkin',text:'Gherkin syntax',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-test-impl',id:'tut-bdd-win-test-impl',text:'Test implementation',refs:[],children:[{href:'tutorials-win.html#tut-bdd-win-create-testsuite',id:'tut-bdd-win-create-testsuite',text:'Creating Test Suite',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-create-testcase',id:'tut-bdd-win-create-testcase',text:'Creating Test Case',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-record-step',id:'tut-bdd-win-record-step',text:'Recording Step implementation',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-step-parametrization',id:'tut-bdd-win-step-parametrization',text:'Step parametrization',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-provide-parameters',id:'tut-bdd-win-provide-parameters',text:'Provide parameters for Step in table',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-sharing',id:'tut-bdd-win-sharing',text:'Sharing data between Steps and Scenarios',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-scenario-outline',id:'tut-bdd-win-scenario-outline',text:'Scenario Outline',refs:[],children:[]}
]}
,{href:'tutorials-win.html#tut-bdd-win-test-exec',id:'tut-bdd-win-test-exec',text:'Test execution',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-test-debug',id:'tut-bdd-win-test-debug',text:'Test debugging',refs:[],children:[]}
,{href:'tutorials-win.html#tut-bdd-win-step-reuse',id:'tut-bdd-win-step-reuse',text:'Re-using Step definitions',refs:[],children:[]}
]}
,{href:'tutorials-win.html#tutorial-migration-bdd-win',id:'tutorial-migration-bdd-win',text:'Tutorial: Migration of existing tests to BDD',refs:[],children:[{href:'tutorials-win.html#bdd-migration-win-extend',id:'bdd-migration-win-extend',text:'Extend existing tests to BDD',refs:[],children:[]}
,{href:'tutorials-win.html#bdd-migration-win-convert',id:'bdd-migration-win-convert',text:'Convert existing tests to BDD',refs:[],children:[]}
]}
]}
]}
,{href:'users-guide.html',id:'users-guide',text:'User Guide',refs:[],children:[{href:'ug-accessing-objects.html',id:'ug-accessing-objects',text:'How to Identify and Access Objects',refs:[],children:[{href:'ug-accessing-objects.html#ug-how-to-access-named-objects',id:'ug-how-to-access-named-objects',text:'How to Access Named Objects',refs:[],children:[]}
,{href:'ug-accessing-objects.html#how-to-access-objects-using-real-multi-property-names',id:'how-to-access-objects-using-real-multi-property-names',text:'How to Access Objects Using Real (Multi-Property) Names',refs:[],children:[]}
,{href:'ug-accessing-objects.html#how-to-access-objects-using-symbolic-names',id:'how-to-access-objects-using-symbolic-names',text:'How to Access Objects Using Symbolic Names',refs:[],children:[]}
,{href:'ug-accessing-objects.html#how-to-access-objects-using-images',id:'how-to-access-objects-using-images',text:'How to Access Objects Using Images',refs:[],children:[]}
]}
,{href:'ugs-webapi.html',id:'ugs-webapi',text:'How to Use the Web API',refs:['rgs-webconvenience'],children:[{href:'ugs-webapi.html#ugsweb-objects',id:'ugsweb-objects',text:'How to Find and Query Web Objects',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsweb-xpath',id:'ugsweb-xpath',text:'How to Use XPath',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsweb-properties',id:'ugsweb-properties',text:'How to Access Web Object Properties',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsweb-methods',id:'ugsweb-methods',text:'How to Call Web Object Functions',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsweb-evaljs',id:'ugsweb-evaljs',text:'How to Use evalJS',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsweb-retrieveJSObject',id:'ugsweb-retrieveJSObject',text:'How to Use retrieveJSObject',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsweb-convenience',id:'ugsweb-convenience',text:'How to Use the Web Convenience API',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsweb-sync',id:'ugsweb-sync',text:'How to Synchronize Web Page Loading for Testing',refs:[],children:[]}
,{href:'ugs-webapi.html#ugs-webwidgets',id:'ugs-webwidgets',text:'How to Test Web Elements',refs:[],children:[{href:'ugs-webapi.html#ugsww-state',id:'ugsww-state',text:'How to Test the State of Web Elements',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsww-checkboxradio',id:'ugsww-checkboxradio',text:'Form Checkboxes and Radiobuttons',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsww-text',id:'ugsww-text',text:'Form Text fields',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsww-select',id:'ugsww-select',text:'Form Selection Boxes',refs:[],children:[]}
,{href:'ugs-webapi.html#ug-web-access-table-cell-contents',id:'ug-web-access-table-cell-contents',text:'How to Access Table Cell Contents',refs:[],children:[]}
,{href:'ugs-webapi.html#ugsww-otherelem',id:'ugsww-otherelem',text:'Non-Form Elements and Synchronization',refs:[],children:[]}
]}
,{href:'ugs-webapi.html#ug-load-testing-web',id:'ug-load-testing-web',text:'How to Do Web Application Load Testing',refs:[],children:[{href:'ugs-webapi.html#tltw-sysinfo',id:'tltw-sysinfo',text:'Recording system information',refs:[],children:[]}
,{href:'ugs-webapi.html#tltw-loadgeneration',id:'tltw-loadgeneration',text:'Generating the load',refs:[],children:[]}
,{href:'ugs-webapi.html#tltw-postprocessing',id:'tltw-postprocessing',text:'Postprocessing',refs:[],children:[]}
]}
]}
,{href:'ug-how.to.use.the.windows.nativeobject.html',id:'ug-how.to.use.the.windows.nativeobject',text:'How to Use the Windows nativeObject API',refs:[],children:[]}
,{href:'ugs-teststatements.html',id:'ugs-teststatements',text:'How to Use Test Statements',refs:[],children:[]}
,{href:'ugs-eventhandlers.html',id:'ugs-eventhandlers',text:'How to Use Event Handlers',refs:[],children:[{href:'ugs-eventhandlers.html#ugseh-global',id:'ugseh-global',text:'Global Event Handlers',refs:[],children:[]}
,{href:'ugs-eventhandlers.html#ugseh-class',id:'ugseh-class',text:'Event Handlers for All Objects of a Specified Type',refs:[],children:[]}
,{href:'ugs-eventhandlers.html#ugseh-object',id:'ugseh-object',text:'Event Handlers for Specific Objects',refs:[],children:[]}
]}
,{href:'ugs-synchpoints.html',id:'ugs-synchpoints',text:'How to Create and Use Synchronization Points',refs:[],children:[]}
,{href:'ugs-webnonhtml.html',id:'ugs-webnonhtml',text:'How to Automate Native Browser Dialogs, ActiveX, and more',refs:[],children:[{href:'ugs-webnonhtml.html#automating-native-browser-dialogs-login-certificates-etc',id:'automating-native-browser-dialogs-login-certificates-etc',text:'Automating native browser dialogs (login, certificates, etc.)',refs:[],children:[{href:'ugs-webnonhtml.html#automating-a-native-login',id:'automating-a-native-login',text:'Automating a native login',refs:[],children:[]}
,{href:'ugs-webnonhtml.html#automating-accepting-certificates',id:'automating-accepting-certificates',text:'Automating accepting certificates',refs:[],children:[{href:'ugs-webnonhtml.html#internet-explorer-6-and-later',id:'internet-explorer-6-and-later',text:'Internet Explorer 8 and later',refs:[],children:[]}
,{href:'ugs-webnonhtml.html#mozilla-firefox',id:'mozilla-firefox',text:'Mozilla Firefox',refs:[],children:[]}
,{href:'ugs-webnonhtml.html#certificate-accepting-chrome',id:'certificate-accepting-chrome',text:'Google Chrome',refs:[],children:[]}
,{href:'ugs-webnonhtml.html#safari',id:'safari',text:'Safari',refs:[],children:[]}
]}
]}
,{href:'ugs-webnonhtml.html#active-x',id:'active-x',text:'ActiveX',refs:[],children:[]}
]}
,{href:'ugs-screenshotsonfail.html',id:'ugs-screenshotsonfail',text:'How to Create Automatic Screenshots on Test Failures and Errors',refs:[],children:[]}
,{href:'how.to.do.keyword.driven.testing.html',id:'how.to.do.keyword.driven.testing',text:'How to Do Keyword-Driven Testing',refs:[],children:[{href:'how.to.do.keyword.driven.testing.html#how.to.create.a.keyword.driven.test',id:'how.to.create.a.keyword.driven.test',text:'How to Create a Keyword-Driven Test',refs:[],children:[]}
,{href:'how.to.do.keyword.driven.testing.html#how.to.create.aut.specific.support.for.keyword.driven.tests',id:'how.to.create.aut.specific.support.for.keyword.driven.tests',text:'How to Create AUT-Specific Support for Keyword Driven Tests',refs:[],children:[]}
,{href:'how.to.do.keyword.driven.testing.html#how.to.create.a.generic.keyword.driver.function',id:'how.to.create.a.generic.keyword.driver.function',text:'How to Create a Generic Keyword Driver Function',refs:[],children:[]}
]}
,{href:'ug-interact-with-files-environment.html',id:'ug-interact-with-files-environment',text:'How to Interact with Files and with the Environment in Test Scripts',refs:[],children:[{href:'ug-interact-with-files-environment.html#ug-interact-with-external-files',id:'ug-interact-with-external-files',text:'How to Interact with External Files in Test Scripts',refs:[],children:[{href:'ug-interact-with-files-environment.html#ug-read-from-external-files',id:'ug-read-from-external-files',text:'How to Read Data from an External File',refs:[],children:[]}
,{href:'ug-interact-with-files-environment.html#ug-write-to-external-file',id:'ug-write-to-external-file',text:'How to Write Data to an External File',refs:[],children:[]}
,{href:'ug-interact-with-files-environment.html#ug-check-existence-external-files',id:'ug-check-existence-external-files',text:'How to Check the Existence of an External File',refs:[],children:[]}
,{href:'ug-interact-with-files-environment.html#ug-remove-external-file',id:'ug-remove-external-file',text:'How to Remove an External File',refs:[],children:[]}
]}
,{href:'ug-interact-with-files-environment.html#ug-comparingfiles',id:'ug-comparingfiles',text:'How to Compare External Files inside Test Scripts',refs:[],children:[]}
,{href:'ug-interact-with-files-environment.html#ug-external-environment',id:'ug-external-environment',text:'How to Read Environment Variables inside Test Scripts',refs:[],children:[]}
]}
,{href:'how.to.access.databases.from.squish.test.scripts.html',id:'how.to.access.databases.from.squish.test.scripts',text:'How to Access Databases from Squish Test Scripts',refs:[],children:[{href:'how.to.access.databases.from.squish.test.scripts.html#ug-how.to.compare.application.data.with.database.data',id:'ug-how.to.compare.application.data.with.database.data',text:'How to Compare Application Data with Database Data',refs:[],children:[{href:'how.to.access.databases.from.squish.test.scripts.html#comparing.a.gui.table.with.a.database.table.in.python',id:'comparing.a.gui.table.with.a.database.table.in.python',text:'Comparing a GUI Table with a Database Table in Python',refs:[],children:[]}
,{href:'how.to.access.databases.from.squish.test.scripts.html#comparing.a.gui.table.with.a.database.table.in.javascript',id:'comparing.a.gui.table.with.a.database.table.in.javascript',text:'Comparing a GUI Table with a Database Table in JavaScript',refs:[],children:[]}
,{href:'how.to.access.databases.from.squish.test.scripts.html#comparing.a.gui.table.with.a.database.table.in.perl',id:'comparing.a.gui.table.with.a.database.table.in.perl',text:'Comparing a GUI Table with a Database Table in Perl',refs:[],children:[]}
,{href:'how.to.access.databases.from.squish.test.scripts.html#comparing.a.gui.table.with.a.database.table.in.tcl',id:'comparing.a.gui.table.with.a.database.table.in.tcl',text:'Comparing a GUI Table with a Database Table in Tcl',refs:[],children:[]}
]}
,{href:'how.to.access.databases.from.squish.test.scripts.html#how.to.log.test.results.directly.into.a.database',id:'how.to.log.test.results.directly.into.a.database',text:'How to Log Test Results Directly into a Database',refs:[],children:[{href:'how.to.access.databases.from.squish.test.scripts.html#logging.results.directly.to.a.database.in.python',id:'logging.results.directly.to.a.database.in.python',text:'Logging Results Directly to a Database in Python',refs:[],children:[]}
,{href:'how.to.access.databases.from.squish.test.scripts.html#logging.results.directly.to.a.database.in.javascript',id:'logging.results.directly.to.a.database.in.javascript',text:'Logging Results Directly to a Database in JavaScript',refs:[],children:[]}
,{href:'how.to.access.databases.from.squish.test.scripts.html#logging.results.directly.to.a.database.in.perl',id:'logging.results.directly.to.a.database.in.perl',text:'Logging Results Directly to a Database in Perl',refs:[],children:[]}
,{href:'how.to.access.databases.from.squish.test.scripts.html#logging.results.directly.to.a.database.in.tcl',id:'logging.results.directly.to.a.database.in.tcl',text:'Logging Results Directly to a Database in Tcl',refs:[],children:[]}
]}
]}
,{href:'ug-how-to-handle-exceptions-raised-in-test-scripts.html',id:'ug-how-to-handle-exceptions-raised-in-test-scripts',text:'How to Handle Exceptions Raised in Test Scripts',refs:[],children:[]}
,{href:'how.to.modify.squish.functions.html',id:'how.to.modify.squish.functions',text:'How to Modify Squish Functions',refs:[],children:[]}
,{href:'ug-editdebug.html',id:'ug-editdebug',text:'How to Edit and Debug Test Scripts',refs:[],children:[{href:'ug-editdebug.html#uged-debugger',id:'uged-debugger',text:'How to Use the Script Debugger',refs:[],children:[]}
,{href:'ug-editdebug.html#uged-recordbp',id:'uged-recordbp',text:'How to Record After a Breakpoint',refs:[],children:[]}
,{href:'ug-editdebug.html#uged-spy',id:'uged-spy',text:'How to Use the Spy',refs:[],children:[]}
]}
,{href:'ug-vps.html',id:'ug-vps',text:'How to Create and Use Verification Points',refs:[],children:[{href:'ug-vps.html#ugv-objprop',id:'ugv-objprop',text:'How to Create and Use Property Verifications',refs:[],children:[]}
,{href:'ug-vps.html#ugv-table',id:'ugv-table',text:'How to Create and Use Table Verifications',refs:[],children:[]}
,{href:'ug-vps.html#ugv-screen',id:'ugv-screen',text:'How to Do Screenshot Verifications',refs:[],children:[{href:'ug-vps.html#ugvs-create',id:'ugvs-create',text:'How to Create and Use Screenshot Verifications',refs:[],children:[]}
,{href:'ug-vps.html#ugvs-mask',id:'ugvs-mask',text:'Image Masks',refs:[],children:[]}
]}
,{href:'ug-vps.html#ugv-visual',id:'ugv-visual',text:'How to Do Visual Verifications',refs:[],children:[{href:'ug-vps.html#ugvv-create',id:'ugvv-create',text:'How to Create and Use Visual Verifications',refs:[],children:[]}
,{href:'ug-vps.html#ugvv-checks',id:'ugvv-checks',text:'Visual Verification Check Stages',refs:[],children:[{href:'ug-vps.html',id:'',text:'Hierarchy Check',refs:[],children:[]}
,{href:'ug-vps.html',id:'',text:'Identify Check',refs:[],children:[]}
,{href:'ug-vps.html',id:'',text:'Content Check',refs:[],children:[]}
,{href:'ug-vps.html',id:'',text:'Geometry Check',refs:[],children:[]}
,{href:'ug-vps.html',id:'',text:'Screenshot Check',refs:[],children:[]}
]}
]}
,{href:'ug-vps.html#ugv-scripting',id:'ugv-scripting',text:'How to Create and Use Property Verification Points in Test Scripts',refs:[],children:[]}
]}
,{href:'ug-shareddata.html',id:'ug-shareddata',text:'How to Create and Use Shared Data and Shared Scripts',refs:[],children:[{href:'ug-shareddata.html#ugsh-datascripts',id:'ugsh-datascripts',text:'How to Store and Locate Shared Scripts and Shared Data Files',refs:[],children:[]}
,{href:'ug-shareddata.html#ugsh-datadriven',id:'ugsh-datadriven',text:'How to Do Data-Driven Testing',refs:[],children:[]}
,{href:'ug-shareddata.html#ugsh-aut',id:'ugsh-aut',text:'How to Use Test Data in the AUT',refs:[],children:[]}
]}
,{href:'ug-imagebasedtesting.html',id:'ug-imagebasedtesting',text:'How to Do Image-Based Testing',refs:[],children:[{href:'ug-imagebasedtesting.html',id:'',text:'Start a New Game',refs:[],children:[]}
,{href:'ug-imagebasedtesting.html',id:'',text:'Move the Pawn',refs:[],children:[]}
,{href:'ug-imagebasedtesting.html',id:'',text:'The Recorded Script',refs:[],children:[]}
,{href:'ug-imagebasedtesting.html',id:'',text:'Tolerant image search',refs:[],children:[]}
,{href:'ug-imagebasedtesting.html',id:'',text:'Verifications by Image-Search',refs:[],children:[]}
]}
,{href:'ug-batchtesting.html',id:'ug-batchtesting',text:'How to Do Automated Batch Testing',refs:[],children:[{href:'ug-batchtesting.html#tb-process',id:'tb-process',text:'Processing Test Results',refs:[],children:[{href:'ug-batchtesting.html#generating-html-test-results',id:'generating-html-test-results',text:'Generating HTML Test Results',refs:[],children:[]}
,{href:'ug-batchtesting.html#generating-xml-test-results',id:'generating-xml-test-results',text:'Generating XML Test Results',refs:[],children:[]}
]}
,{href:'ug-batchtesting.html#tb-autorun',id:'tb-autorun',text:'Automatically Running Tests',refs:[],children:[]}
,{href:'ug-batchtesting.html#tb-conclusion',id:'tb-conclusion',text:'Conclusion',refs:[],children:[]}
]}
,{href:'ug.how.to.create.cross.platform.tests.html',id:'ug.how.to.create.cross.platform.tests',text:'How to Create Cross-Platform Tests',refs:[],children:[{href:'ug.how.to.create.cross.platform.tests.html#ug.how.to.create.cross.platform.java.awt.swing.tests',id:'ug.how.to.create.cross.platform.java.awt.swing.tests',text:'How to Create Cross-Platform Java AWT/Swing Tests',refs:[],children:[]}
]}
]}
,{href:'api.man.html',id:'api.man',text:'API Reference Manual',refs:[],children:[{href:'api.special.functions.html',id:'api.special.functions',text:'Tester-Created Special Functions',refs:[],children:[]}
,{href:'rgs-scripteq.html',id:'rgs-scripteq',text:'Equivalent Script API',refs:[],children:[]}
,{href:'rgs-squish.html',id:'rgs-squish',text:'Squish API',refs:[],children:[{href:'rgs-squish.html#rgss-cfp',id:'rgss-cfp',text:'Constructors, Functions and Properties',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-objh',id:'rgss-objh',text:'Object Access Functions',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-sync-funcs',id:'rgss-sync-funcs',text:'Synchronization Functions',refs:[],children:[]}
,{href:'rgs-squish.html#interaction.functions',id:'interaction.functions',text:'Interaction Functions',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-debugging',id:'rgss-debugging',text:'Debugging Functions',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-conversions',id:'rgss-conversions',text:'Conversion Functions',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-teststatements',id:'rgss-teststatements',text:'Verification Functions',refs:[],children:[{href:'rgs-squish.html#bracketing-function-calls',id:'bracketing-function-calls',text:'Ensuring that functions are always called pairwise',refs:[],children:[]}
]}
,{href:'rgs-squish.html#rgss-createvvp',id:'rgss-createvvp',text:'Script-based Creation of Visual Verification Points',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-testdata',id:'rgss-testdata',text:'Test Data Functions',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-objmap',id:'rgss-objmap',text:'Object Map Functions',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-appcontext',id:'rgss-appcontext',text:'Application Context',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-image-object',id:'rgss-image-object',text:'Image Object',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-screen-object',id:'rgss-screen-object',text:'Screen Object',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-toplevelwindow-object',id:'rgss-toplevelwindow-object',text:'ToplevelWindow Object',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-info',id:'rgss-info',text:'squishinfo Object',refs:[],children:[]}
,{href:'rgs-squish.html#rgs-testinteraction',id:'rgs-testinteraction',text:'testInteraction Functions',refs:[],children:[]}
,{href:'rgs-squish.html#rgs-testsettings',id:'rgs-testsettings',text:'testSettings Object',refs:[],children:[{href:'rgs-squish.html#rgs-testsettings-config',id:'rgs-testsettings-config',text:'Set initial testSettings Object values in the config.xml',refs:[],children:[]}
]}
,{href:'rgs-squish.html#rgs-remotesystem',id:'rgs-remotesystem',text:'RemoteSystem Object',refs:[],children:[]}
,{href:'rgs-squish.html#rg-user.interface.types',id:'rg-user.interface.types',text:'User Interface Types',refs:[],children:[{href:'rgs-squish.html#rg-datetime.type',id:'rg-datetime.type',text:'DateTime Type',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-screenpoint-object',id:'rgss-screenpoint-object',text:'UiTypes.ScreenPoint Class',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-screenrectangle-object',id:'rgss-screenrectangle-object',text:'UiTypes.ScreenRectangle Class',refs:[],children:[]}
]}
,{href:'rgs-squish.html#rgss-misc',id:'rgss-misc',text:'Miscellaneous Functions',refs:[],children:[]}
,{href:'rgs-squish.html#rgss-misc-low-level',id:'rgss-misc-low-level',text:'Low-Level Functions',refs:[],children:[]}
]}
,{href:'rgs-webconvenience.html',id:'rgs-webconvenience',text:'Web Object API',refs:[],children:[{href:'rgs-webconvenience.html#web-Browser-object',id:'web-Browser-object',text:'Browser Object',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-BrowserTab-class',id:'web-BrowserTab-class',text:'BrowserTab Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Anchor-class',id:'web-HTML_Anchor-class',text:'HTML_Anchor Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Array-class',id:'web-HTML_Array-class',text:'HTML_Array Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Button-class',id:'web-HTML_Button-class',text:'HTML_Button Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_ButtonBase-class',id:'web-HTML_ButtonBase-class',text:'HTML_ButtonBase Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CheckBox-class',id:'web-HTML_CheckBox-class',text:'HTML_CheckBox Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CalendarEvent-class',id:'web-HTML_CalendarEvent-class',text:'HTML_CalendarEvent Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CalendarView-class',id:'web-HTML_CalendarView-class',text:'HTML_CalendarView Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_ColorField-class',id:'web-HTML_ColorField-class',text:'HTML_ColorField Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CustomButton-class',id:'web-HTML_CustomButton-class',text:'HTML_CustomButton Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CustomButtonBase-class',id:'web-HTML_CustomButtonBase-class',text:'HTML_CustomButtonBase Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CustomComboBox-class',id:'web-HTML_CustomComboBox-class',text:'HTML_CustomComboBox Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CustomCheckbox-class',id:'web-HTML_CustomCheckbox-class',text:'HTML_CustomCheckbox Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CustomItem-class',id:'web-HTML_CustomItem-class',text:'HTML_CustomItem Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CustomItemView-class',id:'web-HTML_CustomItemView-class',text:'HTML_CustomItemView Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CustomRadioButton-class',id:'web-HTML_CustomRadioButton-class',text:'HTML_CustomRadioButton Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CustomSelectList-class',id:'web-HTML_CustomSelectList-class',text:'HTML_CustomSelectList Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_CustomText-class',id:'web-HTML_CustomText-class',text:'HTML_CustomText Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_DateChooser-class',id:'web-HTML_DateChooser-class',text:'HTML_DateChooser Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Document-class',id:'web-HTML_Document-class',text:'HTML_Document Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_ExpandableSectionHeader-class',id:'web-HTML_ExpandableSectionHeader-class',text:'HTML_ExpandableSectionHeader Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Form-class',id:'web-HTML_Form-class',text:'HTML_Form Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_FormElement-class',id:'web-HTML_FormElement-class',text:'HTML_FormElement Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_ImageButton-class',id:'web-HTML_ImageButton-class',text:'HTML_ImageButton Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Menu-class',id:'web-HTML_Menu-class',text:'HTML_Menu Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_MenuButton-class',id:'web-HTML_MenuButton-class',text:'HTML_MenuButton Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_MenuItem-class',id:'web-HTML_MenuItem-class',text:'HTML_MenuItem Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Object-class',id:'web-HTML_Object-class',text:'HTML_Object Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Option-class',id:'web-HTML_Option-class',text:'HTML_Option Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_RadioButton-class',id:'web-HTML_RadioButton-class',text:'HTML_RadioButton Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Select-class',id:'web-HTML_Select-class',text:'HTML_Select Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_ProgressBar-class',id:'web-HTML_ProgressBar-class',text:'HTML_ProgressBar Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Style-class',id:'web-HTML_Style-class',text:'HTML_Style Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Table-class',id:'web-HTML_Table-class',text:'HTML_Table Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Tab-class',id:'web-HTML_Tab-class',text:'HTML_Tab Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_TabWidget-class',id:'web-HTML_TabWidget-class',text:'HTML_TabWidget Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_Text-class',id:'web-HTML_Text-class',text:'HTML_Text Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_TextArea-class',id:'web-HTML_TextArea-class',text:'HTML_TextArea Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_TextBase-class',id:'web-HTML_TextBase-class',text:'HTML_TextBase Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-HTML_XPathResult-class',id:'web-HTML_XPathResult-class',text:'HTML_XPathResult Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#web-JsObject-class',id:'web-JsObject-class',text:'JsObject Class',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-jsapi',id:'rg-jsapi',text:'JavaScript Extension API',refs:[],children:[{href:'rgs-webconvenience.html#web-Squish-object',id:'web-Squish-object',text:'Squish Object',refs:[],children:[{href:'rgs-webconvenience.html#web-Squish.ObjectName-object',id:'web-Squish.ObjectName-object',text:'ObjectName Object',refs:[],children:[]}
]}
,{href:'rgs-webconvenience.html#ugjs-concept',id:'ugjs-concept',text:'Concepts and Setup',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ugjs-simplenamegen',id:'ugjs-simplenamegen',text:'How to adjust the list of properties used for generating names',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ugjs-simplewidget',id:'ugjs-simplewidget',text:'How to set up Support for Simple Widgets',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ugjs-customname',id:'ugjs-customname',text:'How to Extend the Name Generator and Identification',refs:[],children:[{href:'rgs-webconvenience.html#ugjscn-generate',id:'ugjscn-generate',text:'How to Implement a Custom Name Generator',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ugjscn-match',id:'ugjscn-match',text:'How to Implement a Custom Name Matcher',refs:[],children:[]}
]}
,{href:'rgs-webconvenience.html#ugjs-complexwidgets',id:'ugjs-complexwidgets',text:'How to set up Support for Complex Widgets',refs:[],children:[{href:'rgs-webconvenience.html#ugjscw-structure',id:'ugjscw-structure',text:'DOM structure of the widget',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ugjscw-record',id:'ugjscw-record',text:'Hooks for recording high-level GUI operations',refs:[],children:[{href:'rgs-webconvenience.html#ug.js-extension-api.event.objects',id:'ug.js-extension-api.event.objects',text:'Event Objects',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ug.js-extension-api.object.types',id:'ug.js-extension-api.object.types',text:'Object Types',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ug.js-extension-api.object.names',id:'ug.js-extension-api.object.names',text:'Object Names',refs:[],children:[]}
,{href:'rgs-webconvenience.html',id:'',text:'Register Hook Functions',refs:[],children:[]}
]}
,{href:'rgs-webconvenience.html#ugjscw-replay',id:'ugjscw-replay',text:'Hooks for replaying high-level GUI operations',refs:[],children:[{href:'rgs-webconvenience.html#ug.js-extension-api.find.item',id:'ug.js-extension-api.find.item',text:'Find Item',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ug.js-extension-api.item.handle',id:'ug.js-extension-api.item.handle',text:'Item Handle',refs:[],children:[]}
]}
,{href:'rgs-webconvenience.html#ugjscw-api',id:'ugjscw-api',text:'Exposing the Item View API',refs:[],children:[{href:'rgs-webconvenience.html#ugjscwa-text',id:'ugjscwa-text',text:'Item Text',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ugjscwa-sel',id:'ugjscwa-sel',text:'Item Selection',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ugjscwa-open',id:'ugjscwa-open',text:'Item Handle\'s State',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ugjscwa-item',id:'ugjscwa-item',text:'Item Traversal',refs:[],children:[]}
,{href:'rgs-webconvenience.html#ugjscwa-columns',id:'ugjscwa-columns',text:'Columns',refs:[],children:[]}
]}
,{href:'rgs-webconvenience.html#ugjs-testing',id:'ugjs-testing',text:'Testing and Conclusion',refs:[],children:[]}
]}
,{href:'rgs-webconvenience.html#ug-writing.html.class.specific.extensions',id:'ug-writing.html.class.specific.extensions',text:'Writing HTML Class-Specific Extensions',refs:[],children:[{href:'rgs-webconvenience.html#rg-web-button.support',id:'rg-web-button.support',text:'Button Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-combobox.support',id:'rg-web-combobox.support',text:'Combobox Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-calendarevent.support',id:'rg-web-calendarevent.support',text:'Calendar Event Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-calendarview.support',id:'rg-web-calendarview.support',text:'Calendar View Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-checkbox.support',id:'rg-web-checkbox.support',text:'CheckBox Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-color.picker.support',id:'rg-web-color.picker.support',text:'Color Picker Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-date.picker.support',id:'rg-web-date.picker.support',text:'Date Picker Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-expandable.section.header',id:'rg-web-expandable.section.header',text:'Expandable Section Header Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-menu.support',id:'rg-web-menu.support',text:'Menu Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-menubutton.support',id:'rg-web-menubutton.support',text:'Menu Button Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-menuitem.support',id:'rg-web-menuitem.support',text:'Menu Item Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-progress.bar.support',id:'rg-web-progress.bar.support',text:'Progress Bar Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-radiobutton.support',id:'rg-web-radiobutton.support',text:'RadioButton Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-selectlist.support',id:'rg-web-selectlist.support',text:'Select List Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-tab.widget.support',id:'rg-web-tab.widget.support',text:'Tab Widget Support',refs:[],children:[]}
,{href:'rgs-webconvenience.html#rg-web-textfield.support',id:'rg-web-textfield.support',text:'Text Field Support',refs:[],children:[]}
]}
]}
]}
,{href:'rgs-windowsobjectapi.html',id:'rgs-windowsobjectapi',text:'Windows Object API',refs:[],children:[]}
,{href:'rgs-py.html',id:'rgs-py',text:'Python Notes',refs:[],children:[{href:'rgs-py.html#default.file.encoding.utf8',id:'default.file.encoding.utf8',text:'Default File Encoding (UTF-8)',refs:[],children:[]}
,{href:'rgs-py.html#str.ret.type.def.unicode',id:'str.ret.type.def.unicode',text:'Default type of string return values (Unicode)',refs:[],children:[]}
,{href:'rgs-py.html#squish.python.modules',id:'squish.python.modules',text:'Squish\'s Python Modules',refs:[],children:[]}
,{href:'rgs-py.html#hidden.py.symbols',id:'hidden.py.symbols',text:'Python Symbols Hidden by Squish',refs:[],children:[{href:'rgs-py.html#squish.bool',id:'squish.bool',text:'Squish\'s bool vs. Python\'s bool',refs:[],children:[]}
,{href:'rgs-py.html#squish.int',id:'squish.int',text:'Squish\'s int vs. Python\'s int',refs:[],children:[]}
,{href:'rgs-py.html#squish.long',id:'squish.long',text:'Squish\'s long vs. Python\'s long',refs:[],children:[]}
,{href:'rgs-py.html#squish.object',id:'squish.object',text:'Squish\'s object vs. Python\'s object',refs:[],children:[]}
,{href:'rgs-py.html#squish.type',id:'squish.type',text:'Squish\'s type() vs. Python\'s type()',refs:[],children:[]}
]}
,{href:'rgs-py.html#import.custom.modules',id:'import.custom.modules',text:'Importing Custom Python Modules',refs:[],children:[]}
,{href:'rgs-py.html#squish.funcs.in.py.modules',id:'squish.funcs.in.py.modules',text:'Using Squish Functions in Python Modules/Packages',refs:[],children:[]}
,{href:'rgs-py.html#rgs-python-documentation',id:'rgs-python-documentation',text:'Python Language Documentation',refs:[],children:[]}
]}
,{href:'rgs-tcl.html',id:'rgs-tcl',text:'Tcl Notes',refs:[],children:[{href:'rgs-tcl.html#rgs-tcl-documentation',id:'rgs-tcl-documentation',text:'Tcl Language Documentation',refs:[],children:[]}
]}
,{href:'rgs-js.html',id:'rgs-js',text:'JavaScript Notes and Extension APIs',refs:[],children:[{href:'rgs-js.html#rgs-js-documentation',id:'rgs-js-documentation',text:'JavaScript Language Documentation',refs:[],children:[]}
,{href:'rgs-js.html#language-core',id:'language-core',text:'Language Core',refs:[],children:[]}
,{href:'rgs-js.html#file-object',id:'file-object',text:'File Object',refs:[],children:[]}
,{href:'rgs-js.html#console-object',id:'console-object',text:'Console Object',refs:[],children:[]}
,{href:'rgs-js.html#os-object',id:'os-object',text:'OS Object',refs:[],children:[]}
,{href:'rgs-js.html#xml-object',id:'xml-object',text:'XML Object',refs:[],children:[]}
,{href:'rgs-js.html#sql',id:'sql',text:'SQL',refs:[],children:[{href:'rgs-js.html#sql-object',id:'sql-object',text:'SQL Object',refs:[],children:[]}
,{href:'rgs-js.html#sqlconnection-object',id:'sqlconnection-object',text:'SQLConnection Object',refs:[],children:[]}
,{href:'rgs-js.html#sqlresult-object',id:'sqlresult-object',text:'SQLResult Object',refs:[],children:[]}
]}
,{href:'rgs-js.html#socket-object',id:'socket-object',text:'Socket Object',refs:[],children:[]}
,{href:'rgs-js.html#ws-object',id:'ws-object',text:'WebSocket Object',refs:[],children:[]}
,{href:'rgs-js.html#xhr-object',id:'xhr-object',text:'XMLHttpRequest Object',refs:[],children:[]}
]}
,{href:'rgs-pl.html',id:'rgs-pl',text:'Perl Notes',refs:[],children:[{href:'rgs-pl.html#rgs-perl-documentation',id:'rgs-perl-documentation',text:'Perl Language Documentation',refs:[],children:[]}
]}
,{href:'rgs-rb.html',id:'rgs-rb',text:'Ruby Notes',refs:[],children:[{href:'rgs-rb.html#rgs-ruby-documentation',id:'rgs-ruby-documentation',text:'Ruby Language Documentation',refs:[],children:[]}
]}
,{href:'api.bdt.functions.html',id:'api.bdt.functions',text:'Behavior Driven Testing',refs:[],children:[{href:'api.bdt.functions.html#api.bdt.functions.step',id:'api.bdt.functions.step',text:'Defining Step Implementations using Step',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.functions.simpleplaceholders',id:'api.bdt.functions.simpleplaceholders',text:'Using Step Patterns with Placeholders',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.functions.regexpplaceholders',id:'api.bdt.functions.regexpplaceholders',text:'Using Step Patterns with Regular Expressions',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.members.context',id:'api.bdt.members.context',text:'The BDD context Object',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.members.context.userData',id:'api.bdt.members.context.userData',text:'context.userData: Passing Data Between Steps',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.members.context.table.multiLineText',id:'api.bdt.members.context.table.multiLineText',text:'Accessing Tables and Multi-Line Text',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.functions.givenwhenthen',id:'api.bdt.functions.givenwhenthen',text:'Defining Step Implementations using Given/When/Then',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.functions.overriding_steps',id:'api.bdt.functions.overriding_steps',text:'Step Lookup Order &amp; Overriding Shared Step Implementations',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.functions.aborting_scenarios',id:'api.bdt.functions.aborting_scenarios',text:'Influencing Scenario Execution from Within Step Implementations',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.functions.hooks',id:'api.bdt.functions.hooks',text:'Performing Actions During Test Execution Via Hooks',refs:[],children:[]}
,{href:'api.bdt.functions.html#api.bdt.testcase.anatomy',id:'api.bdt.testcase.anatomy',text:'The Anatomy of a BDD Test Case',refs:[],children:[]}
]}
,{href:'api.fmi.interface.html',id:'api.fmi.interface',text:'FMI Interface support',refs:[],children:[{href:'api.fmi.interface.html#api.fmi.overview',id:'api.fmi.overview',text:'FMI Overview',refs:[],children:[]}
,{href:'api.fmi.interface.html#api.fmi.import',id:'api.fmi.import',text:'Fmi2Import class',refs:[],children:[]}
,{href:'api.fmi.interface.html#api.fmi.variable',id:'api.fmi.variable',text:'Fmi2Variable class',refs:[],children:[]}
,{href:'api.fmi.interface.html#api.fmi.type',id:'api.fmi.type',text:'Fmi2Type class',refs:[],children:[]}
,{href:'api.fmi.interface.html#api.fmi.unit',id:'api.fmi.unit',text:'Fmi2Unit class',refs:[],children:[]}
,{href:'api.fmi.interface.html#api.fmi.state',id:'api.fmi.state',text:'Fmi2State class',refs:[],children:[]}
,{href:'api.fmi.interface.html#api.fmi.examples',id:'api.fmi.examples',text:'Examples',refs:[],children:[{href:'api.fmi.interface.html',id:'',text:'Running a simulation',refs:[],children:[]}
,{href:'api.fmi.interface.html',id:'',text:'Running a simulation in parallel to the AUT',refs:[],children:[]}
,{href:'api.fmi.interface.html',id:'',text:'Waiting for simulation results',refs:[],children:[]}
]}
]}
]}
,{href:'ref.man.html',id:'ref.man',text:'Tools Reference Manual',refs:[],children:[{href:'rg-regressiontesting.html',id:'rg-regressiontesting',text:'Automated Batch Testing',refs:[],children:[{href:'rg-regressiontesting.html#rgr-autotests',id:'rgr-autotests',text:'Automated Test Runs',refs:[],children:[]}
,{href:'rg-regressiontesting.html#rgr-disttesting',id:'rgr-disttesting',text:'Distributed Tests',refs:[],children:[]}
,{href:'rg-regressiontesting.html#rgr-processresults',id:'rgr-processresults',text:'Processing Test Results',refs:[],children:[{href:'rg-regressiontesting.html#rgr-processresults-xml',id:'rgr-processresults-xml',text:'The xml Report Format',refs:[],children:[]}
]}
]}
,{href:'rg-cmdline.html',id:'rg-cmdline',text:'Command Line Reference',refs:[],children:[{href:'rg-cmdline.html#rgc-installer',id:'rgc-installer',text:'Installer',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishconfig',id:'rg-squishconfig',text:'squishconfig',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli',id:'rg-squishrunner-cli',text:'squishrunner',refs:[],children:[{href:'rg-cmdline.html#rg-squishrunner-cli-server-options',id:'rg-squishrunner-cli-server-options',text:'Server Options',refs:[],children:[{href:'rg-cmdline.html#rgcr-debuglog',id:'rgcr-debuglog',text:'squishrunner --debugLog',refs:[],children:[]}
]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-suite.conf',id:'rg-squishrunner-cli-suite.conf',text:'The suite.conf File',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli--testsuite',id:'rg-squishrunner-cli--testsuite',text:'squishrunner --testsuite: Running tests in batch mode',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--resultdir',id:'rg-squishrunner-cli-playback--resultdir',text:'squishrunner --resultdir: Setting the Result Directory',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-report-generators',id:'rg-squishrunner-report-generators',text:'squishrunner --reportgen: Generating Reports',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--testcase',id:'rg-squishrunner-cli-playback--testcase',text:'Playback option --testcase',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli--aut',id:'rg-squishrunner-cli--aut',text:'Playback option --aut',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli---interactive',id:'rg-squishrunner-cli---interactive',text:'Playback option --interactive',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--abortOnFail',id:'rg-squishrunner-cli-playback--abortOnFail',text:'Playback option --abortOnFail',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--webbrowser',id:'rg-squishrunner-cli-playback--webbrowser',text:'Playback option --webbrowser',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--device',id:'rg-squishrunner-cli-playback--device',text:'Playback option --device',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--snoozeFactor',id:'rg-squishrunner-cli-playback--snoozeFactor',text:'Playback option --snoozeFactor',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--exitCodeOnFail',id:'rg-squishrunner-cli-playback--exitCodeOnFail',text:'Playback option --exitCodeOnFail',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--timeout',id:'rg-squishrunner-cli-playback--timeout',text:'Playback option --timeout',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--retry',id:'rg-squishrunner-cli-playback--retry',text:'Playback option --retry',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--scriptargs',id:'rg-squishrunner-cli-playback--scriptargs',text:'Playback option --scriptargs',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--tags',id:'rg-squishrunner-cli-playback--tags',text:'Playback option --tags',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-playback--random',id:'rg-squishrunner-cli-playback--random',text:'Playback option --random',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli--testsuite--record',id:'rg-squishrunner-cli--testsuite--record',text:'squishrunner --record: Recording a Test Case',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-recording--disableEventCompression',id:'rg-squishrunner-cli-recording--disableEventCompression',text:'Recording option --disableEventCompression',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-recording--webbrowser',id:'rg-squishrunner-cli-recording--webbrowser',text:'Recording option --webbrowser',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli-recording--aut',id:'rg-squishrunner-cli-recording--aut',text:'Recording option --aut',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli--testcase',id:'rg-squishrunner-cli--testcase',text:'Executing a Test Case (Advanced)',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli--info',id:'rg-squishrunner-cli--info',text:'Querying for Information',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishrunner-cli--config',id:'rg-squishrunner-cli--config',text:'Configuring squishrunner',refs:[],children:[]}
]}
,{href:'rg-cmdline.html#rgc-squishserver',id:'rgc-squishserver',text:'squishserver',refs:[],children:[{href:'rg-cmdline.html#rg-squishserver--port',id:'rg-squishserver--port',text:'Serving',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishserver--stop',id:'rg-squishserver--stop',text:'Stopping',refs:[],children:[]}
,{href:'rg-cmdline.html#rg-squishserver--config',id:'rg-squishserver--config',text:'Configuring squishserver',refs:[],children:[]}
]}
,{href:'rg-cmdline.html#rgc-squishidl',id:'rgc-squishidl',text:'squishidl',refs:[],children:[]}
,{href:'rg-cmdline.html#rgc-squishide',id:'rgc-squishide',text:'squishide',refs:[],children:[]}
,{href:'rg-cmdline.html#rgc-startaut-cli',id:'rgc-startaut-cli',text:'start*aut',refs:[],children:[{href:'rg-cmdline.html#startaut',id:'startaut',text:'startaut',refs:[],children:[]}
,{href:'rg-cmdline.html#androidobserver',id:'androidobserver',text:'androidobserver',refs:[],children:[{href:'rg-cmdline.html#androidobserver-device',id:'androidobserver-device',text:'--device',refs:[],children:[]}
,{href:'rg-cmdline.html#androidobserver-clear',id:'androidobserver-clear',text:'--clear-app-settings',refs:[],children:[]}
,{href:'rg-cmdline.html#androidobserver-force-rotation',id:'androidobserver-force-rotation',text:'--force-rotation',refs:[],children:[]}
,{href:'rg-cmdline.html#androidobserver-port',id:'androidobserver-port',text:'--port',refs:[],children:[]}
,{href:'rg-cmdline.html#androidobserver-attach-port',id:'androidobserver-attach-port',text:'--attach-port',refs:[],children:[]}
,{href:'rg-cmdline.html#androidobserver-no-autostart-observer',id:'androidobserver-no-autostart-observer',text:'--no-autostart-observer',refs:[],children:[]}
,{href:'rg-cmdline.html#androidobserver-no-ui-automation',id:'androidobserver-no-ui-automation',text:'--no-ui-automation',refs:[],children:[]}
,{href:'rg-cmdline.html#androidobserver-web-hookup-timeout',id:'androidobserver-web-hookup-timeout',text:'--web-hookup-timeout',refs:[],children:[]}
,{href:'rg-cmdline.html#androidobserver-extra',id:'androidobserver-extra',text:'-e key value',refs:[],children:[]}
]}
,{href:'rg-cmdline.html#startjavaaut',id:'startjavaaut',text:'startjavaaut',refs:[],children:[]}
,{href:'rg-cmdline.html#startwinaut',id:'startwinaut',text:'startwinaut',refs:[],children:[]}
]}
,{href:'rg-cmdline.html#rg-webproxy',id:'rg-webproxy',text:'Web Proxy',refs:[],children:[]}
,{href:'rg-cmdline.html#rgc-convertvp',id:'rgc-convertvp',text:'convertvp',refs:[],children:[]}
,{href:'rg-cmdline.html#rgc-xml2jira',id:'rgc-xml2jira',text:'xml2jira',refs:[],children:[{href:'rg-cmdline.html#rgc-xml2jira-synopsis',id:'rgc-xml2jira-synopsis',text:'Synopsis',refs:[],children:[]}
,{href:'rg-cmdline.html#rgc-xml2jira-examples',id:'rgc-xml2jira-examples',text:'Examples',refs:[],children:[]}
]}
,{href:'rg-cmdline.html#rgc-commandfiles',id:'rgc-commandfiles',text:'Command Files',refs:[],children:[]}
,{href:'rg-cmdline.html#rgc-configure',id:'rgc-configure',text:'configure',refs:[],children:[]}
]}
,{href:'rg-envvars.html',id:'rg-envvars',text:'Environment Variables',refs:[],children:[]}
,{href:'rg-ini-files.html',id:'rg-ini-files',text:'Squish Initialization Files',refs:[],children:[{href:'rg-ini-files.html',id:'',text:'.ini Files',refs:[],children:[{href:'rg-ini-files.html#rg-paths.ini-file',id:'rg-paths.ini-file',text:'paths.ini',refs:[],children:[]}
,{href:'rg-ini-files.html#rg-squish.ini-file',id:'rg-squish.ini-file',text:'squish.ini',refs:[],children:[]}
]}
,{href:'rg-ini-files.html#ignoredauts.txt',id:'ignoredauts.txt',text:'ignoredauts.txt',refs:[],children:[]}
]}
,{href:'rghsa-sub.html',id:'rghsa-sub',text:'Record / Replay on Sub-Processes started by the AUT',refs:[],children:[{href:'rghsa-sub.html#rg.squish.for.java.sub.processes',id:'rg.squish.for.java.sub.processes',text:'Hooking up Java Sub-Processes in Squish for Java',refs:[],children:[]}
,{href:'rghsa-sub.html#rg.squish.for.qt.sub.processes',id:'rg.squish.for.qt.sub.processes',text:'Hooking up Qt Sub-Processes in Squish for Qt',refs:[],children:[{href:'rghsa-sub.html#rg.squish.for.qt.sub.processes.on.windows',id:'rg.squish.for.qt.sub.processes.on.windows',text:'Additional Setup for Hooking up Qt Sub-Processes on Windows',refs:[],children:[]}
,{href:'rghsa-sub.html#rg.squish.for.qt.sub.processes.on.windows.non.invasively',id:'rg.squish.for.qt.sub.processes.on.windows.non.invasively',text:'Additional Setup for Hooking up Qt Sub-Processes on Windows Non-Invasively',refs:[],children:[]}
]}
,{href:'rghsa-sub.html#rg.squish.for.windows.sub.processes',id:'rg.squish.for.windows.sub.processes',text:'Hooking up Sub-Processes in Squish for Windows',refs:[],children:[]}
,{href:'rghsa-sub.html#rg.squish.for.macosx.sub.processes',id:'rg.squish.for.macosx.sub.processes',text:'Hooking up Sub-Processes in Squish for macOS',refs:[],children:[]}
]}
,{href:'rghsa-attach.html',id:'rghsa-attach',text:'Attaching to Running Applications',refs:[],children:[{href:'rghsa-attach.html#overview',id:'overview',text:'Overview',refs:[],children:[]}
,{href:'rghsa-attach.html#start-the-aut',id:'start-the-aut',text:'Start the AUT',refs:[],children:[]}
,{href:'rghsa-attach.html#rghsa-attach-registeraut',id:'rghsa-attach-registeraut',text:'Register the AUT',refs:[],children:[]}
,{href:'rghsa-attach.html#rghsa-attach-scripting',id:'rghsa-attach-scripting',text:'Attaching from a Script',refs:[],children:[]}
]}
,{href:'rg-attachToBrowser.html',id:'rg-attachToBrowser',text:'Attaching to a running Web Browser',refs:[],children:[{href:'rg-attachToBrowser.html#rg-attachToBrowser-Overview',id:'rg-attachToBrowser-Overview',text:'Overview and Motivation',refs:[],children:[]}
,{href:'rg-attachToBrowser.html#rg-attachToBrowser-preparation',id:'rg-attachToBrowser-preparation',text:'Making the Browser attachable',refs:[],children:[]}
,{href:'rg-attachToBrowser.html#rg-attachToBrowser-script',id:'rg-attachToBrowser-script',text:'Attaching to the Browser from the Script',refs:[],children:[]}
]}
,{href:'rg-namelookup.html',id:'rg-namelookup',text:'Improving Object Identification',refs:[],children:[{href:'rg-namelookup.html#matching.objects.whose.name.can.change',id:'matching.objects.whose.name.can.change',text:'Matching Objects with Changeable Texts',refs:[],children:[]}
,{href:'rg-namelookup.html#exact-matching-with',id:'exact-matching-with',text:'Exact Matching with =',refs:[],children:[]}
,{href:'rg-namelookup.html#wildcard-matching-with',id:'wildcard-matching-with',text:'Wildcard Matching with ?=',refs:[],children:[]}
,{href:'rg-namelookup.html#regular-expression-regex-matching-with',id:'regular-expression-regex-matching-with',text:'Regular Expression (regex) Matching with ~=',refs:[],children:[]}
,{href:'rg-namelookup.html#real.multi.property.name.properties',id:'real.multi.property.name.properties',text:'Real (Multi-Property) Name Properties',refs:[],children:[]}
]}
,{href:'rg-objectmap.html',id:'rg-objectmap',text:'Object Map',refs:[],children:[{href:'rg-objectmap.html#rgo-concept',id:'rgo-concept',text:'The Concept of the Object Map',refs:[],children:[]}
,{href:'rg-objectmap.html#rgos-create',id:'rgos-create',text:'Creating an Object Map',refs:[],children:[]}
,{href:'rg-objectmap.html#rgos-edit',id:'rgos-edit',text:'Editing an Object Map',refs:[],children:[]}
,{href:'rg-objectmap.html#rgo-squish-scriptbased',id:'rgo-squish-scriptbased',text:'Script-Based Object Map',refs:[],children:[{href:'rg-objectmap.html#rgo-squish-scriptbased-storage',id:'rgo-squish-scriptbased-storage',text:'Storage Location of Script-Based Object Maps',refs:[],children:[{href:'rg-objectmap.html#rgo-squish-scriptbased-shared-map',id:'rgo-squish-scriptbased-shared-map',text:'Shared and Split Script-Based Object Maps',refs:[],children:[]}
]}
,{href:'rg-objectmap.html#rgo-squish-scriptbased-location',id:'rgo-squish-scriptbased-location',text:'Structure of Script-Based Object Maps',refs:[],children:[]}
,{href:'rg-objectmap.html#rgo-squish-scriptbased-api',id:'rgo-squish-scriptbased-api',text:'Script-Based Object Map API',refs:[],children:[{href:'rg-objectmap.html#rgo-squish-scriptbased-api-objectname',id:'rgo-squish-scriptbased-api-objectname',text:'Expressing Object Names in Script Code',refs:[],children:[]}
,{href:'rg-objectmap.html#rgo-squish-scriptbased-api-wildcard',id:'rgo-squish-scriptbased-api-wildcard',text:'Using Wildcard Matching',refs:[],children:[]}
]}
,{href:'rg-objectmap.html#rgo-squish-scriptbased-templates',id:'rgo-squish-scriptbased-templates',text:'Script-Based Object Map Templates',refs:[],children:[]}
]}
,{href:'rg-objectmap.html#rgo-squish-textbased',id:'rgo-squish-textbased',text:'Text-Based Object Map',refs:[],children:[{href:'rg-objectmap.html#rgo-squish-textbased-storage',id:'rgo-squish-textbased-storage',text:'Storage Location of Text-Based Object Maps',refs:[],children:[]}
,{href:'rg-objectmap.html#rgo-squish-textbased-structure',id:'rgo-squish-textbased-structure',text:'Structure of Text-Based Object Maps',refs:[],children:[]}
]}
]}
,{href:'rg-namingconfig.html',id:'rg-namingconfig',text:'Object Name Generation',refs:[],children:[{href:'rg-namingconfig.html#rg-namingconfig-why',id:'rg-namingconfig-why',text:'Object Names',refs:[],children:[]}
,{href:'rg-namingconfig.html#rg-namingconfig-how',id:'rg-namingconfig-how',text:'Defining Property Sets',refs:[],children:[{href:'rg-namingconfig.html#rg-namingconfig-how-location',id:'rg-namingconfig-how-location',text:'Descriptor File Locations',refs:[],children:[]}
,{href:'rg-namingconfig.html#rg-namingconfig-how-format',id:'rg-namingconfig-how-format',text:'Descriptor File Format',refs:[],children:[]}
]}
,{href:'rg-namingconfig.html#rg-namingconfig-how-advanced',id:'rg-namingconfig-how-advanced',text:'Advanced Property Set Definitions',refs:[],children:[{href:'rg-namingconfig.html#rg-namingconfig-how-advanced-catchall',id:'rg-namingconfig-how-advanced-catchall',text:'The Catch-All * Descriptor',refs:[],children:[]}
,{href:'rg-namingconfig.html#rg-namingconfig-how-advanced-groups',id:'rg-namingconfig-how-advanced-groups',text:'Groups of Exclusive Properties',refs:[],children:[]}
,{href:'rg-namingconfig.html#rg-namingconfig-how-advanced-objects',id:'rg-namingconfig-how-advanced-objects',text:'Properties with Object Name Values',refs:[],children:[]}
,{href:'rg-namingconfig.html#rg-namingconfig-how-advanced-excludingproperties',id:'rg-namingconfig-how-advanced-excludingproperties',text:'Excluding Individual Properties From Some Objects\' Real Names',refs:[],children:[]}
]}
,{href:'rg-namingconfig.html#rg-namegen-web',id:'rg-namegen-web',text:'Name Generation Algorithm used by Squish for Web',refs:[],children:[]}
]}
,{href:'rg-templates.html',id:'rg-templates',text:'Testcase Templates',refs:[],children:[{href:'rg-templates.html#rg-templates-new',id:'rg-templates-new',text:'Creating a New Template',refs:[],children:[]}
,{href:'rg-templates.html#rgo-templates-using',id:'rgo-templates-using',text:'Using a Template',refs:[],children:[]}
,{href:'rg-templates.html#rgo-templates-customdir',id:'rgo-templates-customdir',text:'Choosing a Custom Location for Storing Templates',refs:[],children:[]}
]}
,{href:'rg-conversion.html',id:'rg-conversion',text:'Migrating to the Scripted Object Map: Common conversion problems',refs:[],children:[{href:'rg-conversion.html#rg-conversion-realnames',id:'rg-conversion-realnames',text:'Real Names that contain Symbolic Names',refs:[],children:[]}
,{href:'rg-conversion.html#rg-conversion-customfunctions',id:'rg-conversion-customfunctions',text:'Custom functions that take Symbolic Names as parameters',refs:[],children:[]}
,{href:'rg-conversion.html#rg-conversion-escaped',id:'rg-conversion-escaped',text:'Symbolic Names that use characters that need to be escaped in the script language',refs:[],children:[]}
]}
]}
,{href:'ide.ref.man.html',id:'ide.ref.man',text:'IDE Reference Manual',refs:[],children:[{href:'ide.main.window.html',id:'ide.main.window',text:'Main Window',refs:[],children:[{href:'ide.main.window.html#ide.main.window.s.actions',id:'ide.main.window.s.actions',text:'Main Window Actions',refs:[],children:[{href:'ide.main.window.html#ide.add.symbolic.name.action',id:'ide.add.symbolic.name.action',text:'Add Symbolic Name action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.activate.editor.action',id:'ide.activate.editor.action',text:'Activate Editor action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.check.object.existence.action',id:'ide.check.object.existence.action',text:'Check Object Existence action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.close.action',id:'ide.close.action',text:'Close action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.close.all.action',id:'ide.close.all.action',text:'Close All action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.close.perspective.action',id:'ide.close.perspective.action',text:'Close Perspective action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.close.all.perspectives.action',id:'ide.close.all.perspectives.action',text:'Close All Perspectives action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.content.assist.action',id:'ide.content.assist.action',text:'Content Assist action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.convert.line.delimiters.to.action',id:'ide.convert.line.delimiters.to.action',text:'Convert Line Delimiters To action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.copy.action',id:'ide.copy.action',text:'Copy action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.customize.perspective.action',id:'ide.customize.perspective.action',text:'Customize Perspective action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.cut.action',id:'ide.cut.action',text:'Cut action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.delete.action',id:'ide.delete.action',text:'Delete action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.delete.test.suite.action',id:'ide.delete.test.suite.action',text:'Delete Test Suite action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.file.action',id:'ide.file.action',text:'File action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.find.and.replace.action',id:'ide.find.and.replace.action',text:'Find and Replace action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.find.next.action',id:'ide.find.next.action',text:'Find Next action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.find.previous.action',id:'ide.find.previous.action',text:'Find Previous action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.import.test.resource.action',id:'ide.import.test.resource.action',text:'Import Test Resource action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.key.assist.action',id:'ide.key.assist.action',text:'Key Assist action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.last.edit.location.action',id:'ide.last.edit.location.action',text:'Last Edit Location action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.launch.aut.action',id:'ide.launch.aut.action',text:'Launch AUT action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.manage.auts.action',id:'ide.manage.auts.action',text:'Manage AUTs action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.maximize.active.view.or.editor.action',id:'ide.maximize.active.view.or.editor.action',text:'Maximize Active View or Editor action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.minimize.active.view.or.editor.action',id:'ide.minimize.active.view.or.editor.action',text:'Minimize Active View or Editor action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.move.action',id:'ide.move.action',text:'Move action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.new.action',id:'ide.new.action',text:'New action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.new.test.case.action',id:'ide.new.test.case.action',text:'New Test Case action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.new.test.suite.action',id:'ide.new.test.suite.action',text:'New Test Suite action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.next.annotation.action',id:'ide.next.annotation.action',text:'Next Annotation action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.next.editor.action',id:'ide.next.editor.action',text:'Next Editor action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.next.perspective.action',id:'ide.next.perspective.action',text:'Next Perspective action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.next.view.action',id:'ide.next.view.action',text:'Next View action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.open.file.action',id:'ide.open.file.action',text:'Open File action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.open.test.suite.action',id:'ide.open.test.suite.action',text:'Open Test Suite action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.open.perspective.action',id:'ide.open.perspective.action',text:'Open Perspective action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.paste.action',id:'ide.paste.action',text:'Paste action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.preferences.action',id:'ide.preferences.action',text:'Preferences action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.previous.annotation.action',id:'ide.previous.annotation.action',text:'Prev­ious An­no­ta­tion action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.previous.editor.action',id:'ide.previous.editor.action',text:'Previous Editor action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.previous.perspective.action',id:'ide.previous.perspective.action',text:'Previous Perspective action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.previous.view.action',id:'ide.previous.view.action',text:'Previous View action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.project.action',id:'ide.project.action',text:'Project action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.properties.action',id:'ide.properties.action',text:'Properties action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.quick.access.action',id:'ide.quick.access.action',text:'Quick Access action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.quit.aut.action',id:'ide.quit.aut.action',text:'Quit AUT action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.record.snippet.action',id:'ide.record.snippet.action',text:'Re­cord Snip­pet action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.record.test.case.action',id:'ide.record.test.case.action',text:'Record Test Case action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.redo.action',id:'ide.redo.action',text:'Redo action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.refresh.action',id:'ide.refresh.action',text:'Refresh action',refs:[],children:[]}
,{href:'ide.main.window.html#remove.all.breakpoints.action',id:'remove.all.breakpoints.action',text:'Remove All Breakpoints action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.rename.action',id:'ide.rename.action',text:'Rename action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.reset.perspective.action',id:'ide.reset.perspective.action',text:'Reset Perspective action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.resume.action',id:'ide.resume.action',text:'Resume action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.run.test.case.action',id:'ide.run.test.case.action',text:'Run Test Case action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.save.action',id:'ide.save.action',text:'Save action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.save.all.action',id:'ide.save.all.action',text:'Save All action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.save.perspective.as.action',id:'ide.save.perspective.as.action',text:'Save Perspective As action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.search.action',id:'ide.search.action',text:'Search action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.select.all.action',id:'ide.select.all.action',text:'Select All action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.show.system.menu.action',id:'ide.show.system.menu.action',text:'Show System Menu action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.show.view.action',id:'ide.show.view.action',text:'Show View action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.show.view.menu.action',id:'ide.show.view.menu.action',text:'Show View Menu action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.show.whitespace.characters.action',id:'ide.show.whitespace.characters.action',text:'Show Whitespace Characters action',refs:[],children:[]}
,{href:'ide.main.window.html#skip.all.breakpoints.action',id:'skip.all.breakpoints.action',text:'Skip All Breakpoints action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.squish.server.settings',id:'ide.squish.server.settings',text:'Squish Server Settings',refs:[],children:[]}
,{href:'ide.main.window.html#ide.squish.spy.perspective.action',id:'ide.squish.spy.perspective.action',text:'Squish Spy Perspective action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.squish.test.debugging.perspective.action',id:'ide.squish.test.debugging.perspective.action',text:'Squish Test De­bug­ging Per­spec­tive action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.squish.test.management.perspective.action',id:'ide.squish.test.management.perspective.action',text:'Squish Test Management Perspective action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.step.into.action',id:'ide.step.into.action',text:'Step Into action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.step.over.action',id:'ide.step.over.action',text:'Step Over action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.step.return.action',id:'ide.step.return.action',text:'Step Return action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.suspend.action',id:'ide.suspend.action',text:'Suspend action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.switch.to.editor.action',id:'ide.switch.to.editor.action',text:'Switch to Editor action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.terminate.action',id:'ide.terminate.action',text:'Terminate action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.toggle.block.selection.mode.action',id:'ide.toggle.block.selection.mode.action',text:'Toggle Block Selection Mode action',refs:[],children:[]}
,{href:'ide.main.window.html#toggle.breakpoint.action',id:'toggle.breakpoint.action',text:'Toggle Breakpoint action',refs:[],children:[]}
,{href:'ide.main.window.html#toggle.line.breakpoint.action',id:'toggle.line.breakpoint.action',text:'Toggle Line Breakpoint action',refs:[],children:[]}
,{href:'ide.main.window.html#toggle.method.breakpoint.action',id:'toggle.method.breakpoint.action',text:'Toggle Method Breakpoint action',refs:[],children:[]}
,{href:'ide.main.window.html#toggle.watchpoint.action',id:'toggle.watchpoint.action',text:'Toggle Watchpoint action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.undo.action',id:'ide.undo.action',text:'Undo action',refs:[],children:[]}
,{href:'ide.main.window.html#use.step.filters.action',id:'use.step.filters.action',text:'Use Step Filters action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.working.set.action',id:'ide.working.set.action',text:'Working Set action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.workspace.action',id:'ide.workspace.action',text:'Workspace action',refs:[],children:[]}
]}
,{href:'ide.main.window.html#ide.perspectives',id:'ide.perspectives',text:'Perspectives',refs:[],children:[{href:'ide.main.window.html#ide.squish.spy.perspective',id:'ide.squish.spy.perspective',text:'Squish Spy Perspective',refs:[],children:[]}
,{href:'ide.main.window.html#ide.squish.test.management.perspective',id:'ide.squish.test.management.perspective',text:'Squish Test Management Perspective',refs:[],children:[]}
,{href:'ide.main.window.html#ide.squish.test.debugging.perspective',id:'ide.squish.test.debugging.perspective',text:'Squish Test Debugging Perspective',refs:[],children:[]}
]}
,{href:'ide.main.window.html#ide-the-controlbar-window',id:'ide-the-controlbar-window',text:'Control Bar Window',refs:[],children:[{href:'ide.main.window.html#ide.stop.recording.action',id:'ide.stop.recording.action',text:'Stop Recording action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.insert.object.properties.verification.point.action',id:'ide.insert.object.properties.verification.point.action',text:'Verify &gt; Properties action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.insert.screenshot.verification.point.action',id:'ide.insert.screenshot.verification.point.action',text:'Verify &gt; Screenshot action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.insert.table.verification.point.action',id:'ide.insert.table.verification.point.action',text:'Verify &gt; Table action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.insert.visual.verification.point.action',id:'ide.insert.visual.verification.point.action',text:'Verify &gt; Visual action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.insert.mouseclick.action',id:'ide.insert.mouseclick.action',text:'Insert &gt; mouseClick(&lt;Image&gt;) action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.insert.doubleclick.action',id:'ide.insert.doubleclick.action',text:'Insert &gt; doubleClick(&lt;Image&gt;) action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.insert.tapobject.action',id:'ide.insert.tapobject.action',text:'Insert &gt; tapObject(&lt;Image&gt;) action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.insert.comment.action',id:'ide.insert.comment.action',text:'Insert &gt; Comment action',refs:[],children:[]}
,{href:'ide.main.window.html#ide.record.or.replay.cancel.action',id:'ide.record.or.replay.cancel.action',text:'Cancel action',refs:[],children:[]}
]}
]}
,{href:'ide.views.html',id:'ide.views',text:'Views',refs:[],children:[{href:'ide.views.html#ide.the-application-objects.view',id:'ide.the-application-objects.view',text:'Application Objects view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-breakpoints.view',id:'ide.the-breakpoints.view',text:'Breakpoints view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-console.view',id:'ide.the-console.view',text:'Console view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-call-hierarchy.view',id:'ide.the-call-hierarchy.view',text:'Call Hierarchy view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-debug.view',id:'ide.the-debug.view',text:'Debug view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-editor.view',id:'ide.the-editor.view',text:'Editor view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-global-scripts.view',id:'ide.the-global-scripts.view',text:'Global Scripts view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-image.view',id:'ide.the-image.view',text:'Image Viewer',refs:[],children:[]}
,{href:'ide.views.html#ide.the-methods.view',id:'ide.the-methods.view',text:'Methods view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-object-map.view',id:'ide.the-object-map.view',text:'Object Map view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-outline.view',id:'ide.the-outline.view',text:'Outline view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-properties.view',id:'ide.the-properties.view',text:'Properties view',refs:[],children:[]}
,{href:'ide.views.html#ide.runner.server.log.view',id:'ide.runner.server.log.view',text:'Runner/Server Log view',refs:[],children:[]}
,{href:'ide.views.html#ide.search.image.view',id:'ide.search.image.view',text:'Search Image Editor view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-search.view',id:'ide.the-search.view',text:'Search view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-settings.view',id:'ide.the-settings.view',text:'Settings view',refs:[],children:[{href:'ide.views.html#ide.the-settings.view.general',id:'ide.the-settings.view.general',text:'Test Suite Information',refs:[],children:[]}
,{href:'ide.views.html#ide.the-settings.view.aut',id:'ide.the-settings.view.aut',text:'AUT Information',refs:[],children:[]}
,{href:'ide.views.html#ide.the-settings.view.testsettings',id:'ide.the-settings.view.testsettings',text:'Test Settings',refs:[],children:[]}
,{href:'ide.views.html#ide.the-settings.view.imagesearch',id:'ide.the-settings.view.imagesearch',text:'Image Search',refs:[],children:[]}
,{href:'ide.views.html#ide.the-settings.view.descriptions',id:'ide.the-settings.view.descriptions',text:'Descriptions',refs:[],children:[]}
]}
,{href:'ide.views.html#ide.the-test-results.view',id:'ide.the-test-results.view',text:'Test Results view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-test-suites.view',id:'ide.the-test-suites.view',text:'Test Suites view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-test-summary.view',id:'ide.the-test-summary.view',text:'Test Summary view',refs:[],children:[]}
,{href:'ide.views.html#ide.the-variables.view',id:'ide.the-variables.view',text:'Variables view',refs:[],children:[]}
,{href:'ide.views.html#ide.verification.point.creator.view',id:'ide.verification.point.creator.view',text:'Verification Point Creator view',refs:[],children:[]}
]}
,{href:'ide.dialogs.html',id:'ide.dialogs',text:'Dialogs',refs:[],children:[{href:'ide.dialogs.html#ide.customize.perspective.dialog',id:'ide.customize.perspective.dialog',text:'Customize Perspective dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.find.and.replace.dialog',id:'ide.find.and.replace.dialog',text:'Find/Replace dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.image.not.found.dialog',id:'ide.image.not.found.dialog',text:'Image Not Found dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.image.search.preview.dialog',id:'ide.image.search.preview.dialog',text:'Image Search Preview dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.import.squish.resource.dialog',id:'ide.import.squish.resource.dialog',text:'Import Squish Resource dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.search.image.selection.dialog',id:'ide.search.image.selection.dialog',text:'The Search Image Selection dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#manage.auts.dialog',id:'manage.auts.dialog',text:'Manage AUTs dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.new.dialog',id:'ide.new.dialog',text:'New dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.new.squish.test.case.wizard',id:'ide.new.squish.test.case.wizard',text:'New Squish Test Case wizard',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.new.squish.test.data.dialog',id:'ide.new.squish.test.data.dialog',text:'New Squish Test Data dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.new.squish.test.script.dialog',id:'ide.new.squish.test.script.dialog',text:'New Squish Test Script dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.new.squish.test.suite.wizard',id:'ide.new.squish.test.suite.wizard',text:'New Squish Test Suite wizard',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.object.not.found.dialog',id:'ide.object.not.found.dialog',text:'Object Not Found dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.open.perspective.dialog',id:'ide.open.perspective.dialog',text:'Open Perspective dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.preferences.dialog',id:'ide.preferences.dialog',text:'Pref­er­ences dialog',refs:[],children:[{href:'ide.dialogs.html#dynamic.languages.pane',id:'dynamic.languages.pane',text:'Dynamic Languages pane',refs:[],children:[]}
,{href:'ide.dialogs.html#general.pane',id:'general.pane',text:'General pane',refs:[],children:[{href:'ide.dialogs.html#the.general.pane.s.child.panes',id:'the.general.pane.s.child.panes',text:'General pane\'s child panes',refs:[],children:[]}
]}
,{href:'ide.dialogs.html#javascript.pane',id:'javascript.pane',text:'JavaScript pane',refs:[],children:[]}
,{href:'ide.dialogs.html#perl.epic.pane',id:'perl.epic.pane',text:'Perl EPIC pane',refs:[],children:[]}
,{href:'ide.dialogs.html#python.pane',id:'python.pane',text:'Python pane',refs:[],children:[]}
,{href:'ide.dialogs.html#run.debug.pane',id:'run.debug.pane',text:'Run/Debug pane',refs:[],children:[]}
,{href:'ide.dialogs.html#squish.pane',id:'squish.pane',text:'Squish pane',refs:[],children:[{href:'ide.dialogs.html#the.squish.pane.s.child.panes',id:'the.squish.pane.s.child.panes',text:'Squish Preferences Child Panes',refs:[],children:[]}
]}
,{href:'ide.dialogs.html#tcl.pane',id:'tcl.pane',text:'Tcl pane',refs:[],children:[]}
,{href:'ide.dialogs.html#team.pane',id:'team.pane',text:'Team pane',refs:[],children:[]}
]}
,{href:'ide.dialogs.html#ide.screenshot.verification.dialog',id:'ide.screenshot.verification.dialog',text:'Screenshot Verification Point dialog',refs:[],children:[{href:'ide.dialogs.html#ide.setting.comparison.modes',id:'ide.setting.comparison.modes',text:'Screenshot Comparison Modes',refs:[],children:[{href:'ide.dialogs.html#ide.setting.comparison.mode.strict',id:'ide.setting.comparison.mode.strict',text:'Strict Comparison Mode',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.setting.comparison.mode.pixel',id:'ide.setting.comparison.mode.pixel',text:'Fuzzy Pixel Comparison Mode',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.setting.comparison.mode.histogram',id:'ide.setting.comparison.mode.histogram',text:'Histogram Comparison Mode',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.setting.comparison.mode.correlation',id:'ide.setting.comparison.mode.correlation',text:'Correlation Comparison Mode',refs:[],children:[]}
]}
,{href:'ide.dialogs.html#ide.setting.masks',id:'ide.setting.masks',text:'Setting Masks',refs:[],children:[]}
]}
,{href:'ide.dialogs.html#ide.search.dialog',id:'ide.search.dialog',text:'Search dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.show.view.dialog',id:'ide.show.view.dialog',text:'Show View dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.squish.server.settings.dialog',id:'ide.squish.server.settings.dialog',text:'Squish Server Settings dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.switch.to.editor.dialog',id:'ide.switch.to.editor.dialog',text:'Switch to Editor dialog',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.vvp.dialog',id:'ide.vvp.dialog',text:'Visual Verification Point editor',refs:[],children:[{href:'ide.dialogs.html#ide.vvp.edit',id:'ide.vvp.edit',text:'Plain editing mode',refs:[],children:[{href:'ide.dialogs.html',id:'',text:'Identifying Properties',refs:[],children:[]}
,{href:'ide.dialogs.html',id:'',text:'Content Properties',refs:[],children:[]}
,{href:'ide.dialogs.html',id:'',text:'Geometry Properties',refs:[],children:[]}
,{href:'ide.dialogs.html',id:'',text:'Screenshot',refs:[],children:[]}
]}
,{href:'ide.dialogs.html#ide.vvp.diff',id:'ide.vvp.diff',text:'View visual differences',refs:[],children:[]}
,{href:'ide.dialogs.html#ide.vvp.tune',id:'ide.vvp.tune',text:'Tuning of Visual Verifications',refs:[],children:[{href:'ide.dialogs.html',id:'',text:'Handling of Screenshot check failures',refs:[],children:[]}
,{href:'ide.dialogs.html',id:'',text:'Handling of Content check failures',refs:[],children:[]}
,{href:'ide.dialogs.html',id:'',text:'Handling of Geometry check failures',refs:[],children:[]}
,{href:'ide.dialogs.html',id:'',text:'Creation vs. execution state',refs:[],children:[]}
,{href:'ide.dialogs.html',id:'',text:'Failures due to transient state',refs:[],children:[]}
]}
]}
]}
,{href:'ide.keyboard.shortcuts.html',id:'ide.keyboard.shortcuts',text:'Keyboard Shortcuts',refs:[],children:[]}
]}
,{href:'addons.html',id:'addons',text:'Add-Ons Reference Manual',refs:[],children:[{href:'ao-intor.html',id:'ao-intor',text:'Introduction',refs:[],children:[]}
,{href:'ao-third-party.html',id:'ao-third-party',text:'Third-Party Add-Ons',refs:[],children:[]}
,{href:'ao-ant.html',id:'ao-ant',text:'Ant integration',refs:[],children:[{href:'ao-ant.html#ao-ant-obtaining',id:'ao-ant-obtaining',text:'Obtaining the Ant Plugin',refs:[],children:[]}
,{href:'ao-ant.html#ao-ant-installation',id:'ao-ant-installation',text:'Installing the Ant Plugin',refs:[],children:[]}
,{href:'ao-ant.html#ao-ant-example',id:'ao-ant-example',text:'Using the Ant Plugin',refs:[],children:[]}
,{href:'ao-ant.html#ao-ant-xml',id:'ao-ant-xml',text:'Ant Plugin XML reference',refs:[],children:[{href:'ao-ant.html#book-ant-squish-config-tag',id:'book-ant-squish-config-tag',text:'squish:config',refs:[],children:[]}
,{href:'ao-ant.html#book-ant-squish-runtest-tag',id:'book-ant-squish-runtest-tag',text:'squish:runtest',refs:[],children:[]}
,{href:'ao-ant.html#book-ant-squish-testcase-tag',id:'book-ant-squish-testcase-tag',text:'squish:testcase',refs:[],children:[]}
,{href:'ao-ant.html#book-ant-squish-report-tag',id:'book-ant-squish-report-tag',text:'squish:report',refs:[],children:[]}
]}
]}
,{href:'ao-bamboo.html',id:'ao-bamboo',text:'Atlassian Bamboo integration',refs:[],children:[{href:'ao-bamboo.html#ao-bamboo-intro',id:'ao-bamboo-intro',text:'Introduction',refs:[],children:[]}
,{href:'ao-bamboo.html#ao-bamboo-inst',id:'ao-bamboo-inst',text:'Installation',refs:[],children:[]}
,{href:'ao-bamboo.html#ao-bamboo-capability',id:'ao-bamboo-capability',text:'Squish capability',refs:[],children:[]}
,{href:'ao-bamboo.html#ao-bamboo-job',id:'ao-bamboo-job',text:'Job definition',refs:[],children:[]}
,{href:'ao-bamboo.html#ao-bamboo-task',id:'ao-bamboo-task',text:'Task definition',refs:[],children:[]}
,{href:'ao-bamboo.html#ao-bamboo-results',id:'ao-bamboo-results',text:'Test results',refs:[],children:[]}
]}
,{href:'ao-cc.html',id:'ao-cc',text:'CruiseControl integration',refs:[],children:[{href:'ao-cc.html#ao-cc-obtaining',id:'ao-cc-obtaining',text:'Obtaining the CruiseControl Plugin',refs:[],children:[]}
,{href:'ao-cc.html#ao-cc-installation',id:'ao-cc-installation',text:'Installing the CruiseControl Plugin',refs:[],children:[]}
,{href:'ao-cc.html#ao-cc-example',id:'ao-cc-example',text:'Using the CruiseControl Plugin',refs:[],children:[]}
,{href:'ao-cc.html#ao-cc-xml',id:'ao-cc-xml',text:'CruiseControl XML reference',refs:[],children:[{href:'ao-cc.html#book-cc-squish-squishtest-tag',id:'book-cc-squish-squishtest-tag',text:'squishtest',refs:[],children:[]}
,{href:'ao-cc.html#book-cc-squish-testcase-tag',id:'book-cc-squish-testcase-tag',text:'testcase',refs:[],children:[]}
,{href:'ao-cc.html#book-cc-squish-report-tag',id:'book-cc-squish-report-tag',text:'report',refs:[],children:[]}
]}
]}
,{href:'eclipse.ide.integration.html',id:'eclipse.ide.integration',text:'Eclipse IDE Integration',refs:[],children:[{href:'eclipse.ide.integration.html#download.and.install.the.squish.plugins',id:'download.and.install.the.squish.plugins',text:'Download and Install the Squish Integration',refs:[],children:[]}
]}
,{href:'ao-tptp.html',id:'ao-tptp',text:'Eclipse Test &amp; Performance Tools Platform (TPTP) integration',refs:[],children:[]}
,{href:'ao-qc.html',id:'ao-qc',text:'HP Quality Center Integration',refs:[],children:[{href:'ao-qc.html#book-qc-integration-features',id:'book-qc-integration-features',text:'Integration Features',refs:[],children:[]}
,{href:'ao-qc.html#book-qc-integration-installation',id:'book-qc-integration-installation',text:'Installing the HP Quality Center Plugin',refs:[],children:[{href:'ao-qc.html#book-qc-integration-installation-common',id:'book-qc-integration-installation-common',text:'Common',refs:[],children:[]}
,{href:'ao-qc.html#book-qc-integration-installation-qc10',id:'book-qc-integration-installation-qc10',text:'Installation for HP Quality Center 10',refs:[],children:[]}
,{href:'ao-qc.html#book-qc-integration-installation-qc11-server',id:'book-qc-integration-installation-qc11-server',text:'Installation for HP Quality Center 11 - Server',refs:[],children:[]}
,{href:'ao-qc.html#book-qc-integration-installation-qc11-client',id:'book-qc-integration-installation-qc11-client',text:'Installation for HP Quality Center 11 - Client',refs:[],children:[]}
]}
,{href:'ao-qc.html#book-qc-plugin-config',id:'book-qc-plugin-config',text:'Configuring the HP Quality Center Plugin',refs:[],children:[]}
,{href:'ao-qc.html#book-qc-plugin-usageqc',id:'book-qc-plugin-usageqc',text:'Using the Integration From HP Quality Center',refs:[],children:[{href:'ao-qc.html#book-qc-plugin-usageqc-newtests',id:'book-qc-plugin-usageqc-newtests',text:'Creating New Test Cases',refs:[],children:[]}
,{href:'ao-qc.html#book-qc-plugin-usageqc-runtest',id:'book-qc-plugin-usageqc-runtest',text:'Running Squish Tests',refs:[],children:[]}
]}
,{href:'ao-qc.html#book-qc-importer',id:'book-qc-importer',text:'Importing test cases from the command line',refs:[],children:[{href:'ao-qc.html#book-qc-importer-intro',id:'book-qc-importer-intro',text:'The qcimporter.exe utility',refs:[],children:[]}
,{href:'ao-qc.html#book-qc-importer-install',id:'book-qc-importer-install',text:'Installing the qcimporter.exe utility',refs:[],children:[]}
,{href:'ao-qc.html#book-qc-importer-synopsis',id:'book-qc-importer-synopsis',text:'Synopsis',refs:[],children:[]}
,{href:'ao-qc.html#book-qc-importer-examples',id:'book-qc-importer-examples',text:'Examples',refs:[],children:[]}
]}
]}
,{href:'ao-hudson.html',id:'ao-hudson',text:'Jenkins integration',refs:[],children:[{href:'ao-hudson.html#ao-jenkins-obtaining',id:'ao-jenkins-obtaining',text:'Obtaining the Plugin',refs:[],children:[]}
,{href:'ao-hudson.html#ao-jenkins-installation',id:'ao-jenkins-installation',text:'Installing the Plugin',refs:[],children:[]}
,{href:'ao-hudson.html#ao-jenkins-example',id:'ao-jenkins-example',text:'Using the Plugin',refs:[],children:[{href:'ao-hudson.html#ao-jenkins-squish-global-configuration',id:'ao-jenkins-squish-global-configuration',text:'Squish Global Configuration',refs:[],children:[]}
,{href:'ao-hudson.html#ao-jenkins-example-freestyle-projects',id:'ao-jenkins-example-freestyle-projects',text:'Freestyle Projects',refs:[],children:[]}
,{href:'ao-hudson.html#ao-jenkins-example-pipeline-jobs',id:'ao-jenkins-example-pipeline-jobs',text:'Pipeline Jobs',refs:[],children:[]}
]}
,{href:'ao-hudson.html#ao-jenkins-execution-results',id:'ao-jenkins-execution-results',text:'Execution Results',refs:[],children:[]}
]}
,{href:'ao-maven.html',id:'ao-maven',text:'Maven integration',refs:[],children:[{href:'ao-maven.html#ao-maven-obtaining',id:'ao-maven-obtaining',text:'Obtaining the Maven Plugin',refs:[],children:[]}
,{href:'ao-maven.html#ao-maven-installation',id:'ao-maven-installation',text:'Installing the Maven Plugin',refs:[],children:[]}
,{href:'ao-maven.html#ao-maven-example',id:'ao-maven-example',text:'Using the Maven Plugin',refs:[],children:[]}
,{href:'ao-maven.html#ao-maven-xml',id:'ao-maven-xml',text:'Maven Plugin XML reference',refs:[],children:[{href:'ao-maven.html#book-maven-squish-runtest-tag',id:'book-maven-squish-runtest-tag',text:'run-test',refs:[],children:[]}
,{href:'ao-maven.html#book-maven-squish-testcases-element',id:'book-maven-squish-testcases-element',text:'testCases',refs:[],children:[]}
,{href:'ao-maven.html#book-maven-squish-tagFilters-element',id:'book-maven-squish-tagFilters-element',text:'tagFilters',refs:[],children:[]}
,{href:'ao-maven.html#book-maven-squish-scriptArgs-element',id:'book-maven-squish-scriptArgs-element',text:'scriptArgs',refs:[],children:[]}
,{href:'ao-maven.html#book-maven-squish-reports-element',id:'book-maven-squish-reports-element',text:'reports',refs:[],children:[]}
]}
]}
,{href:'ao-teamcity.html',id:'ao-teamcity',text:'JetBrains TeamCity integration',refs:[],children:[{href:'ao-teamcity.html#ao-teamcity-intro',id:'ao-teamcity-intro',text:'Introduction',refs:[],children:[]}
,{href:'ao-teamcity.html#ao-teamcity-inst',id:'ao-teamcity-inst',text:'Installation',refs:[],children:[]}
,{href:'ao-teamcity.html#ao-teamcity-job',id:'ao-teamcity-job',text:'Build Step definition',refs:[],children:[]}
,{href:'ao-teamcity.html#ao-teamcity-execution',id:'ao-teamcity-execution',text:'Build execution',refs:[],children:[]}
]}
,{href:'ao-testtracktcm.html',id:'ao-testtracktcm',text:'TestTrack TCM integration',refs:[],children:[{href:'ao-testtracktcm.html#ao-testtracktcm-obtaining',id:'ao-testtracktcm-obtaining',text:'Obtaining the TestTrack TCM Plugin',refs:[],children:[]}
,{href:'ao-testtracktcm.html#ao-testtracktcm-installation',id:'ao-testtracktcm-installation',text:'Installing the TestTrack TCM Plugin',refs:[],children:[]}
,{href:'ao-testtracktcm.html#ao-testtracktcm-configuration',id:'ao-testtracktcm-configuration',text:'Configuring the TestTrack TCM Plugin',refs:[],children:[]}
,{href:'ao-testtracktcm.html#ao-testtracktcm-formatdescriptionfile',id:'ao-testtracktcm-formatdescriptionfile',text:'The .squishtcm description file',refs:[],children:[]}
,{href:'ao-testtracktcm.html#ao-testtracktcm-formatconfigfile',id:'ao-testtracktcm-formatconfigfile',text:'The configuration file',refs:[],children:[]}
,{href:'ao-testtracktcm.html#ao-testtracktcm-example',id:'ao-testtracktcm-example',text:'Example: How to run a Squish test case',refs:[],children:[]}
]}
,{href:'ao-rqmintegration.html',id:'ao-rqmintegration',text:'IBM Rational Quality Manager integration',refs:[],children:[{href:'ao-rqmintegration.html#ao-rqmintegration-installation',id:'ao-rqmintegration-installation',text:'Installing the Squish Adapter',refs:[],children:[]}
,{href:'ao-rqmintegration.html#ao-rqmintegration-concept',id:'ao-rqmintegration-concept',text:'Squish Adapter Concept',refs:[],children:[]}
,{href:'ao-rqmintegration.html#ao-rqmintegration-configuration-file',id:'ao-rqmintegration-configuration-file',text:'Preparing test configuration files',refs:[],children:[]}
,{href:'ao-rqmintegration.html#ao-rqmintegration-start',id:'ao-rqmintegration-start',text:'Starting the Squish Adapter',refs:[],children:[{href:'ao-rqmintegration.html#ao-rqmintegration-start-preconditions',id:'ao-rqmintegration-start-preconditions',text:'Preconditions',refs:[],children:[]}
,{href:'ao-rqmintegration.html#ao-rqmintegration-start-synopsis',id:'ao-rqmintegration-start-synopsis',text:'Synopsis',refs:[],children:[]}
,{href:'ao-rqmintegration.html#ao-rqmintegration-start-example',id:'ao-rqmintegration-start-example',text:'Example',refs:[],children:[]}
,{href:'ao-rqmintegration.html#ao-rqmintegration-restarting',id:'ao-rqmintegration-restarting',text:'Restarting the Squish Adapter',refs:[],children:[]}
]}
,{href:'ao-rqmintegration.html#ao-rqmintegration-using-the-adapter',id:'ao-rqmintegration-using-the-adapter',text:'Using the Squish Adapter from Rational Quality Manager',refs:[],children:[]}
,{href:'ao-rqmintegration.html#ao-rqmintegration-execution',id:'ao-rqmintegration-execution',text:'Executing a Test Script',refs:[],children:[]}
]}
,{href:'ao-vs-integration.html',id:'ao-vs-integration',text:'Microsoft Visual Studio Integration',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2010-integration',id:'ao-vs2010-integration',text:'Microsoft Visual Studio 2010 Integration',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2010-integration-obtaining',id:'ao-vs2010-integration-obtaining',text:'Obtaining the Visual Studio Integration',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-requirements',id:'ao-vs2010-integration-requirements',text:'Requirements',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2010-integration-requirements-vs',id:'ao-vs2010-integration-requirements-vs',text:'Requirements for Test Execution through Visual Studio',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-requirements-mtm',id:'ao-vs2010-integration-requirements-mtm',text:'Requirements for Test Execution through Microsoft Test Manager',refs:[],children:[]}
]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-installation',id:'ao-vs2010-integration-installation',text:'Installing the Visual Studio Integration',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-creatingtest',id:'ao-vs2010-integration-creatingtest',text:'Creating a Visual Studio Squish Test Project',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2010-integration-create-vsproject',id:'ao-vs2010-integration-create-vsproject',text:'Creating the Visual Studio Test Project',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-addsquishtest',id:'ao-vs2010-integration-addsquishtest',text:'Adding the Visual Studio Squish Test to the Test Project',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-selecttestsuite',id:'ao-vs2010-integration-selecttestsuite',text:'Importing the Squish Test Suite',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-configuresquish',id:'ao-vs2010-integration-configuresquish',text:'Configuring the Squish Installation for Test Runs',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2010-integration-defaultsquishinstallation',id:'ao-vs2010-integration-defaultsquishinstallation',text:'Default Squish Installation',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-multiplequishinstallations',id:'ao-vs2010-integration-multiplequishinstallations',text:'Multiple Squish Installations',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-fixedpathsquishinstallation',id:'ao-vs2010-integration-fixedpathsquishinstallation',text:'Setting the Squish Installation Using a Fixed Path',refs:[],children:[]}
]}
]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-vs',id:'ao-vs2010-integration-executingtest-vs',text:'Executing a Test through Visual Studio',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-vs-startrun',id:'ao-vs2010-integration-executingtest-vs-startrun',text:'Starting Test Execution through Visual Studio',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-vs-results',id:'ao-vs2010-integration-executingtest-vs-results',text:'Checking the Test Results',refs:[],children:[]}
]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-mtm',id:'ao-vs2010-integration-executingtest-mtm',text:'Executing a Test through Microsoft Test Manager',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-mtm-preparations',id:'ao-vs2010-integration-executingtest-mtm-preparations',text:'Preparing Test Execution through Microsoft Test Manager',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-mtm-preparetestproject',id:'ao-vs2010-integration-executingtest-mtm-preparetestproject',text:'Preparing the Visual Studio Squish Test Project',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-mtm-preparetfsbuild',id:'ao-vs2010-integration-executingtest-mtm-preparetfsbuild',text:'Preparing the Team Foundation Server Build',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-mtm-prepareworkitem',id:'ao-vs2010-integration-executingtest-mtm-prepareworkitem',text:'Publishing the Squish Test as a Work Item',refs:[],children:[]}
]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-mtm-run',id:'ao-vs2010-integration-executingtest-mtm-run',text:'Starting Test Execution through Microsoft Test Manager',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2010-integration-executingtest-mtm-results',id:'ao-vs2010-integration-executingtest-mtm-results',text:'Checking the Test Results',refs:[],children:[]}
]}
]}
,{href:'ao-vs-integration.html#ao-vs2012-integration',id:'ao-vs2012-integration',text:'Microsoft Visual Studio 2012/2013 Integration',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2012-integration-obtaining',id:'ao-vs2012-integration-obtaining',text:'Obtaining the Visual Studio Integration',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-requirements',id:'ao-vs2012-integration-requirements',text:'Requirements',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2012-integration-requirements-vs',id:'ao-vs2012-integration-requirements-vs',text:'Requirements for Test Execution through Visual Studio',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-requirements-mtm',id:'ao-vs2012-integration-requirements-mtm',text:'Requirements for Test Execution through Microsoft Test Manager',refs:[],children:[]}
]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-installation',id:'ao-vs2012-integration-installation',text:'Installing the Visual Studio Integration',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-creatingtest',id:'ao-vs2012-integration-creatingtest',text:'Creating a Visual Studio Squish Test Project',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2012-integration-create-vsproject',id:'ao-vs2012-integration-create-vsproject',text:'Creating the Visual Studio Test Project',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-addsquishtest',id:'ao-vs2012-integration-addsquishtest',text:'Adding the Visual Studio Squish Test to the Test Project',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-selecttestsuite',id:'ao-vs2012-integration-selecttestsuite',text:'Importing the Squish Test Suite',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-configuresquish',id:'ao-vs2012-integration-configuresquish',text:'Configuring the Squish Installation for Test Runs',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2012-integration-defaultsquishinstallation',id:'ao-vs2012-integration-defaultsquishinstallation',text:'Default Squish Installation',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-multiplequishinstallations',id:'ao-vs2012-integration-multiplequishinstallations',text:'Multiple Squish Installations',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-fixedpathsquishinstallation',id:'ao-vs2012-integration-fixedpathsquishinstallation',text:'Setting the Squish Installation Using a Fixed Path',refs:[],children:[]}
]}
]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-vs',id:'ao-vs2012-integration-executingtest-vs',text:'Executing a Test through Visual Studio',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-vs-startrun',id:'ao-vs2012-integration-executingtest-vs-startrun',text:'Starting Test Execution through Visual Studio',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-vs-results',id:'ao-vs2012-integration-executingtest-vs-results',text:'Checking the Test Results',refs:[],children:[]}
]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-mtm',id:'ao-vs2012-integration-executingtest-mtm',text:'Executing a Test through Microsoft Test Manager',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-mtm-preparations',id:'ao-vs2012-integration-executingtest-mtm-preparations',text:'Preparing Test Execution through Microsoft Test Manager',refs:[],children:[{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-mtm-preparetestproject',id:'ao-vs2012-integration-executingtest-mtm-preparetestproject',text:'Preparing the Visual Studio Squish Test Project',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-mtm-preparetfsbuild',id:'ao-vs2012-integration-executingtest-mtm-preparetfsbuild',text:'Preparing the Team Foundation Server Build',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-mtm-prepareworkitem',id:'ao-vs2012-integration-executingtest-mtm-prepareworkitem',text:'Publishing the Squish Test as a Work Item',refs:[],children:[]}
]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-mtm-run',id:'ao-vs2012-integration-executingtest-mtm-run',text:'Starting Test Execution through Microsoft Test Manager',refs:[],children:[]}
,{href:'ao-vs-integration.html#ao-vs2012-integration-executingtest-mtm-results',id:'ao-vs2012-integration-executingtest-mtm-results',text:'Checking the Test Results',refs:[],children:[]}
]}
]}
]}
]}
,{href:'faq.html',id:'faq',text:'Frequently Asked Questions',refs:[],children:[]}
,{href:'book-ack.html',text:'Appendix 1',children:[]}
,{href:'book-index.html',text:'Appendix 2',children:[]}
,{href:'book-glossary.html',text:'Glossary',children:[]}
];
