Package Name:  squish-6.4.1-windows
Internal Name: /home/autotest/public_html/binpackages/20180815-1700/bin_package/6.4.1/win/Hoheluft-QtNone-TkNone-iOSNone-MSVC9-b970747
Git Branch:    6.4.1
Git Revision:  d5f29913879adc584e155e57b22284542b6fd198

Python Version: default
Patched:        False
Debug Build:    False
Debug Symbols:  False
Pure Qt4 Build: False
Include IDE:    True

Python:         S:/binPackage/Python/x86/2.7.10binary_ssl
Tcl:            S:/binPackage/tcl/x86/8.6.4
Perl:           S:/binPackage/Perl/x86/5.22.0
Ruby:           S:/binPackage/Ruby/x86/2.2.2gem
