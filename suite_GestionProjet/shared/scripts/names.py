# encoding: UTF-8

from objectmaphelper import *

nawo_Configuration_Dialog = {"text": "Nawo Configuration", "type": "Dialog"}
nawo_Configuration_Play_Button = {"container": nawo_Configuration_Dialog, "text": "Play!", "type": "Button"}
nawo_Window = {"text": "Nawo", "type": "Window"}
nawo_Configuration_TabFolder = {"container": nawo_Configuration_Dialog, "type": "TabFolder"}
input_TabItem = {"container": nawo_Configuration_TabFolder, "text": "Input", "type": "TabItem"}
graphics_TabItem = {"container": nawo_Configuration_TabFolder, "text": "Graphics", "type": "TabItem"}
nawo_Configuration_ComboBox = {"container": nawo_Configuration_Dialog, "type": "ComboBox"}
nawo_Configuration_Quit_Button = {"container": nawo_Configuration_Dialog, "text": "Quit", "type": "Button"}
