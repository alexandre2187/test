# -*- coding: utf-8 -*-

import names


def main():
    startApplication("Nawo");
    clickButton(waitForObject(names.nawo_Configuration_Play_Button))
    mouseClick(waitForImage("Projet2"))
    mouseClick(waitForObject(names.nawo_Window), 374, 754, MouseButton.PrimaryButton)
    mouseClick(waitForObject(names.nawo_Window), 443, 718, MouseButton.PrimaryButton)
    type(waitForObject(names.nawo_Window), "new")
    mouseClick(waitForObject(names.nawo_Window), 447, 754, MouseButton.PrimaryButton)
    snooze(2)
