# -*- coding: utf-8 -*-

import names

def main():
    startApplication("Nawo");
    clickButton(waitForObject(names.nawo_Configuration_Play_Button))
    mouseClick(waitForImage("Projet2"))
    snooze(1)
    mouseClick(waitForObject(names.nawo_Window), 368, 192, MouseButton.PrimaryButton)
    snooze(1)
    mouseClick(waitForObject(names.nawo_Window), 477, 755, MouseButton.PrimaryButton)
    mouseClick(waitForObject(names.nawo_Window), 449, 723, MouseButton.PrimaryButton)
    type(waitForObject(names.nawo_Window), "test")
    mouseClick(waitForObject(names.nawo_Window), 473, 756, MouseButton.PrimaryButton)
    snooze(2)